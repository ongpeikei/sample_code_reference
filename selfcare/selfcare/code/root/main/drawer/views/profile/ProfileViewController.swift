//
//  ProfileViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 20/06/2017.
//
//

import UIKit
import Fabric

class ProfileViewController: UIViewController {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblChangeCoverPhoto: UILabel!
    @IBOutlet weak var vAccStatus: UIView!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == nil {
            Global.containerView = segue.destination as! CustomCardCellViewController
        }
        
        // to prepare the table UI
        prepareUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // don't move this it'll affect the UX
        prepareLayout()
        setDefaultValue()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // don't move this it'll affect the UX
        updateNavBarUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kPROFILE)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kPROFILE)
    }
    
    func prepareLayout() {
        lblName.font = globalInstance.getFontAttribute(.kBOLD, .kMEDIUM)
        lblName.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        lblStatus.font = globalInstance.getFontAttribute(.kREGULAR, .kMEDIUM)
        lblStatus.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        vAccStatus = globalInstance.setRoundedLabel(RADIUS, vAccStatus)
        vAccStatus.backgroundColor = globalInstance.getAccountStatusColor()
    }
    
    fileprivate func setDefaultValue() {
        let fileName = CustomerDetailsModel.sharedInstance.getProfilePicture()
        if fileName == "" {
            imageProfile.image = UIImage(named: "logo_place_holder")
            imageProfile = globalInstance.roundedImage(imageProfile)
        } else {
            let imagePath = globalInstance.getImagePath() + fileName
            let url = URL(string: imagePath)
            let data = try? Data(contentsOf: url!)
            if data != nil {
                imageProfile.image = UIImage(data: data!)
                imageProfile = globalInstance.roundedImage(imageProfile)
            }
        }
        
        lblChangeCoverPhoto.text = "change your cover photo"
        lblName.text = CustomerDetailsModel.sharedInstance.getFullName()
        lblStatus.text = CustomerDetailsModel.sharedInstance.getAccountStatus()
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        print("PROFILE SCREEN: PROFILE PIC TAPPED!!")
    }
    
    func changeCoverPhotoTapped(tapGestureRecognizer: UITapGestureRecognizer){
        print("PROFILE SCREEN: CHANGE COVER PHOTO TAPPED!!")
    }
    
    private func updateNavBarUI() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 11, width: 22, height: 22)
        back.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 100, height: 24))
        label.text = "profile"
        label.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        label.textAlignment = NSTextAlignment.left
        label.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftView
        
        // to apply same color contrast and remove the nav 1px bottom line
        navigationController?.navigationBar.barTintColor = UIColor(red: 43/255, green: 40/255, blue: 83/255, alpha: 1.0)
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @objc func goBack() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kBACK_CLICKED)
        Global.sharedInstance.goBack(self)
    }
    
    private func prepareUI() {
        Global.pageType = Global.VIEW_TYPE.kPROFILE
        Global.cellSequence = [CustomCardCellViewController.CARD_CELL.kLIST]
    }
    
}
