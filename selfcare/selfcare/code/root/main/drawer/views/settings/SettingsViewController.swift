//
//  SettingsViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 11/05/2017.
//
//

import UIKit

class SettingsViewController: UIViewController {
    let globalInstance = Global.sharedInstance
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        prepareUI()
        updateNavBarUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kSETTINGS)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kSETTINGS)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false //adjust the tableViews scrollview to origin
    }
    
    private func prepareUI() {
        Global.pageType = Global.VIEW_TYPE.kAPP_SETTINGS
        Global.cellSequence = [CustomCardCellViewController.CARD_CELL.kDETAILED]
    }
    
    private func updateNavBarUI() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 11, width: 22, height: 22)
        back.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 100, height: 24))
        label.text = "settings"
        label.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        label.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        label.textAlignment = NSTextAlignment.left
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftView
        
        // to apply same color contrast and remove the nav 1px bottom line
        navigationController?.navigationBar.barTintColor = UIColor(red: 43/255, green: 40/255, blue: 83/255, alpha: 1.0)
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @objc func goBack() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kBACK_CLICKED)
        
        Global.sharedInstance.goBack(self)
    }
    
}
