//
//  SwitchProductViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 12/07/2017.
//
//

import UIKit
import Fabric

class SwitchProductViewController: UIViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //prepareUI()
        updateNavBarUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Global.pageType = Global.VIEW_TYPE.kSWITCH_PRODUCT
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kSWITCHPRODUCT)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kSWITCHPRODUCT)
    }
    
    private func prepareUI() {
        Global.pageType = Global.VIEW_TYPE.kSWITCH_PRODUCT
        
        let prodTypes: [String] =  SwitchModel.getProductTypes()
        var types: Array<String> = Array()
        
        for _ in prodTypes {
            types.append(CustomCardCellViewController.CARD_CELL.kCARD)
        }
        
        Global.cellSequence = types
    }
    
    private func updateNavBarUI() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 7, width: 32, height: 32)
        back.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 100, height: 24))
        label.text = "switch"
        label.textColor = .white
        label.textAlignment = NSTextAlignment.left
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftView
        
        // to apply same color contrast and remove the nav 1px bottom line
        navigationController?.navigationBar.barTintColor = UIColor(red: 43/255, green: 40/255, blue: 83/255, alpha: 1.0)
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @objc func goBack() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kBACK_CLICKED)
        
        Global.sharedInstance.goBack(self)
    }
    
}
