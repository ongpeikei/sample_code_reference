//
//  SwitchAccountViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 12/07/2017.
//
//

import UIKit
import Fabric

class SwitchAccountViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var cvGrid: UICollectionView!
    private var accountNumbers: [AccountNumberEntity] = [AccountNumberEntity]()
    
    var selectedProductType: Int = 0
    var hasSelectedAccount: Bool = false
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        accountNumbers = CustomerDetailsModel.sharedInstance.getAccountsByProductType(SwitchModel.getProductTypes()[selectedProductType])
        
        for accountNumber in accountNumbers {
            if accountNumber.accountNo == SwitchModel.getSelectedAccountNo() {
                hasSelectedAccount = true
                break
            }
        }

        cvGrid.register(UINib(nibName: "GridCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CustomCardCellViewController.CARD_CELL.kCARD)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateNavBarUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // to prevent dashboard to load the API 
        Global.sharedInstance.setIsFromDrawer(true)
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kSWITCHACCOUNT)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kSWITCHACCOUNT)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return accountNumbers.count // return total number of acc per product
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = collectionView.frame.size.width
        
        return CGSize(width: collectionViewSize/2, height: 93) // currently the height of the custom xib "GridCollectionViewCell"
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCardCellViewController.CARD_CELL.kCARD, for: indexPath) as! GridCollectionViewCell
        
        let accountNumber: AccountNumberEntity = accountNumbers[indexPath.row]
        let accStatus = accountNumber.accountStatus
        
        cell.lblTop.text = "account number"
        cell.lblCenter.text = accountNumber.accountNo
        cell.lblBottom.text = "status: " + Global.sharedInstance.accountStatus(accStatus)
        cell.switchIndicator?.isHidden = true
        
        if hasSelectedAccount {
            if(accountNumber.accountNo.isEqual(SwitchModel.getSelectedAccountNo())) {
                cell.switchIndicator?.isHidden = false
            }
        } else {
            if indexPath.row == 0 {
                cell.switchIndicator?.isHidden = false
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let accountNumber: AccountNumberEntity = accountNumbers[indexPath.row]
        
        SwitchModel.saveSelectedProductType(SwitchModel.getProductTypes()[selectedProductType])
        SwitchModel.saveSelectedAccountNo(accountNumber.accountNo)
        
        Global.sharedInstance.setLoadMoreShouldDisplay(true)
        self.performSegue(withIdentifier: UNWIND_TO_ROOT, sender: self)
    }
    
    private func updateNavBarUI() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 7, width: 32, height: 32)
        back.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 100, height: 24))
        label.text = "switch"
        label.textColor = .white
        label.textAlignment = NSTextAlignment.left
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftView
        
        // to apply same color contrast and remove the nav 1px bottom line
        navigationController?.navigationBar.barTintColor = UIColor(red: 43/255, green: 40/255, blue: 83/255, alpha: 1.0)
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @objc func goBack() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kBACK_CLICKED)
        
        Global.sharedInstance.goBack(self)
    }
    
}
