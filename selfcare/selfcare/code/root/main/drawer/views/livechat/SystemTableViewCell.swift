//
//  SystemTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 21/06/2017.
//
//

import UIKit

class SystemTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDateStamp: UILabel!
    @IBOutlet weak var btnRestart: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareLayout()
    }
    
    func prepareLayout() {
        lblMessage.font = globalInstance.getFontAttribute(.kREGULAR, .kMEDIUM)
        lblMessage.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblDateStamp.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL_X)
        lblDateStamp.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        btnRestart.layer.cornerRadius = 5
        btnRestart.titleLabel?.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kMEDIUM)
        btnRestart.setTitleColor(globalInstance.getColorAttribute(.kWHITE, .DEFAULT), for: UIControlState.normal)
        btnRestart.backgroundColor = globalInstance.getColorAttribute(.kBLUE, .DEFAULT)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
