//
//  AgentRndTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 22/06/2017.
//
//

import UIKit

class AgentRndTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTimeStamp: UILabel!
    @IBOutlet weak var lblDisplayName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width:0 ,height:5)
        self.layer.shadowOpacity = 0.08
        
        prepareLayout()
    }
    
    func prepareLayout() {
        lblMessage.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblMessage.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblTimeStamp.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL_XX)
        lblTimeStamp.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblDisplayName.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblDisplayName.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
