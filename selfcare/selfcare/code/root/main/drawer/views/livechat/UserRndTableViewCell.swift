//
//  UserRndTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 22/06/2017.
//
//

import UIKit

class UserRndTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lblMessage: MsgLabel!
    @IBOutlet weak var lblTimeStamp: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width:0 ,height:5)
        self.layer.shadowOpacity = 0.1
        
        prepareLayout()
    }
    
    func prepareLayout() {
        lblMessage.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblMessage.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        lblTimeStamp.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL_XX)
        lblTimeStamp.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
