//
//  AgentTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 21/06/2017.
//
//

import UIKit

class AgentTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTimeStamp: UILabel!
    @IBOutlet weak var lblDisplayName: UILabel!
    @IBOutlet weak var bubble: Bubble!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width:0 ,height:5)
        self.layer.shadowOpacity = 0.08
        
        prepareLayout()
    }
    
    func prepareLayout() {
        lblMessage.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblMessage.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblTimeStamp.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL_XX)
        lblTimeStamp.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblDisplayName.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblDisplayName.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class Bubble : UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()

        var corner: UIRectCorner 
        if (self.restorationIdentifier?.isEqual(ChatModel.BUBBLE_ID.kAGENT))! {
            corner = [.topRight, .bottomRight, .bottomLeft]
        
        } else if (self.restorationIdentifier?.isEqual(ChatModel.BUBBLE_ID.kUSER))! {
            corner = [.topLeft, .bottomRight, .bottomLeft]
        
        } else {
            corner = [.topRight, .topLeft, .bottomRight, .bottomLeft]
        }
        
        let path = UIBezierPath(roundedRect:self.bounds, byRoundingCorners:corner,
                            cornerRadii: CGSize(width: 40, height:  40))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}

