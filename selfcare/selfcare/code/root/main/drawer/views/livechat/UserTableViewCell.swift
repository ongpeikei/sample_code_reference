//
//  UserTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 21/06/2017.
//
//

import UIKit

class UserTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lblMessage: MsgLabel!
    @IBOutlet weak var lblTimeStamp: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width:0 ,height:5)
        self.layer.shadowOpacity = 0.1
        
        prepareLayout()
    }
    
    func prepareLayout() {
        lblMessage.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblMessage.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        lblTimeStamp.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL_XX)
        lblTimeStamp.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class MsgLabel : UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: 0, bottom: bottomInset, right: 0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            let msgFont:UIFont = Global.sharedInstance.getFontAttribute(.kREGULAR, .kSMALL)! //update when specs changes
            let msgWidth = UIScreen.main.bounds.width - 114
            let msgHeight = Global.sharedInstance.getEstimatedLabelHeight(byText: self.text!, font: msgFont, labelWidth: msgWidth)
            
            var contentSize = super.intrinsicContentSize
            contentSize.height = msgHeight + topInset + bottomInset
            contentSize.width = msgWidth
            return contentSize
        }
    }
}
