//
//  EnquiryTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 26/06/2017.
//
//

import UIKit

class EnquiryTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var btnRadio: UIImageView!
    @IBOutlet weak var lblEnquiry: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width:0 ,height:5)
        self.layer.shadowOpacity = 0.08
        
        prepareLayout()
    }
    
    func prepareLayout() {
        lblEnquiry.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblEnquiry.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_58)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
