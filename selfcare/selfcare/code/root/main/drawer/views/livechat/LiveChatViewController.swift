//
//  LiveChatViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 06/06/2017.
//
//

import UIKit
import GrowingTextView
import Fabric
import Crashlytics

protocol ChatDelegator {
    func callEndSessionFromDialog()
    func callRetainSessionFromDialog()
    
    func callGoBackFromDialog()
    func callRestartChatFromDialog()
}

class LiveChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GrowingTextViewDelegate, ChatDelegator {
    static let sharedInstance = LiveChatViewController()
    
    let globalInstance = Global.sharedInstance
    let model = ChatModel.sharedInstance
    let defaults = UserDefaults.standard

    @IBOutlet weak var tvChat: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var textViewChat: GrowingTextView!
    
    @IBOutlet weak var lblEnquiryTypeHeader: UILabel!
    @IBOutlet weak var viewEnquiry: UIView!
    @IBOutlet weak var tvEnquiry: UITableView!
    @IBOutlet weak var lcBottomTVChat: NSLayoutConstraint!
    
    @IBOutlet weak var btnStartChat: UIButton!
    
    fileprivate var newMsgCount:Int = 0
    fileprivate var timer:Timer!
    fileprivate var shouldScrollDown: Bool = true
    
    fileprivate var enquiryCategory:Array = [String]()
    fileprivate var enquiryType:String = ""
    
    fileprivate var tap: UITapGestureRecognizer? = nil
       
    // bubble key
    enum PARTICIPANT_TYPE {
        case kAGENT
        case kUSER
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        enquiryCategory = ["enquiry", "bill charges & payment", "self-care", "technical enquiries", "mobile apps", "installation/delivery", "service reactivation", "service cancellation request", "report lost/damage device"]
        
        // dissmiss keyboard on background tap
        tap = UITapGestureRecognizer(target: self, action: #selector(LiveChatViewController.dismissKeyboard))
        
        guard !(tap == nil) else {
            // tap is nil
            return
        }
        
        // start chatview
        if globalInstance.getLiveChatSession() {
            viewEnquiry.isHidden = true
            tvChat.delegate = self
            
            // resume live chat sesion here
            self.timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.chatPoll), userInfo: nil, repeats: true)
            self.chatPoll()
            self.reloadChat()
        } else {
            viewEnquiry.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kLIVECHATINQUIRY)
        
        let initialIndexPath = NSIndexPath(row: 0, section: 0)
        tvEnquiry.selectRow(at: initialIndexPath as IndexPath, animated: false, scrollPosition: .none)
        tvEnquiry.delegate?.tableView!(tvEnquiry, didSelectRowAt: initialIndexPath as IndexPath)
        
        updateNavBarUI()
        prepareLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scrollTable()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tvChat {
            return newMsgCount
        } else {
            return enquiryCategory.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Chat TableView
        if tableView == tvChat {
            let details = model.getNewMsgResponse()[indexPath.row]
            let msg = details[ChatModel.CONSTANTS.kVALUE] as! String
            let type = details[ChatModel.CONSTANTS.kTYPE] as! String
            var timeStamp = ""
            var dateStamp = ""
            var displayName = ""
            
            if (type.isEqual(ChatModel.VALUE.TEXT) ) {
                timeStamp = details[ChatModel.CONSTANTS.kTIME_STAMP] as! String
                dateStamp = details[ChatModel.CONSTANTS.kDATE_STAMP] as! String
                displayName = details[ChatModel.CONSTANTS.kDISPLAY_NAME] as! String
            }
            
            if (details[ChatModel.CONSTANTS.kPARTICIPANT_TYPE] as! String).isEqual(ChatModel.VALUE.WEB_USER) {
                if isConsecutiveMsg(.kUSER, indexPath){
                    let userRndCell = tableView.dequeueReusableCell(withIdentifier:ChatModel.BUBBLE_ID.kUSER_ROUND, for: indexPath) as! UserRndTableViewCell
                    userRndCell.lblMessage.text = msg
                    userRndCell.lblTimeStamp.text = timeStamp
                    
                    return userRndCell
                } else {
                    let userCell = tableView.dequeueReusableCell(withIdentifier: ChatModel.BUBBLE_ID.kUSER, for: indexPath) as! UserTableViewCell
                    userCell.lblMessage.text = msg
                    userCell.lblTimeStamp.text = timeStamp
                    
                    return userCell
                }
            } else if (details[ChatModel.CONSTANTS.kPARTICIPANT_TYPE] as! String).isEqual(ChatModel.VALUE.AGENT) {
                
                if isConsecutiveMsg(.kAGENT, indexPath){
                    let agentRndCell = tableView.dequeueReusableCell(withIdentifier:ChatModel.BUBBLE_ID.kAGENT_ROUND, for: indexPath) as! AgentRndTableViewCell
                    agentRndCell.lblMessage.text = msg
                    agentRndCell.lblTimeStamp.text = timeStamp
                    agentRndCell.lblDisplayName.text = " - " + displayName
                    
                    return agentRndCell
                    
                } else {
                    let agentCell = tableView.dequeueReusableCell(withIdentifier: ChatModel.BUBBLE_ID.kAGENT, for: indexPath) as! AgentTableViewCell
                    agentCell.lblMessage.text = msg
                    agentCell.lblDisplayName.text = " - " + displayName
                    agentCell.lblTimeStamp.text = timeStamp
                    
                    return agentCell
                }
                
            } else {
                let systemCell = tableView.dequeueReusableCell(withIdentifier: ChatModel.BUBBLE_ID.kSYSTEM, for: indexPath) as! SystemTableViewCell
                systemCell.lblMessage.text = msg
                
                if (type.isEqual(ChatModel.VALUE.TYPING_INDICATOR)) {
                    systemCell.lblDateStamp.isHidden = true
                    systemCell.btnRestart.isHidden = true
                
                } else if (type.isEqual(ChatModel.VALUE.DISCONNECTED)) {
                    systemCell.lblDateStamp.isHidden = true
                    systemCell.btnRestart.isHidden = false
                    
                    systemCell.btnRestart.addTarget(self, action: #selector(restartChat), for: .touchUpInside)
                
                } else {
                    systemCell.btnRestart.isHidden = true
                    systemCell.lblDateStamp.isHidden = false
                    systemCell.lblDateStamp.text = dateStamp + "   " + timeStamp
                }
                
                return systemCell
            }
        } else { // Enquiry TableView
            let enquiryCell = tableView.dequeueReusableCell(withIdentifier: "ENQUIRY", for: indexPath) as! EnquiryTableViewCell
            enquiryCell.lblEnquiry.text = enquiryCategory[indexPath.row]
            
            return enquiryCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tvEnquiry {
            let enquiryCell = tvEnquiry.cellForRow(at: indexPath) as! EnquiryTableViewCell
            enquiryCell.btnRadio.image = UIImage(named: "btn_radio_selected")
            
            enquiryType = enquiryCategory[indexPath.row]
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == tvEnquiry {
            let enquiryCell = tvEnquiry.cellForRow(at: indexPath) as! EnquiryTableViewCell
            
            enquiryCell.btnRadio.image = UIImage(named: "btn_radio_unselected")
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView)
        
        let lastSectionIndex = tvChat.numberOfSections - 1
        let lastRowIndex = tvChat.numberOfRows(inSection: lastSectionIndex) - 1
        let pathToLastRow = IndexPath(row: lastRowIndex, section: lastSectionIndex)
        let isLastRowDisplayed: Bool = (tvChat.indexPathsForVisibleRows?.contains(pathToLastRow))!
        
        if translation.y > 0 { // scroll down
            if !isLastRowDisplayed {
                shouldScrollDown = false
            }
        } else { // scroll up
            if isLastRowDisplayed {
                shouldScrollDown = true
            }
        }
    }
    
    private func updateNavBarUI() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 11, width: 22, height: 22)
        back.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 100, height: 24))
        label.text = "live chat"
        label.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        label.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        label.textAlignment = NSTextAlignment.left
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftView
        
        // to apply same color contrast and remove the nav 1px bottom line
        navigationController?.navigationBar.barTintColor = UIColor(red: 43/255, green: 40/255, blue: 83/255, alpha: 1.0)
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func prepareLayout() {
        lblEnquiryTypeHeader.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblEnquiryTypeHeader.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_58)
        
        btnStartChat.titleLabel?.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        btnStartChat.titleLabel?.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        btnStartChat.backgroundColor = globalInstance.getColorAttribute(.kBLUE, .DEFAULT)
        
        textViewChat.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE)
        textViewChat.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        textViewChat.placeHolderColor = globalInstance.getColorAttribute(.kLIGHT_GREY, .DEFAULT)!
        textViewChat.layer.cornerRadius = 4
        
        btnSend.layer.cornerRadius = 4
        
        tvChat.allowsSelection = false
    }
    
    @objc func goBack() {
        if globalInstance.getLiveChatSession() {
            let dialog = CustomDialogView(type: .CHAT_END)
            dialog.delegateChat = self
            dialog.show(animated: true)
        } else {
            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kBACK_CLICKED)
            globalInstance.goBack(self)
        }
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        lcBottomTVChat.constant = (keyboardSize?.height)!
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        lcBottomTVChat.constant = 0
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        lcBottomTVChat.constant = (keyboardSize?.height)!
        
        scrollTable()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func scrollTable() {
        let lastSectionIndex = tvChat.numberOfSections - 1
        let lastRowIndex = tvChat.numberOfRows(inSection: lastSectionIndex) - 1
        
        guard lastRowIndex > 0 else {
            
            return
        }
        
        DispatchQueue.main.async(execute: { () -> Void in
            let pathToLastRow = IndexPath(row: lastRowIndex, section: lastSectionIndex)
            self.tvChat.scrollToRow(at: pathToLastRow, at: UITableViewScrollPosition.none, animated: false)
        })
        
        let pathToLastRow = IndexPath(row: lastRowIndex, section: lastSectionIndex)
        self.tvChat.scrollToRow(at: pathToLastRow, at: UITableViewScrollPosition.none, animated: false)
    }
    
    func isConsecutiveMsg(_ participantType: PARTICIPANT_TYPE, _ indexPath: IndexPath) -> Bool {
        var prevParticipantType: String = ""
        if (model.getNewMsgResponse().count > 1) && (indexPath.row > 0) {
            let rawParticipant:String? = (model.getNewMsgResponse()[indexPath.row - 1])[ChatModel.CONSTANTS.kPARTICIPANT_TYPE] as? String
            prevParticipantType = (rawParticipant != nil) ? rawParticipant! : ""
        }
        
        switch participantType {
        case .kAGENT:
            if prevParticipantType.isEqual(ChatModel.VALUE.AGENT) {
                return true
            }
            
        case .kUSER:
            if prevParticipantType.isEqual(ChatModel.VALUE.WEB_USER) {
                return true
            }
        }
        
        return false
    }
    
    @IBAction func startChat(_ sender: UIButton) {
        sender.isUserInteractionEnabled = false
        
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kSTART_CHAT_CLICKED)
        FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kSTART_CHAT_CLICKED)
        
        globalInstance.setIsEndChatFromLogout(false) //reset to default value
        startLiveChat()
    }
    
    func startLiveChat() {
        // reset messages
        model.setNewMsgResponse([AnyObject]())
        newMsgCount = 0
        tvChat.reloadData()
        
        globalInstance.showHUD()
        
        model.prepareChatStartParams(ChatModel.VALUE.LTE, ChatModel.VALUE.WORKGROUP, CustomerDetailsModel.sharedInstance.getFullName(), enquiryType);
        
        Global.webServiceMngr.makeRequestWithoutToken(model.getChatStartParams()) { (isResponseDidFail, isServerMaintenance) in
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                self.globalInstance.dismissHUD()
                return
            }
            
            if isResponseDidFail { // if fail do something here
                self.globalInstance.dismissHUD()
                self.btnStartChat.isUserInteractionEnabled = true
            } else {
                self.globalInstance.dismissHUD()
                FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kLIVECHAT)
                FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kLIVECHAT)
                
                self.viewEnquiry.isHidden = true
                self.view.addGestureRecognizer(self.tap!)
                self.btnStartChat.isUserInteractionEnabled = true
                self.timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.chatPoll), userInfo: nil, repeats: true)
            }
        }
    }
    
    @objc func chatPoll(){
        Global.webServiceMngr.makeRequestWithoutToken(model.getChatPollParams()) { (isResponseDidFail, isServerMaintenance) in
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                if !self.globalInstance.getLiveChatSession() {
                    self.showDisconnectedDialog()
                }
            } else {
                if self.model.getShouldReloadTable() {
                    self.reloadChat()
                }
                
                if !self.globalInstance.getLiveChatSession() {
                    self.stopPoll()
                }
            }
        }
    }
    
    private func reloadChat() {
        updateMsgList()
        checkTypingIndicator()
        checkIfDisconnected()
        
        newMsgCount = model.getNewMsgResponse().count
        
        tvChat.reloadData()
        
        if shouldScrollDown {
            scrollTable()
        }
    }
    
    private func updateMsgList() {
        var messageList: Array = [AnyObject]()
        
        for item in model.getNewMsgResponse() {
            if (item[ChatModel.CONSTANTS.kTYPE] as! String).isEqual(ChatModel.VALUE.TEXT) {
                messageList.append(item)
            }
        }
        
        if messageList.count > 0 {
            model.setNewMsgResponse(messageList)
        }
    }
    
    private func checkTypingIndicator() {
        var msgWithTypingIndicator:Array = model.getNewMsgResponse()
        
        if model.getIsAgentTyping() {
            msgWithTypingIndicator.append(([ChatModel.CONSTANTS.kPARTICIPANT_TYPE:ChatModel.VALUE.SYSTEM,
                                            ChatModel.CONSTANTS.kTYPE:ChatModel.VALUE.TYPING_INDICATOR,
                                            ChatModel.CONSTANTS.kVALUE:ChatModel.VALUE.AGENT_IS_TYPING] as AnyObject))
            
            model.setNewMsgResponse(msgWithTypingIndicator)
        }
    }
    
    private func checkIfDisconnected() {
        var msgWithDisconnected:Array = model.getNewMsgResponse()
        
        if !globalInstance.getLiveChatSession() {
            msgWithDisconnected.append(([ChatModel.CONSTANTS.kPARTICIPANT_TYPE:ChatModel.VALUE.SYSTEM,
                                         ChatModel.CONSTANTS.kTYPE:ChatModel.VALUE.DISCONNECTED,
                                         ChatModel.CONSTANTS.kVALUE:ChatModel.VALUE.LIVE_CHAT_ENDED] as AnyObject))
            
            model.setNewMsgResponse(msgWithDisconnected)
        }
    }
    
    @IBAction func sendButton(_ sender: UIButton) {
        if (textViewChat.text != "") {
            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kSEND_CHAT_CLICKED)
            
            sendMessage()
        } else {
            return
        }
    }
    
    func sendMessage() {
        model.prepareChatSendParams(textViewChat.text!)
        Global.webServiceMngr.makeRequestWithoutToken(model.getChatSendParams()) { (isResponseDidFail, isServerMaintenance) in
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                // failed, will do something here
            } else {
                self.chatPoll()
                
                self.textViewChat.text = "" // clear chat text box
            }
        }
    }
    
    private func stopPoll() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
    }
    
    @objc func restartChat() {
        dismissKeyboard()
        
        view.removeGestureRecognizer(tap!)
        viewEnquiry.isHidden = false
    }
    
    private func showDisconnectedDialog() {
        stopPoll()
        
        let dialog = CustomDialogView(type: .CHAT_DISCONNECTED)
        dialog.delegateChat = self
        dialog.show(animated: true)
    }
    
    private func endSession() {
        Global.webServiceMngr.makeRequestWithoutToken(model.getChatExitParams()) { (isResponseDidFail, isServerMaintenance) in
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                // failed, will do something here
            } else {
                if !self.globalInstance.getIsEndChatFromLogout() {
                    self.model.setNewMsgResponse([AnyObject]())
                    self.newMsgCount = 0
                    self.reloadChat()
                    
                    self.globalInstance.goBack(self)
                    self.globalInstance.clearParticipanID()
                }
            }
            
            if self.globalInstance.getIsEndChatFromLogout() {
                self.model.setChatResponse([AnyObject]())
                self.model.setNewMsgResponse([AnyObject]())
                self.newMsgCount = 0
                self.globalInstance.clearParticipanID()
            }
        }
    }
    
// MARK: DELEGATE methods
    func callEndSessionFromDialog() {
        stopPoll()
        endSession()
    }
    
    func callRetainSessionFromDialog() {
        stopPoll()
        model.setMessageLog() //save messages in database
        globalInstance.goBack(self)
    }
    
    func callGoBackFromDialog() {
        globalInstance.goBack(self)
        globalInstance.clearParticipanID()
    }
    
    func callRestartChatFromDialog() {
        restartChat()
    }
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
}
