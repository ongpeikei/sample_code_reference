//
//  DrawerMenuViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 01/06/2017.
//
//

import UIKit
import KYDrawerController
import Fabric

protocol DrawerDelegator {
    func callSegueFromDialog(_ segue: String)
}

class DrawerMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DrawerDelegator {
    @IBOutlet var tvDrawer: UITableView?
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnSwitch: UIButton!
    @IBOutlet weak var vAccStatus: UIView!
    
    @IBOutlet weak var vSwitch: UIView!
    let globalInstance = Global.sharedInstance
    var kCELL: String = "CELL"
    
    enum MENU: Int {
        //case kWHATSNEW
        case kPROFILE
        case kLIVECHAT
        case kHISTORY
        //case kSUPPORT
        case kSETTINGS
        //case kNOTIFICATIONS
        //case kORDERS
        case kLOGOUT
        
        static let count: Int = {
            var max: Int = 0
            while let _ = MENU(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // don't move this it'll affect the UX
        prepareLayout()
        setDefaultValue()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kDRAWER)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kDRAWER)
    }
    
    fileprivate func setDefaultValue() {
        let fileName = CustomerDetailsModel.sharedInstance.getProfilePicture()
        if fileName == "" {
            imgProfilePic.image = UIImage(named: "logo_place_holder")
            imgProfilePic = globalInstance.roundedImage(imgProfilePic)
        } else {
            let imagePath = globalInstance.getImagePath() + fileName
            let url = URL(string: imagePath)
            let data = try? Data(contentsOf: url!)
            if data != nil {
                imgProfilePic.image = UIImage(data: data!)
                imgProfilePic = globalInstance.roundedImage(imgProfilePic)
            }
        }
        
        lblName.text = CustomerDetailsModel.sharedInstance.getFullName()
        lblStatus.text = CustomerDetailsModel.sharedInstance.getAccountStatus()
    }
    
    fileprivate func prepareLayout() {
        lblName.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE)
        lblName.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        lblStatus.font = globalInstance.getFontAttribute(.kREGULAR, .kMEDIUM)
        lblStatus.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        vAccStatus.backgroundColor = globalInstance.getAccountStatusColor()
        vAccStatus = globalInstance.setRoundedLabel(RADIUS, vAccStatus)
        
        let prodTypes: [String] =  SwitchModel.getProductTypes()
        let accountNumbers = CustomerDetailsModel.sharedInstance.getAccountNoList()
        
        if (prodTypes.count > 1 || (accountNumbers?.count)! > 1) {
            vSwitch.isHidden = false
        } else {
            vSwitch.isHidden = true
        }
        
        btnSwitch.titleLabel?.font = globalInstance.getFontAttribute(.kBOLD, .kMEDIUM)
        btnSwitch.titleLabel?.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
    }

    @IBAction func switchButtonTapped(_ sender: Any) {
        globalInstance.setIsFromDrawer(true)
        
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(.closed, animated: true)
        
        self.performSegue(withIdentifier: DRAWER_SWITCH_PRODUCT, sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        
        return header
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MENU.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCELL, for: indexPath) as! DrawerMenuTableViewCell
        cell.selectionStyle = .none
        
        switch indexPath.row {
        /*case MENU.kWHATSNEW.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_profile")
            cell.lblTitle.text = "what's new"
            break*/
        case MENU.kPROFILE.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_profile")
            cell.lblTitle.text = "profile"
            
            break
        case MENU.kLIVECHAT.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_livechat")
            cell.lblTitle.text = "live chat"
            
            break
        case MENU.kHISTORY.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_history")
            cell.lblTitle.text = "history"
            
            break
        /*case MENU.kSUPPORT.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_profile")
            cell.lblTitle.text = "support"
            break*/
        case MENU.kSETTINGS.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_settings")
            cell.lblTitle.text = "settings"
            
            break
        /*case MENU.kNOTIFICATIONS.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_profile")
            cell.lblTitle.text = "notifications"
            break
        case MENU.kORDERS.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_profile")
            cell.lblTitle.text = "orders"
            break*/
        case MENU.kLOGOUT.rawValue:
            cell.imgThumbnail.image = UIImage(named: "ic_logout")
            cell.lblTitle.text = "log out"
            
            break
        default:
            cell.lblTitle.text = "default"
            
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // to determine selection is from drawer
        globalInstance.setIsFromDrawer(true)
        
        // to close the drawer before leaving
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(.closed, animated: true)
        
        switch indexPath.row {
        /*case MENU.kWHATSNEW.rawValue:
            self.performSegue(withIdentifier: DRAWER_WHATS_NEW, sender: self)
            break*/
        case MENU.kPROFILE.rawValue:
            self.performSegue(withIdentifier: DRAWER_PROFILE, sender: self)
            
            break
        case MENU.kLIVECHAT.rawValue:
            self.performSegue(withIdentifier: DRAWER_LIVE_CHAT, sender: self)
            
            break
        case MENU.kHISTORY.rawValue:
            self.performSegue(withIdentifier: DRAWER_HISTORY, sender: self)
            
            break
        /*case MENU.kSUPPORT.rawValue:
            self.performSegue(withIdentifier: DRAWER_SUPPORT, sender: self)
            break*/
        case MENU.kSETTINGS.rawValue:
            self.performSegue(withIdentifier: DRAWER_SETTINGS, sender: self)
            
            break
        /*case MENU.kNOTIFICATIONS.rawValue:
            self.performSegue(withIdentifier: DRAWER_NOTIFICATIONS, sender: self)
            break
        case MENU.kORDERS.rawValue:
            self.performSegue(withIdentifier: DRAWER_ORDERS, sender: self)
            break*/
        case MENU.kLOGOUT.rawValue:
            FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kLOGOUT_DIALOG)
            
            let dialog = CustomDialogView(type: .LOGOUT)
            dialog.delegateDrawer = self
            dialog.show(animated: true)
            
            break
        default:
            // default method
            break
        }
    }
    
    func callSegueFromDialog(_ segue: String) {
        self.performSegue(withIdentifier: segue, sender: self)
    }

}
