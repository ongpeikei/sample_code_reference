//
//  DrawerMenuTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 06/06/2017.
//
//

import UIKit

class DrawerMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    let globalInstance = Global.sharedInstance

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        prepareLayout()
    }
    
    func prepareLayout() {
        lblTitle.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblTitle.textColor = globalInstance.getColorAttribute(.kDARK_GREY, .DEFAULT)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
