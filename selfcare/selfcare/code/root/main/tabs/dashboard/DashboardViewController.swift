//
//  DashboardViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 11/05/2017.
//
//

import XLPagerTabStrip
import Fabric

protocol DashboardDelegator {
    func updateDashboardContainer(_ size:CGSize)
    func callPreviewPDF(_ url : URL)
}

class DashboardViewController: UIViewController, IndicatorInfoProvider, DashboardDelegator, UIDocumentInteractionControllerDelegate {
    var tabBarTitle: IndicatorInfo = "overview"
    let globalInstance = Global.sharedInstance
    let model = DashboardModel.sharedInstance
    var paymentFlag: Bool = false
    
    var paymentStatus = ""
    
    fileprivate var customCardCellViewController:CustomCardCellViewController!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lcContainerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblProductType: UILabel!
    @IBOutlet weak var buttonAccDrpdwn: UIButton!
    @IBOutlet weak var vAccStatus: UIView!
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return tabBarTitle
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        customCardCellViewController = segue.destination as! CustomCardCellViewController
        customCardCellViewController.delegateDashboard = self
        
        // to prepare the table UI
        prepareUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // don't move this it'll affect the UX
        prepareLayout()
        setDefaultValue()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // validation if the view is from drawer (it should not call an API)
        if globalInstance.getIsFromDrawer() {
            globalInstance.setIsFromDrawer(false) // reset to default value
        } else {
            FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kDASHBOARD)
            FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kDASHBOARD)
            
            // validation to show the push notif dialog (ONLY when the view is already shown)
            if globalInstance.getShouldShowPushNotif() {
                PushNotification.sharedInstance.showDialog()
                
                globalInstance.setShouldShowPushNotif(false) // reset the value
            }
            
            callPaymentResultDialog() //Note: return when viewDidAppear is fix for KYDrawer
            
            if !Global.webServiceMngr.isServerMaintenance {
                callDashboardApi()
            }
            
            if globalInstance.getPaymentShouldShowError()! {
                globalInstance.showGeneralError()
                
                globalInstance.setPaymentShouldShowError(false)
            }
            
            // don't move this it'll cause crash on switch
            prepareUI()
        }
    }
    
    func updateDashboardContainer(_ size: CGSize) {
        if size.height == 0 {
            scrollView.isScrollEnabled = false
            
            //  validation for iphone SE/5s/5 screen
            let screenHeight = UIScreen.main.bounds.height
            if screenHeight == 568.0 {
                scrollView.isScrollEnabled = true
            }
        } else {
            scrollView.isScrollEnabled = true
            lcContainerHeight.constant = size.height + 66 //+66 or -70
        }
    }
    
    @IBAction func tappedAcctDrpdwn(_ sender: Any) {
        print("account selection")
    }
    
    func callDashboardApi() {
        // if network fails, don't call API anymore
        if globalInstance.getIsOffline() {
            let didSaveFail: Bool = model.setDashboardApiResponse(DummyResponse.getOutstandingBalance())
            
            if !didSaveFail {
                self.prepareUI()
                self.customCardCellViewController.callReloadTable()
            }
        } else {
            if Global.webServiceMngr.isServerMaintenance {
                self.globalInstance.dismissHUD()
                
                return
            }
            
            if Global.webServiceMngr.isOffline() {
                self.validateLoadMore()
            } else {
                if model.getOutstandingBalResponse() == nil || !globalInstance.getIsDashboardApiTokenRefreshed() || LoginModel.isTokenExpired() {
                    model.prepareDashboardApi(SwitchModel.getSelectedAccountNo()!)
                    globalInstance.showHUD()
                    
                    Global.webServiceMngr.makeRequest(model.getDashboardApiRequestParams()) { (isResponseDidFail, isServerMaintenance) in
                        self.globalInstance.dismissHUD()
                        
                        if isServerMaintenance { // to stop the process and avoid unexpected crashing
                            return
                        }
                        
                        if isResponseDidFail { // if fail do something here
                            // by default this is the action
                            self.globalInstance.showGeneralError()
                            
                            return
                        } else {
                            self.globalInstance.setIsDashboardApiTokenRefreshed(true)
                        }
                        
                        // this should execute whether isResponseDidFail is true/false
                        self.validateLoadMore()
                    }
                } else {
                    self.validateLoadMore()
                }
            }
        }
    }
    
    func callCreditUnbilledApi() {
        if globalInstance.getIsOffline() {
            let didSaveFail: Bool = model.setCreditUnbilledResponse(DummyResponse.getUnbilledLines())
            
            if !didSaveFail {
                self.prepareUI()
                self.customCardCellViewController.callReloadTable()
            }
        } else {
            /*print("CreditUnbilledBalanceResponse: \(model.getCreditUnbilledBalanceResponse() == nil)")
            print("LoginModel.isTokenExpired(): \(LoginModel.isTokenExpired())")
            print("UnbilledFlag: \(globalInstance.getUnbilledFlag()!)")
            print("IsCreditUnbilledApiTokenRefreshed: \(globalInstance.getIsCreditUnbilledApiTokenRefreshed())") -- uncomment when debugging */
            
            // Important checker especially when logout is called and still pages at the back are called
            if SwitchModel.getSelectedAccountNo() != nil {
                if (model.getCreditUnbilledBalanceResponse() == nil || LoginModel.isTokenExpired() || globalInstance.getUnbilledFlag()! || !globalInstance.getIsCreditUnbilledApiTokenRefreshed()) {
                    globalInstance.showHUD()
                    
                    model.prepareDashboardApi(SwitchModel.getSelectedAccountNo()!)
                    Global.webServiceMngr.makeRequest(model.getCreditUnbilledRequestParams()) { (isResponseDidFail, isServerMaintenance) in
                        if isServerMaintenance { // to stop the process and avoid unexpected crashing
                            self.globalInstance.dismissHUD()
                            
                            return
                        }
                        
                        if isResponseDidFail { // if fail do something here
                            // by default this is the action
                            self.globalInstance.showGeneralError()
                            
                            self.reloadAndDismissHUD()
                        } else {
                            self.globalInstance.setIsCreditUnbilledApiTokenRefreshed(true)
                            
                            self.reloadAndDismissHUD()
                        }
                        
                        self.globalInstance.setLoadMoreShouldDisplay(false)
                        self.globalInstance.setUnbilledFlag(false)
                    }
                } else {
                    self.reloadAndDismissHUD()
                }
            }
        }
    }
    
    fileprivate func prepareUI() {
        // we need this to update the current pageType
        Global.pageType = Global.VIEW_TYPE.kDASHBOARD
        Global.cellSequence = [CustomCardCellViewController.CARD_CELL.kCARD,
                               CustomCardCellViewController.CARD_CELL.kBUTTON,
                               CustomCardCellViewController.CARD_CELL.kBANNER,
                               CustomCardCellViewController.CARD_CELL.kBUTTON,
                               CustomCardCellViewController.CARD_CELL.kCARD,
                               CustomCardCellViewController.CARD_CELL.kCARD]
    }
    
    fileprivate func prepareLayout() {
        lblName.font = globalInstance.getFontAttribute(.kBOLD, .kMEDIUM)
        lblName.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        lblStatus.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblStatus.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        vAccStatus.backgroundColor = globalInstance.getAccountStatusColor()
        
        lblTitle.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblTitle.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        lblAccount.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE)
        lblAccount.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        lblProductType.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
    }
    
    fileprivate func setDefaultValue() {
        let fileName = CustomerDetailsModel.sharedInstance.getProfilePicture()
        if fileName == "" {
            imageProfile.image = UIImage(named: "logo_place_holder")
            imageProfile = globalInstance.roundedImage(imageProfile)
        } else {
            let imagePath = globalInstance.getImagePath() + fileName
            let url = URL(string: imagePath)
            
            let data = try? Data(contentsOf: url!)
            if data != nil {
                imageProfile.image = UIImage(data: data!)
                imageProfile = globalInstance.roundedImage(imageProfile)
            }
        }
        
        lblName.text = CustomerDetailsModel.sharedInstance.getFullName()
        
        lblStatus.text = CustomerDetailsModel.sharedInstance.getAccountStatus()
        vAccStatus = globalInstance.setRoundedLabel(RADIUS, vAccStatus)
        
        lblTitle.text = "account number"
        lblAccount.text = SwitchModel.getSelectedAccountNo()
        
        updateProductType()
    }
    
    private func updateProductType() {
        let prodType = SwitchModel.getSelectedProductType()
        
        let blackAttribute:[NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): globalInstance.getFontAttribute(.kBLACK, .kSMALL)!]
        let semiBoldAttribute:[NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue):globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)!]
        let productStr:String = "webe"
        var productTypeStr:String = ""
        var bgColor: UIColor = UIColor.clear
        
        if prodType == Global.PROD_TYPE.kMOBILE.rawValue {
            productTypeStr = "mobile®"
            bgColor = globalInstance.getColorAttribute(.kTEAL, .DEFAULT)!
            
        } else if prodType == Global.PROD_TYPE.kBROADBAND.rawValue {
            productTypeStr = "broadband®"
            bgColor = globalInstance.getColorAttribute(.kYELLOW, .DEFAULT)!
        }
        
        let productBlack = NSMutableAttributedString(string: productStr, attributes: blackAttribute)
        let productTypeSemiBold = NSMutableAttributedString(string: productTypeStr, attributes: semiBoldAttribute)
        
        let productTypeValue = NSMutableAttributedString()
        productTypeValue.append(productBlack)
        productTypeValue.append(productTypeSemiBold)
        
        lblProductType.attributedText = productTypeValue
        lblProductType.backgroundColor = bgColor
        lblProductType.layer.masksToBounds = true
        lblProductType.layer.cornerRadius = 4
    }
    
    func callPaymentResultDialog() {
        if globalInstance.getPaymentBillFlag()! {
            if (PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.SUCCESS) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
                    let dialog = CustomDialogView(type: .VIEW_RECEIPT, receiptSource: .kPAYMENT)
                    dialog.delegateDashboard = self
                    dialog.show(animated: true)
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
                    let dialog = CustomDialogView(type: .PAYMENT_FAILED)
                    dialog.show(animated: true)
                }
            }
            
            globalInstance.setPaymentBillFlag(false)
        } else if globalInstance.getPaymentCreditLimitFlag()! {
            if (PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.SUCCESS) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
                    let dialog = CustomDialogView(type: .PAYMENT_CREDIT_LIMIT_SUCCESS)
                    dialog.show(animated: true)
                }
            } else if (PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.FAILED || PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.PENDING || PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.PARTIAL) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
                    let dialog = CustomDialogView(type: .PAYMENT_CREDIT_LIMIT_FAILED)
                    dialog.show(animated: true)
                }
            }
            
            globalInstance.setPaymentCreditLimitFlag(false)
        }
    }
    
    func shouldShowLoadMore() {
        globalInstance.setLoadMoreShouldDisplay(true)
        scrollView.setContentOffset(CGPoint.zero, animated: true)
        
        reloadAndDismissHUD()
    }
    
    //preview pdf
    func callPreviewPDF(_ url: URL) {
        let docController = UIDocumentInteractionController(url: url)
        docController.delegate = self
        docController.presentPreview(animated: true)
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func validateLoadMore() {
        /*print("loadmoreshoulddisplay flag value: \(globalInstance.getLoadMoreShouldDisplay()!)") --uncomment when debugging */
        
        if globalInstance.getLoadMoreShouldDisplay()! {
            DispatchQueue.main.async {
                self.shouldShowLoadMore() // validate via token expiration
            }
        } else {
            callCreditUnbilledApi()
        }
    }
    
    func reloadAndDismissHUD() {
        self.prepareUI()
        self.customCardCellViewController.callReloadTable()
        
        self.globalInstance.dismissHUD()
    }
    
}
