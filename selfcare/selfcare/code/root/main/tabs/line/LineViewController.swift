//
//  LineViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 11/05/2017.
//
//

import XLPagerTabStrip
import Fabric

protocol LineDelegator {
    func updateLineContainer(_ size:CGSize)
    func updateLineData()
}

class LineViewController: UIViewController, IndicatorInfoProvider, LineDelegator {
    let globalInstance = Global.sharedInstance
    let model = AddOnBalanceModel.sharedInstance
    
    fileprivate var customCardCellViewController:CustomCardCellViewController!
    fileprivate var mainPage:MainPageViewController!
    
    var itemInfo: IndicatorInfo = ""
    var lineNumber: Int = 0
    
    @IBOutlet weak var lcContainerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        customCardCellViewController = segue.destination as! CustomCardCellViewController
        customCardCellViewController.delegateLine = self
        Global.containerView = customCardCellViewController
        
        // to prepare the table UI
        prepareUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // validation if the view is from drawer (it should not call an API)
        if globalInstance.getIsFromDrawer() {
            globalInstance.setIsFromDrawer(false) // reset to default value
        } else {
            FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kLINE)
            FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kLINE)
            
            globalInstance.setMainPageCurrentPosition(lineNumber)

            if !Global.webServiceMngr.isServerMaintenance {
                updateLineData()
            }
            
            // we need this to update the current pageType
            prepareUI()
        }
        
    }
    
    public func updateLineData() {
        let lineAlias = CustomerDetailsModel.sharedInstance.getAccountLines()[Global.sharedInstance.getSelectedMsisdn()].lineAlias
        let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
        
        if self.lblName != nil {
            self.lblName.text = lineAlias
        } else {
            return
        }
        
        if self.lblNumber != nil {
            self.lblNumber.text = globalInstance.formatMsisdn(msisdnByPosition)
        } else {
            return
        }
        
        callAddOnBalanceApi()
    }

    func updateLineContainer(_ size: CGSize) {
        lcContainerHeight.constant = size.height + 76
    }
    
    @IBAction func tappedButtonEdit(_ sender: Any) {
        
    }
    
    func callAddOnBalanceApi() {
        if globalInstance.getIsOffline() {
            let didSaveFail: Bool = model.setAddOnBalanceApiResponse(DummyResponse.getAddOnBalance())
            
            if !didSaveFail {
                self.prepareUI()
                self.customCardCellViewController.callReloadTable()
            }
        } else {
            if Global.webServiceMngr.isServerMaintenance {
                self.globalInstance.dismissHUD()
                
                return
            }
            
            if Global.webServiceMngr.isOffline() {
                self.prepareUI()
                self.customCardCellViewController.callReloadTable()
            } else {
                let msisdn = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
                UserDefaults.standard.set(msisdn, forKey: Global.USERDEFAULTS_KEY.CURRENT_MSISDN)
                
                if (model.getAddOnBalanceResponse(msisdn) == nil || LoginModel.isTokenExpired() || !globalInstance.getIsAddOnBalanceApiTokenRefreshed(msisdn) || globalInstance.getLineFlag()!) {
                    globalInstance.showHUD()
                    
                    model.prepareAddOnBalanceApi(SwitchModel.getSelectedAccountNo()!, msisdn)
                    Global.webServiceMngr.makeRequest(model.getAddOnBalanceApiRequestParams()) { (isResponseDidFail, isServerMaintenance) in
                        self.globalInstance.dismissHUD()
                        
                        if isServerMaintenance { // to stop the process and avoid unexpected crashing
                            return
                        }
                        
                        if isResponseDidFail { // if fail do something here
                            // by default this is the action
                            self.globalInstance.showGeneralError()
                        } else {
                            self.globalInstance.dismissHUD()
                            
                            self.prepareUI()
                            self.customCardCellViewController.callReloadTable()
                            self.globalInstance.setIsAddOnBalanceApiTokenRefreshed(true, false)
                        }
                    }
                } else {
                    self.prepareUI()
                    self.customCardCellViewController.callReloadTable()
                }
                
                self.globalInstance.setLineFlag(false)
            }
        }
    }
    
    private func prepareUI() {
        Global.pageType = Global.VIEW_TYPE.kLINE
        Global.cellSequence = [CustomCardCellViewController.CARD_CELL.kPAGER,
                               CustomCardCellViewController.CARD_CELL.kLIST,
                               CustomCardCellViewController.CARD_CELL.kLIST]
    }
    
    private func prepareLayout() {
        buttonEdit.titleLabel?.font = globalInstance.getFontAttribute(.kREGULAR, .kMEDIUM)
        buttonEdit.titleLabel?.textColor = globalInstance.getColorAttribute(.kBLUE, .DEFAULT)
        
        lblName.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblName.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        
        lblNumber.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE)
        lblNumber.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
    }
    
}
