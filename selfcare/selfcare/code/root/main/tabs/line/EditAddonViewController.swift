//
//  EditAddonViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 12/07/2017.
//
//

import UIKit

class EditAddonViewController: UIViewController {
    let globalInstance = Global.sharedInstance
    
    fileprivate var customCardCellViewController:CustomCardCellViewController!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        customCardCellViewController = segue.destination as! CustomCardCellViewController
        prepareUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        prepareUI()
        updateNavBarUI()
        checkIRPayment()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kADDON)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kADDON)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false //adjust the tableViews scrollview to origin
    }
    
    private func prepareUI() {
        Global.pageType = Global.VIEW_TYPE.kEDIT_ADDON
//        DataFactory.sharedInstance.setAddOnVasServicesList(DummyResponse.retrieveServices())
        
        let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
        var sequences: Array<String> = Array()
        
        if services.count > 0 {
            for _ in services {
                sequences.append(CustomCardCellViewController.CARD_CELL.kDETAILED)
            }
        }
        
        Global.cellSequence = sequences
    }
    
    private func checkIRPayment() {
        if globalInstance.getPaymentIRFlag()! {
            if (PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.SUCCESS) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
                    let dialog = CustomDialogView(type: .PAYMENT_SUCCESS)
                    dialog.show(animated: true)
                }
            } else if (PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.FAILED || PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.PENDING || PaymentModel.sharedInstance.getPaymentStatus() == Global.PAYMENT_STATUS.PARTIAL) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
                    let dialog = CustomDialogView(type: .PAYMENT_FAILED)
                    dialog.show(animated: true)
                }
            }
            globalInstance.dismissHUD()
            globalInstance.setPaymentIRFlag(false)
        } else {
            globalInstance.dismissHUD()
            customCardCellViewController.callReloadTable()
        }
    }
    
    private func updateNavBarUI() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 11, width: 22, height: 22)
        back.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 200, height: 24))
        label.text = "edit add on"
        label.textColor = .white
        label.textAlignment = NSTextAlignment.left
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftView
        
        // to apply same color contrast and remove the nav 1px bottom line
        navigationController?.navigationBar.barTintColor = UIColor(red: 43/255, green: 40/255, blue: 83/255, alpha: 1.0)
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @objc func goBack() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kBACK_CLICKED)
        
        Global.sharedInstance.goBack(self)
    }
    
}
