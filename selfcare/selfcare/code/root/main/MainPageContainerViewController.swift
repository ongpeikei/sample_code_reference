//
//  MainPageContainerViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 01/06/2017.
//
//

import UIKit
import KYDrawerController
import KCFloatingActionButton
import Fabric

class MainPageContainerViewController: UIViewController {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var fabMenu: KCFloatingActionButton!
    
    var quicktips: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // don't move this, it'll make the tabbar BLANK
        updateNavBarUI()
        prepareFAB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kMAIN)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kMAIN)
        
        if UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.FIRST_INSTALL) == false {
            UserDefaults.standard.set(true, forKey: Global.USERDEFAULTS_KEY.FIRST_INSTALL)
            UserDefaults.standard.synchronize()
            
            showQuickTips()
        }
    }
    
    @objc func openMenu(_ sender: Any) {
        let drawerController = navigationController?.parent as? KYDrawerController
            drawerController?.setDrawerState(.opened, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func updateNavBarUI() {
        // don't remove this drawerController manipulation (this solve the issue on drawer UI)
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(.opened, animated: false)
        drawerController?.setDrawerState(.closed, animated: false)
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_menu"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 11, width: 22, height: 22)
        back.addTarget(self, action: #selector(self.openMenu), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 200, height: 24))
        label.text = "selfcare"
        label.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        label.textAlignment = NSTextAlignment.left
        label.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftView
        
        // to apply same color contrast and remove the nav 1px bottom line
        navigationController?.navigationBar.barTintColor = globalInstance.getColorAttribute(.kPRIMARY, .DEFAULT)
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    fileprivate func prepareFAB() {
        fabMenu.buttonColor = globalInstance.getColorAttribute(.kACCENT, .DEFAULT)!
        
        let itemChat = KCFloatingActionButtonItem()
        itemChat.buttonColor = globalInstance.getColorAttribute(.kYELLOW, .DEFAULT)!
        itemChat.iconImageView.image = UIImage(named: "ic_livechat")
        itemChat.iconImageView.frame = CGRect(x: 11, y: 11, width: 20, height: 20)
        itemChat.iconImageView.tintColor = UIColor.white
        fabMenu.addItem(item: itemChat)
        
        let itemMenu = KCFloatingActionButtonItem()
        itemMenu.buttonColor = globalInstance.getColorAttribute(.kYELLOW, .DEFAULT)!
        itemMenu.iconImageView.image = UIImage(named: "ic_menu")
        itemMenu.iconImageView.frame = CGRect(x: 11, y: 11, width: 20, height: 20)
        fabMenu.addItem(item: itemMenu)
        
        itemChat.handler = { (itemChat) in
            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kFAB_LIVECHAT_CLICKED)
            FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kFAB_LIVECHAT_CLICKED)
            
            self.showChat()
        }
        
        itemMenu.handler = { (itemMenu) in
            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kFAB_MENU_CLICKED)
            FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kFAB_MENU_CLICKED)
            
            self.showMenu()
        }
    }
    
    fileprivate func showChat() {
        globalInstance.setIsFromDrawer(true)
        self.performSegue(withIdentifier: SHOW_LIVE_CHAT, sender: self)
    }
    
    fileprivate func showMenu() {
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(.opened, animated: true)
    }
    
    fileprivate func showQuickTips() {
        // to display quick tips once (ONLY)
        var topController = UIApplication.shared.keyWindow?.rootViewController
        if topController == UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController?.presentedViewController {
                topController = presentedViewController
            }
        }
        
        quicktips = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: (topController?.view.frame.size.height)!))
        quicktips.image = UIImage(named:"overlay-quicktips")
        quicktips.contentMode = UIViewContentMode.scaleToFill
        topController?.view.addSubview(quicktips)
        
        let dismissQuickTips = UITapGestureRecognizer(target: self, action: #selector(dismissImageView(gestureRecognizer:)))
        // Add the UITapGestureRecognizer to the image view
        quicktips.isUserInteractionEnabled = true
        quicktips.addGestureRecognizer(dismissQuickTips)
    }
    
   @objc func dismissImageView(gestureRecognizer: UITapGestureRecognizer) {
        // Remove the image view
        quicktips.removeFromSuperview()
    }

}
