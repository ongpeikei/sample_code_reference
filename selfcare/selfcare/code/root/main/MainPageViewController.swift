//
//  MainPageViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 04/05/2017.
//
//

import UIKit
import XLPagerTabStrip

class MainPageViewController: ButtonBarPagerTabStripViewController {
    let globalInstance = Global.sharedInstance
    var delegateLine: LineDelegator!
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        var mainPageArr:Array = [AnyObject]()
        let lineCount = CustomerDetailsModel.sharedInstance.getAccountLines()
        
        mainPageArr.append(globalInstance.initViewController(Global.VIEW_TYPE.kDASHBOARD))
    
        if (lineCount.count > 0) {
            for (index, _) in lineCount.enumerated() {
                let line: LineViewController = globalInstance.initViewController(Global.VIEW_TYPE.kLINE) as! LineViewController
                
                let lineAlias = CustomerDetailsModel.sharedInstance.getAccountLines()[index].lineAlias
                line.itemInfo.title = lineAlias
                line.lineNumber = index
                mainPageArr.append(line)
            }
        }
        
        return mainPageArr as! [UIViewController]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        buttonBarView.backgroundColor = globalInstance.getColorAttribute(.kPRIMARY, .DEFAULT)
        buttonBarView.selectedBar.backgroundColor = globalInstance.getColorAttribute(.kACCENT, .DEFAULT)
    }
    
}
