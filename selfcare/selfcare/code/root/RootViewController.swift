//
//  RootViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 03/05/2017.
//
//

import UIKit
import Alamofire
import Locksmith
import Fabric
import Crashlytics

let UNWIND_TO_ROOT = "UNWIND_TO_ROOT"

let ROOT_TO_LOGIN = "ROOT_TO_LOGIN"
let ROOT_TO_MAIN = "ROOT_TO_MAIN"
let ROOT_TO_WALKTHROUGH = "ROOT_TO_WALKTHROUGH"

let DRAWER_SWITCH_PRODUCT = "DRAWER_SWITCH_PRODUCT"
let DRAWER_SWITCH_ACCOUNT = "DRAWER_SWITCH_ACCOUNT"
let DRAWER_WHATS_NEW = "DRAWER_WHATS_NEW"
let DRAWER_PROFILE = "DRAWER_PROFILE"
let DRAWER_LIVE_CHAT = "DRAWER_LIVE_CHAT"
let DRAWER_HISTORY = "DRAWER_HISTORY"
let DRAWER_SUPPORT = "DRAWER_SUPPORT"
let DRAWER_SETTINGS = "DRAWER_SETTINGS"
let DRAWER_NOTIFICATIONS = "DRAWER_NOTIFICATIONS"
let DRAWER_ORDERS = "DRAWER_ORDERS"

let SHOW_EDIT_ADDON = "SHOW_EDIT_ADDON"
let SHOW_CHARGES_BREAKDOWN = "SHOW_CHARGES_BREAKDOWN"
let SHOW_UNBILLED_USAGE = "SHOW_UNBILLED_USAGE"
let SHOW_LIVE_CHAT = "SHOW_LIVE_CHAT"
let SHOW_PAYMENT = "SHOW_PAYMENT"

let CHECKER_FCM_TOKEN =  "fcmToken"

let DELAY = 0.0 // change 2 to desired number of seconds

var didUnwind = false

protocol RootDelegator {
    func callLoginFromDialog()
    func callRootMethodFromDialog()
}

class RootViewController: UIViewController, RootDelegator {
    let globalInstance = Global.sharedInstance
    let customerDetailsModel = CustomerDetailsModel.sharedInstance
    let model = DashboardModel.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gif = UIImage.gifImageWithName("bg_gif")
        let imgGIF = UIImageView(image: gif)
        imgGIF.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        view.addSubview(imgGIF)
        view.sendSubview(toBack:  imgGIF)
        
        let staticBG = UIImage(named: "bg_splash_static")
        let imgBG = UIImageView(image: staticBG)
        imgBG.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        view.addSubview(imgBG)
        view.sendSubview(toBack:  imgBG)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Global.pageType = Global.VIEW_TYPE.kDEFAULT
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kROOT)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kROOT)
        
        if didUnwind { // reset to default value
            didUnwind = false
        } else {
            if globalInstance.checkUserCredentials() {
                callCustomerDetailsApi()
            } else {
                showLoginView()
            }
        }
    }
    
    @IBAction private func unwindToRoot(segue: UIStoryboardSegue) {
        didUnwind = true
        
        if segue.source is LoginViewController {
            print("LOGIN")
            
            showLoginView()
        } else if segue.source is PasswordViewController {
            print("WILL TRY LOGIN")
            
            if globalInstance.getIsOffline() {
                callCustomerDetailsApi()
            } else {
                if globalInstance.checkUserCredentials() {
                    callCustomerDetailsApi()
                } else {
                    showLoginView()
                }
            }
        } else if segue.source is SwitchAccountViewController {
            print("SWITCH")
            
            showMainView()
        } else if segue.source is DrawerMenuViewController { // ACTION: this will logout
            print("LOGOUT")
            
            showLoginView()
        }
    }
    
    // this is required when using custom unwind
    /*override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = CustomUnwind(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }*/
    
    @IBAction private func loginButton(_ sender: Any) {
        showLoginView()
    }

    @IBAction private func mainButton(_ sender: Any) {
        showMainView()
    }
    
    private func showLoginView() {
        // turn off push notification
        PushNotification.sharedInstance.setPushNotifEnable(false)
        PushNotification.sharedInstance.clearCache()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + DELAY) {
            self.performSegue(withIdentifier: ROOT_TO_LOGIN, sender: self)
        }
    }
    
    private func showMainView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + DELAY) {
            self.performSegue(withIdentifier: ROOT_TO_MAIN, sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callCustomerDetailsApi() {
        if globalInstance.getIsOffline() {
            let didSaveFailed: Bool = customerDetailsModel.setCustomerDetailsResponse(DummyResponse.customerDetails())
            
            if !didSaveFailed {
                self.showMainView()
            }
        } else {
            //var credentials = Locksmith.loadDataForUserAccount(userAccount: Global.LOCKSMITH_KEY.CREDENTIALS)
            let username = UserDefaults.standard.object(forKey: Global.LOCKSMITH_KEY.EMAIL)  as! String
            
            if Global.webServiceMngr.isOffline() {
                self.logUser(CustomerDetailsModel.sharedInstance.getEmail(), CustomerDetailsModel.sharedInstance.getCustId())
                
                // this should not call here, we should call the "self.callRequestPushToken()" first
                showMainView()
            } else {
                if customerDetailsModel.getCustomerDetails() == nil || LoginModel.isTokenExpired() || !globalInstance.getIsCustomerDetailsApiTokenRefreshed() {
                    customerDetailsModel.prepareCustomerDetailsParams(username)
                    Global.webServiceMngr.makeRequest(customerDetailsModel.getCustomerDetailsParams()) { (isResponseDidFail, isServerMaintenance) in
                        if isServerMaintenance { // to stop the process and avoid unexpected crashing
                            self.globalInstance.dismissHUD()
                            
                            return
                        }
                        
                        if isResponseDidFail { // if fail do something here
                            self.globalInstance.dismissHUD()
                            
                            var dialogType:CustomDialogView.DIALOG = .GENERAL_ERROR
                            
                            if Global.pageType == .kEMAIL {
                                // if network fails, this should be the dialog type
                                if Global.webServiceMngr.isOffline() {
                                    dialogType = .CONNECTION_ERROR
                                }
                            }
                            
                            // add some delay to wait for some transitions
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                let dialog = CustomDialogView(type: dialogType)
                                dialog.delegateRoot = self
                                dialog.show(animated: true)
                                
                                return
                            }
                        } else {
                            self.logUser(CustomerDetailsModel.sharedInstance.getEmail(), CustomerDetailsModel.sharedInstance.getCustObjId()!)
                            
                            // set this to true since the API of customerDetails is SUCCESS
                            self.globalInstance.setIsCustomerDetailsApiTokenRefreshed(true)
                            
                            self.callRequestPushToken()
                        }
                    }
                } else {
                    self.logUser(CustomerDetailsModel.sharedInstance.getEmail(), CustomerDetailsModel.sharedInstance.getCustId())
                    
                    // this should not call here, we should call the "self.callRequestPushToken()" first
                    showMainView()
                }
            }
        }
    }
    
    private func callRequestPushToken() {
        PushNotification.sharedInstance.handlePushTokenAPICall(.kLOGIN, responseHandler: { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                var dialogType:CustomDialogView.DIALOG = .GENERAL_ERROR
                
                if Global.pageType == .kEMAIL {
                    // if network fails, this should be the dialog type
                    if Global.webServiceMngr.isOffline() {
                        dialogType = .CONNECTION_ERROR
                    }
                }
                
                // add some delay to wait for some transitions
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    let dialog = CustomDialogView(type: dialogType)
                    dialog.delegateRoot = self
                    dialog.show(animated: true)
                    
                    return
                }
            } else {
                self.showMainView()
            }
        })
    }
    
    func callLoginFromDialog() {
        self.showLoginView()
    }
    
    func callRootMethodFromDialog() {
        if CustomerDetailsModel.sharedInstance.getCustomerDetails() == nil {
            self.showLoginView()
        } else {
            self.showMainView()
        }
    }
    
    func logUser(_ username: String, _ customerID: String) {
        Crashlytics.sharedInstance().setUserName(username)
        Crashlytics.sharedInstance().setUserIdentifier(customerID)
    }

}

