//
//  PasswordViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 29/06/2017.
//
//

import UIKit
import Locksmith
import Alamofire
import SVProgressHUD
import Fabric

protocol PasswordDelegator {
    func updateUI()
    func callDismissToRootFromDialog()
}

class PasswordViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, PasswordDelegator {
    
    @IBOutlet var vKeyboardToolbar: UIView!
    
    @IBOutlet weak var lblPasswordTop: UILabel!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var lblPasswordErrorMsg: UILabel!
    
    let globalInstance = Global.sharedInstance
    let loginModel = LoginModel.sharedInstance
    
    //Password Labels
    let PASSWORD_PLACEHOLDER = "password"
    let EMPTY_PASSWORD = "input password"
    let INVALID_PASSWORD = "invalid password"
    let VALIDATION_PASS = "CALL LOGIN API"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.willShowKeyboard), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didShowKeyboard), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        updateNavBarUI()
        prepareLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Global.pageType = Global.VIEW_TYPE.kPASSWORD
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kPASSWORD)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kPASSWORD)
        updateUI()

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        lblPasswordErrorMsg.sizeToFit()
    }
    
    private func prepareLayout() {
        lblPasswordTop.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblPasswordTop.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_54)
        
        tfPassword.delegate = self
        tfPassword.autocorrectionType = .no
        tfPassword.inputAccessoryView = vKeyboardToolbar
        
        btnForgotPassword.titleLabel?.font = globalInstance.getFontAttribute(.kBOLD, .kMEDIUM)
        btnForgotPassword.setTitleColor(globalInstance.getColorAttribute(.kBLACK, .OPACITY_34), for: .normal)
        
        lblPasswordErrorMsg.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
        lblPasswordErrorMsg.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
    }
    
    private func updateNavBarUI() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 11, width: 22, height: 22)
        back.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 200, height: 24))
        label.text = "sign in"
        label.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        label.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftView
        
        // to apply same color contrast and remove the nav 1px bottom line
        navigationController?.navigationBar.barTintColor = UIColor(red: 43/255, green: 40/255, blue: 83/255, alpha: 1.0)
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    //MARK: - TEXTFIELD DELEGATE
    internal func textFieldDidBeginEditing(_ textField: UITextField){
        textField.placeholder = nil
        
        if textField .isEqual(tfPassword) {
            lblPasswordErrorMsg.text = CLEAR_TEXT
        }
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(tfPassword) {
            lblPasswordErrorMsg.text = CLEAR_TEXT
        }
        
        return true
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        guard
            textField.text?.isEmpty == true
            else {
                
                return
        }
        
        if textField .isEqual(tfPassword){
            tfPassword.placeholder = PASSWORD_PLACEHOLDER
        }
    }
    
    @objc func willShowKeyboard(_ notification: Notification) {
        UIView.setAnimationsEnabled(false) //cancel animation
    }
    
    @objc func didShowKeyboard(_ notification: Notification) {
        UIView.setAnimationsEnabled(false) //cancel animation
    }
    
    //MARK: - Actions
    @IBAction func tappedForgotPassword(_ sender: Any) {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kFORGOT_PASSWORD_CLICKED)
        FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kFORGOT_PASSWORD_CLICKED)
        
        forgotPassword()
    }
    
    @IBAction func tappedLogin(_ sender: UIButton) {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kLOGIN_BUTTON_TAPPED)
        
        if globalInstance.getIsOffline() {
            dismissToRoot()
            
            // set offline credentials from dummy
            let credentials = Locksmith.loadDataForUserAccount(userAccount: Global.LOCKSMITH_KEY.CREDENTIALS)
            
            if credentials == nil {
                //save when no data is available
                    try? Locksmith.saveData(data: [Global.LOCKSMITH_KEY.EMAIL: CustomerDetailsModel.sharedInstance.getEmail(),
                                                   Global.LOCKSMITH_KEY.PASSWORD: Global.LOCKSMITH_KEY.PASSWORD],
                                            forUserAccount: Global.LOCKSMITH_KEY.CREDENTIALS)
            }
            
            try? Locksmith.updateData(data: [Global.LOCKSMITH_KEY.EMAIL: CustomerDetailsModel.sharedInstance.getEmail(),
                                             Global.LOCKSMITH_KEY.PASSWORD: Global.LOCKSMITH_KEY.PASSWORD],
                                      forUserAccount: Global.LOCKSMITH_KEY.CREDENTIALS)
            
        } else {
            let password = tfPassword.text
            
            guard password?.isEmpty == false else {
                lblPasswordErrorMsg.text = EMPTY_PASSWORD
                
                return
            }
            
            callLoginAPI()
        }
    }
    
    //MARK: - Functions
    fileprivate func forgotPassword() {
        let dialog = CustomDialogView(type: .INPUT_FORGOT_PASSWORD)
        dialog.delegatePassword = self
        dialog.show(animated: true)
    }
    
    func callLoginAPI() {
        //var credentials = Locksmith.loadDataForUserAccount(userAccount: Global.LOCKSMITH_KEY.CREDENTIALS)
        let username = UserDefaults.standard.object(forKey: Global.LOCKSMITH_KEY.EMAIL)  as! String
    
        tfPassword.resignFirstResponder()
        
        // update HUD Layout
        globalInstance.showHUD()
        
        loginModel.prepareLoginParams(username, tfPassword.text!)
        Global.webServiceMngr.makeRequest(loginModel.getRequestLoginParams()) { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                if self.tfPassword.canBecomeFirstResponder {
                    self.tfPassword.becomeFirstResponder()
                }
                
                // if network fails, this should show
                if Global.webServiceMngr.isOffline() {
                    self.lblPasswordErrorMsg.text = NO_CONNECTION
                    
                    return
                }
                
                // don't change the location of this code since it should cater only if the password is WRONG
                // or anykind of error except NO INTERNET or SERVER MAINTENANCE
                FabricAnalytics.sharedInstance.answersAnalyticsLogin(false)
                
                // if specific error, this should show
                if self.globalInstance.getShouldShowSeperateError() {
                    self.lblPasswordErrorMsg.text = self.INVALID_PASSWORD
                    
                    return
                }
                
                // by default this is the action
                self.globalInstance.showGeneralError()
            } else {
                FabricAnalytics.sharedInstance.answersAnalyticsLogin(true)
                
                PushNotification.sharedInstance.setPushNotifEnable(true)
                PushNotification.sharedInstance.setIsUserFromLogin(true)
                
                self.savePassword()
                self.dismissToRoot()
            }
        }
    }
    
    private func savePassword() {
        UserDefaults.standard.set(Global.LOCKSMITH_KEY.PASSWORD, forKey: Global.LOCKSMITH_KEY.PASSWORD)
        UserDefaults.standard.synchronize()
    }
    
    @objc func goBack() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kBACK_CLICKED)
        
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromLeft
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    private func dismissToRoot() {
        self.performSegue(withIdentifier: UNWIND_TO_ROOT, sender: self)
        UIView.setAnimationsEnabled(true)
    }
    
// MARK: DELEGATES
    func updateUI() {
        if tfPassword.canBecomeFirstResponder {
            tfPassword.becomeFirstResponder()
        }
    }
    
    func callDismissToRootFromDialog() {
        dismissToRoot()
    }
    
}
