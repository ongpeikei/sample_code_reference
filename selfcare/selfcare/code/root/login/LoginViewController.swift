//
//  LoginViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 03/05/2017.
//
//

import UIKit
import Alamofire
import Locksmith
import SVProgressHUD
import Fabric
import Crashlytics

//Login Labels
let EMAIL_PLACEHOLDER = "email address"
let TOP_LABEL_SIGNIN = "sign in"
let TOP_LABEL_EMAIL = "email address"
let CLEAR_TEXT = ""

//Login Error Message
let EMPTY_EMAIL = "input email address"
let INVALID_EMAIL = "invalid email address"
let EMPTY_PASSWORD = "input password"
let NO_CONNECTION = "Please check your internet connection and let's get linked again."
let EMPTY_NRIC = "input valid NRIC"
let EMPTY_PASSPORT = "input valid passport number"
let INVALID_NRIC = "invalid NRIC"
let INVALID_PASSPORT = "invalid passport number"
let INVALID_INPUT = "invalid input, no special characters allowed"
let EXCEEDED_AMOUNT = "exceeded maximum limit amount"

//Login Animation Settings
let ANIMATION_DURATION = 0.3
let ANIMATION_DELAY = 0
let TRANSITION_DURATION = 0.5

let ALPHA_TO_HIDE = CGFloat(0.3)
let ALPHA_TO_SHOW = CGFloat(1.0)

//Login CollectionView Configuration
var cellWidth: CGFloat = 0.0
let COLLECTIONVIEW_CELL_ID = "LOGIN_CELL"

//Login Segue
let LOGIN_TO_PASSWORD = "LOGIN_TO_PASSWORD"

protocol LoginDelegator {
    func updateUI()
    func callDismissToRootFromDialog()
}

class LoginViewController: UIViewController, UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, LoginDelegator {

    @IBOutlet internal weak var passwordTextField: UITextField!
    
    @IBOutlet weak var vLoginCollectionViewHolder: UIView!
    @IBOutlet weak var cvLogin: UICollectionView!
    @IBOutlet weak var pcLoginPagerIndicator: UIPageControl!
    
    @IBOutlet weak var navItemBar: UINavigationBar!
    
    //@IBOutlet weak var vStatusBar: UIView!
    @IBOutlet weak var lblEmailTopText: UILabel!
    @IBOutlet internal weak var tfEmail: UITextField!
    
    @IBOutlet weak var btnForgot: UIButton!
    @IBOutlet internal weak var lblEmailErrorMsg: UILabel!
    
    @IBOutlet weak var vSocialButtonHolder: UIView!
    
    @IBOutlet var vKeyboardToolbar: UIView!
    
    @IBOutlet weak var lblVersion: UILabel!
    
    let globalInstance = Global.sharedInstance
    let logInModel = LoginModel.sharedInstance
    
    var items:Array = [AnyObject]()
    var infiniteItems:Array = [AnyObject]()
    
    var executeOnce = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        cvLogin.register(UINib(nibName: "LoginCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: COLLECTIONVIEW_CELL_ID)
        
        items = getCellDataLogin()
        
        // to create infinite loop
        infiniteItems = items
        infiniteItems.append(items[0])
        infiniteItems.insert(items[3], at: 0)
        
        pcLoginPagerIndicator.isUserInteractionEnabled = false
        
        executeOnce = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)

        updateUI()
        prepareLayout()
        updateNavBarUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Global.pageType = Global.VIEW_TYPE.kEMAIL
        
        FabricAnalytics.sharedInstance.crashlyticsScreen(FabricAnalytics.SCREEN_KEY.kLOGIN)
        FabricAnalytics.sharedInstance.answersScreen(FabricAnalytics.SCREEN_KEY.kLOGIN)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        lblEmailErrorMsg.sizeToFit()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return infiniteItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTIONVIEW_CELL_ID, for: indexPath) as! LoginCollectionViewCell
        
        let index = indexPath.row
        
        cell.imgIcon.image = UIImage(named: (infiniteItems[index]["image"] as? String)!)
        cell.lblTitle.text = (infiniteItems[index]["title"] as? String)!
        cell.lblDescription.text = (infiniteItems[index]["description"] as? String)!
        
        // TODO: this is the expected behaviour
        if executeOnce {
            if index == 0 {
                executeOnce = false
                
                let newIndexPath = IndexPath(row: 1, section: 0)
                cvLogin.scrollToItem(at: newIndexPath, at: .centeredHorizontally, animated: false)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updateLoginPager()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateLoginPager()
    }
    
    //MARK: - TextField Delegates
    internal func textFieldDidBeginEditing(_ textField: UITextField){
        textField.placeholder = nil
        if navItemBar.isHidden { //check if navItem is hidden
            clickNextAnimations()
        } else {
            return
        }
        if textField .isEqual(tfEmail) {
            lblEmailErrorMsg.text = CLEAR_TEXT
        }
        lblEmailTopText.text = TOP_LABEL_EMAIL
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField .isEqual(tfEmail) {
            lblEmailErrorMsg.text = CLEAR_TEXT
        }
        
        return true
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        guard
            textField.text?.isEmpty == true else {
                return
        }
        
        if textField .isEqual(tfEmail) {
            lblEmailErrorMsg.text = CLEAR_TEXT
            
            tfEmail.placeholder = EMAIL_PLACEHOLDER //Check how to customize
        }
        else {
            return
        }
    }
    
    //MARK: - Actions
    @IBAction private func clickedNext(_ sender: Any) {
        if globalInstance.getIsOffline() {
            goToPasswordView()
        } else {
            validateEmail()
        }
    }
    
    @IBAction func tappedForgotEmail(_ sender: Any) {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kFORGOT_EMAIL_CLICKED)
        FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kFORGOT_EMAIL_CLICKED)
        
        forgotEmail()
    }
    
    @IBAction func tappedFBLogin(_ sender: Any) {
        facebookLogin()
    }
    
    @IBAction func tappedGoogleLogin(_ sender: Any) {
        googleLogin()
    }
    
    //MARK: - Functions
    @objc func clickedCancel(_ sender: Any) {
        cancelEmailInput()
    }
    
    func updateLoginPager() {
        var currentPage = pcLoginPagerIndicator.currentPage
        var newIndexPath:IndexPath
        
        let numberOfCells = infiniteItems.count
        
        let pageWidth = cvLogin.frame.size.width;
        let page = cvLogin.contentOffset.x / pageWidth;
        if page == 0 { // we are within the fake last, so delegate real last
            currentPage = numberOfCells - 1
            newIndexPath = IndexPath(row: currentPage - 1, section: 0) // -1 because the last one is the fake first object
            
            cvLogin.scrollToItem(at: newIndexPath, at: .centeredHorizontally, animated: false)
        } else if page == CGFloat(numberOfCells - 1) { // we are within the fake first, so delegate the real first
            currentPage = 0
            newIndexPath = IndexPath(row: currentPage + 1, section: 0) // +1 because the first one is the fake last object
            
            cvLogin.scrollToItem(at: newIndexPath, at: .centeredHorizontally, animated: false)
        } else { // real page is always fake minus one
            currentPage = Int(CGFloat(page - 1))
        }
        
        pcLoginPagerIndicator.currentPage = currentPage
    }
    
    func prepareLayout(){
        lblEmailTopText.text = TOP_LABEL_SIGNIN
        lblEmailTopText.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblEmailTopText.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_54)
        
        tfEmail.delegate = self
        tfEmail.autocorrectionType = .no
        tfEmail.inputAccessoryView = vKeyboardToolbar
        
        btnForgot.titleLabel?.font = globalInstance.getFontAttribute(.kBOLD, .kMEDIUM)
        btnForgot.setTitleColor(globalInstance.getColorAttribute(.kBLACK, .OPACITY_34), for: .normal)
        
        lblEmailErrorMsg.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
        lblEmailErrorMsg.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
        
        let versionObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        let version = versionObject as! String
        lblVersion.text = "v " + version
        lblVersion.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL)
        lblVersion.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_54)
        
        configurePageControl()
    }
    
    func configurePageControl(){
        cvLogin.dataSource = self
        cvLogin.delegate = self
        pcLoginPagerIndicator.numberOfPages = items.count
        cellWidth = CGFloat(cvLogin.frame.width)
    }
    
    private func updateNavBarUI() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        
        let back = UIButton(type: .custom) as UIButton
        back.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        back.frame = CGRect(x: 0, y: 11, width: 22, height: 22)
        back.addTarget(self, action: #selector(clickedCancel(_:)), for: UIControlEvents.touchUpInside)
        customView.addSubview(back)
        
        let marginX = CGFloat(back.frame.origin.x + back.frame.size.width + 16)
        let marginY = CGFloat((customView.frame.height/2) - 24/2)
        let label = UILabel(frame: CGRect(x: marginX, y: marginY, width: 200, height: 24))
        label.text = "sign in"
        label.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        label.textColor = globalInstance.getColorAttribute(.kWHITE, .DEFAULT)
        label.textAlignment = NSTextAlignment.left
        customView.addSubview(label)
        
        let leftView = UIBarButtonItem(customView: customView)
        let navItem = UINavigationItem(title: "")
        navItem.leftBarButtonItem = leftView;
        navItemBar.setItems([navItem], animated: false)
        
        // to apply same color contrast and remove the nav 1px bottom line
        navItemBar.barTintColor = globalInstance.getColorAttribute(.kPRIMARY, .DEFAULT)
        navItemBar.isTranslucent = false
        navItemBar.setBackgroundImage(UIImage(), for: .default)
        navItemBar.shadowImage = UIImage()
    }
    
    private func cancelEmailInput() {
        clickCancelAnimations()
        lblEmailTopText.text = TOP_LABEL_SIGNIN
        
        tfEmail.text = CLEAR_TEXT
        tfEmail.placeholder = EMAIL_PLACEHOLDER
        tfEmail.resignFirstResponder()
        lblEmailErrorMsg.text = CLEAR_TEXT
    }
    
    private func clickCancelAnimations(){
        UIView.setAnimationsEnabled(true)
        UIView.animate(withDuration: ANIMATION_DURATION, delay: TimeInterval(ANIMATION_DELAY), options: [.curveEaseIn],
                       animations: {
                        self.vLoginCollectionViewHolder.isHidden = false
                        self.cvLogin.alpha = ALPHA_TO_SHOW
                        
                        self.navItemBar.isHidden = true
                        self.navItemBar.alpha = ALPHA_TO_HIDE
                        
                        self.vSocialButtonHolder.isHidden = false
        }, completion : nil )
    }
    
    private func forgotEmail() {
        tfEmail.resignFirstResponder()
        
        let dialog = CustomDialogView(type: .SELECTION_FORGOT_EMAIL)
        dialog.delegateLogin = self
        dialog.show(animated: true)
    }
    
    private func facebookLogin() {
        print ("Facebook Login button is Tapped")
    }
    
    private func googleLogin() {
        print ("Google Login button is Tapped")
    }
    
    private func validateEmail() {
        let username = tfEmail.text
        
        guard username?.isEmpty == false else {
            lblEmailErrorMsg.text = EMPTY_EMAIL
            return
        }
        
        guard globalInstance.isValidEmail(testStr: tfEmail.text!)else {
            lblEmailErrorMsg.text = INVALID_EMAIL
            return
        }
        
        callValidateCredentials(username!)
    }
    
    private func callValidateCredentials(_ username: String) {
        tfEmail.resignFirstResponder()
        
        // update HUD Layout
        globalInstance.showHUD()
        
        globalInstance.removeKey(Global.SELECTED_ACCOUNT_NO)
        globalInstance.removeKey(Global.SELECTED_PRODUCT_TYPE)
        
        logInModel.prepareUsernameVerifyParams(username)
        Global.webServiceMngr.makeRequest(logInModel.getRequestUsernameVerifyParams()) { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                if self.tfEmail.canBecomeFirstResponder {
                    self.tfEmail.becomeFirstResponder()
                }
                
                // if network fails, this should show
                if Global.webServiceMngr.isOffline() {
                    self.lblEmailErrorMsg.text = NO_CONNECTION
                    
                    return
                }
                
                // if specific error, this should show
                if self.globalInstance.getShouldShowSeperateError() {
                    self.lblEmailErrorMsg.text = INVALID_EMAIL
                    
                    return
                }
                
                // by default this is the action
                self.globalInstance.showGeneralError()
            } else {
                self.saveEmail()
                
                self.goToPasswordView()
                
                self.globalInstance.dismissHUD()
            }
        }
    }
    
    fileprivate func saveEmail(){
        UserDefaults.standard.set(self.tfEmail.text, forKey: Global.LOCKSMITH_KEY.EMAIL)
        UserDefaults.standard.synchronize()
    }
    
    /*fileprivate func saveEmail() {
        var credentials = Locksmith.loadDataForUserAccount(userAccount: Global.LOCKSMITH_KEY.CREDENTIALS)
        
        guard (credentials?["email"] != nil) else {
            //save when no data is available
            try? Locksmith.saveData(data: [Global.LOCKSMITH_KEY.EMAIL: self.tfEmail.text!], forUserAccount: Global.LOCKSMITH_KEY.CREDENTIALS)
            return
        }
        try? Locksmith.updateData(data: [Global.LOCKSMITH_KEY.EMAIL: self.tfEmail.text!], forUserAccount: Global.LOCKSMITH_KEY.CREDENTIALS)
    }*/
    
    private func clickNextAnimations(){
        UIView.setAnimationsEnabled(true)
        UIView.animate(withDuration: ANIMATION_DURATION, delay: TimeInterval(ANIMATION_DELAY), options: [.curveEaseInOut],
                       animations: {
                        self.vLoginCollectionViewHolder.isHidden = true
                        self.cvLogin.alpha = ALPHA_TO_HIDE
                        
                        self.navItemBar.isHidden = false
                        self.navItemBar.alpha = ALPHA_TO_SHOW
                        
                        self.vSocialButtonHolder.isHidden = true
        }, completion : nil )
    }
    
    private func goToPasswordView() {
        let transition: CATransition = CATransition()
        transition.duration = TRANSITION_DURATION
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromRight
        self.view.window!.layer.add(transition, forKey: nil)
        self.performSegue(withIdentifier: LOGIN_TO_PASSWORD, sender: self)
    }
    
    func getCellDataLogin() -> [AnyObject] {
        let data = [["image": "logo_live_chat", "title": "live chat", "description": "chat with our agents 24/7, 7 days a week."],
                    ["image": "logo_on_the_go", "title": "on the go", "description": "check all your account info at your fingertips."],
                    ["image": "logo_make_payments", "title": "make payments", "description": "make payments whenever, wherever your are."],
                    ["image": "logo_history", "title": "history", "description": "view your purchases, status and more instantly."]]
        
        return data as [AnyObject]
    }
    
    private func dismissToRoot() {
        self.performSegue(withIdentifier: UNWIND_TO_ROOT, sender: self)
    }

// MARK: DELEGATE
    func updateUI(){
        if !(navItemBar.isHidden) { //check if navItem is not hidden
            if tfEmail.canBecomeFirstResponder {
                tfEmail.becomeFirstResponder()
            }
        }
    }
    
    func callDismissToRootFromDialog() {
        dismissToRoot()
    }
    
}
