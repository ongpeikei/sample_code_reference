//
//  MasterApiModel.swift
//  selfcare
//
//  Created by ONG PEI KEI on 17/07/2017.
//
//

import Foundation

class DashboardModel {
    static let sharedInstance = DashboardModel()
    
    struct CONSTANTS{
        static let RESPONSE = "response"
    }
    //REQUEST parameters
    fileprivate var acctCode : String = ""
    
    struct PARAM {
        static let kACCT_CODE = "acctCode"
    }
    
    static var accCode:String = ""
    static var appVer:String = ""
    static var source:String = ""
    
    // parameter response
    fileprivate var balanceList: UsageBalanceEntity? = nil
    fileprivate var billList:  BillDetailsEntity? = nil
    fileprivate var unbilledList: Array<UnbilledLineEntity>? = nil

    
    //Prepare request params
    func prepareDashboardApi(_ accCode: String){
        DashboardModel.accCode = accCode
    }
    
    func getDashboardApiRequestParams() -> [String:AnyObject] {
        let reqType = WebServiceManager.REQUEST.kDASHBOARD
        let reqParam:[String:AnyObject] = [PARAM.kACCT_CODE: DashboardModel.accCode as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_VERSION: StaticStrings.GENERAL_VALUE.APP_VERSION as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_SOURCE: StaticStrings.GENERAL_VALUE.APP_SOURCE as AnyObject] as [String:AnyObject]
        
        return [WebServiceManager.TAG.REQUEST_TYPE:reqType as AnyObject, WebServiceManager.TAG.REQUEST_PARAMS:reqParam as AnyObject]
    }
    
    func setDashboardApiResponse(_ response:[String: AnyObject]) -> Bool {
        guard
            (response[DashboardModel.CONSTANTS.RESPONSE] as? [String:AnyObject]) != nil else  {
                return true
        }
        
        DataFactory.sharedInstance.saveOutstandingBalance(response)
        
        return false
    }
    
    // OUTSTANDING BALANCE LIST
    func getOutstandingBalResponse() -> OutstandingBalanceDB? {
        return DataFactory.sharedInstance.getOutstandingBalance();
    }
    
    func getBalanceList() -> UsageBalanceEntity? {
        //guard
        //   (balanceList != nil) else {
                let outstandingBalResponse = getOutstandingBalResponse()
                if outstandingBalResponse != nil {
                        balanceList = (outstandingBalResponse?.balanceList!)!
                }
        
                return outstandingBalResponse?.balanceList
        //}
        //    return self.balanceList
    }
    
    // BILL LIST
    func getBillList() -> BillDetailsEntity? {
        //guard
        //    (billList != nil) else {
                let outstandingBalResponse = getOutstandingBalResponse()
                
                if outstandingBalResponse != nil {
                        billList = (outstandingBalResponse?.billList!)!
                }
        
                return billList != nil ? billList : BillDetailsEntity()
       // }
          //  return self.billList
        
    }

    //CREDIT UNBILLED API
    func getCreditUnbilledRequestParams() -> [String:AnyObject] {
        let reqType = WebServiceManager.REQUEST.kCREDIT_UNBILLED
        let reqParam:[String:AnyObject] = [PARAM.kACCT_CODE: DashboardModel.accCode as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_VERSION: StaticStrings.GENERAL_VALUE.APP_VERSION as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_SOURCE: StaticStrings.GENERAL_VALUE.APP_SOURCE as AnyObject] as [String:AnyObject]
        
        return [WebServiceManager.TAG.REQUEST_TYPE:reqType as AnyObject, WebServiceManager.TAG.REQUEST_PARAMS:reqParam as AnyObject]
    }
    
    func setCreditUnbilledResponse(_ response:[String: AnyObject]) -> Bool {
        guard
            (response[DashboardModel.CONSTANTS.RESPONSE] as? [String:AnyObject]) != nil else  {
                return true
            }
        
        DataFactory.sharedInstance.saveCreditUnbilledBalance(response)
    
        return false
    }
    
    func getCreditUnbilledBalanceResponse() -> CreditUnbilledDB? {
        return DataFactory.sharedInstance.getCreditUnbilledBalance()
    }
    
    func getUnbilledLines() -> [UnbilledLineEntity]? {
        let creditUnbilledBalResponse = getCreditUnbilledBalanceResponse()
        
        if creditUnbilledBalResponse != nil {
            unbilledList = (creditUnbilledBalResponse?.unbilledLinesList!)!
        }
        
        return unbilledList != nil ? unbilledList : [UnbilledLineEntity]()
    }
    
}
