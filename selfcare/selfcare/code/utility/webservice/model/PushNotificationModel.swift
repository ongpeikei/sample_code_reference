//
//  PushNotificationModel.swift
//  selfcare
//
//  Created by ONG PEI KEI on 03/08/2017.
//
//

import Foundation
import UIKit

class PushNotificationModel {
    static let sharedInstance = PushNotificationModel()
    
    // parameter request
    struct PARAM {
        static let kACCOUNT_CODE = "acctCode"
        static let kTOKEN = "token"
        static let kSOURCE = "source"
        static let kVERSION = "version"
        static let kNETWORK_TYPE = "networkType"
        static let kUUID = "uuid"
        static let kACTION = "action"
    }
    
    struct ACTION_TYPE {
        static let kENABLE = "1"
        static let kDISABLE = "0"
    }
    
    fileprivate var acctCode : String = ""
    fileprivate var token : String = ""
    fileprivate var source : String = ""
    fileprivate var version : String = ""
    fileprivate var networkType : String = ""
    fileprivate var uuid : String = UIDevice.current.identifierForVendor!.uuidString
    fileprivate var action : String = ""

    //Prepare params
    func preparePushTokenParams( _ acctCode: String, _ networkType: String, _ action: String){
        self.acctCode = acctCode
        self.token = PushNotification.sharedInstance.fetchFcmToken()!
        self.networkType = networkType
        self.action = action
    }
    
    func getPushTokenParams() -> [String:AnyObject] {
        let reqType = WebServiceManager.REQUEST.kPUSH_TOKEN
        let reqParam:[String:AnyObject] = [PARAM.kACCOUNT_CODE: acctCode,
                                           PARAM.kTOKEN: token,
                                           PARAM.kSOURCE: source,
                                           PARAM.kVERSION: version,
                                           PARAM.kNETWORK_TYPE: networkType,
                                           PARAM.kUUID: uuid,
                                           StaticStrings.GENERAL_KEY.kAPP_VERSION: StaticStrings.GENERAL_VALUE.APP_VERSION as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_SOURCE: StaticStrings.GENERAL_VALUE.APP_SOURCE as AnyObject,
                                           PARAM.kACTION: action as AnyObject] as [String:AnyObject]
        
        return [WebServiceManager.TAG.REQUEST_TYPE:reqType as AnyObject, WebServiceManager.TAG.REQUEST_PARAMS:reqParam as AnyObject]
    }
    
    func setPushTokenResponse(_ response:[String: AnyObject]) -> Bool {
        /*guard
            /* condition here */ else  {
                return false
        }*/ //update and remove comment when specs is ready
        
        // save in DB if needed
        
        return false
    }
    
    func getPushTokenResponse() { // modify return when specs is ready
        // return based on API specs
    }
    
}
