//
//  SwitchModel.swift
//  selfcare
//
//  Created by ONG PEI KEI on 19/07/2017.
//
//

import Foundation

class SwitchModel {
    
    public static func getProductTypes() -> [String] {
        return CustomerDetailsModel.sharedInstance.getProductTypes()
    }
    
    public static func getSelectedProductType() -> String? {
        checkProductType()
        
        return Global.sharedInstance.getSavedStringValue(Global.SELECTED_PRODUCT_TYPE)
    }
    
    public static func saveSelectedProductType(_ prodType: String) {
        //print("saving prodtype: " + prodType)
        Global.sharedInstance.saveStringValue(prodType, Global.SELECTED_PRODUCT_TYPE)
        //print("saved prodtype: " + prodType)
    }
    
    public static func getSelectedAccountNo() -> String? {
        checkAccountNo()
        return Global.sharedInstance.getSavedStringValue(Global.SELECTED_ACCOUNT_NO)
    }
    
    public static func saveSelectedAccountNo(_ prodType: String) {
        //print("saving acct no: " + prodType)
        Global.sharedInstance.saveStringValue(prodType, Global.SELECTED_ACCOUNT_NO)
        //print("saved acct no: " + prodType)
    }
    
    /**
     Verifies if there is selected product type saved in userdefault
     */
    private static func checkProductType() {
        if Global.sharedInstance.getSavedStringValue(Global.SELECTED_PRODUCT_TYPE) == nil {
            let prodTypes: [String] = getProductTypes()
            
            if prodTypes.count > 1 {
                for prodType in prodTypes {
                    if prodType == Global.PROD_TYPE.kMOBILE.rawValue {
                        saveSelectedProductType(prodType)
                        break
                    }
                }
            } else if (prodTypes.count == 1) {
                for prodType in prodTypes {
                    saveSelectedProductType(prodType)
                    break
                }
            }
        }
    }
    
    /**
     Verifies if there is selected account number saved in userdefault
     */
    private static func checkAccountNo() {
        if Global.sharedInstance.getSavedStringValue(Global.SELECTED_ACCOUNT_NO) == nil {
            let accountNumbers: [AccountNumberEntity] = CustomerDetailsModel.sharedInstance.getAccountsByProductType(getSelectedProductType()!)

            if accountNumbers.count > 0 {
                let accountNumber: AccountNumberEntity = accountNumbers[0]
                saveSelectedAccountNo(accountNumber.accountNo)
            }
        }
        
    }    
}
