//
//  SimDetailsModel.swift
//  selfcare
//
//  Created by ONG PEI KEI on 14/07/2017.
//
//

import Foundation

class SimDetailsModel {
    static let sharedInstance = SimDetailsModel()
    
    struct CONSTANTS {
        static let kRESPONSE = "response"
        
    }
    
    //REQUEST
    fileprivate var acctCode : String = ""
    fileprivate var imsi : String = ""
    fileprivate var iccid : String = ""
    
    fileprivate var providerId : String = "2"
    fileprivate var msisdn : String = ""
    
    //RESPONSE
    fileprivate var simDetailsResponse : [String : AnyObject]? = nil
    fileprivate var pin1 : String? = ""
    fileprivate var puk1 : String? = ""
    
    fileprivate var activationCode : String = ""
    
    fileprivate var responseBool : Bool = false
    
    //Parameters
    struct PARAM {
        static let kACCT_CODE = "acctCode"
        static let kIMSI = "imsi"
        static let kICCID = "iccid"
        
        static let kACTIVATION_CODE = "activationCode"
        static let kMSISDN = "msisdn"
        static let kPROVIDER_ID = "providerId"
    }
    
    // PREPARE parameter methods
    func prepareRequestAPNParams(_ acctCode:String, _ msisdn:String) {
        self.acctCode = acctCode
        self.msisdn = msisdn
    }
    
    func prepareTriggerAPNSettingsParams(_ acctCode:String, _ msisdn:String, _ activationCode:String) {
        self.acctCode = acctCode
        self.msisdn = msisdn
        self.activationCode = activationCode
    }

    func prepareSimDetailsParams(_ acctCode:String, _ imsi:String, _ iccid:String) {
        self.acctCode = acctCode
        self.imsi = imsi
        self.iccid = iccid
    }
    
    // GET parameters methods
    func getRequestAPNParams() -> [String: Any] {
        let reqType = WebServiceManager.REQUEST.kREQUEST_APN
        let reqParam:[String:AnyObject] = [PARAM.kACCT_CODE:acctCode as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_SOURCE: StaticStrings.GENERAL_VALUE.APP_SOURCE as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_VERSION: StaticStrings.GENERAL_VALUE.APP_VERSION as AnyObject,
                                           PARAM.kPROVIDER_ID:providerId as AnyObject,
                                           PARAM.kMSISDN:msisdn as AnyObject]
        
        return [WebServiceManager.TAG.REQUEST_TYPE:reqType, WebServiceManager.TAG.REQUEST_PARAMS:reqParam]
    }
    
    func getTriggerAPNSettingsParams() -> [String: Any] {
        let reqType = WebServiceManager.REQUEST.kTRIGGER_APN_SETTINGS
        let reqParam:[String:AnyObject] = [PARAM.kACCT_CODE:acctCode as AnyObject,
                                           PARAM.kMSISDN:msisdn as AnyObject,
                                           PARAM.kPROVIDER_ID:providerId as AnyObject,
                                           PARAM.kACTIVATION_CODE:activationCode as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_VERSION: StaticStrings.GENERAL_VALUE.APP_VERSION as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_SOURCE: StaticStrings.GENERAL_VALUE.APP_SOURCE as AnyObject]
        
        return [WebServiceManager.TAG.REQUEST_TYPE:reqType, WebServiceManager.TAG.REQUEST_PARAMS:reqParam]
    }
    
    func getSimDetailsParams() -> [String: Any] {
        let reqType = WebServiceManager.REQUEST.kSIM_DETAILS
        let reqParam:[String:AnyObject] = [PARAM.kACCT_CODE:acctCode as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_SOURCE: StaticStrings.GENERAL_VALUE.APP_SOURCE as AnyObject,
                                           StaticStrings.GENERAL_KEY.kAPP_VERSION: StaticStrings.GENERAL_VALUE.APP_VERSION as AnyObject,
                                           PARAM.kIMSI:imsi as AnyObject,
                                           PARAM.kICCID:iccid as AnyObject]
        
        return [WebServiceManager.TAG.REQUEST_TYPE:reqType, WebServiceManager.TAG.REQUEST_PARAMS:reqParam]
    }
    
    // SET response methods
    func setRequestAPNResponse(_ response:[String: AnyObject]) -> Bool {
        guard
            (response[SimDetailsModel.CONSTANTS.kRESPONSE] as? Bool) != nil else {
                
                return true // true means fail
        }
        
        if (responseBool == true) {
            print("SMS SENT")
        } else {
            print("SMS SENT FAILED")
        }
        
        return false
    }
    
    func setTriggerAPNSettingsResponse(_ response:[String: AnyObject]) -> Bool {
        guard
            (response[SimDetailsModel.CONSTANTS.kRESPONSE] as? [String : AnyObject]) != nil else {
            
                return true
        }
        
        if (responseBool == true) {
            print("APN SUCCESS !! ")
        } else {
            print ("APN FAILED !! ")
        }
            return false
    }
    
    func setSimDetailsResponse(_ response:[String: AnyObject]) -> Bool {
            guard
                (response[SimDetailsModel.CONSTANTS.kRESPONSE] as? [String:AnyObject]) != nil else {
        
                    return true // true means fail
            }
        
        DataFactory.sharedInstance.saveSimDetails(response) //<- Dummy Data
        
        return false
    }
    
    func getSimDetails(_ simByMsisdn: String) -> SimDetailsDB? {
        return DataFactory.sharedInstance.getSimDetails(simByMsisdn)
    }
    
    func getPin1(_ simByMsisdn: String) -> String? {
        pin1 = getSimDetails(simByMsisdn)?.pin1
        
        return pin1
    }
    
    func getPuk1(_ simByMsisdn: String) -> String? {
        puk1 = getSimDetails(simByMsisdn)?.puk1
        
        return puk1
    }
    
}
