//
//  WebServiceManager.swift
//  selfcare
//
//  Created by ONG PEI KEI on 05/05/2017.
//
//

import Foundation
import Alamofire

class WebServiceManager {
    static let sharedInstance = WebServiceManager()
    private var OAuthToken: String?
    
    static let LIVECHAT_URL = "<Sensitive_Data>"
    
    //STAGING
    var BASE_URL = "<Sensitive_Data>"
    var AUTH_BASE_URL = "<Sensitive_Data>"
    
    // TODO: supply value when OAuth 2.0 integration is done
    static let CLIENT_ID = " "
    static let CLIENT_SECRET = " "
    
    static let REDIRECT_URI = " "
    
    var errorMessage: AnyObject? = "" as AnyObject
    
    var isServerMaintenance: Bool = false
    
    var refreshCounter = 0
    var shouldPerformRefreshToken: Bool = false
    
    typealias CompletionBlock = (_ responseDidFail : Bool, _ isServerMaintenance : Bool) -> Void
    
    // Request type
    enum REQUEST {
        case kRETRIEVE_AUTH
        case kUSER_VERIFY
        case kREFRESH_TOKEN
        
        case kFORGOT_USERNAME
        case kFORGOT_PASSWORD
        
        case kPUSH_TOKEN
        
        case kCUSTOMER_DETAILS
        case kDASHBOARD
        case kSUBSCRIPTION
        case kADD_ON_BALANCE
        case kCREDIT_UNBILLED
        
        case kSERVICES
        case kPURCHASE_ADD_ON
        case kSUBSCRIBE_VAS
        case kSUBSCRIBE_VAS_IR
        case kIPAY_LINK
        case kPAYMENT_STATUS
        
        case kREQUEST_APN
        case kTRIGGER_APN_SETTINGS
        case kSIM_DETAILS
        
        case kHISTORY_BILL
        case kHISTORY_ADDON
        case kHISTORY_DCB
        
        case kCREDIT_LIMIT
        case kUPDATE_CREDIT_LIMIT
        case kRETRIEVE_GOOD_PAYMASTER
        
        case kCHAT_START
        case kCHAT_RECONNECT
        case kCHAT_POLL
        case kCHAT_SEND
        case kCHAT_EXIT
    }
    
    // Headers API key and value
    struct HEADER {
        /*
         
         Remove
         
         */
    }
    
    // Request Tag
    struct TAG {
        static let REQUEST_TYPE = "requestType"
        static let REQUEST_PARAMS = "params"
    }
    
    struct RESPONSE_KEY {
        static let CODE = "code"
        static let STATUS = "status"
        static let ERROR_MSG = "errorMsg"
    }
    
    struct RESPONSE_RESULT {
        static let FAILURE = "failure"
        static let FAILED = "Failed"
    }
    
    struct ERROR_CODE {
        /*
         
         Remove
         
         */"
    }
    
    // API end points
    struct ENDPOINT {
        /*
         
         Remove
         
         */
      
    }
    
    func isOffline() -> Bool {
        if (!(NetworkReachabilityManager()?.isReachable)!) {
            return true
        } else {
            return false
        }
    }
    
    func checkServer() {
        if Global.sharedInstance.getProductionServer() {
            print("PRODUCTION")
            BASE_URL = "<Sensitive_Data>"
            AUTH_BASE_URL = "<Sensitive_Data>"
        }
    }
    
    func makeRequest(_ requestDetails: Dictionary<String, Any>, responseHandler: CompletionBlock?) {
        // default value
        self.isServerMaintenance = false
        
        guard
            let requestType = requestDetails[TAG.REQUEST_TYPE] as? REQUEST

            else {
                return
            }
        
        let reqUrl = getRequestURL(requestType)
        let reqMethod = getRequestMethod(requestType)
        var reqHeader: [String: String]
        
        reqHeader = [HEADER.CONTENT_TYPE: HEADER.CONTENT_VALUE, HEADER.ACCEPT_TYPE: HEADER.CONTENT_VALUE, HEADER.TOKEN: "Bearer " + LoginModel.getToken()]
        
        if reqMethod == .get {
             Alamofire.request(reqUrl, method: reqMethod, parameters: nil, encoding: JSONEncoding.default, headers:reqHeader)
                .responseJSON(completionHandler: { (responseData) in
                    var isResponseDidFail: Bool
                    
                    switch responseData.result {
                    case .success(let data):
                        guard let json = data as? [String: AnyObject] else {
                            return responseHandler!(false, self.isServerMaintenance) // no data to parse
                        }
                    
                        isResponseDidFail = self.filterResponse(requestType, json) as! Bool
                    default:
                        isResponseDidFail = true
                    }
                    
                responseHandler!(isResponseDidFail, self.isServerMaintenance)
            })
        } else {
            guard
                let reqParam = requestDetails[TAG.REQUEST_PARAMS] as? [String: Any]
            
                else {
                    return
            }
            
            /*-- PRINT REQUEST PARAMS -- uncomment when debugging
             print("REQUEST PARAMS: ")
             print(reqParam) */
            
            if LoginModel.isTokenExpired() && (requestType != REQUEST.kUSER_VERIFY || requestType != REQUEST.kRETRIEVE_AUTH) {
                refreshCounter = 0
                Global.sharedInstance.setLoadMoreShouldDisplay(true)
                performRefreshToken(reqUrl, reqParam, reqMethod, reqHeader, requestType, responseHandler)
            } else {
                executeApiRequest(reqUrl, reqParam, reqMethod, reqHeader, requestType, { (isResponseDidFail, isServerMaintenance) in
                    if self.shouldPerformRefreshToken == true {
                        self.performRefreshToken(reqUrl, reqParam, reqMethod, reqHeader, requestType, responseHandler)
                    } else {
                        responseHandler!(isResponseDidFail, isServerMaintenance)
                    }
                })
            }
            
        }
    }
    
    func performRefreshToken(_ url: String, _ param: Dictionary<String, Any>, _ method: HTTPMethod, _ header: [String: String], _ type: REQUEST, _ responseHandler: CompletionBlock?) {
        self.refreshCounter += 1
        shouldPerformRefreshToken = false
        
        refreshTokenRequest(header, method, { (isResponseDidFail, isServerMaintenance) in
            if isResponseDidFail { // if fail do something here
                if (self.refreshCounter <= 2) {
                    self.performRefreshToken(url, param, method, header, type, responseHandler)
                } else {
                    return responseHandler!(true, true)
                }
            } else { //succes
                let reqHeader = [HEADER.CONTENT_TYPE: HEADER.CONTENT_VALUE, HEADER.ACCEPT_TYPE: HEADER.CONTENT_VALUE, HEADER.TOKEN: "Bearer " + LoginModel.getToken()]
                self.executeApiRequest(url, param, method, reqHeader, type, { (isResponseDidFail, isServerMaintenance) in
                    if self.shouldPerformRefreshToken == true {
                        if (self.refreshCounter <= 2) {
                            self.performRefreshToken(url, param, method, header, type, responseHandler)
                        } else {
                            return responseHandler!(true, true)
                        }
                    } else {
                        return responseHandler!(isResponseDidFail, isServerMaintenance)
                    }
                })
            }
        })
    }
    
    func refreshTokenRequest(_ reqHeader: [String:String], _ reqMethod:HTTPMethod, _ responseHandler: CompletionBlock?) {
        let reqUrl:String = (AUTH_BASE_URL + ENDPOINT.REFRESH_TOKEN as NSString) as String
        let reqParam:[String:Any] = [LoginModel.PARAM.kGRANT_TYPE: StaticStrings.LOGIN_MODEL.GRANT_TYPE as AnyObject,
                                     LoginModel.PARAM.kCLIENT_ID: StaticStrings.LOGIN_MODEL.CLIENT_ID as AnyObject,
                                     LoginModel.PARAM.kCLIENT_SECRET: StaticStrings.LOGIN_MODEL.CLIENT_SECRET as AnyObject,
                                     StaticStrings.LOGIN_MODEL.GRANT_TYPE:LoginModel.getAuth()?.refreshToken as AnyObject]
        /*-- PRINT REQUEST PARAMS -- uncomment when debugging
        print("REQUEST PARAMS: ")
        print(reqParam) */
        
        Alamofire.request(reqUrl, method: reqMethod, parameters: reqParam, encoding: JSONEncoding.default, headers:reqHeader)
            .responseJSON(completionHandler: { (responseData) in
                var isResponseDidFail: Bool
                
                switch responseData.result {
                case .success(let data):
                    print(reqUrl + " RESPONSE" , data)
                    guard let json = data as? [String: AnyObject] else {
                        return responseHandler!(false, self.isServerMaintenance) // no data to parse
                    }
                    
                    isResponseDidFail = self.filterResponse(REQUEST.kREFRESH_TOKEN, json) as! Bool
                default:
                    isResponseDidFail = true
                }
                
                return responseHandler!(isResponseDidFail, self.isServerMaintenance)
            })
    }
    
    func executeApiRequest(_ reqUrl: String, _ reqParam: [String:Any], _ reqMethod: HTTPMethod,_ reqHeader: [String: String], _ requestType : REQUEST,_ responseHandler: CompletionBlock?) {
        Alamofire.request(reqUrl, method: reqMethod, parameters: reqParam, encoding: JSONEncoding.default, headers:reqHeader)
            .responseJSON(completionHandler: { (responseData) in
                var isResponseDidFail: Bool
                
                switch responseData.result {
                case .success(let data):
                    print(reqUrl + " RESPONSE" , data)
                    guard let json = data as? [String: AnyObject] else {
                        return responseHandler!(false, self.isServerMaintenance) // no data to parse
                    }
                    
                    isResponseDidFail = self.filterResponse(requestType, json) as! Bool
                default:
                    isResponseDidFail = true
                }
                
                return responseHandler!(isResponseDidFail, self.isServerMaintenance)
            })
    }
    
    private func getRequestURL(_ keyAPI: REQUEST) -> String {
        checkServer()
        
        var url: NSString// = WebServiceManager.API_BASE_URL
        
        switch keyAPI {
        case .kRETRIEVE_AUTH:
            url = AUTH_BASE_URL + ENDPOINT.RETRIEVE_AUTH as NSString
            
        case .kUSER_VERIFY:
            url = AUTH_BASE_URL + ENDPOINT.USER_VERIFY as NSString
            
            break
            /*
             
             Remove
             
             */
            break
        case .kCHAT_POLL:
            url = WebServiceManager.LIVECHAT_URL + ENDPOINT.LIVECHAT_POLL + ChatModel.sharedInstance.getParticipantID() as NSString
            
            break
        case .kCHAT_SEND:
            url = WebServiceManager.LIVECHAT_URL + ENDPOINT.LIVECHAT_SEND_MSG + ChatModel.sharedInstance.getParticipantID() as NSString
            
            break
        case .kCHAT_EXIT:
            url = WebServiceManager.LIVECHAT_URL + ENDPOINT.LIVECHAT_EXIT + ChatModel.sharedInstance.getParticipantID() as NSString
            
            break
        }
        
        return url as String;
    }
    
    private func getRequestMethod(_ keyAPI: REQUEST) -> HTTPMethod {
        switch keyAPI {
        case .kCHAT_POLL:
            return .get
        default:
            return .post
        }
    }
    
    private func filterResponse(_ requestType: REQUEST, _ response: [String: AnyObject]) -> Any {
        var didFail = true
        Global.sharedInstance.setShouldShowSeperateError(false)
        var status: String = ""
        
        if (requestType != REQUEST.kCHAT_START && requestType != REQUEST.kCHAT_RECONNECT && requestType != REQUEST.kCHAT_POLL && requestType != REQUEST.kCHAT_SEND && requestType != REQUEST.kCHAT_EXIT) {
            status = response[RESPONSE_KEY.STATUS] as! String
        }
        
        if status.caseInsensitiveCompare(RESPONSE_RESULT.FAILED) == ComparisonResult.orderedSame  || status.caseInsensitiveCompare(RESPONSE_RESULT.FAILURE) == ComparisonResult.orderedSame  {
            
            self.handleSuccessResponseWithError(response, requestType)
        } else {
            switch requestType { // evaluate the response
            case .kRETRIEVE_AUTH:
                didFail = LoginModel.sharedInstance.setLoginResponse(response)
                
                break
            case .kREFRESH_TOKEN:
                self.refreshCounter = 0
                didFail = LoginModel.sharedInstance.setRefreshToken(response)
               
                break
                
                /*
                 
                 Remove
                 
                 */
                 */
            case .kCHAT_EXIT:
                didFail = ChatModel.sharedInstance.setChatExitResponse(response)
                
                break
            default: // returning true means it fail
                return didFail
            }
        }
        
        return didFail
    }
    
    private func handleSuccessResponseWithError(_ responseError: [String: AnyObject], _ reqKey:REQUEST) {
        guard responseError[RESPONSE_KEY.CODE] != nil else {
            return
        }
        
        errorMessage = responseError[RESPONSE_KEY.ERROR_MSG] as AnyObject
        let errorCode = String(format:"%@", responseError[RESPONSE_KEY.CODE] as! NSObject )
        
        if errorCode.isEqual(ERROR_CODE.TOKEN_EXPIRED) {
            if reqKey == REQUEST.kREFRESH_TOKEN {
                //set false to refresh
                Global.sharedInstance.setIsApiTokenRefreshedToFalse(false, true)
                Global.sharedInstance.setLoadMoreShouldDisplay(true)
                
                //flag to call perform refresh token
                if self.refreshCounter >= 2 {
                    isServerMaintenance = true
                    showDialog(dialogType: .REFRESH_TOKEN_FAILED)
                }
            } else {
                Global.sharedInstance.setLoadMoreShouldDisplay(true)
                
                //flag to call perform refresh token
                if self.refreshCounter >= 2 {
                    isServerMaintenance = true
                    showDialog(dialogType: .REFRESH_TOKEN_FAILED)
                } else {
                    shouldPerformRefreshToken = true
                }
            }
        } else if errorCode.isEqual(ERROR_CODE.SERVER_MAINTENANCE) {
            Global.sharedInstance.dismissHUD()
            
            isServerMaintenance = true
            showDialog(dialogType: .SERVER_MAINTENANCE)
        } else if errorCode.isEqual(ERROR_CODE.NEW_VERSION) {
            Global.sharedInstance.dismissHUD()
            
            isServerMaintenance = true
            showDialog(dialogType: .NEW_VERSION)
        } else if errorCode.isEqual(ERROR_CODE.REFRESH_TOKEN_FAILED) && (reqKey == REQUEST.kREFRESH_TOKEN) {
            Global.sharedInstance.dismissHUD()
            
            isServerMaintenance = true
            shouldPerformRefreshToken = false
            self.refreshCounter = 3 // to avoid repetition
            
            showDialog(dialogType: .REFRESH_TOKEN_FAILED)
        } else if errorCode.isEqual(ERROR_CODE.FORGOT_FAILED) && (reqKey == REQUEST.kFORGOT_USERNAME || reqKey == REQUEST.kFORGOT_PASSWORD) {
            Global.sharedInstance.setShouldShowSeperateError(true)
        } else if errorCode.isEqual(ERROR_CODE.REFRESH_TOKEN_FAILED) && (reqKey == REQUEST.kIPAY_LINK) {
            Global.sharedInstance.setShouldShowSeperateError(true)
        }
        
        if isServerMaintenance {
            PushNotification.sharedInstance.setPushNotifEnable(false)
            PushNotification.sharedInstance.clearCache()
        }
    }
    
    func showDialog(dialogType: CustomDialogView.DIALOG) {
        let dialog = CustomDialogView(type: dialogType)
        
        switch Global.pageType {
        case .kEMAIL:
            let vc: LoginViewController = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController as! LoginViewController
            dialog.delegateLogin = vc
            
            break
        case .kPASSWORD:
            let nc = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.presentedViewController
            let vc:PasswordViewController = nc!.childViewControllers[0] as! PasswordViewController
            dialog.delegatePassword = vc
            
            break
        default: // this is expecting that you're inside the MAIN
            if UIApplication.shared.keyWindow?.rootViewController?.presentedViewController != nil {
                let nc = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.childViewControllers[1] as! UINavigationController
                let vc:DrawerMenuViewController = nc.topViewController as! DrawerMenuViewController
                dialog.delegateDrawer = vc
            } else {
                let nc = UIApplication.shared.keyWindow?.rootViewController
                let vc:RootViewController = nc as! RootViewController
                dialog.delegateRoot = vc
            }
            
            break
        }
        
        dialog.show(animated: true)
    }
    
    func makeRequestWithoutToken(_ requestDetails: Dictionary<String, Any>, responseHandler: CompletionBlock?) {
        self.isServerMaintenance = false
        
        guard
            let requestType = requestDetails[TAG.REQUEST_TYPE] as? REQUEST
            
            else {
                return
        }
        
        let reqUrl = getRequestURL(requestType)
        let reqMethod = getRequestMethod(requestType)
        var reqHeader: [String: String]
        
        reqHeader = [HEADER.CONTENT_TYPE: HEADER.CONTENT_VALUE, HEADER.ACCEPT_TYPE: HEADER.CONTENT_VALUE]
        
        if reqMethod == .get {
            Alamofire.request(reqUrl, method: reqMethod, parameters: nil, encoding: JSONEncoding.default, headers:reqHeader)
                .responseJSON(completionHandler: { (responseData) in
                    var isResponseDidFail: Bool
                    
                    switch responseData.result {
                    case .success(let data):
                        //print("!!!! GET DATA: " , data)
                        guard let json = data as? [String: AnyObject] else {
                            return responseHandler!(false, self.isServerMaintenance) // no data to parse
                        }
                        
                        isResponseDidFail = self.filterResponse(requestType, json) as! Bool
                    default:
                        
                        isResponseDidFail = true
                    }
                    
                    return responseHandler!(isResponseDidFail, self.isServerMaintenance)
                })
        } else {
            
            guard
                let reqParam = requestDetails[TAG.REQUEST_PARAMS] as? [String: Any]
                
                else {
                    return
            }
            
            /*-- PRINT REQUEST PARAMS -- uncomment when debugging
             print("REQUEST PARAMS: ")
             print(reqParam)*/
            
            executeApiRequest(reqUrl, reqParam, reqMethod, reqHeader, requestType, { (isResponseDidFail, isServerMaintenance) in
                return responseHandler!(isResponseDidFail, isServerMaintenance)
            })
            
        }
    }
    
}
