//
//  ChatMessageDetailEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 03/08/2017.
//
//

import Foundation

class ChatMessageDetailEntity: NSObject, NSCoding {
    fileprivate var mValue: String? = ""
    fileprivate var mParticipantType: String? = ""
    fileprivate var mDisplayName: String? = ""
    fileprivate var mDate: String? = ""
    fileprivate var mType: String? = ""
    fileprivate var mSequenceNumber: Int? = 0
    fileprivate var mTimeStamp: String? = ""
    
    struct MODEL {
        static let kVALUE = "mValue"
        static let kPARTICIPANT_TYPE = "mParticipantType"
        static let kDISPLAY_NAME = "mDisplayName"
        static let kDATE_STAMP = "mDate"
        static let kTYPE = "mType"
        static let kSEQUENCE_NUMBER = "mSequenceNumber"
        static let kTIME_STAMP = "mTimeStamp"
    }
    
    init(_ responseObject : [String : AnyObject]) {
        super.init()
        
        self.mValue = responseObject[ChatModel.CONSTANTS.kVALUE] as? String
        self.mParticipantType = responseObject[ChatModel.CONSTANTS.kPARTICIPANT_TYPE] as? String
        self.mDisplayName = responseObject[ChatModel.CONSTANTS.kDISPLAY_NAME] as? String
        self.mDate = responseObject[ChatModel.CONSTANTS.kDATE_STAMP] as? String
        self.mType = responseObject[ChatModel.CONSTANTS.kTYPE] as? String
        self.mSequenceNumber = responseObject[ChatModel.CONSTANTS.kSEQUENCE_NUMBER] as? Int
        self.mTimeStamp = responseObject[ChatModel.CONSTANTS.kTIME_STAMP] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mValue = decoder.decodeObject(forKey: MODEL.kVALUE) as? String
        self.mParticipantType = decoder.decodeObject(forKey: MODEL.kPARTICIPANT_TYPE) as? String
        self.mDisplayName = decoder.decodeObject(forKey: MODEL.kDISPLAY_NAME) as? String
        self.mDate = decoder.decodeObject(forKey: MODEL.kDATE_STAMP) as? String
        self.mType = decoder.decodeObject(forKey: MODEL.kTYPE) as? String
        self.mSequenceNumber = decoder.decodeObject(forKey: MODEL.kSEQUENCE_NUMBER) as? Int
        self.mTimeStamp = decoder.decodeObject(forKey: MODEL.kTIME_STAMP) as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mValue, forKey: MODEL.kVALUE)
        coder.encode(mParticipantType, forKey: MODEL.kPARTICIPANT_TYPE)
        coder.encode(mDisplayName, forKey: MODEL.kDISPLAY_NAME)
        coder.encode(mDate, forKey: MODEL.kDATE_STAMP)
        coder.encode(mType, forKey: MODEL.kTYPE)
        coder.encode(mSequenceNumber, forKey: MODEL.kSEQUENCE_NUMBER)
        coder.encode(mTimeStamp, forKey: MODEL.kTIME_STAMP)
    }
    
    // entity attribute GETTER & SETTER
    public var eventMessage: String {
        get {
            guard
                ((self.mValue as String?) != nil) else{
                    self.mValue = ""
                    return self.mValue!
            }
            return self.mValue!
        }
        set(eventMsg) {
            self.mValue = eventMsg
        }
    }
    
    public var participantType: String {
        get {
            guard
                ((self.mParticipantType as String?) != nil) else{
                    self.mParticipantType = ""
                    return self.mParticipantType!
            }
            return self.mParticipantType!
        }
        set(participntType) {
            self.mParticipantType = participntType
        }
    }
    
    public var displayName: String {
        get {
            guard
                ((self.mDisplayName as String?) != nil) else{
                    self.mDisplayName = ""
                    return self.mDisplayName!
            }
            return self.mDisplayName!
        }
        set(displayNme) {
            self.mDisplayName = displayNme
        }
    }
    
    public var dateStamp: String {
        get {
            guard
                ((self.mDate as String?) != nil) else{
                    self.mDate = ""
                    return self.mDate!
            }
            return self.mDate!
        }
        set(dateStmp) {
            self.mDate = dateStmp
        }
    }
    
    public var eventType: String {
        get {
            guard
                ((self.mType as String?) != nil) else{
                    self.mType = ""
                    return self.mType!
            }
            return self.mType!
        }
        set(evnType) {
            self.mType = evnType
        }
    }
    
    public var sequenceNumber: Int {
        get {
            return self.mSequenceNumber!
        }
        set(sequenceNo) {
            self.mSequenceNumber = sequenceNo
        }
    }
    
    public var timeStamp: String {
        get {
            guard
                ((self.mTimeStamp as String?) != nil) else{
                    self.mTimeStamp = ""
                    return self.mTimeStamp!
            }
            return self.mTimeStamp!
        }
        set(timeStmp) {
            self.mTimeStamp = timeStmp
        }
    }
    
}
