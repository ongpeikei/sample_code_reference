//
//  UsageBalanceEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 17/07/2017.
//
//

import Foundation

class UsageBalanceEntity: NSObject, NSCoding {
    
    
    fileprivate var mAcctResCode:String? = ""
    fileprivate var mBalanceId:String? = ""
    fileprivate var mLeftBal:String? = ""
    fileprivate var mBalanceName:String? = ""
    fileprivate var mEffDate:String? = ""
    fileprivate var mExpDate:String? = ""
    fileprivate var mUnitType:String? = ""
    fileprivate var mUsedBal:String? = ""
    
    override init() {
        super.init()
    }
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        self.mAcctResCode = ((responseObject["acctResCode"]) != nil) ? responseObject["acctResCode"] as? String : ""
        self.mBalanceId = ((responseObject["balanceId"]) != nil) ? responseObject["balanceId"] as? String : ""
        self.mLeftBal = ((responseObject["leftBalance"]) != nil) ? responseObject["leftBalance"] as? String : ""
        self.mBalanceName = ((responseObject["balanceName"]) != nil) ? responseObject["balanceName"] as? String : ""
        self.mEffDate = ((responseObject["effDate"]) != nil) ? responseObject["effDate"] as? String : ""
        self.mExpDate = ((responseObject["expDate"]) != nil) ? responseObject["expDate"] as? String : ""
        self.mUnitType = ((responseObject["unitType"]) != nil) ? responseObject["unitType"] as? String : ""
        self.mUsedBal = ((responseObject["usedBal"]) != nil) ? responseObject["usedBal"] as? String : ""
    }
    
    required init(coder decoder: NSCoder) {
        self.mAcctResCode = decoder.decodeObject(forKey: "mAcctResCode") as? String
        self.mBalanceId = decoder.decodeObject(forKey: "mBalanceId") as? String
        self.mLeftBal = decoder.decodeObject(forKey: "mLeftBal") as? String
        self.mBalanceName = decoder.decodeObject(forKey: "mBalanceName") as? String
        self.mEffDate = decoder.decodeObject(forKey: "mEffDate") as? String
        self.mUnitType = decoder.decodeObject(forKey: "mUnitType") as? String
        self.mUsedBal = decoder.decodeObject(forKey: "mUsedBal") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mAcctResCode, forKey: "mAcctResCode")
        coder.encode(mBalanceId, forKey: "mBalanceId")
        coder.encode(mLeftBal, forKey: "mLeftBal")
        coder.encode(mBalanceName, forKey: "mBalanceName")
        coder.encode(mEffDate, forKey: "mEffDate")
        coder.encode(mExpDate, forKey: "mExpDate")
        coder.encode(mUnitType, forKey: "mUnitType")
        coder.encode(mUsedBal, forKey: "mUsedBal")
    }

    public var acctResCode: String {
        
        get {
            return self.mAcctResCode!
        }
        set(accResCode) {
            self.mAcctResCode = accResCode
        }
        
    }
    
    public var balanceId: String {
        
        get {
            guard
                ((self.mBalanceId as String?) != nil) else{
                    return "-"
            }
            return self.mBalanceId!
        }
        set(balId) {
            self.mBalanceId = balId
        }
        
    }
    
    public var leftBalance: String {
        
        get {
            guard
                ((self.mLeftBal as String?) != nil) else{
                    return "-"
            }
            return self.mLeftBal!
        }
        set(leftBal) {
            self.mLeftBal = leftBal
        }
        
    }
    
    public var balName: String {
        
        get {
            guard
                ((self.mBalanceName as String?) != nil) else{
                    return "-"
            }
            return self.mBalanceName!
        }
        set(balanceName) {
            self.mBalanceName = balanceName
        }
        
    }

    public var effDate: String {
        
        get {
            guard
                ((self.mEffDate as String?) != nil) else{
                    return "-"
            }
            return self.mEffDate!
        }
        set(efdate) {
            self.mEffDate = efdate
        }
        
    }
    
    public var expDate: String {
        
        get {
            guard
                ((self.mExpDate as String?) != nil) else{
                    return "-"
            }
            return self.mExpDate!
        }
        set(exDate) {
            self.mExpDate = exDate
        }
        
    }

    public var unitType: String {
        
        get {
            guard
                ((self.mUnitType as String?) != nil) else{
                    return "-"
            }
            return self.mUnitType!
        }
        set(unitTyp) {
            self.mUnitType = unitTyp
        }
        
    }
    
    public var usedBal: String {
        
        get {
            guard
                ((self.mUsedBal as String?) != nil) else{
                    return "-"
            }
            return self.mUsedBal!
        }
        set(usedBl) {
            self.mUsedBal = usedBl
        }
        
    }

    
}
