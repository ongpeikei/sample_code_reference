//
//  AddOnBalanceDetailsEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 21/07/2017.
//
//

import Foundation

class AddOnBalanceDetailsEntity: NSObject, NSCoding {
    fileprivate var mAcctResCode: String? = ""
    fileprivate var mBalanceId: String? = ""
    fileprivate var mDescription: String? = ""
    fileprivate var mEffDate: String? = ""
    fileprivate var mExpDate: String? = ""
    fileprivate var mLeftBal: String? = ""
    fileprivate var mLeftPercent:String? = ""
    fileprivate var mMsisdn: String? = ""
    fileprivate var mName: String? = ""
    fileprivate var mTotal:String? = ""
    fileprivate var mUsedBal:String? = ""
    
    init(_ responseObject : [String : AnyObject]) {
        super.init()
        
        self.mAcctResCode = responseObject["acctResCode"] as? String
        self.mBalanceId = responseObject["balanceId"] as? String
        self.mDescription = responseObject["description"] as? String
        self.mEffDate = responseObject["effDate"] as? String
        self.mExpDate = responseObject["expDate"] as? String
        self.mLeftBal = responseObject["leftBal"] as? String
        self.mLeftPercent = responseObject["leftPercent"] as? String
        self.mMsisdn = responseObject["msisdn"] as? String
        self.mName = responseObject["name"] as? String
        self.mTotal = responseObject["total"] as? String
        self.mUsedBal = responseObject["usedBal"] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mAcctResCode = decoder.decodeObject(forKey: "mAcctResCode") as? String
        self.mBalanceId = decoder.decodeObject(forKey: "mBalanceId") as? String
        self.mDescription = decoder.decodeObject(forKey: "mDescription") as? String
        self.mEffDate = decoder.decodeObject(forKey: "mEffDate") as? String
        self.mExpDate = decoder.decodeObject(forKey: "mExpDate") as? String
        self.mLeftBal = decoder.decodeObject(forKey: "mLeftBal") as? String
        self.mLeftPercent = decoder.decodeObject(forKey: "mLeftPercent") as? String
        self.mMsisdn = decoder.decodeObject(forKey: "mMsisdn") as? String
        self.mName = decoder.decodeObject(forKey: "mName") as? String
        self.mTotal = decoder.decodeObject(forKey: "mTotal") as? String
        self.mUsedBal = decoder.decodeObject(forKey: "mUsedBal") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mAcctResCode, forKey: "mAcctResCode")
        coder.encode(mBalanceId, forKey: "mBalanceId")
        coder.encode(mDescription, forKey: "mDescription")
        coder.encode(mEffDate, forKey: "mEffDate")
        coder.encode(mExpDate, forKey: "mExpDate")
        coder.encode(mLeftBal, forKey: "mLeftBal")
        coder.encode(mLeftPercent, forKey: "mLeftPercent")
        coder.encode(mMsisdn, forKey: "mMsisdn")
        coder.encode(mName, forKey: "mName")
        coder.encode(mTotal, forKey: "mTotal")
        coder.encode(mUsedBal, forKey: "mUsedBal")
    }

    public var acctResCode: String {
        get {
            guard
                ((self.mAcctResCode as String?) != nil) else {
                    self.mAcctResCode = ""
                    return self.mAcctResCode!
            }
            return self.mAcctResCode!
        }
        set(acctResCde) {
            self.mAcctResCode = acctResCde
        }
    }
    
    public var balanceId: String {
        get {
            guard
                ((self.mBalanceId as String?) != nil) else {
                    self.mBalanceId = ""
                    return self.mBalanceId!
            }
            return self.mBalanceId!
        }
        set(balId) {
            self.mBalanceId = balId
        }
    }
    
    public var desc: String {
        get {
            guard
                ((self.mDescription as String?) != nil) else {
                    return "-"
            }
            return self.mDescription!
        }
        set(descptn) {
            self.mDescription = descptn
        }
    }
    
    public var effDate: String {
        get {
            guard
                ((self.mEffDate as String?) != nil) else {
                    return "-"
            }
            return self.mEffDate!
        }
        set(effDte) {
            self.mEffDate = effDte
        }
    }
    
    public var expDate: String {
        get {
            guard
                ((self.mExpDate as String?) != nil) else {
                    return "-"
            }
            return self.mExpDate!
        }
        set(expDte) {
            self.mExpDate = expDte
        }
    }
    
    public var leftBal: String {
        get {
            guard
                ((self.mLeftBal as String?) != nil) else {
                    return "-"
            }
            return self.mLeftBal!
        }
        set(leftBalance) {
            self.mLeftBal = leftBalance
        }
    }
    
    public var leftPercent: String {
        get {
            guard
                ((self.mLeftPercent as String?) != nil) else {
                    self.mLeftPercent = "0"
                    return self.mLeftPercent!
            }
            return self.mLeftPercent!
        }
        set(leftPercnt) {
            self.mLeftPercent = leftPercnt
        }
    }
    
    public var msisdn: String {
        get {
            guard
                ((self.mMsisdn as String?) != nil) else {
                    return "-"
            }
            return self.mMsisdn!
        }
        set(msisDn) {
            self.mMsisdn = msisDn
        }
    }
    
    public var name: String {
        get {
            guard
                ((self.mName as String?) != nil) else {
                    return "-"
            }
            return self.mName!
        }
        set(nme) {
            self.mName = nme
        }
    }
    
    public var total: String {
        get {
            guard
                ((self.mTotal as String?) != nil) else {
                    return "-"
            }
            return self.mTotal!
        }
        set(totl) {
            self.mTotal = totl
        }
    }
    
    public var usedBal: String {
        get {
            guard
                ((self.mUsedBal as String?) != nil) else {
                    return "-"
            }
            return self.mUsedBal!
        }
        set(usedBalance) {
            self.mUsedBal = usedBalance
        }
    }
    
}
