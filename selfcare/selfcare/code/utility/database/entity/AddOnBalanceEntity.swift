//
//  AddOnBalanceEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 21/07/2017.
//
//

import Foundation
import CoreData

class AddOnBalanceEntity: NSObject, NSCoding {
    fileprivate var mAddOnBalanceResponse: [String:AnyObject]? = nil
    
    fileprivate var mAddOnBalanceList: Array? = [AddOnBalanceDetailsEntity]()
    fileprivate var mSubscribedAddOns: Array? = [SubscribedAddOnsEntity]()
    fileprivate var mMsisdn: String? = ""
    
    init(_ responseObject : [String : AnyObject]) {
        super.init()
        self.mAddOnBalanceResponse = (responseObject["response"] as? [String:AnyObject])!
        
        self.mMsisdn = mAddOnBalanceResponse?["msisdn"] as? String
        
        // parse addOnBalanceList
        let addOnList:Array<AnyObject>? = mAddOnBalanceResponse?["addOnBalanceList"] as? Array<AnyObject>
        
        if addOnList == nil {
            mAddOnBalanceList = [AddOnBalanceDetailsEntity]()
        } else if (addOnList?.count)! > 0 {
            for addOn in addOnList! {
                
                let addOnBalDet: AddOnBalanceDetailsEntity = AddOnBalanceDetailsEntity.init(addOn as! [String : AnyObject])
                mAddOnBalanceList?.append(addOnBalDet)
            }
        }  else {
            mAddOnBalanceList = [AddOnBalanceDetailsEntity]()
        }
        
        // parse subscribedAddOns
        let subscribedAddOnList:Array<AnyObject>? = mAddOnBalanceResponse?["subscribedAddOns"] as? Array<AnyObject>
        
        if subscribedAddOnList == nil {
            mSubscribedAddOns = [SubscribedAddOnsEntity]()
        } else if (subscribedAddOnList?.count)! > 0 {
            for subscribedAddOn in subscribedAddOnList! {
                
                let subscribedAddOnDet: SubscribedAddOnsEntity = SubscribedAddOnsEntity.init(subscribedAddOn as! [String : AnyObject])
                mSubscribedAddOns?.append(subscribedAddOnDet)
            }
        }  else {
            mSubscribedAddOns = [SubscribedAddOnsEntity]()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mAddOnBalanceList = decoder.decodeObject(forKey: "mAddOnBalanceList") as? Array<AddOnBalanceDetailsEntity>
        self.mSubscribedAddOns = decoder.decodeObject(forKey: "mSubscribedAddOns") as? Array<SubscribedAddOnsEntity>
        self.mMsisdn = decoder.decodeObject(forKey: "mMsisdn") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mAddOnBalanceList, forKey: "mAddOnBalanceList")
        coder.encode(mSubscribedAddOns, forKey: "mSubscribedAddOns")
        coder.encode(mMsisdn, forKey: "mMsisdn")
    }
    
    public var addOnBalanceList: [AddOnBalanceDetailsEntity] {
        get {
            return self.mAddOnBalanceList!
        }
        set(addOnBalList) {
            self.mAddOnBalanceList = addOnBalList
        }
    }
    
    public var subscribedAddOns: [SubscribedAddOnsEntity] {
        get {
            return self.mSubscribedAddOns!
        }
        set(subdAddOns) {
            self.mSubscribedAddOns = subdAddOns
        }
    }
    
    public var msisdn: String {
        get {
            guard
                ((self.mMsisdn as String?) != nil) else{
                    self.mMsisdn = ""
                    return self.mMsisdn!
            }
            
            return self.mMsisdn!
        }
        set(Msisdn) {
            self.mMsisdn = Msisdn
        }
    }
    
}

@objc(AddOnBalanceDB)
class AddOnBalanceDB : NSManagedObject {
    
    @NSManaged public var addOnBalanceList: Array<AddOnBalanceDetailsEntity>?
    @NSManaged public var subscribedAddOns: Array<SubscribedAddOnsEntity>?
    @NSManaged public var msisdn: String?
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    convenience init(_ response : AddOnBalanceEntity, entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        self.init(entity: entity, insertInto: context)
        self.addOnBalanceList = response.addOnBalanceList
        self.subscribedAddOns = response.subscribedAddOns
    }
    
    public static func getAddOnBalance(_ context: NSManagedObjectContext, _ msisdn : String) -> AddOnBalanceDB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "AddOnBalanceDB", in: context)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "msisdn", msisdn)
        
        do {
            
            let results: Array<AddOnBalanceDB> = try context.fetch(fetchRequest) as! Array<AddOnBalanceDB>
            
            if results.count > 0 {
                for addOnBalance in results {
                    if (addOnBalance.msisdn == msisdn) {
                        return addOnBalance
                    }
                }
                
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func saveAddOnBalance(_ response : AddOnBalanceEntity, context: NSManagedObjectContext?) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "AddOnBalanceDB", in: context!)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "msisdn", response.msisdn)
        
        var isSaveDidFail: Bool = true
        
        do {
            
            let results: Array<AddOnBalanceDB> = try context!.fetch(fetchRequest) as! Array<AddOnBalanceDB>
            
            //print(results.count)
            
            if results.count > 0 {
                for  addOnBalance in results {
                    if (addOnBalance.msisdn == response.msisdn) {
                        let updatedObj = self.updateAddOnBalance(addOnBalance, response,  context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createAddOnBalance(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
                
            } else {
                let newObj = self.createAddOnBalance(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        
                        return false // saving success
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return isSaveDidFail
    }
    
    private static func createAddOnBalance(_ response : AddOnBalanceEntity, context: NSManagedObjectContext?) -> AddOnBalanceDB? {
        if let addOnBalEntity = NSEntityDescription.insertNewObject(forEntityName: "AddOnBalanceDB", into: context!) as? AddOnBalanceDB {
            
            addOnBalEntity.msisdn =  response.msisdn// <------ use msisdn from user defaults
            addOnBalEntity.addOnBalanceList = response.addOnBalanceList
            addOnBalEntity.subscribedAddOns = response.subscribedAddOns
            
            return addOnBalEntity
        }
        
        return AddOnBalanceDB()
    }
    
    private static func updateAddOnBalance(_ addOnBalanceMO: AddOnBalanceDB,_ addOnBalanceEntity : AddOnBalanceEntity, context: NSManagedObjectContext?) -> AddOnBalanceDB? {
        let updatedAddOnBalEntity: AddOnBalanceDB = addOnBalanceMO
        
        updatedAddOnBalEntity.addOnBalanceList = addOnBalanceEntity.addOnBalanceList
        updatedAddOnBalEntity.subscribedAddOns = addOnBalanceEntity.subscribedAddOns
        updatedAddOnBalEntity.msisdn = addOnBalanceEntity.msisdn // <------ use msisdn from user defaults
        
        return updatedAddOnBalEntity
    }
    
}

