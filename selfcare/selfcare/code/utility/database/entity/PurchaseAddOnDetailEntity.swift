//
//  PurchaseAddOnDetailEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 18/07/2017.
//
//

import Foundation

class PurchaseAddOnDetailEntity: NSObject, NSCoding {
    // string constant key for database MO
    struct MODEL_OBJECT {
        static let kIPP_NAME = "mIppName"
        static let kPRICE = "mPrice"
        static let kPURCHASE_DATE_TIME = "mPurchaseDatetime"
        static let kORIGINAL_PURCHASE_DATE = "mOriginalPurchaseDate"
        static let kPURCHASE_CHANNEL = "mPurchaseChannel"
        static let kMSISDN = "mMsisdn"
    }
    
    //RESPONSE
    fileprivate var mIppName: String? = ""
    fileprivate var mPrice: String? = ""
    fileprivate var mPurchaseDatetime: String? = ""
    fileprivate var mOriginalPurchaseDate: String? = ""
    fileprivate var mPurchaseChannel: String? = ""
    fileprivate var mMsisdn: String? = ""
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        let responseKey = HistoryModel.RESPONSE.self
        
        self.mIppName = responseObject[responseKey.kIPP_NAME] as? String
        self.mPrice = responseObject[responseKey.kPRICE] as? String
        self.mPurchaseDatetime = responseObject[responseKey.kPURCHASE_DATE_TIME] as? String
        self.mOriginalPurchaseDate = responseObject[responseKey.kORIGINAL_PURCHASE_DATE] as? String
        self.mPurchaseChannel = responseObject[responseKey.kPURCHASE_CHANNEL] as? String
        self.mMsisdn = responseObject[responseKey.kMSISDN] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mIppName = decoder.decodeObject(forKey: MODEL_OBJECT.kIPP_NAME) as? String
        self.mPrice = decoder.decodeObject(forKey: MODEL_OBJECT.kPRICE) as? String
        self.mPurchaseDatetime = decoder.decodeObject(forKey: MODEL_OBJECT.kPURCHASE_DATE_TIME) as? String
        self.mOriginalPurchaseDate = decoder.decodeObject(forKey: MODEL_OBJECT.kORIGINAL_PURCHASE_DATE) as? String
        self.mPurchaseChannel = decoder.decodeObject(forKey: MODEL_OBJECT.kPURCHASE_CHANNEL) as? String
        self.mMsisdn = decoder.decodeObject(forKey: MODEL_OBJECT.kMSISDN) as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mIppName, forKey: MODEL_OBJECT.kIPP_NAME)
        coder.encode(mPrice, forKey: MODEL_OBJECT.kPRICE)
        coder.encode(mPurchaseDatetime, forKey: MODEL_OBJECT.kPURCHASE_DATE_TIME)
        coder.encode(mOriginalPurchaseDate, forKey: MODEL_OBJECT.kORIGINAL_PURCHASE_DATE)
        coder.encode(mPurchaseChannel, forKey: MODEL_OBJECT.kPURCHASE_CHANNEL)
        coder.encode(mMsisdn, forKey: MODEL_OBJECT.kMSISDN)
    }
    
    // entity attribute GETTER & SETTER
    public var ippName: String {
        get {
            guard
                ((self.mIppName as String?) != nil) else{
                    return "-"
            }
            
            return self.mIppName!
        }
        set(ippNme) {
            self.mIppName = ippNme
        }
    }
    
    public var price: String {
        get {
            guard
                ((self.mPrice as String?) != nil) else{
                    return "-"
            }
            
            return self.mPrice!
        }
        set (prce) {
            self.mPrice = prce
        }
    }
    
    public var purchaseDatetime: String {
        get {
            guard
            ((self.mPurchaseDatetime as String?) != nil) else{
                return "-"
            }
            
            return self.mPurchaseDatetime!
        }
        set (purchaseDtetime) {
            self.mPurchaseDatetime = purchaseDtetime
        }
    }
    
    public var originalPurchaseDate: String {
        get {
            guard
                ((self.mOriginalPurchaseDate as String?) != nil) else{
                    return "-"
            }
            
            return self.mOriginalPurchaseDate!
        }
        set (originalPurchaseDte) {
            self.mOriginalPurchaseDate = originalPurchaseDte
        }
    }
    
    public var purchaseChannel: String {
        get {
            guard
                ((self.mPurchaseChannel as String?) != nil) else{
                    return "-"
            }
            
            return self.mPurchaseChannel!
        }
        set (purchaseChl) {
            self.mPurchaseChannel = purchaseChl
        }
    }
    
    public var msisdn: String {
        get {
            guard
                ((self.mMsisdn as String?) != nil) else{
                    return "-"
            }
            
            return self.mMsisdn!
        }
        set (msisdNumber) {
            self.mMsisdn = msisdNumber
        }
    }
    
}
