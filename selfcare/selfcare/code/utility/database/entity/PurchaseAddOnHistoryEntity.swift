//
//  PurchaseAddOnHistoryEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 20/07/2017.
//
//

import Foundation
import CoreData

class PurchaseAddOnHistoryEntity: NSObject, NSCoding {
    // Response contant keys
    struct RESPONSE {
        static let kACCOUNT_NUMBER = "accountNumber" // TODO: Update when other API key is ready in Dummy Response
    }
    
    // string constant key for database MO
    struct MODEL_OBJECT {
        static let kPURCHASE_HISTORY_LIST = "mPurchaseHistoryList"
        static let kACCOUNT_NUMBER = "mAccountNumber"
    }
    
    fileprivate var historyPurchaseAddOnResponse: [String:AnyObject]? = nil
    
    fileprivate var mPurchaseHistoryList: Array? = [PurchaseAddOnDetailEntity]()
    fileprivate var mAccountNumber: String? = ""

    init(_ responseObject : [String : AnyObject]) {
        super.init()
        let responseKey = HistoryModel.RESPONSE.self
        
        historyPurchaseAddOnResponse = (responseObject[responseKey.kRESPONSE] as? [String:AnyObject])!
        
        self.mAccountNumber = SwitchModel.getSelectedAccountNo()
        
        let purchaseList: Array<AnyObject>? = historyPurchaseAddOnResponse?[responseKey.kHISTORY_ADDON_LIST] as? Array<AnyObject>
        
        if purchaseList == nil {
            mPurchaseHistoryList = [PurchaseAddOnDetailEntity]()
        } else if (purchaseList?.count)! > 0 {
            for purchase in purchaseList! {
                let purchaseDetails: PurchaseAddOnDetailEntity = PurchaseAddOnDetailEntity.init(purchase as! [String : AnyObject])
                mPurchaseHistoryList?.append(purchaseDetails)
            }
        }  else {
            mPurchaseHistoryList = [PurchaseAddOnDetailEntity]()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mPurchaseHistoryList = decoder.decodeObject(forKey: MODEL_OBJECT.kPURCHASE_HISTORY_LIST) as? Array<PurchaseAddOnDetailEntity>
        self.mAccountNumber = decoder.decodeObject(forKey: MODEL_OBJECT.kACCOUNT_NUMBER) as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mPurchaseHistoryList, forKey: MODEL_OBJECT.kPURCHASE_HISTORY_LIST)
        coder.encode(mAccountNumber, forKey: MODEL_OBJECT.kACCOUNT_NUMBER)
    }
    
    public var purchaseHistoryList: [PurchaseAddOnDetailEntity] {
        get {
            return self.mPurchaseHistoryList!
        }
        set (purchaseList) {
            self.mPurchaseHistoryList = purchaseList
        }
        
    }
    
    public var accountNumber: String {
        get {
            guard
                ((self.mAccountNumber as String?) != nil) else{
                    return "-"
            }
            
            return self.mAccountNumber!
        }
        set (accountNmber) {
            self.mAccountNumber = accountNmber
        }
        
    }
    
}

@objc(DBHistoryAddOn)
class DBHistoryAddOn : NSManagedObject {
    
    @NSManaged public var purchaseHistoryList: Array<PurchaseAddOnDetailEntity>?
    @NSManaged public var accountNumber: String?
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    convenience init(_ response : [PurchaseAddOnDetailEntity], entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        self.init(entity: entity, insertInto: context)
        self.purchaseHistoryList = response
    }
    
    public static func getPurchaseAddOnHistory(_ context: NSManagedObjectContext) -> DBHistoryAddOn? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: DataFactory.DATABASE.HISTORY_ADDON, in: context)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@",
                                             PurchaseAddOnHistoryEntity.RESPONSE.kACCOUNT_NUMBER,
                                             SwitchModel.getSelectedAccountNo()!)
        
        do {
            
            let results: Array<DBHistoryAddOn> = try context.fetch(fetchRequest) as! Array<DBHistoryAddOn>
            
            if results.count > 0 {
                for purchaseHistory in results {
                    if (purchaseHistory.accountNumber?.isEqual(SwitchModel.getSelectedAccountNo()))! {
                        return results[0]
                    }
                }
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func savePurchaseAddOnHistoryList(_ response : PurchaseAddOnHistoryEntity, context: NSManagedObjectContext?) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: DataFactory.DATABASE.HISTORY_ADDON, in: context!)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@",
                                             PurchaseAddOnHistoryEntity.RESPONSE.kACCOUNT_NUMBER,
                                             SwitchModel.getSelectedAccountNo()!)
        
        var isSaveDidFail: Bool = true
        
        do {
            
            let results: Array<DBHistoryAddOn> = try context!.fetch(fetchRequest) as! Array<DBHistoryAddOn>
            
            //print(results.count)
            
            if results.count > 0 {
                for  purchaseHistory in results {
                    if (purchaseHistory.accountNumber == SwitchModel.getSelectedAccountNo()!) {
                        let updatedObj = self.updatePurchaseAddOnHistoryList(purchaseHistory, response,  context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createPurchaseAddOnHistoryList(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
                
            } else {
                //add bill history
                let newObj = self.createPurchaseAddOnHistoryList(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        
                        return false // saving success
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return isSaveDidFail
    }
    
    private static func createPurchaseAddOnHistoryList(_ response : PurchaseAddOnHistoryEntity, context: NSManagedObjectContext?) -> DBHistoryAddOn? {
        if let purchaseHistoryEntity = NSEntityDescription.insertNewObject(forEntityName: DataFactory.DATABASE.HISTORY_ADDON, into: context!) as? DBHistoryAddOn {
            
            purchaseHistoryEntity.accountNumber = SwitchModel.getSelectedAccountNo()!
            purchaseHistoryEntity.purchaseHistoryList = response.purchaseHistoryList
            
            return purchaseHistoryEntity
        }
        
        return DBHistoryAddOn()
    }
    
    private static func updatePurchaseAddOnHistoryList(_ purchaseHistoryMO: DBHistoryAddOn,_ purchaseHistoryEntity : PurchaseAddOnHistoryEntity, context: NSManagedObjectContext?) -> DBHistoryAddOn? {
        let updatedPurchaseDetEntity: DBHistoryAddOn = purchaseHistoryMO
        
        updatedPurchaseDetEntity.purchaseHistoryList = purchaseHistoryEntity.purchaseHistoryList
        updatedPurchaseDetEntity.accountNumber = SwitchModel.getSelectedAccountNo()!
        
        return updatedPurchaseDetEntity
    }
    
}
