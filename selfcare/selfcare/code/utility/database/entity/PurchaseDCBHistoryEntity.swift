//
//  PurchaseDCBHistoryEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 15/08/2017.
//
//

import Foundation
import CoreData

class PurchaseDCBHistoryEntity: NSObject, NSCoding {
    
    // Response contant keys
    struct RESPONSE {
        static let kACCOUNT_NUMBER = "accountNumber" // TODO: Update when other API key is ready in Dummy Response
    }
    
    // string constant key for database MO
    struct MODEL_OBJECT {
        static let kCHARGES_DTO_LIST = "mChargesDtoList"
        static let kACCOUNT_NUMBER = "mAccountNumber"
    }
    
    fileprivate var historyPurchaseDCBResponse: [String:AnyObject]? = nil
    
    fileprivate var mChargesDtoList: Array? = [PurchaseDCBDetailEntity]()
    fileprivate var mAccountNumber: String? = ""

    init(_ responseObject : [String : AnyObject]) {
        super.init()
        
        let responseKey = HistoryModel.RESPONSE.self
        
        historyPurchaseDCBResponse = (responseObject[responseKey.kRESPONSE] as? [String:AnyObject])!
        
        self.mAccountNumber = SwitchModel.getSelectedAccountNo()
        
        let purchaseDCBList: Array<AnyObject>? = historyPurchaseDCBResponse?[responseKey.kHISTORY_DCB_LIST] as? Array<AnyObject>
        
        if purchaseDCBList == nil {
            mChargesDtoList = [PurchaseDCBDetailEntity]()
        } else if (purchaseDCBList?.count)! > 0 {
            for dcb in purchaseDCBList! {
                let purchaseDetails: PurchaseDCBDetailEntity = PurchaseDCBDetailEntity.init(dcb as! [String : AnyObject])
                mChargesDtoList?.append(purchaseDetails)
            }
        }  else {
            mChargesDtoList = [PurchaseDCBDetailEntity]()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mChargesDtoList = decoder.decodeObject(forKey: MODEL_OBJECT.kCHARGES_DTO_LIST) as? Array<PurchaseDCBDetailEntity>
        self.mAccountNumber = decoder.decodeObject(forKey: MODEL_OBJECT.kACCOUNT_NUMBER) as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mChargesDtoList, forKey: MODEL_OBJECT.kCHARGES_DTO_LIST)
        coder.encode(mAccountNumber, forKey: MODEL_OBJECT.kACCOUNT_NUMBER)
    }
    
    public var historyDCBList: [PurchaseDCBDetailEntity] {
        get {
            return self.mChargesDtoList!
        }
        set (purchaseAddOnList) {
            self.mChargesDtoList = purchaseAddOnList
        }
        
    }
    
    public var accountNumber: String {
        get {
            guard
                ((self.mAccountNumber as String?) != nil) else{
                    return "-"
            }
            
            return self.mAccountNumber!
        }
        set (accountNmber) {
            self.mAccountNumber = accountNmber
        }
        
    }

}

@objc(DBHistoryDCB)
class DBHistoryDCB : NSManagedObject {
    
    @NSManaged public var chargesDtoList: Array<PurchaseDCBDetailEntity>? // TODO: ChanGe to PurchaseDCBDetailsEntity
    @NSManaged public var accountNumber: String?
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    convenience init(_ response : [PurchaseDCBDetailEntity], entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        self.init(entity: entity, insertInto: context)
        self.chargesDtoList = response
    }
    
    public static func getPurchaseDCBHistory(_ context: NSManagedObjectContext) -> DBHistoryDCB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: DataFactory.DATABASE.HISTORY_DCB, in: context)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@",
                                             PurchaseDCBHistoryEntity.RESPONSE.kACCOUNT_NUMBER,
                                             SwitchModel.getSelectedAccountNo()!)
        
        do {
            
            let results: Array<DBHistoryDCB> = try context.fetch(fetchRequest) as! Array<DBHistoryDCB>
            
            if results.count > 0 {
                for purchaseHistory in results {
                    if (purchaseHistory.accountNumber?.isEqual(SwitchModel.getSelectedAccountNo()))! {
                        return results[0]
                    }
                }
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func savePurchaseDCBHistoryList(_ response : PurchaseDCBHistoryEntity, context: NSManagedObjectContext?) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: DataFactory.DATABASE.HISTORY_DCB, in: context!)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@",
                                             PurchaseDCBHistoryEntity.RESPONSE.kACCOUNT_NUMBER,
                                             SwitchModel.getSelectedAccountNo()!)
        
        var isSaveDidFail: Bool = true
        
        do {
            
            let results: Array<DBHistoryDCB> = try context!.fetch(fetchRequest) as! Array<DBHistoryDCB>
            
            //print(results.count)
            
            if results.count > 0 {
                for  purchaseDCB in results {
                    if (purchaseDCB.accountNumber == SwitchModel.getSelectedAccountNo()!) {
                        let updatedObj = self.updatePurchaseDCBHistoryList(purchaseDCB, response,  context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createPurchaseDCBHistoryList(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
                
            } else {
                //add bill history
                let newObj = self.createPurchaseDCBHistoryList(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        
                        return false // saving success
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return isSaveDidFail
    }
    
    private static func createPurchaseDCBHistoryList(_ response : PurchaseDCBHistoryEntity, context: NSManagedObjectContext?) -> DBHistoryDCB? {
        if let DCBHistoryEntity = NSEntityDescription.insertNewObject(forEntityName: DataFactory.DATABASE.HISTORY_DCB, into: context!) as? DBHistoryDCB {
            
            DCBHistoryEntity.accountNumber = SwitchModel.getSelectedAccountNo()!
            DCBHistoryEntity.chargesDtoList = response.historyDCBList
            
            return DCBHistoryEntity
        }
        
        return DBHistoryDCB()
    }
    
    private static func updatePurchaseDCBHistoryList(_ historyDCBMO: DBHistoryDCB,_ purchaseHistoryEntity : PurchaseDCBHistoryEntity, context: NSManagedObjectContext?) -> DBHistoryDCB? {
        let updatedDCBDetailEntity: DBHistoryDCB = historyDCBMO
        
        updatedDCBDetailEntity.chargesDtoList = purchaseHistoryEntity.historyDCBList
        updatedDCBDetailEntity.accountNumber = SwitchModel.getSelectedAccountNo()!
        
        return updatedDCBDetailEntity
    }
    
}
