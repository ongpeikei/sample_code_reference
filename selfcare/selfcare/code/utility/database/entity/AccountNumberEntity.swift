//
//  AccountNumberEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 18/07/2017.
//
//

import Foundation

class AccountNumberEntity: NSObject, NSCoding {
    
    //RESPONSE
    fileprivate var mAccountNo : String? = ""
    fileprivate var mAccountStatus : String? = ""
    fileprivate var mAccountAutopay : String? = ""
    fileprivate var mAccountAutopayToken : String? = ""
    fileprivate var mAcctProdType : String? = ""
    fileprivate var mAcctFlag : String? = ""
    fileprivate var mAccountLines: Array? = [LineDetailsEntity]()
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        
        self.mAccountNo = responseObject["accountNo"] as? String
        self.mAccountStatus = responseObject["accountStatus"] as? String
        self.mAccountAutopay = responseObject["accountAutopay"] as? String
        self.mAccountAutopayToken = responseObject["accountAutopayToken"] as? String
        self.mAcctFlag = responseObject["acctFlag"] as? String
        self.mAcctProdType = responseObject["acctProdType"] as? String
        
        let lines: Array? = responseObject["accountLines"] as? Array<AnyObject>
        
        if lines == nil {
            mAccountLines = [LineDetailsEntity]()
        } else if (lines?.count)! > 0 {
            for lineObject in lines! {
                /*print("raw line \(lineObject)") -- uncomment when debugging */
                
                let lineDetails: LineDetailsEntity = LineDetailsEntity.init(lineObject as! [String : AnyObject])
                
                /*print("parsed line \(lineDetails)") -- uncomment when debugging */
                mAccountLines?.append(lineDetails)
            }
        }  else {
            mAccountLines = [LineDetailsEntity]()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mAccountLines = decoder.decodeObject(forKey: "mAccountLines") as? Array<LineDetailsEntity>
        
        self.mAccountNo = decoder.decodeObject(forKey: "mAccountNo") as? String
        self.mAccountStatus = decoder.decodeObject(forKey: "mAccountStatus") as? String
        self.mAccountAutopay = decoder.decodeObject(forKey: "mAccountAutopay") as? String
        self.mAccountAutopayToken = decoder.decodeObject(forKey: "mAccountAutopayToken") as? String
        self.mAcctProdType = decoder.decodeObject(forKey: "mAcctProdType") as? String
        self.mAcctFlag = decoder.decodeObject(forKey: "mAcctFlag")as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mAccountLines, forKey: "mAccountLines")
        coder.encode(mAccountNo, forKey: "mAccountNo")
        coder.encode(mAccountStatus, forKey: "mAccountStatus")
        coder.encode(mAccountAutopay, forKey: "mAccountAutopay")
        coder.encode(mAccountAutopayToken, forKey: "mAccountAutopayToken")
        coder.encode(mAcctProdType, forKey: "mAcctProdType")
        coder.encode(mAcctFlag, forKey: "mAcctFlag")
    
        
    }
    
    // entity attribute GETTER & SETTER
    
    public var accountNo: String {
        get {
            guard
                ((self.mAccountNo as String?) != nil) else{
                    self.mAccountNo = ""
                    return self.mAccountNo!
            }
            
            return self.mAccountNo!
        }
        set(accountNo) {
            self.mAccountNo = accountNo
        }
    }
    
    public var accountStatus: String {
        get {
            guard
                ((self.mAccountStatus as String?) != nil) else{
                    self.mAccountStatus = ""
                    return self.mAccountStatus!
            }
            
            return self.mAccountStatus!
        }
        set(accountSttus) {
            self.mAccountStatus = accountSttus
        }
    }
    
    public var accountLines: [LineDetailsEntity] {
        get {
            return self.mAccountLines!
        }
        set(accLines){
            self.mAccountLines = accLines
        }
    }
    
    public var accountAutopay: String {
        get {
            guard
                ((self.mAccountAutopay as String?) != nil) else{
                    self.mAccountAutopay = ""
                    return self.mAccountAutopay!
            }
            
            return self.mAccountAutopay!
        }
        set(accountAutopay) {
            self.mAccountAutopay = accountAutopay
        }
    }
    
    public var accountAutopayToken: String {
        get {
            guard
                ((self.mAccountAutopayToken as String?) != nil) else{
                    self.mAccountAutopayToken = ""
                    return self.mAccountAutopayToken!
            }
            
            return self.mAccountAutopayToken!
        }
        set(accountAutopay) {
            self.mAccountAutopayToken = accountAutopay
        }
    }

    public var acctProdType: String {
        get {
            guard
                ((self.mAcctProdType as String?) != nil) else{
                    self.mAcctProdType = ""
                    return self.mAcctProdType!
            }
            
            return self.mAcctProdType!
        }
        set(accountAutopay) {
            self.mAcctProdType = acctProdType
        }
    }
    
}
