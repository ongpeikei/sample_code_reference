//
//  SimDetailsEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 14/07/2017.
//
//

import Foundation
import CoreData

class SimDetailsEntity: NSObject, NSCoding {
    
    //RESPONSE
    fileprivate var response: [String:AnyObject]? = nil
    fileprivate var simDetailsResponse: [String:AnyObject]? = nil
    
    fileprivate var mAccountNo : String? = ""
    fileprivate var mMsisdn : String? = ""
    fileprivate var mImsi : String? = ""
    fileprivate var mImsi2 : String? = ""
    fileprivate var mIccid : String? = ""
    fileprivate var mPin1 : String? = ""
    fileprivate var mPin2 : String? = ""
    fileprivate var mPuk1 : String? = ""
    fileprivate var mPuk2 : String? = ""
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        self.response = (responseObject["response"] as? [String:AnyObject])!
        self.simDetailsResponse = self.response?["simDetailsResponse"] as? [String : AnyObject]
        
        self.mAccountNo = simDetailsResponse?["accountNo"] as? String
        self.mIccid = simDetailsResponse?["iccid"] as? String
        self.mImsi = simDetailsResponse?["imsi"] as? String
        self.mImsi2 = simDetailsResponse?["imsi2"] as? String
        self.mMsisdn = simDetailsResponse?["msisdn"] as? String
        self.mPin1 = simDetailsResponse?["pin1"] as? String
        self.mPin2 = simDetailsResponse?["pin2"] as? String
        self.mPuk1 = simDetailsResponse?["puk1"] as? String
        self.mPuk2 = simDetailsResponse?["puk2"] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mAccountNo = decoder.decodeObject(forKey: "mAccountNo") as? String
        self.mIccid = decoder.decodeObject(forKey: "mIccid") as? String
        self.mImsi = decoder.decodeObject(forKey: "mImsi") as? String
        self.mImsi2 = decoder.decodeObject(forKey: "mImsi2") as? String
        self.mMsisdn = decoder.decodeObject(forKey: "mMsisdn") as? String
        self.mPin1 = decoder.decodeObject(forKey: "mPin1") as? String
        self.mPin2 = decoder.decodeObject(forKey: "mPin2") as? String
        self.mPuk1 = decoder.decodeObject(forKey: "mPuk1") as? String
        self.mPuk2 = decoder.decodeObject(forKey: "mPuk2") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mAccountNo, forKey: "mAccountNo")
        coder.encode(mIccid, forKey: "mIccid")
        coder.encode(mImsi, forKey: "mImsi")
        coder.encode(mImsi2, forKey: "mImsi2")
        coder.encode(mMsisdn, forKey: "mMsisdn")
        coder.encode(mPin1, forKey: "mPin1")
        coder.encode(mPin2, forKey: "mPin2")
        coder.encode(mPuk1, forKey: "mPuk1")
        coder.encode(mPuk2, forKey: "mPuk2")
    }
    
    // entity attribute GETTER & SETTER
    
    public var accountNo: String {
        
        get {
            guard
                ((self.mAccountNo as String?) != nil) else{
                    return "-"
            }
            return self.mAccountNo!
        }
        set(simAccountNo) {
            self.mAccountNo = simAccountNo
        }
    }
    
    public var iccid: String {
        
        get {
            guard
                ((self.mIccid as String?) != nil) else{
                    return "-"
            }
            return self.mIccid!
        }
        set(simIccid) {
            self.mIccid = simIccid
        }
    }
    
    public var imsi: String {
        
        get {
            guard
                ((self.mImsi as String?) != nil) else{
                    return "-"
            }
            return self.mImsi!
        }
        set(simImsi) {
            self.mImsi = simImsi
        }
    }
    
    public var imsi2: String {
        
        get {
            guard
                ((self.mImsi2 as String?) != nil) else{
                    return "-"
            }
            return self.mImsi2!
        }
        set(simImsi2) {
            self.mImsi2 = simImsi2
        }
    }
    
    public var msisdn: String {
        
        get {
            guard
                ((self.mMsisdn as String?) != nil) else{
                    return "-"
            }
            return self.mMsisdn!
        }
        set(simMsisdn) {
            self.mMsisdn = simMsisdn
        }
    }
    
    public var pin1: String {
        
        get {
            guard
                ((self.mPin1 as String?) != nil) else{
                    return "-"
            }
            return self.mPin1!
        }
        set(simPin1) {
            self.mPin1 = simPin1
        }
    }
    
    public var pin2: String {
        
        get {
            guard
                ((self.mPin2 as String?) != nil) else{
                    return "-"
            }
            return self.mPin2!
        }
        set(simPin2) {
            self.mPin2 = simPin2
        }
    }
    
    public var puk1: String {
        
        get {
            guard
                ((self.mPuk1 as String?) != nil) else{
                    return "-"
            }
            return self.mPuk1!
        }
        set(simPuk1) {
            self.mPuk1 = simPuk1
        }
    }
    
    public var puk2: String {
        
        get {
            guard
                ((self.mPuk2 as String?) != nil) else{
                    return "-"
            }
            return self.mPuk2!
        }
        set(simPuk2) {
            self.mPuk2 = simPuk2
        }
    }
    
}

@objc (SimDetailsDB)
class SimDetailsDB: NSManagedObject {
    
    @NSManaged public var accountNo: String?
    @NSManaged public var iccid: String?
    @NSManaged public var imsi: String?
    @NSManaged public var imsi2: String?
    @NSManaged public var msisdn: String?
    @NSManaged public var pin1: String?
    @NSManaged public var pin2: String?
    @NSManaged public var puk1: String?
    @NSManaged public var puk2: String?
    
    public static func getSimDetails(_ context: NSManagedObjectContext, _ msisdn: String) -> SimDetailsDB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "SimDetailsDB", in: context)
        
        fetchRequest.entity = entityDescription
        
        do {
            let results: Array<SimDetailsDB> = try context.fetch(fetchRequest) as! Array<SimDetailsDB>
            
            if results.count > 0 {
                
                for simDetail in results {
                    if (simDetail.msisdn == msisdn) {
                        return simDetail
                    }
                }
                
                return nil
            } else {
                return nil
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func saveSimDetails(_ response : SimDetailsEntity, context: NSManagedObjectContext?) -> Bool? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "SimDetailsDB", in: context!)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "msisdn", response.msisdn)
        
        var isSaveDidFail: Bool = true
        
        do {
            let results: Array<SimDetailsDB> = try context!.fetch(fetchRequest) as! Array<SimDetailsDB>
            
            //print(results.count) //DB result count for debugging
            
            if results.count > 0 {
                for  SimDetailsDB in results {
                    if (SimDetailsDB.msisdn == response.msisdn) {
                        let updatedObj = self.updateSimDetails(SimDetailsDB, response,  context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createSimDetails(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
            } else {
                //add customer details
                let newObj = self.createSimDetails(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        return false
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return isSaveDidFail
        
    }
    
    private static func createSimDetails(_ response : SimDetailsEntity, context: NSManagedObjectContext?) -> SimDetailsDB? {
        if let simDetailsEntity = NSEntityDescription.insertNewObject(forEntityName: "SimDetailsDB", into: context!) as? SimDetailsDB {
            simDetailsEntity.accountNo = response.accountNo
            simDetailsEntity.iccid = response.iccid
            simDetailsEntity.imsi = response.imsi
            simDetailsEntity.imsi2 = response.imsi2
            simDetailsEntity.msisdn = response.msisdn
            simDetailsEntity.pin1 = response.pin1
            simDetailsEntity.pin2 = response.pin2
            simDetailsEntity.puk1 = response.puk1
            simDetailsEntity.puk2 = response.puk2
            
            return simDetailsEntity
        }
        
        return SimDetailsDB()
    }
    
    private static func updateSimDetails(_ simDetailsMO: SimDetailsDB,_ simDetailsEntity : SimDetailsEntity, context: NSManagedObjectContext?) -> SimDetailsDB? {
        let updatedSimDetailsEntity: SimDetailsDB = simDetailsMO
        
        updatedSimDetailsEntity.accountNo = simDetailsEntity.accountNo
        updatedSimDetailsEntity.iccid = simDetailsEntity.iccid
        updatedSimDetailsEntity.imsi = simDetailsEntity.imsi
        updatedSimDetailsEntity.imsi2 = simDetailsEntity.imsi2
        updatedSimDetailsEntity.msisdn = simDetailsEntity.msisdn
        updatedSimDetailsEntity.pin1 = simDetailsEntity.pin1
        updatedSimDetailsEntity.pin2 = simDetailsEntity.pin2
        updatedSimDetailsEntity.puk1 = simDetailsEntity.puk1
        updatedSimDetailsEntity.puk2 = simDetailsEntity.puk2
        
        return updatedSimDetailsEntity
    }
    
}
