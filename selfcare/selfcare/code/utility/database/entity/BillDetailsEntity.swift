//
//  BillDetailsEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 17/07/2017.
//
//

import Foundation

class BillDetailsEntity: NSObject, NSCoding {
    //bill details
    fileprivate var mGstAmt: String? = ""
    fileprivate var mGstBaseAmt: String? = ""
    fileprivate var mAdjustmentAmt: String? = ""
    fileprivate var mBillDate: String? = ""
    fileprivate var mBillNumber: String? = ""
    fileprivate var mBillStatus: String? = ""
    fileprivate var mBillingCycle:String? = ""
    fileprivate var mCurrentChargeAmt: String? = ""
    fileprivate var mDueDate: String? = ""
    fileprivate var mPaymentAmt:String? = ""
    fileprivate var mPreviousBalAmt:String? = ""
    fileprivate var mRebateAmt: String? = ""
    fileprivate var mRoundingAmount: String? = ""
    fileprivate var mTotalAmt:String? = ""
    fileprivate var mLatestPaymentDetailList: Array? = [LatestPaymentDetailEntity]()
    
    override init() {
        super.init()
    }
    
    init(_ responseObject : [String : AnyObject]) {
        super.init()
        
        self.mGstAmt = ((responseObject["GSTAmt"]) != nil) ? responseObject["GSTAmt"] as? String : ""
        self.mGstBaseAmt = ((responseObject["GSTBaseAmt"]) != nil) ? responseObject["GSTBaseAmt"] as? String : ""
        self.mAdjustmentAmt = ((responseObject["adjustmentAmt"]) != nil) ? responseObject["adjustmentAmt"] as? String : ""
        self.mBillDate = ((responseObject["billDate"]) != nil) ? responseObject["billDate"] as? String : ""
        self.mBillNumber = ((responseObject["billNumber"]) != nil) ? responseObject["billNumber"] as? String : ""
        self.mBillStatus = ((responseObject["billStatus"]) != nil) ? responseObject["billStatus"] as? String : ""
        self.mBillingCycle = ((responseObject["billCycle"]) != nil) ? responseObject["billCycle"] as? String : ""
        self.mCurrentChargeAmt = ((responseObject["currentChargeAmt"]) != nil) ? responseObject["currentChargeAmt"] as? String : ""
        self.mDueDate = ((responseObject["dueDate"]) != nil) ? responseObject["dueDate"] as? String : ""
        self.mPaymentAmt = ((responseObject["paymentAmt"]) != nil) ? responseObject["paymentAmt"] as? String : ""
        self.mPreviousBalAmt = ((responseObject["previousBalAmt"]) != nil) ? responseObject["previousBalAmt"] as? String : ""
        self.mRebateAmt = ((responseObject["rebateAmt"]) != nil) ? responseObject["rebateAmt"] as? String : ""
        self.mRoundingAmount = ((responseObject["roundingAmount"]) != nil) ? responseObject["roundingAmount"] as? String : ""
        self.mTotalAmt = ((responseObject["totalAmt"]) != nil) ? responseObject["totalAmt"] as? String : ""
        
        
        let latestPaymentDetailsArr:Array<AnyObject>? = responseObject["latestPaymentDetail"] as? Array<AnyObject>
        
        if latestPaymentDetailsArr == nil {
            mLatestPaymentDetailList = [LatestPaymentDetailEntity]()
        } else if latestPaymentDetailsArr != nil {
            for latestPaymentDetailObj in latestPaymentDetailsArr! {
                
                let latestPaymentDetail: LatestPaymentDetailEntity = LatestPaymentDetailEntity.init(latestPaymentDetailObj as! [String : AnyObject])
                mLatestPaymentDetailList?.append(latestPaymentDetail)
            }
        }  else {
            mLatestPaymentDetailList = [LatestPaymentDetailEntity]()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mGstAmt = decoder.decodeObject(forKey: "mGstAmt") as? String
        self.mGstBaseAmt = decoder.decodeObject(forKey: "mGstBaseAmt") as? String
        self.mAdjustmentAmt = decoder.decodeObject(forKey: "mAdjustmentAmt") as? String
        self.mBillDate = decoder.decodeObject(forKey: "mBillDate") as? String
        self.mBillNumber = decoder.decodeObject(forKey: "mBillNumber") as? String
        self.mBillStatus = decoder.decodeObject(forKey: "mBillStatus") as? String
        self.mBillingCycle = decoder.decodeObject(forKey: "mBillingCycle") as? String
        self.mCurrentChargeAmt = decoder.decodeObject(forKey: "mCurrentChargeAmt") as? String
        self.mDueDate = decoder.decodeObject(forKey: "mDueDate") as? String
        self.mPaymentAmt = decoder.decodeObject(forKey: "mPaymentAmt") as? String
        self.mPreviousBalAmt = decoder.decodeObject(forKey: "mPreviousBalAmt") as? String
        self.mRebateAmt = decoder.decodeObject(forKey: "mRebateAmt") as? String
        self.mRoundingAmount = decoder.decodeObject(forKey: "mRoundingAmount") as? String
        self.mTotalAmt = decoder.decodeObject(forKey: "mTotalAmt") as? String
        self.mLatestPaymentDetailList = decoder.decodeObject(forKey: "mLatestPaymentDetailList") as? Array<LatestPaymentDetailEntity>
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mGstAmt, forKey: "mGstAmt")
        coder.encode(mGstBaseAmt, forKey: "mGstBaseAmt")
        coder.encode(mAdjustmentAmt, forKey: "mAdjustmentAmt")
        coder.encode(mBillDate, forKey: "mBillDate")
        coder.encode(mBillNumber, forKey: "mBillNumber")
        coder.encode(mBillStatus, forKey: "mBillStatus")
        coder.encode(mBillingCycle, forKey: "mBillingCycle")
        coder.encode(mCurrentChargeAmt, forKey: "mCurrentChargeAmt")
        coder.encode(mDueDate, forKey: "mDueDate")
        coder.encode(mPaymentAmt, forKey: "mPaymentAmt")
        coder.encode(mPreviousBalAmt, forKey: "mPreviousBalAmt")
        coder.encode(mRebateAmt, forKey: "mRebateAmt")
        coder.encode(mRoundingAmount, forKey: "mRoundingAmount")
        coder.encode(mTotalAmt, forKey: "mTotalAmt")
        coder.encode(mLatestPaymentDetailList, forKey: "mLatestPaymentDetailList")
    }
    
    public var gstAmt: String {
        get {
            guard
                ((self.mGstAmt as String?) != nil) else{
                    self.mGstAmt = "0.00"
                    return self.mGstAmt!
            }
            return self.mGstAmt != "" ? self.mGstAmt! : "0.00"
        }
        set (gstAmount) {
            self.mGstAmt = gstAmount
        }
    }

    public var gstBaseAmt: String {
        get {
            guard
                ((self.mGstBaseAmt as String?) != nil) else{
                    self.mGstBaseAmt = "0.00"
                    return self.mGstBaseAmt!
            }
            return self.mGstBaseAmt != "" ? self.mGstBaseAmt! : "0.00"
        }
        set (gstBseAmt) {
            self.mGstBaseAmt = gstBseAmt
        }
    }
    
    public var adjustmentAmt: String {
        get {
            guard
                ((self.mAdjustmentAmt as String?) != nil) else{
                    self.mAdjustmentAmt = "0.00"
                    return self.mAdjustmentAmt!
            }
            return self.mAdjustmentAmt != "" ? self.mAdjustmentAmt! : "0.00"
        }
        set (adjtmentAmount) {
            self.mAdjustmentAmt = adjtmentAmount
        }
    }
    
    public var billDate: String {
        get {
            guard
                ((self.mBillDate as String?) != nil) else{
                    return "-"
            }
            return self.mBillDate!
        }
        set (billDte) {
            self.mBillDate = billDte
        }
    }
    
    public var billNumber: String {
        get {
            guard
                ((self.mBillNumber as String?) != nil) else{
                    return "-"
            }
            return self.mBillNumber!
        }
        set (billNo) {
            self.mBillNumber = billNo
        }
    }
    
    public var billStatus: String {
        get {
            guard
                ((self.mBillStatus as String?) != nil) else{
                    self.mBillStatus = ""
                    return self.mBillStatus!
            }
            return self.mBillStatus!
        }
        set (billStat) {
            self.mBillStatus = billStat
        }
    }
    
    public var billingCycle: String {
        get {
            guard
                ((self.mBillingCycle as String?) != nil) else{
                    return "-"
            }
            return self.mBillingCycle!
        }
        set(billngCycle) {
            self.mBillingCycle = billngCycle
        }
    }
    
    public var currentChargeAmt: String {
        get {
            guard
                ((self.mCurrentChargeAmt as String?) != nil) else{
                    self.mCurrentChargeAmt = "0.00"
                    return self.mCurrentChargeAmt!
            }
            return self.mCurrentChargeAmt != "" ? self.mCurrentChargeAmt! : "0.00"
        }
        set (currentChargeAmount) {
            self.mCurrentChargeAmt = currentChargeAmount
        }
    }
    
    public var dueDate: String {
        get {
            guard
                ((self.mDueDate as String?) != nil) else{
                    return "-"
            }
            return self.mDueDate!
        }
        set (dueDte) {
            self.mDueDate = dueDte
        }
    }
    
    public var paymentAmt: String {
        get {
            guard
                ((self.mPaymentAmt as String?) != nil) else{
                    self.mPaymentAmt = "0.00"
                    return self.mPaymentAmt!
            }
            return self.mPaymentAmt != "" ? self.mPaymentAmt! : "0.00"
        }
        set (paymentAmount) {
            self.mPaymentAmt = paymentAmount
        }
    }
    
    public var previousBalAmt: String {
        get {
            guard
                ((self.mPreviousBalAmt as String?) != nil) else{
                    self.mPreviousBalAmt = "0.00"
                    return self.mPreviousBalAmt!
            }
            return self.mPreviousBalAmt != "" ? self.mPreviousBalAmt! : "0.00"
        }
        set (prevBalAmt) {
            self.mPreviousBalAmt = prevBalAmt
        }
    }
    
    public var rebateAmt: String {
        get {
            guard
                ((self.mRebateAmt as String?) != nil) else{
                    self.mRebateAmt = "0.00"
                    return self.mRebateAmt!
            }
            return self.mRebateAmt != "" ? self.mRebateAmt! : "0.00"
        }
        set (rebteAmt) {
            self.mRebateAmt = rebteAmt
        }
    }

    public var roundingAmount: String {
        get {
            guard
                ((self.mRoundingAmount as String?) != nil) else{
                    self.mRoundingAmount = "0.00"
                    return self.mRoundingAmount!
            }
            return self.mRoundingAmount != "" ? self.mRoundingAmount! : "0.00"
        }
        set (rndingAmount) {
            self.mRoundingAmount = rndingAmount
        }
    }
    
    public var totalAmt: String {
        get {
            guard
                ((self.mTotalAmt as String?) != nil) else{
                    self.mTotalAmt = "0.00"
                    return self.mTotalAmt!
            }
            return self.mTotalAmt != "" ? self.mTotalAmt! : "0.00"
        }
        set (totalAmount) {
            self.mTotalAmt = totalAmount
        }
    }
    
    public var latestPaymentDetailList: [LatestPaymentDetailEntity] {
        get {
            return self.mLatestPaymentDetailList!
        }
        set (latestPaymentDetList) {
            self.mLatestPaymentDetailList = latestPaymentDetList
        }
    }
    
}
