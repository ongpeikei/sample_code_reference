//
//  CreditUnbilledBalanceEntity.swift
//  selfcare
//
//
//

import Foundation
import CoreData

class CreditUnbilledBalanceEntity: NSObject, NSCoding {
    fileprivate var mCreditUnbilledBalanceResponse: [String:AnyObject]? = nil
    
    fileprivate var mTotalCreditLimit:String? = ""
    fileprivate var mCreditLimitUsed:String? = ""
    fileprivate var mCreditLimitLeft:String? = ""
    fileprivate var mTotalUnbilledAmount:String? = ""
    fileprivate var mDepositValue:String? = ""
    fileprivate var mAcctCode: String? = ""
    fileprivate var mUnbilledLines:Array<UnbilledLineEntity>? = [UnbilledLineEntity]()
    
    override init() {
        super.init()
    }
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        self.mCreditUnbilledBalanceResponse = responseObject["response"] as? [String : AnyObject]
        
        self.mAcctCode = SwitchModel.getSelectedAccountNo()
        self.mTotalCreditLimit = ((self.mCreditUnbilledBalanceResponse?["totalCreditLimit"]) != nil) ? self.mCreditUnbilledBalanceResponse?["totalCreditLimit"] as? String : "0.00"
        self.mCreditLimitUsed = ((self.mCreditUnbilledBalanceResponse?["creditLimitUsed"]) != nil) ? self.mCreditUnbilledBalanceResponse?["creditLimitUsed"] as? String : "0.00"
        self.mCreditLimitLeft = ((self.mCreditUnbilledBalanceResponse?["creditLimitLeft"]) != nil) ? self.mCreditUnbilledBalanceResponse?["creditLimitLeft"] as? String : "0.00"
        self.mTotalUnbilledAmount = ((self.mCreditUnbilledBalanceResponse?["totalUnbilledAmount"]) != nil) ? self.mCreditUnbilledBalanceResponse?["totalUnbilledAmount"] as? String : "0.00"
        self.mDepositValue = ((self.mCreditUnbilledBalanceResponse?["depositValue"]) != nil) ? self.mCreditUnbilledBalanceResponse?["depositValue"] as? String : "0.00"
        
        // parse unbilled list
        let unbilledList:Array? = mCreditUnbilledBalanceResponse?["unbilledLinesList"] as? Array<[String : AnyObject]>
        
        if unbilledList == nil {
            mUnbilledLines = [UnbilledLineEntity]()
        } else if (unbilledList?.count)! > 0 {
            for unbilled in unbilledList! {
                
                let unbilledDetails: UnbilledLineEntity = UnbilledLineEntity.init(unbilled)
                mUnbilledLines?.append(unbilledDetails)
            }
        } else {
            mUnbilledLines = [UnbilledLineEntity]()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mUnbilledLines = decoder.decodeObject(forKey: "mUnbilledLines") as? Array<UnbilledLineEntity>
        self.mTotalCreditLimit = decoder.decodeObject(forKey: "mTotalCreditLimit") as? String
        self.mCreditLimitUsed = decoder.decodeObject(forKey: "mCreditLimitUsed") as? String
        self.mCreditLimitLeft = decoder.decodeObject(forKey: "mCreditLimitLeft") as? String
        self.mTotalUnbilledAmount = decoder.decodeObject(forKey: "mTotalUnbilledAmount") as? String
        self.mDepositValue = decoder.decodeObject(forKey: "mDepositValue") as? String
        self.mAcctCode = decoder.decodeObject(forKey: "mAcctCode") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mUnbilledLines, forKey: "mUnbilledLines")
        coder.encode(mTotalCreditLimit, forKey: "mTotalCreditLimit")
        coder.encode(mCreditLimitUsed, forKey: "mCreditLimitUsed")
        coder.encode(mCreditLimitLeft, forKey: "mCreditLimitLeft")
        coder.encode(mTotalUnbilledAmount, forKey: "mTotalUnbilledAmount")
        coder.encode(mDepositValue, forKey: "mDepositValue")
        coder.encode(mAcctCode, forKey: "mAcctCode")
    }

    
    public var totalCreditLimit: String {
    
        get {
            guard
                ((self.mTotalCreditLimit as String?) != nil) else{
                    return "-"
            }
            return self.mTotalCreditLimit!
        }
        set(ttlCreditLimit) {
            self.mTotalCreditLimit = ttlCreditLimit
        }
    
    }
    
    public var creditLimitUsed: String {
        
        get {
            guard
                ((self.mCreditLimitUsed as String?) != nil) else{
                    return "-"
            }
            return self.mCreditLimitUsed!
        }
        set(crditLimitUsed) {
            self.mCreditLimitUsed = crditLimitUsed
        }
        
    }
    
    public var creditLimitLeft: String {
        
        get {
            guard
                ((self.mCreditLimitLeft as String?) != nil) else{
                    return "-"
            }
            return self.mCreditLimitLeft!
        }
        set(crdtLimitLeft) {
            self.mCreditLimitLeft = crdtLimitLeft
        }
        
    }
    
    public var totalUnbilledAmount: String {
        
        get {
            guard
                ((self.mTotalUnbilledAmount as String?) != nil) else{
                    return "-"
            }
            return self.mTotalUnbilledAmount != "" ? self.mTotalUnbilledAmount! : "0.00"
        }
        set(ttlUnbilledAmt) {
            self.mTotalUnbilledAmount = ttlUnbilledAmt
        }
        
    }
    
    public var unbilledLinesList: [UnbilledLineEntity] {
        
        get {
            return self.mUnbilledLines!
        }
        set(arrUnbilldLineObj) {
            self.mUnbilledLines = arrUnbilldLineObj
        }
    }
    
    public var depositValue: String {
        
        get {
            guard
                ((self.mDepositValue as String?) != nil) else{
                    return "-"
            }
            return self.mDepositValue != "" ? self.mDepositValue! : "0.00"
        }
        set(depVal) {
            self.mDepositValue = depVal
        }
        
    }
}

@objc(CreditUnbilledDB)
class CreditUnbilledDB : NSManagedObject {
    
    @NSManaged public var unbilledLinesList: [UnbilledLineEntity]?
    @NSManaged public var acctCode: String?
    @NSManaged public var creditLimitLeft: String?
    @NSManaged public var creditLimitUsed: String?
    @NSManaged public var depositValue: String?
    @NSManaged public var totalCreditLimit: String?
    @NSManaged public var totalUnbilledAmount: String?
    
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    convenience init(_ response : CreditUnbilledBalanceEntity, entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        self.init(entity: entity, insertInto: context)
        self.unbilledLinesList = response.unbilledLinesList
    }
    
    public static func getCreditUnbilledBalance(_ context: NSManagedObjectContext) -> CreditUnbilledDB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "CreditUnbilledDB", in: context)
        
        fetchRequest.entity = entityDescription
        //fetchRequest.predicate = NSPredicate(format: "%K = %@", "msisdn", response.msisdn)
        
        do {
            
            let results: Array<CreditUnbilledDB> = try context.fetch(fetchRequest) as! Array<CreditUnbilledDB>
            
            if results.count > 0 {
                for i in (0...results.count - 1).reversed() {
                    let creditUnbilledBal = results[i]
                    if (creditUnbilledBal.acctCode?.isEqual(SwitchModel.getSelectedAccountNo()))! {
                        return creditUnbilledBal
                    }
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func saveCreditUnbilledBalance(_ response : CreditUnbilledBalanceEntity, context: NSManagedObjectContext?) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "CreditUnbilledDB", in: context!)
        
        fetchRequest.entity = entityDescription
//        fetchRequest.predicate = NSPredicate(format: "%K = %@", "msisdn", response.msisdn)
        
        var isSaveDidFail: Bool = true
        
        do {
            
            let results: Array<CreditUnbilledDB> = try context!.fetch(fetchRequest) as! Array<CreditUnbilledDB>
            
            //print(results.count)
            
            if results.count > 0 {
                for  creditUnbilledDB in results {
                    if (creditUnbilledDB.acctCode == SwitchModel.getSelectedAccountNo()) {
                        let updatedObj = self.updateCreditUnbilledBalance(creditUnbilledDB, response,  context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createCreditUnbilledBalance(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
                
            } else {
                let newObj = self.createCreditUnbilledBalance(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        
                        return false // saving success
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return isSaveDidFail
    }
    
    private static func createCreditUnbilledBalance(_ response : CreditUnbilledBalanceEntity, context: NSManagedObjectContext?) -> CreditUnbilledDB? {
        if let creditUnbilledEntity = NSEntityDescription.insertNewObject(forEntityName: "CreditUnbilledDB", into: context!) as? CreditUnbilledDB {
            
            creditUnbilledEntity.acctCode = SwitchModel.getSelectedAccountNo()
            creditUnbilledEntity.totalCreditLimit = response.totalCreditLimit
            creditUnbilledEntity.totalUnbilledAmount = response.totalUnbilledAmount
            creditUnbilledEntity.creditLimitUsed = response.creditLimitUsed
            creditUnbilledEntity.creditLimitLeft = response.creditLimitLeft
            creditUnbilledEntity.depositValue = response.depositValue
            creditUnbilledEntity.unbilledLinesList = response.unbilledLinesList
            
            return creditUnbilledEntity
        }
        
        return CreditUnbilledDB()
    }
    
    private static func updateCreditUnbilledBalance(_ creditUnbilledMO: CreditUnbilledDB,_ creditUnbilledEntity : CreditUnbilledBalanceEntity, context: NSManagedObjectContext?) -> CreditUnbilledDB? {
        let updatedCreditUnbilledEntity: CreditUnbilledDB = creditUnbilledMO
        
        updatedCreditUnbilledEntity.acctCode = SwitchModel.getSelectedAccountNo()
        updatedCreditUnbilledEntity.totalCreditLimit = creditUnbilledEntity.mTotalCreditLimit
        updatedCreditUnbilledEntity.totalUnbilledAmount = creditUnbilledEntity.mTotalUnbilledAmount
        updatedCreditUnbilledEntity.creditLimitLeft = creditUnbilledEntity.mCreditLimitLeft
        updatedCreditUnbilledEntity.creditLimitUsed = creditUnbilledEntity.mCreditLimitUsed
        updatedCreditUnbilledEntity.depositValue = creditUnbilledEntity.mDepositValue
        updatedCreditUnbilledEntity.unbilledLinesList = creditUnbilledEntity.unbilledLinesList
        
        return updatedCreditUnbilledEntity
    }
    
}
