//
//  CreditLimitEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 18/07/2017.
//
//

import Foundation

class CreditLimitEntity : NSObject, NSCoding {
    
    fileprivate var mCreditLimitId : String? = ""
    fileprivate var mCreditLimitDepositAmount : String? = ""
    fileprivate var mCreditLimitUpgradeAmount : String? = ""

    init(_ responseObject : AnyObject){
        super.init()
        
        self.mCreditLimitId = responseObject["creditLimitId"] as? String
        self.mCreditLimitDepositAmount = responseObject["creditLimitDepositAmount"] as? String
        self.mCreditLimitUpgradeAmount = responseObject["creditLimitUpgradeAmount"] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mCreditLimitId = decoder.decodeObject(forKey: "mCreditLimitId") as? String
        self.mCreditLimitDepositAmount = decoder.decodeObject(forKey: "mCreditLimitDepositAmount") as? String
        self.mCreditLimitUpgradeAmount = decoder.decodeObject(forKey: "mCreditLimitUpgradeAmount") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mCreditLimitId, forKey: "mCreditLimitId")
        coder.encode(mCreditLimitDepositAmount, forKey: "mCreditLimitDepositAmount")
        coder.encode(mCreditLimitUpgradeAmount, forKey: "mCreditLimitUpgradeAmount")
    }

    public var creditLimitId: String {
        
        get {
            guard
                ((self.mCreditLimitId as String?) != nil) else{
                    return "-"
            }
            return self.mCreditLimitId!
        }
        set(strCreditLimitId) {
            self.mCreditLimitId = strCreditLimitId
        }
    }
    
    public var creditLimitDepositAmount: String {
        
        get {
            guard
                ((self.mCreditLimitDepositAmount as String?) != nil) else{
                    return "-"
            }
            return self.mCreditLimitDepositAmount!
        }
        set(strCreditLimitDepositAmount) {
            self.mCreditLimitDepositAmount = strCreditLimitDepositAmount
        }
    }
    
    public var creditLimitUpgradeAmount: String {
        
        get {
            guard
                ((self.mCreditLimitUpgradeAmount as String?) != nil) else{
                    return "-"
            }
            return self.mCreditLimitUpgradeAmount!
        }
        set(strCreditLimitUpgradeAmount) {
            self.mCreditLimitUpgradeAmount = strCreditLimitUpgradeAmount
        }
    }
        
}


