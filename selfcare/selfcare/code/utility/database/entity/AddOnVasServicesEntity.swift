//
//  AddOnVasServicesEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 20/07/2017.
//
//

import Foundation

class AddOnVasServicesEntity: NSObject, NSCoding {
        
    fileprivate var mName:String? = ""
    fileprivate var mCode:String? = ""
    fileprivate var mDescription:String? = ""
    fileprivate var mPrice:String? = ""
    fileprivate var mChargeType:String? = ""
    
    fileprivate var mCategory:String? = ""
    fileprivate var mDepositRequired:Bool? = false
    fileprivate var mDepositItem:String? = ""
    fileprivate var mDepositValue:String? = ""
    fileprivate var mServiceType:String? = ""
    
    fileprivate var mIsActivated:String? = ""
    fileprivate var mKeepDisplay:String? = ""
    fileprivate var mButtonDisplay:String? = ""
    fileprivate var mBankTxn:String? = ""
    
    fileprivate var mExpiryDate:String? = ""
    fileprivate var mEffDate:String? = ""
    fileprivate var mO_className:String? = ""
    
    override init() {
        super.init()
    }

    init(_ response: [String: AnyObject]) {
        super.init()
        self.mName = ((response["name"]) != nil) ? response["name"] as? String  : ""
        self.mCode = ((response["code"]) != nil) ? response["code"] as? String  : ""
        self.mDescription = ((response["description"]) != nil) ? response["description"] as? String  : "-"
        self.mPrice = ((response["price"]) != nil) ? response["price"] as? String  : ""
        self.mChargeType = ((response["chargeType"]) != nil) ? response["chargeType"] as? String : ""
        self.mCategory = ((response["category"]) != nil) ? response["category"] as? String : ""
        self.mDepositRequired = ((response["depositRequired"]) != nil) ? response["depositRequired"] as? Bool : false
        self.mDepositItem = ((response["depositItem"]) != nil) ? response["depositItem"] as? String : ""
        self.mDepositValue = ((response["depositValue"]) != nil) ? response["depositValue"] as? String : "0.00"
        self.mServiceType = ((response["serviceType"]) != nil) ? response["serviceType"] as? String : ""
        self.mIsActivated = ((response["isActivated"]) != nil) ? response["isActivated"] as? String : "0"
        
        self.mKeepDisplay = ((response["keepDisplay"]) != nil) ? response["keepDisplay"] as? String : ""
        self.mButtonDisplay = ((response["buttonDisplay"]) != nil) ? response["buttonDisplay"] as? String : ""
        self.mBankTxn = ((response["bankTxn"]) != nil) ? response["bankTxn"] as? String : ""
        self.mExpiryDate = ((response["expiryDate"]) != nil) ? response["expiryDate"] as? String : ""
        self.mEffDate = ((response["effDate"]) != nil) ? response["effDate"] as? String : ""
        self.mO_className = ((response["o_className"]) != nil) ? response["o_className"] as? String : ""
    }
    
    required init(coder decoder: NSCoder) {
        self.mName = decoder.decodeObject(forKey: "mName") as? String
        self.mCode = decoder.decodeObject(forKey: "mCode") as? String
        self.mDescription = decoder.decodeObject(forKey: "mDescription") as? String
        self.mPrice = decoder.decodeObject(forKey: "mPrice") as? String
        self.mChargeType = decoder.decodeObject(forKey: "mChargeType") as? String
        self.mCategory = decoder.decodeObject(forKey: "mCategory") as? String
        self.mDepositRequired = decoder.decodeObject(forKey: "mDepositRequired") as? Bool
        self.mDepositItem = decoder.decodeObject(forKey: "mDepositItem") as? String
        self.mDepositValue = decoder.decodeObject(forKey: "mDepositValue") as? String
        self.mServiceType = decoder.decodeObject(forKey: "mServiceType") as? String
        self.mIsActivated = decoder.decodeObject(forKey: "mIsActivated") as? String
        self.mKeepDisplay = decoder.decodeObject(forKey: "mKeepDisplay") as? String
        self.mButtonDisplay = decoder.decodeObject(forKey: "mButtonDisplay") as? String
        self.mBankTxn = decoder.decodeObject(forKey: "mBankTxn") as? String
        self.mExpiryDate = decoder.decodeObject(forKey: "mExpiryDate") as? String
        self.mEffDate = decoder.decodeObject(forKey: "mEffDate") as? String
        self.mO_className = decoder.decodeObject(forKey: "mO_className") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mName, forKey: "mName")
        coder.encode(mCode, forKey: "mCode")
        coder.encode(mDescription, forKey: "mDescription")
        coder.encode(mPrice, forKey: "mPrice")
        coder.encode(mChargeType, forKey: "mChargeType")
        coder.encode(mCategory, forKey: "mCategory")
        coder.encode(mDepositRequired, forKey: "mDepositRequired")
        coder.encode(mDepositItem, forKey: "mDepositItem")
        coder.encode(mDepositValue, forKey: "mDepositValue")
        coder.encode(mServiceType, forKey: "mServiceType")
        coder.encode(mIsActivated, forKey: "mIsActivated")
        coder.encode(mKeepDisplay, forKey: "mKeepDisplay")
        coder.encode(mButtonDisplay, forKey: "mButtonDisplay")
        coder.encode(mBankTxn, forKey: "mBankTxn")
        coder.encode(mExpiryDate, forKey: "mExpiryDate")
        coder.encode(mEffDate, forKey: "mEffDate")
        coder.encode(mO_className, forKey: "mO_className")
    }
    
    public var name: String? {
        
        get {
            guard
                ((self.mName as String?) != nil) else{
                    self.mName = ""
                    return self.mName!
            }
            return self.mName
        }
        set(nme) {
            self.mName = nme
        }
        
    }
    
    public var code: String? {
        
        get {
            guard
                ((self.mCode as String?) != nil) else{
                    self.mCode = ""
                    return self.mCode!
            }
            return self.mCode
        }
        set(cde) {
            self.mCode = cde
        }
        
    }
    
    public var descrption: String? {
        
        get {
            guard
                ((self.mDescription as String?) != nil) else{
                    self.mDescription = ""
                    return self.mDescription!
            }
            return self.mDescription
        }
        set(desc) {
            self.mDescription = desc
        }
        
    }
    
    public var price: String? {
        
        get {
            guard
                ((self.mPrice as String?) != nil) else{
                    self.mPrice = ""
                    return self.mPrice!
            }
            return self.mPrice
        }
        set(prce) {
            self.mPrice = prce
        }
        
    }
    
    public var chargeType: String? {
        
        get {
            if (mChargeType != nil) {
                return self.mChargeType?.replacingOccurrences(of: "_", with: " ")
            } else {
                return ""
            }
            
        }
        set(chrgeType) {
            self.mChargeType = chrgeType
        }
        
    }
    
    public var category: String? {
        
        get {
            guard
                ((self.mCategory as String?) != nil) else{
                    self.mCategory = ""
                    return self.mCategory!
            }
            return self.mCategory
        }
        set(categry) {
            self.mCategory = categry
        }
        
    }
    
    public var depositRequired: Bool? {
        
        get {
            return self.mDepositRequired
        }
        set(depostRequired) {
            self.mDepositRequired = depostRequired
        }
        
    }
    
    public var depositItem: String? {
        
        get {
            guard
                ((self.mDepositItem as String?) != nil) else{
                    self.mDepositItem = ""
                    return self.mDepositItem!
            }
            return self.mDepositItem
        }
        set(depostItem) {
            self.mDepositItem = depostItem
        }
        
    }
    
    
    public var depositValue: String? {
        
        get {
            guard
                ((self.mDepositValue as String?) != nil) else{
                    self.mDepositValue = ""
                    return self.mDepositValue!
            }
            return self.mDepositValue
        }
        set(depostValue) {
            self.mDepositValue = depostValue
        }
        
    }
    
    public var serviceType: String? {
        
        get {
            guard
                ((self.mServiceType as String?) != nil) else{
                    self.mServiceType = ""
                    return self.mServiceType!
            }
            return self.mServiceType
        }
        set(servceType) {
            self.mServiceType = servceType
        }
        
    }
    
    public var isActivated: String? {
        
        get {
            guard
                ((self.mIsActivated as String?) != nil) else{
                    self.mIsActivated = ""
                    return self.mIsActivated!
            }
            return self.mIsActivated
        }
        set(isActvated) {
            self.mIsActivated = isActvated
        }
        
    }
    
    public var keepDisplay: String? {
        
        get {
            guard
                ((self.mKeepDisplay as String?) != nil) else{
                    self.mKeepDisplay = ""
                    return self.mKeepDisplay!
            }
            return self.mKeepDisplay
        }
        set(keepDsplay) {
            self.mKeepDisplay = keepDsplay
        }
        
    }
    
    public var buttonDisplay: String? {
        
        get {
            guard
                ((self.mButtonDisplay as String?) != nil) else{
                    self.mButtonDisplay = ""
                    return self.mButtonDisplay!
            }
            return self.mButtonDisplay
        }
        set(buttonDsplay) {
            self.mButtonDisplay = buttonDsplay
        }
        
    }
    
    public var bankTxn: String? {
        
        get {
            guard
                ((self.mBankTxn as String?) != nil) else{
                    self.mBankTxn = ""
                    return self.mBankTxn!
            }
            return self.mBankTxn
        }
        set(bnkTxn) {
            self.mBankTxn = bnkTxn
        }
        
    }
    
    public var expiryDate: String? {
        
        get {
            guard
                ((self.mExpiryDate as String?) != nil) else{
                    self.mExpiryDate = ""
                    return self.mExpiryDate!
            }
            return self.mExpiryDate
        }
        set(expryDate) {
            self.mExpiryDate = expryDate
        }
        
    }
    
    public var effDate: String? {
        
        get {
            guard
                ((self.mEffDate as String?) != nil) else{
                    self.mEffDate = ""
                    return self.mEffDate!
            }
            return self.mEffDate
        }
        set(effDte) {
            self.mEffDate = effDte
        }
        
    }
    
    public var o_classname: String? {
        
        get {
            guard
                ((self.mO_className as String?) != nil) else{
                    self.mO_className = ""
                    return self.mO_className!
            }
            return self.mO_className
        }
        set(oClassName) {
            self.mO_className = oClassName
        }
        
    }
    
}
