//
//  BillHistoryEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 19/07/2017.
//
//

import Foundation
import CoreData

class BillHistoryEntity: NSObject, NSCoding {
    fileprivate var billHistoryResponse: [String:AnyObject]? = nil
    
    fileprivate var mBillList: Array? = [BillDetailsEntity]()
    fileprivate var mAccountNumber: String? = ""
    
    init(_ responseObject : [String : AnyObject]) {
        super.init()
        self.billHistoryResponse = (responseObject["response"] as? [String:AnyObject])!
        
        self.mAccountNumber = SwitchModel.getSelectedAccountNo()
        
        let billList:Array<AnyObject>? = billHistoryResponse?["billList"] as? Array<AnyObject>
        
        if billList == nil {
            mBillList = [BillDetailsEntity]()
        } else if (billList?.count)! > 0 {
            for bill in billList! {
                
                let billDetails: BillDetailsEntity = BillDetailsEntity.init(bill as! [String : AnyObject])
                mBillList?.append(billDetails)
            }
        }  else {
            mBillList = [BillDetailsEntity]()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mBillList = decoder.decodeObject(forKey: "mBillList") as? Array<BillDetailsEntity>
        self.mAccountNumber = decoder.decodeObject(forKey: "mAccountNumber") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mBillList, forKey: "mBillList")
        coder.encode(mAccountNumber, forKey: "mAccountNumber")
    }
    
    public var billList: [BillDetailsEntity] {
        get {
            return self.mBillList!
        }
        set (billLst) {
            self.mBillList = billLst
        }
    }
    
    public var accountNumber: String {
        get {
            guard
                ((self.mAccountNumber as String?) != nil) else{
                    return "-"
            }
            
            return self.mAccountNumber!
        }
        set (accountNmber) {
            self.mAccountNumber = accountNmber
        }
    }
    
}

@objc(BillHistoryDB)
class BillHistoryDB : NSManagedObject {
    
    @NSManaged public var billList: Array<BillDetailsEntity>?
    @NSManaged public var accountNumber: String?
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    convenience init(_ response : [BillDetailsEntity], entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        self.init(entity: entity, insertInto: context)
        self.billList = response
    }
    
    public static func getBillHistory(_ context: NSManagedObjectContext, _ accountNumber: String) -> BillHistoryDB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: DataFactory.DATABASE.HISTORY_BILL, in: context)
        
        fetchRequest.entity = entityDescription
        
        do {
            
            let results: Array<BillHistoryDB> = try context.fetch(fetchRequest) as! Array<BillHistoryDB>
            
            if results.count > 0 {
                for billHistory in results {
                    if (billHistory.accountNumber == accountNumber) {
                        return billHistory
                    }
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func saveBillHistoryList(_ response : BillHistoryEntity, context: NSManagedObjectContext?) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: DataFactory.DATABASE.HISTORY_BILL, in: context!)
        
        fetchRequest.entity = entityDescription
        
        var isSaveDidFail: Bool = true
        
        do {
            
            let results: Array<BillHistoryDB> = try context!.fetch(fetchRequest) as! Array<BillHistoryDB>
            
            //print(results.count)
            
            if results.count > 0 {
                for  billHistory in results {
                    if (billHistory.accountNumber == response.accountNumber) {
                        let updatedObj = self.updateBillHistoryList(billHistory, response,  context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createBillHistoryList(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
                
            } else {
                //add bill history
                let newObj = self.createBillHistoryList(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        
                        return false // saving success
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return isSaveDidFail
    }
    
    private static func createBillHistoryList(_ response : BillHistoryEntity, context: NSManagedObjectContext?) -> BillHistoryDB? {
        if let billHistoryEntity = NSEntityDescription.insertNewObject(forEntityName: DataFactory.DATABASE.HISTORY_BILL, into: context!) as? BillHistoryDB {
            
            billHistoryEntity.accountNumber = response.accountNumber
            billHistoryEntity.billList = response.billList
            
            return billHistoryEntity
        }
        
        return nil
    }
    
    private static func updateBillHistoryList(_ billHistoryMO: BillHistoryDB,_ billHistoryEntity : BillHistoryEntity, context: NSManagedObjectContext?) -> BillHistoryDB? {
        let updatedBillDetEntity: BillHistoryDB = billHistoryMO
        
        updatedBillDetEntity.billList = billHistoryEntity.billList
        updatedBillDetEntity.accountNumber = SwitchModel.getSelectedAccountNo()
        
        return updatedBillDetEntity
    }
    
}


