//
//  SubscribedAddOnsEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 21/07/2017.
//
//

import Foundation

class SubscribedAddOnsEntity: NSObject, NSCoding {
    fileprivate var mName:String? = ""
    fileprivate var mCode:String? = ""
    fileprivate var mDescription:String? = ""
    fileprivate var mCategory:String? = ""
    fileprivate var mPrice:String? = ""
    fileprivate var mServiceType:String? = ""
    fileprivate var mIsActivated:String? = ""
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        
        self.mName = ((responseObject["name"]) != nil) ? responseObject["name"] as? String : ""
        self.mCode = ((responseObject["code"]) != nil) ? responseObject["code"] as? String : ""
        self.mDescription = ((responseObject["description"]) != nil) ? responseObject["description"] as? String : ""
        self.mCategory = ((responseObject["category"]) != nil) ? responseObject["category"] as? String : ""
        self.mPrice = ((responseObject["price"]) != nil) ? responseObject["price"] as? String : ""
        self.mServiceType = ((responseObject["serviceType"]) != nil) ? responseObject["serviceType"] as? String : ""
        self.mIsActivated = ((responseObject["isActivated"]) != nil) ? responseObject["isActivated"] as? String : ""
    }
    
    required init(coder decoder: NSCoder) {
        self.mName = decoder.decodeObject(forKey: "mName") as? String
        self.mCode = decoder.decodeObject(forKey: "mCode") as? String
        self.mDescription = decoder.decodeObject(forKey : "mDescription") as? String
        self.mCategory = decoder.decodeObject(forKey: "mCategory") as? String
        self.mPrice = decoder.decodeObject(forKey: "mPrice") as? String
        self.mServiceType = decoder.decodeObject(forKey : "mServiceType") as? String
        self.mIsActivated = decoder.decodeObject(forKey : "mIsActivated") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mName, forKey: "mName")
        coder.encode(mCode, forKey: "mCode")
        coder.encode(mDescription, forKey: "mDescription")
        coder.encode(mCategory, forKey: "mCategory")
        coder.encode(mPrice, forKey: "mPrice")
        coder.encode(mServiceType, forKey: "mServiceType")
        coder.encode(mIsActivated, forKey: "mIsActivated")
    }

    public var name: String {
        get {
            guard self.mName != "",
                  self.mName != nil else {
                return "-"
            }
            
            return self.mName!
        }
        set(nme) {
            self.mName = nme
        }
    }
    
    public var code: String {
        get {
            return self.mCode!
        }
        set(cde) {
            self.mCode = cde
        }
    }
    
    public var desc: String {
        get {
            guard self.mDescription != "",
                  self.mDescription != nil else {
                return "-"
            }
            
            return self.mDescription!
        }
        set(descptn) {
            self.mDescription = descptn
        }
    }
    
    public var category: String {
        get {
            return self.mCategory!
        }
        set(categry) {
            self.mCategory = categry
        }
    }
    
    public var price: String {
        get {
            return self.mPrice!
        }
        set(prce) {
            self.mPrice = prce
        }
    }
    
    public var serviceType: String {
        get {
            return self.mServiceType!
        }
        set(servceType) {
            self.mServiceType = servceType
        }
    }
    
    public var isActivated: String {
        get {
            guard self.mIsActivated != "",
                  self.mIsActivated != nil else {
                return "1"
            }
            
            return self.mIsActivated!
        }
        set(isActivted) {
            self.mIsActivated = isActivted
        }
    }
    
}
