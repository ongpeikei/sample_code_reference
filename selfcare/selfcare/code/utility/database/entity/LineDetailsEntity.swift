//
//  LineDetailsEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 18/07/2017.
//
//

import Foundation

class LineDetailsEntity: NSObject, NSCoding {

    //RESPONSE
    fileprivate var mProductName : String? = ""
    fileprivate var mLineAlias : String? = ""
    fileprivate var mMsisdn : String? = ""
    fileprivate var mProductTaxCode : String? = ""
    fileprivate var mImsi : String? = ""
    fileprivate var mIccid : String? = ""
    fileprivate var mSubStatus : String? = ""
    fileprivate var mProductType : String? = ""
    fileprivate var mDescription : String? = ""
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        
        self.mProductName = responseObject["productName"] as? String
        self.mLineAlias = responseObject["lineAlias"] as? String
        self.mMsisdn = responseObject["msisdn"] as? String
        self.mProductTaxCode = responseObject["productTaxCode"] as? String
        self.mImsi = responseObject["imsi"] as? String
        self.mIccid = responseObject["iccid"] as? String
        self.mSubStatus = responseObject["subStatus"] as? String
        self.mProductType = responseObject["productType"] as? String
        self.mDescription = responseObject["description"] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mProductName = decoder.decodeObject(forKey: "mProductName") as? String
        self.mLineAlias = decoder.decodeObject(forKey: "mLineAlias") as? String
        self.mMsisdn = decoder.decodeObject(forKey: "mMsisdn") as? String
        self.mProductTaxCode = decoder.decodeObject(forKey: "mProductTaxCode") as? String
        self.mImsi = decoder.decodeObject(forKey: "mImsi") as? String
        self.mIccid = decoder.decodeObject(forKey: "mIccid") as? String
        self.mSubStatus = decoder.decodeObject(forKey: "mSubStatus") as? String
        self.mProductType = decoder.decodeObject(forKey: "mProductType") as? String
        self.mDescription = decoder.decodeObject(forKey: "mDescription") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mProductName, forKey: "mProductName")
        coder.encode(mLineAlias, forKey: "mLineAlias")
        coder.encode(mMsisdn, forKey: "mMsisdn")
        coder.encode(mProductTaxCode, forKey: "mProductTaxCode")
        coder.encode(mImsi, forKey: "mImsi")
        coder.encode(mIccid, forKey: "mIccid")
        coder.encode(mSubStatus, forKey: "mSubStatus")
        coder.encode(mProductType, forKey: "mProductType")
        coder.encode(mDescription, forKey: "mDescription")
            }
    // entity attribute GETTER & SETTER
    
    public var productName: String {
        
        get {
            guard
                ((self.mProductName as String?) != nil) else{
                    return "-"
            }
            return self.mProductName!
        }
        set(productName) {
            self.mProductName = productName
        }
        
    }
    
    public var lineAlias: String {
        
        get {
            guard
                ((self.mLineAlias as String?) != nil) else{
                    return "-"
            }
            return self.mLineAlias!
        }
        set(lineAlias) {
            self.mLineAlias = lineAlias
        }
        
    }
    
    public var msisdn: String {
        
        get {
            guard
                ((self.mMsisdn as String?) != nil) else{
                    return "-"
            }
            return self.mMsisdn!
        }
        set(msisdn) {
            self.mMsisdn = msisdn
        }
        
    }
    
    public var productTaxCode: String {
        
        get {
            guard
                ((self.mProductTaxCode as String?) != nil) else{
                    return "-"
            }
            return self.mProductTaxCode!
        }
        set(productTaxCode) {
            self.mProductTaxCode = productTaxCode
        }
        
    }
    
    public var imsi: String {
        
        get {
            guard
                ((self.mImsi as String?) != nil) else{
                    return "-"
            }
            return self.mImsi!
        }
        set(imsi) {
            self.mImsi = imsi
        }
        
    }
    
    public var iccid: String {
        
        get {
            guard
                ((self.mIccid as String?) != nil) else{
                    return "-"
            }
            return self.mIccid!
        }
        set(iccid) {
            self.mIccid = iccid
        }
        
    }
    
    public var subStatus: String {
        
        get {
            guard
                ((self.mSubStatus as String?) != nil) else{
                    return "-"
            }
            return self.mSubStatus!
        }
        set(subStatus) {
            self.mSubStatus = subStatus
        }
        
    }
    
    public var productType: String {
        
        get {
            guard
                ((self.mProductType as String?) != nil) else{
                    return "-"
            }
            return self.mProductType!
        }
        set(productType) {
            self.mProductType = productType
        }
        
    }
    
    public var lineDescription: String {
        
        get {
            guard
                ((self.mDescription as String?) != nil) else{
                    return "-"
            }
            return self.mDescription!
        }
        set(lineDescription) {
            self.mDescription = lineDescription
        }
        
    }
    
}
