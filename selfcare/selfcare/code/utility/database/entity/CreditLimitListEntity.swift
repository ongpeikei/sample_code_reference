//
//  CreditLimitListEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 18/07/2017.
//
//

import Foundation
import CoreData

class CreditLimitListEntity: NSObject, NSCoding {
    
    //RESPONSE
    struct CONSTANTS {
        static let kRESPONSE = "response"
    }
    
    fileprivate var mCreditLimit:[CreditLimitEntity]? = nil
    fileprivate var mAccountNumber: String? = ""
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        
        mAccountNumber = SwitchModel.getSelectedAccountNo()
        
        let response:[String: AnyObject] = (responseObject["response"] as? [String: AnyObject])!
        let creditLimitList:[AnyObject]? = (response["creditLimitList"] as? [AnyObject])!
        
        if creditLimitList == nil {
            mCreditLimit = Array()
        } else if (creditLimitList?.count)! > 0 {
            mCreditLimit = Array()
            for AnyObject in creditLimitList! {
                let creditLimit:CreditLimitEntity = CreditLimitEntity.init(AnyObject)
                mCreditLimit?.append(creditLimit)
            }
            
        } else {
            mCreditLimit = Array()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mAccountNumber = decoder.decodeObject(forKey: "mAccountNumber") as? String
        self.mCreditLimit = decoder.decodeObject(forKey: "mCreditLimit") as? Array<CreditLimitEntity>
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mAccountNumber, forKey: "mAccountNumber")
        coder.encode(mCreditLimit, forKey: "mCreditLimit")
    }
    
    public var creditLimitList: [CreditLimitEntity] {
        
        get {
            return self.mCreditLimit!
        }
        set(arrCreditLimitObj) {
            self.mCreditLimit = arrCreditLimitObj
        }
        
    }
    
    public var accountNumber: String {
        
        get {
            return self.mAccountNumber!
        }
        set(arrAccountNumberObj) {
            self.mAccountNumber = arrAccountNumberObj
        }
        
    }

}

@objc (CreditLimitListDB)
class CreditLimitListDB: NSManagedObject {
    
    @NSManaged public var accountNumber: String?
    @NSManaged public var creditLimitList: Array<CreditLimitEntity>?
    
    public static func getCreditLimitList(_ context: NSManagedObjectContext, _ accountNumber: String) -> CreditLimitListDB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "CreditLimitListDB", in: context)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "accountNumber", accountNumber)
        
        do {
            let results: Array<CreditLimitListDB> = try context.fetch(fetchRequest) as! Array<CreditLimitListDB>
            
            if results.count > 0 {
                for creditLimitList in results {
                    if (creditLimitList.accountNumber == accountNumber) {
                        return creditLimitList
                    }
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func saveCreditLimitList(_ response : CreditLimitListEntity, context: NSManagedObjectContext?) -> Bool? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "CreditLimitListDB", in: context!)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "accountNumber", response.accountNumber)
        
        var isSaveDidFail: Bool = true
        
        do {
            let results: Array<CreditLimitListDB> = try context!.fetch(fetchRequest) as! Array<CreditLimitListDB>
            
            //print(results.count) //DB result count for debugging
            
            if results.count > 0 {
                for  CreditLimitListDB in results {
                    if (CreditLimitListDB.accountNumber == response.accountNumber) {
                        let updatedObj = self.updateCreditLimitList(CreditLimitListDB, response, context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createCreditLimitList(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
            } else {
                //add credit limit
                let newObj = self.createCreditLimitList(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        isSaveDidFail = false
                        
                        return false // saving success
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return isSaveDidFail
        
    }
    
    private static func createCreditLimitList(_ response : CreditLimitListEntity, context: NSManagedObjectContext?) -> CreditLimitListDB? {
        if let CreditLimitListEntity = NSEntityDescription.insertNewObject(forEntityName: "CreditLimitListDB", into: context!) as? CreditLimitListDB {
            CreditLimitListEntity.accountNumber = response.accountNumber
            CreditLimitListEntity.creditLimitList = response.creditLimitList
            
            return CreditLimitListEntity
        }
        
        return CreditLimitListDB()
    }
    
    private static func updateCreditLimitList(_ CreditLimitListMO: CreditLimitListDB,_ CreditLimitListEntity : CreditLimitListEntity, context: NSManagedObjectContext?) -> CreditLimitListDB? {
        let updatedCreditLimitListEntity: CreditLimitListDB = CreditLimitListMO
        
        updatedCreditLimitListEntity.accountNumber = SwitchModel.getSelectedAccountNo()
        updatedCreditLimitListEntity.creditLimitList = CreditLimitListEntity.creditLimitList
        
        return updatedCreditLimitListEntity
    }
    
}
