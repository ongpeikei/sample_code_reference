//
//  AuthEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 30/07/2017.
//
//

import Foundation
import CoreData

class AuthEntity: NSObject, NSCoding {
    fileprivate var mToken:String? = ""
    fileprivate var mExpiryDate:TimeInterval? = 0
    fileprivate var mRefreshToken:String? = ""
    
    override init() {
        super.init()
    }
    
    init(_ responseObject: [String: AnyObject]) {
        self.mToken = responseObject["access_token"] as? String
        self.mExpiryDate = responseObject["expires_in"] as? TimeInterval
        self.mRefreshToken = responseObject[StaticStrings.LOGIN_MODEL.GRANT_TYPE] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mToken = decoder.decodeObject(forKey: "mToken") as? String
        self.mExpiryDate = decoder.decodeObject(forKey: "mExpiryDate") as? TimeInterval
        self.mRefreshToken = decoder.decodeObject(forKey: "mRefreshToken") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mToken, forKey: "mToken")
        coder.encode(mExpiryDate, forKey: "mExpiryDate")
        coder.encode(mRefreshToken, forKey: "mRefreshToken")
    }
    
    public var token: String? {
        
        get {
            return self.mToken != nil ? self.mToken : ""
        }
        set(tken) {
            self.mToken = tken
        }
        
    }
    
    public var expiryDate: TimeInterval? {
        
        get {
            return self.mExpiryDate != nil ? self.mExpiryDate : 0
        }
        set(expiryDte) {
            self.mExpiryDate = expiryDte
        }
        
    }
    
    public var refreshToken: String? {
        
        get {
            return self.mRefreshToken != nil ? self.mRefreshToken : ""
        }
        set(refreshTken) {
            self.mRefreshToken = refreshTken
        }
        
    }

}

@objc (AuthDB)
class AuthDB: NSManagedObject {
    
    @NSManaged public var expiryDate: CLongLong
    @NSManaged public var refreshToken: String?
    @NSManaged public var token: String?
    
    
    public static func getAuth(_ context: NSManagedObjectContext) -> AuthDB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "AuthDB", in: context)
        
        fetchRequest.entity = entityDescription
        
        do {
            let results: Array<AuthDB> = try context.fetch(fetchRequest) as! Array<AuthDB>
            
            /*print("authdb count: \(results.count)") -- uncomment when debugging */
            
            if results.count > 0 {
                
                return results[0]
            
            } else {
                return nil
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func saveAuth(_ response : AuthEntity, context: NSManagedObjectContext?) -> Bool? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "AuthDB", in: context!)
        fetchRequest.entity = entityDescription
        
        //AuthDB records should always be cleared. Only 1 auth it could have
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest);
        
        var isSaveDidFail: Bool = true
    
        do {
            try context?.execute(deleteRequest)
            try context?.save()
            
            //add auth
            let newObj = self.createAuth(response, context: context)
            
            if (newObj != nil) {
                do {
                    try context?.save()
                    isSaveDidFail = false
                    
                    return isSaveDidFail
                } catch {
                        fatalError("Failure to save context: \(error)")
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return isSaveDidFail
        
    }
    
    private static func createAuth(_ response : AuthEntity, context: NSManagedObjectContext?) -> AuthDB? {
        if let authEntity = NSEntityDescription.insertNewObject(forEntityName: "AuthDB", into: context!) as? AuthDB {
            authEntity.expiryDate = CLongLong(response.expiryDate!)
            authEntity.refreshToken = response.refreshToken
            authEntity.token = response.token
            
            return authEntity
        }
        
        return AuthDB()
    }
    
    private static func updateAuth(_ authMO: AuthDB,_ authEntity : AuthEntity, context: NSManagedObjectContext?) -> AuthDB? {
        let authEntty: AuthDB = authMO
        
        authEntty.expiryDate = CLongLong(authEntity.expiryDate!)
        authEntty.refreshToken = authEntity.refreshToken
        authEntty.token = authEntity.token
        
        return authEntty
    }
}
