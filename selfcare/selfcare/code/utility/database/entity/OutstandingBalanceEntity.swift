//
//  OutstandingBalanceEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 18/07/2017.
//
//

import Foundation
import CoreData

class OutstandingBalanceEntity: NSObject, NSCoding {
    fileprivate var mOutstandingBalanceResponse: [String:AnyObject]? = nil
    fileprivate var mBalanceList: UsageBalanceEntity?
    fileprivate var mBillList: BillDetailsEntity?
    fileprivate var mAcctCode: String? = ""

     
    init(_ responseObject : [String : AnyObject]){
        super.init()
        self.mOutstandingBalanceResponse = responseObject["response"] as? [String : AnyObject]
        
        self.mAcctCode = SwitchModel.getSelectedAccountNo()
    
        // parse balanceList
        let balanceList = ((mOutstandingBalanceResponse?["balance"]) != nil) ? mOutstandingBalanceResponse? ["balance"] as? [String : AnyObject] : [String : AnyObject]()
        
        if balanceList == nil {
            mBalanceList = UsageBalanceEntity()
        } else if (balanceList?.count)! > 0 {
            let balanceDetails: UsageBalanceEntity = UsageBalanceEntity.init(balanceList!)
            mBalanceList = balanceDetails
        
        }  else {
            mBalanceList = UsageBalanceEntity()
        }
    
        // parse billList
//        let billList:AnyObject = (mOutstandingBalanceResponse?["bill"]!)!
        let billList = ((mOutstandingBalanceResponse?["bill"]) != nil) ? mOutstandingBalanceResponse? ["bill"] as? [String:AnyObject] : [String : AnyObject]()
        
        if billList == nil {
            mBillList = BillDetailsEntity()
        } else if (billList?.count)! > 0 {
            let billDetails: BillDetailsEntity = BillDetailsEntity.init(billList!)
            mBillList = billDetails
            
        } else {
            mBillList = BillDetailsEntity()
        }
    
    }
    
    required init(coder decoder: NSCoder) {
        self.mBalanceList = decoder.decodeObject(forKey: "mBalanceList") as? UsageBalanceEntity
        self.mBillList = decoder.decodeObject(forKey: "mBillList") as? BillDetailsEntity
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mBalanceList, forKey: "mBalanceList")
        coder.encode(mBillList, forKey: "mBillList")
    }

    
    public var balanceList: UsageBalanceEntity? {
        get {
            return self.mBalanceList
        }
        set(balList) {
            self.mBalanceList = balList
        }
    }
    
    public var billList: BillDetailsEntity? {
        get {
            return self.mBillList
        }
        set(bilList) {
            self.mBillList = bilList
        }
    }
    
}
    
@objc(OutstandingBalanceDB)
class OutstandingBalanceDB : NSManagedObject {
        
        @NSManaged public var balanceList: UsageBalanceEntity?
        @NSManaged public var billList: BillDetailsEntity?
        @NSManaged public var acctCode: String?
        
        override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
            super.init(entity: entity, insertInto: context)
        }
        
        convenience init(_ response : OutstandingBalanceEntity, entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
            self.init(entity: entity, insertInto: context)
            self.balanceList = response.balanceList
            self.billList = response.billList
        }
        
        public static func getOutstandingBalance(_ context: NSManagedObjectContext) -> OutstandingBalanceDB? {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            let entityDescription = NSEntityDescription.entity(forEntityName: "OutstandingBalanceDB", in: context)
            
            fetchRequest.entity = entityDescription
            fetchRequest.predicate = NSPredicate(format: "%K = %@", "acctCode", SwitchModel.getSelectedAccountNo()!)
            
            do {
                
                let results: Array<OutstandingBalanceDB> = try context.fetch(fetchRequest) as! Array<OutstandingBalanceDB>
                
                if results.count > 0 {
                    for outstandingBal in results {
                        if (outstandingBal.acctCode == SwitchModel.getSelectedAccountNo()) {
                            return outstandingBal
                        }
                    }
                }
                
            } catch {
                let fetchError = error as NSError
                print(fetchError)
            }
            
            return nil
        }
        
        public static func saveOutstandingBalance(_ response : OutstandingBalanceEntity, context: NSManagedObjectContext?) -> Bool {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            let entityDescription = NSEntityDescription.entity(forEntityName: "OutstandingBalanceDB", in: context!)
            
            fetchRequest.entity = entityDescription
            fetchRequest.predicate = NSPredicate(format: "%K = %@", "acctCode", SwitchModel.getSelectedAccountNo()!)
            
            var isSaveDidFail: Bool = true
            
            do {
                
                let results: Array<OutstandingBalanceDB> = try context!.fetch(fetchRequest) as! Array<OutstandingBalanceDB>
                
                //print(results.count)
                
                if results.count > 0 {
                    for  outstandingBal in results {
                        if (outstandingBal.acctCode == SwitchModel.getSelectedAccountNo()) {
                            let updatedObj = self.updateOutstandingBalance(outstandingBal, response,  context: context)
                            
                            if (updatedObj != nil) {
                                do {
                                    try context?.save()
                                    isSaveDidFail = false
                                    
                                    break
                                } catch {
                                    fatalError("Failure to save context: \(error)")
                                }
                            }
                        } else {
                            let newObj = self.createOutstandingBalance(response, context: context)
                            
                            if (newObj != nil) {
                                do {
                                    try context?.save()
                                    isSaveDidFail = false
                                    
                                    break
                                } catch {
                                    fatalError("Failure to save context: \(error)")
                                }
                            }
                        }
                    }
                    
                } else {
                    let newObj = self.createOutstandingBalance(response, context: context)
                    
                    if (newObj != nil) {
                        do {
                            try context?.save()
                            
                            return false // saving success
                        } catch {
                            fatalError("Failure to save context: \(error)")
                        }
                    }
                }
                
            } catch {
                let fetchError = error as NSError
                print(fetchError)
            }
            
            return isSaveDidFail
        }
        
        private static func createOutstandingBalance(_ response : OutstandingBalanceEntity, context: NSManagedObjectContext?) -> OutstandingBalanceDB? {
            if let outstandingBalEntity = NSEntityDescription.insertNewObject(forEntityName: "OutstandingBalanceDB", into: context!) as? OutstandingBalanceDB {
                
                outstandingBalEntity.acctCode = SwitchModel.getSelectedAccountNo()
                outstandingBalEntity.balanceList = response.balanceList
                outstandingBalEntity.billList = response.billList
                
                return outstandingBalEntity
            }
            
            return OutstandingBalanceDB()
        }
        
        private static func updateOutstandingBalance(_ outstandingBalanceMO: OutstandingBalanceDB,_ outstandingBalEntity : OutstandingBalanceEntity, context: NSManagedObjectContext?) -> OutstandingBalanceDB? {
            let updateOutstandingBalEntity: OutstandingBalanceDB = outstandingBalanceMO
            
            updateOutstandingBalEntity.balanceList = outstandingBalEntity.balanceList
            updateOutstandingBalEntity.billList = outstandingBalEntity.billList
            updateOutstandingBalEntity.acctCode = SwitchModel.getSelectedAccountNo()
            
            return updateOutstandingBalEntity
        }
    
}
