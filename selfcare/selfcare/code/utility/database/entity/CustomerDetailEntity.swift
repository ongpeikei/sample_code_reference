//
//  CustomerDetailEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 03/07/2017.
//
//

import Foundation
import CoreData
import Locksmith

class CustomerDetailEntity: NSObject, NSCoding {
    let customerDetailsModel = CustomerDetailsModel.sharedInstance
    
    fileprivate var customerDetailResponse: [String:AnyObject]? = nil
    
    fileprivate var mEmail:String? = ""
    fileprivate var mPassword:String? = ""
    fileprivate var mGender:String? = ""
    fileprivate var mNationality:String? = ""
    fileprivate var mPreferredLanguage:String? = ""
    fileprivate var mDateOfBirth:String? = "" // not sure if will use date
    fileprivate var mFullName:String? = ""
    fileprivate var mDisplayName:String? = ""
    fileprivate var mContactNumber:String? = ""
    fileprivate var mCustomerObjectId:String? = ""
    fileprivate var mProfilePicture:String? = ""
    fileprivate var mCustIdNo:String? = ""
    fileprivate var mCustIdType:String? = ""
    fileprivate var mBillingAddress:[String:AnyObject]? = nil
    fileprivate var mAccountNoList:Array? = [AccountNumberEntity]()

    
    init(_ responseObj : AnyObject) {
        super.init()
        self.customerDetailResponse = (responseObj["response"] as? [String:AnyObject])!
        
        let accountNoListArr:Array<AnyObject>? = customerDetailResponse?["accountNoList"] as? Array<AnyObject>
        
        if accountNoListArr == nil {
            mAccountNoList = [AccountNumberEntity]()
        } else if (accountNoListArr?.count)! > 0 {
            for accountNoListObject in accountNoListArr! {
                
                let accountNoList: AccountNumberEntity = AccountNumberEntity.init(accountNoListObject as! [String : AnyObject])
                mAccountNoList?.append(accountNoList)
            }
        } else {
            mAccountNoList = [AccountNumberEntity]()
        }
        
        self.mBillingAddress = customerDetailResponse?["billingAddress"] as? [String:AnyObject]
        self.mEmail = customerDetailResponse?["email"] as? String
        self.mPassword = customerDetailResponse?["password"] as? String
        self.mGender = customerDetailResponse?["gender"] as? String
        self.mNationality = customerDetailResponse?["nationality"] as? String
        self.mPreferredLanguage = customerDetailResponse?["preferredLanguage"] as? String
        self.mDateOfBirth = customerDetailResponse?["dateOfBirth"] as? String
        self.mFullName = customerDetailResponse?["fullName"] as? String
        self.mDisplayName = customerDetailResponse?["displayName"] as? String
        self.mContactNumber = customerDetailResponse?["contactNumber"] as? String
        self.mCustomerObjectId = customerDetailResponse?["customerObjectId"] as? String
        self.mProfilePicture = customerDetailResponse?["profilePicture"] as? String
        self.mCustIdNo = customerDetailResponse?["custIdNo"] as? String
        self.mCustIdType = customerDetailResponse?["custIdType"] as? String
        
        customerDetailsModel.saveCustomerObjId(mCustomerObjectId!)
    
    }
    
    required init(coder decoder: NSCoder) {
        self.mAccountNoList = decoder.decodeObject(forKey: "mAccountNoList") as? Array<AccountNumberEntity>
        self.mBillingAddress = decoder.decodeObject(forKey: "mBillingAddress") as? [String: AnyObject]
        self.mEmail = decoder.decodeObject(forKey: "mEmail") as? String
        self.mPassword = decoder.decodeObject(forKey: "mPassword") as? String
        self.mGender = decoder.decodeObject(forKey: "mGender")as? String
        self.mNationality = decoder.decodeObject(forKey: "mNationality") as? String
        self.mPreferredLanguage = decoder.decodeObject(forKey: "mPreferredLanguage") as? String
        self.mDateOfBirth = decoder.decodeObject(forKey: "mDateOfBirth") as? String
        self.mFullName = decoder.decodeObject(forKey: "mFullName") as? String
        self.mDisplayName = decoder.decodeObject(forKey: "mDisplayName") as? String
        self.mContactNumber = decoder.decodeObject(forKey: "mContactNumber") as? String
        self.mCustomerObjectId = decoder.decodeObject(forKey: "mCustomerObjectId") as? String
        self.mProfilePicture = decoder.decodeObject(forKey: "mProfilePicture") as? String
        self.mCustIdNo = decoder.decodeObject(forKey: "mCustIdNo") as? String
        self.mCustIdType = decoder.decodeObject(forKey: "mCustIdType") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mAccountNoList, forKey: "mAccountNoList")
        coder.encode(mBillingAddress, forKey: "mBillingAddress")
        coder.encode(mEmail, forKey: "mEmail")
        coder.encode(mPassword, forKey: "mPassword")
        coder.encode(mGender, forKey: "mGender")
        coder.encode(mNationality, forKey: "mNationality")
        coder.encode(mPreferredLanguage, forKey: "mPreferredLanguage")
        coder.encode(mDateOfBirth, forKey: "mDateOfBirth")
        coder.encode(mFullName, forKey: "mFullName")
        coder.encode(mDisplayName, forKey: "mDisplayName")
        coder.encode(mContactNumber, forKey: "mContactNumber")
        coder.encode(mCustomerObjectId, forKey: "mCustomerObjectId")
        coder.encode(mProfilePicture, forKey: "mProfilePicture")
        coder.encode(mCustIdNo, forKey: "mCustIdNo")
        coder.encode(mCustIdType, forKey: "mCustIdType")
    }

    // entity attribute GETTER & SETTER
    
    public var email: String {
        get {
            guard
                ((self.mEmail as String?) != nil) else{
                    return "-"
            }
            return self.mEmail!
        }
        set(email) {
            self.mEmail = email
        }
    }
    
    public var password: String {
        get {
            guard
                ((self.mPassword as String?) != nil) else{
                    return "-"
            }
            return self.mPassword!
        }
        set(password) {
            self.mPassword = password
        }
    }
    
    public var gender: String {
        get {
            guard
                ((self.mGender as String?) != nil) else{
                    return "-"
            }
            return self.mGender!
        }
        set(gender) {
            self.mGender = gender
        }
    }
    
    public var nationality: String {
        get {
            guard
                ((self.mNationality as String?) != nil) else{
                    return "-"
            }
            return self.mNationality!
        }
        set(nationality) {
            self.mNationality = nationality
        }
    }
    
    public var preferredLanguage: String {
        get {
            guard
                ((self.mPreferredLanguage as String?) != nil) else{
                    return "-"
            }
            return self.mPreferredLanguage!
        }
        set(preferredLanguage) {
            self.mPreferredLanguage = preferredLanguage
        }
    }
    
    public var dateOfBirth: String {
        get {
            guard
                ((self.mDateOfBirth as String?) != nil) else{
                    return "-"
            }
            return self.mDateOfBirth!
        }
        set(dateOfBirth) {
            self.mDateOfBirth = dateOfBirth
        }
    }
    
    public var fullName: String {
        get {
            guard
                ((self.mFullName as String?) != nil) else{
                    return "-"
            }
            return self.mFullName!
        }
        set(fullname) {
            self.mFullName = fullname
        }
    }
    
    public var displayName: String {
        get {
            guard
                ((self.mDisplayName as String?) != nil) else{
                    return "-"
            }
            return self.mDisplayName!
        }
        set(displayName) {
            self.mDisplayName = displayName
        }
    }

    public var contactNumber: String {
        get {
            guard
                ((self.mContactNumber as String?) != nil) else{
                    return "-"
            }
            return self.mContactNumber!
        }
        set(contactNumber) {
            self.mContactNumber = contactNumber
        }
    }
    
    public var customerObjectId: String {
        get {
            guard
                ((self.mCustomerObjectId as String?) != nil) else{
                    return "-"
            }
            return self.mCustomerObjectId!
        }
        set(custObjId) {
            self.mCustomerObjectId = custObjId
        }
    }
    
    public var profilePicture: String {
        get {
            guard ((self.mProfilePicture as String?) != nil) else {
                return "-"
            }
            
            return self.mProfilePicture!
        }
        set(profilePic){
            self.mProfilePicture = profilePic
        }
    }
    
    public var customerIdNumber: String {
        get {
            guard
                ((self.mCustIdNo as String?) != nil) else{
                    return "-"
            }
            
            return self.mCustIdNo!
        }
        set(custIdNo) {
            self.mCustIdNo = custIdNo
        }
    }
    
    public var customerIdType: String {
        get {
            guard
                ((self.mCustIdType as String?) != nil) else{
                    return "-"
            }
            
            return self.mCustIdType!
        }
        set(custIdType) {
            self.mCustIdType = custIdType
        }
    }
    
    public var billingAddress: [String:AnyObject] {
        get {
            guard ((self.mBillingAddress as [String: AnyObject]?) != nil) else {
                self.mBillingAddress = [String:AnyObject]()
                return self.mBillingAddress!
            }
            
            return self.mBillingAddress!
        }
        set(billingAddress) {
            self.mBillingAddress = billingAddress
        }
    }
    
    public var accountNoList: Array<AccountNumberEntity> {
        get {
            return self.mAccountNoList!
        }
        set(accountList) {
            self.mAccountNoList = accountList
        }
    }
    
}

@objc(CustomerDetailsDB)
class CustomerDetailsDB : NSManagedObject {
    
    @NSManaged public var accountNoList: Array<AccountNumberEntity>?
    @NSManaged public var billingAddress: [String: AnyObject]?
    @NSManaged public var contactNumber: String?
    @NSManaged public var dateOfBirth: String?
    @NSManaged public var displayName: String?
    @NSManaged public var email: String?
    @NSManaged public var fullName: String?
    @NSManaged public var gender: String?
    @NSManaged public var nationality: String?
    @NSManaged public var password: String?
    @NSManaged public var customerObjectId: String?
    @NSManaged public var preferredLanguage: String?
    @NSManaged public var profilePicture: String?
    @NSManaged public var custIdNo: String?
    @NSManaged public var custIdType: String?
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    convenience init(_ response : CustomerDetailEntity, entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        self.init(entity: entity, insertInto: context)
        self.accountNoList = response.accountNoList
        self.billingAddress = response.billingAddress
        self.email = response.email
    }
    
    public static func getCustomerDetail(_ context: NSManagedObjectContext, _ custObjId: String) -> CustomerDetailsDB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "CustomerDetailsDB", in: context)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "customerObjectId", custObjId)
        
        do {
            
            let results: Array<CustomerDetailsDB> = try context.fetch(fetchRequest) as! Array<CustomerDetailsDB>
            
            if results.count > 0 {
                for customerDet in results {
                    if (customerDet.customerObjectId == custObjId){
                        return customerDet
                    }
                }
            }
    
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return nil
    }
    
    public static func saveCustomerDetail(_ response : CustomerDetailEntity, context: NSManagedObjectContext?) -> Bool? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "CustomerDetailsDB", in: context!)
        
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "%K = %@", "customerObjectId", response.customerObjectId)
        
        var isSaveDidFail: Bool = true
        
        do {
            let results: Array<CustomerDetailsDB> = try context!.fetch(fetchRequest) as! Array<CustomerDetailsDB>
            
            //print(results.count)
            
            if results.count > 0 {
                for  CustomerDetailsDB in results {
                    if (CustomerDetailsDB.customerObjectId == response.customerObjectId) {
                        let updatedObj = self.updateCustomerDetail(CustomerDetailsDB, response,  context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createCustomerDetail(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
            } else {
                //add customer details
                let newObj = self.createCustomerDetail(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        return false
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return isSaveDidFail
    }
    
    private static func createCustomerDetail(_ response : CustomerDetailEntity, context: NSManagedObjectContext?) -> CustomerDetailsDB? {
        if let customerDetEnt = NSEntityDescription.insertNewObject(forEntityName: "CustomerDetailsDB", into: context!) as? CustomerDetailsDB {
        
            customerDetEnt.accountNoList = response.accountNoList
            customerDetEnt.billingAddress = response.billingAddress
            customerDetEnt.contactNumber = response.contactNumber
            customerDetEnt.displayName = response.displayName
            customerDetEnt.dateOfBirth = response.dateOfBirth
            customerDetEnt.email = response.email
            customerDetEnt.fullName = response.fullName
            customerDetEnt.gender = response.gender
            customerDetEnt.nationality = response.nationality
            customerDetEnt.password = response.password
            customerDetEnt.customerObjectId = response.customerObjectId
            customerDetEnt.preferredLanguage = response.preferredLanguage
            customerDetEnt.profilePicture = response.profilePicture
            customerDetEnt.custIdNo = response.customerIdNumber
            customerDetEnt.custIdType = response.customerIdType
            
            return customerDetEnt
        }
        return CustomerDetailsDB()
    }
    
    private static func updateCustomerDetail(_ customerDetMO: CustomerDetailsDB,_ customerDetEntity : CustomerDetailEntity, context: NSManagedObjectContext?) -> CustomerDetailsDB? {
        let updatedCustomerDetEntity: CustomerDetailsDB = customerDetMO
    
        updatedCustomerDetEntity.accountNoList = customerDetEntity.accountNoList
        updatedCustomerDetEntity.billingAddress = customerDetEntity.billingAddress
        updatedCustomerDetEntity.contactNumber = customerDetEntity.contactNumber
        updatedCustomerDetEntity.displayName = customerDetEntity.displayName
        updatedCustomerDetEntity.email = customerDetEntity.email
        updatedCustomerDetEntity.fullName = customerDetEntity.fullName
        updatedCustomerDetEntity.gender = customerDetEntity.gender
        updatedCustomerDetEntity.nationality = customerDetEntity.nationality
        updatedCustomerDetEntity.password = customerDetEntity.password
        updatedCustomerDetEntity.preferredLanguage = customerDetEntity.preferredLanguage
        updatedCustomerDetEntity.dateOfBirth = customerDetEntity.dateOfBirth
        updatedCustomerDetEntity.customerObjectId = customerDetEntity.customerObjectId
        updatedCustomerDetEntity.profilePicture = customerDetEntity.profilePicture
        updatedCustomerDetEntity.custIdNo = customerDetEntity.customerIdNumber
        updatedCustomerDetEntity.custIdType = customerDetEntity.customerIdType
        
        return updatedCustomerDetEntity
    }
    
}
