//
//  PurchaseDCBDetailEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 15/08/2017.
//
//

import Foundation

class PurchaseDCBDetailEntity: NSObject, NSCoding {
    // string constant key for database MO
    struct MODEL_OBJECT {
        static let kACCT_CODE = "mAcctCode"
        static let kMSISDN = "mMsisdn"
        static let kCHARGE_DATE = "mChargeDate"
        static let kCHARGE_AMOUNT = "mChargeAmount"
        static let kCHARGE_ITEM_CODE = "mChargeItemCode"
        static let kCHARGE_DESCRIPTION = "mChargeDescription"
        static let kPURCHASE_CHANNEL = "mPurchaseChannel"
        static let kTRANSACTION_SN = "mTransactionSN"
        static let kOTC_TYPE = "mOtcType"
    }
    
    //RESPONSE
    fileprivate var mAcctCode: String? = ""
    fileprivate var mMsisdn: String? = ""
    fileprivate var mChargeDate: String? = ""
    fileprivate var mChargeAmount: String? = ""
    fileprivate var mChargeItemCode: String? = ""
    fileprivate var mChargeDescription: String? = ""
    fileprivate var mPurchaseChannel: String? = ""
    fileprivate var mTransactionSN: String? = ""
    fileprivate var mOtcType: String? = ""
    
    init(_ responseObject : [String : AnyObject]){
        super.init()

        let responseKey = HistoryModel.RESPONSE.self
        
        self.mAcctCode = responseObject[responseKey.kACCT_CODE] as? String
        self.mMsisdn = responseObject[responseKey.kMSISDN] as? String
        self.mChargeDate = responseObject[responseKey.kCHARGE_DATE] as? String
        self.mChargeAmount = responseObject[responseKey.kCHARGE_AMOUNT] as? String
        self.mChargeItemCode = responseObject[responseKey.kCHARGE_ITEM_CODE] as? String
        self.mChargeDescription = responseObject[responseKey.kCHARGE_DESCRIPTION] as? String
        self.mPurchaseChannel = responseObject[responseKey.kPURCHASE_CHANNEL] as? String
        self.mTransactionSN = responseObject[responseKey.kTRANSACTION_SN] as? String
        self.mOtcType = responseObject[responseKey.kOTC_TYPE] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mAcctCode = decoder.decodeObject(forKey: MODEL_OBJECT.kACCT_CODE) as? String
        self.mMsisdn = decoder.decodeObject(forKey: MODEL_OBJECT.kMSISDN) as? String
        self.mChargeDate = decoder.decodeObject(forKey: MODEL_OBJECT.kCHARGE_DATE) as? String
        self.mChargeAmount = decoder.decodeObject(forKey: MODEL_OBJECT.kCHARGE_AMOUNT) as? String
        self.mChargeItemCode = decoder.decodeObject(forKey: MODEL_OBJECT.kCHARGE_ITEM_CODE) as? String
        self.mChargeDescription = decoder.decodeObject(forKey: MODEL_OBJECT.kCHARGE_DESCRIPTION) as? String
        self.mPurchaseChannel = decoder.decodeObject(forKey: MODEL_OBJECT.kPURCHASE_CHANNEL) as? String
        self.mTransactionSN = decoder.decodeObject(forKey: MODEL_OBJECT.kTRANSACTION_SN) as? String
        self.mOtcType = decoder.decodeObject(forKey: MODEL_OBJECT.kOTC_TYPE) as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mAcctCode, forKey: MODEL_OBJECT.kACCT_CODE)
        coder.encode(mMsisdn, forKey: MODEL_OBJECT.kMSISDN)
        coder.encode(mChargeDate, forKey: MODEL_OBJECT.kCHARGE_DATE)
        coder.encode(mChargeAmount, forKey: MODEL_OBJECT.kCHARGE_AMOUNT)
        coder.encode(mChargeItemCode, forKey: MODEL_OBJECT.kCHARGE_ITEM_CODE)
        coder.encode(mChargeDescription, forKey: MODEL_OBJECT.kCHARGE_DESCRIPTION)
        coder.encode(mPurchaseChannel, forKey: MODEL_OBJECT.kPURCHASE_CHANNEL)
        coder.encode(mTransactionSN, forKey: MODEL_OBJECT.kTRANSACTION_SN)
        coder.encode(mOtcType, forKey: MODEL_OBJECT.kOTC_TYPE)
    }
    
    // entity attribute GETTER & SETTER
    public var acctCode: String {
        get {
            guard
                ((self.mAcctCode as String?) != nil) else{
                    return "-"
            }
            
            return self.mAcctCode!
        }
        set(accountCode) {
            self.mAcctCode = accountCode
        }
    }
    
    public var msisdn: String {
        get {
            guard
                ((self.mMsisdn as String?) != nil) else{
                    return "-"
            }
            
            return self.mMsisdn!
        }
        set (msisdNumber) {
            self.mMsisdn = msisdNumber
        }
    }
    
    public var chargeDate: String {
        get {
            guard
                ((self.mChargeDate as String?) != nil) else{
                    return "-"
            }
            
            return self.mChargeDate!
        }
        set (chargeDte) {
            self.mChargeDate = chargeDte
        }
    }
    
    public var chargeAmount: String {
        get {
            guard
                ((self.mChargeAmount as String?) != nil) else{
                    return "-"
            }
            
            return self.mChargeAmount!
        }
        set (chargeAmt) {
            self.mChargeAmount = chargeAmt
        }
    }
    
    public var chargeItemCode: String {
        get {
            guard
                ((self.mChargeItemCode as String?) != nil) else{
                    return "-"
            }
            
            return self.mChargeItemCode!
        }
        set (chargeItemCde) {
            self.mChargeItemCode = chargeItemCde
        }
    }
    
    public var chargeDescription: String {
        get {
            guard
                ((self.mChargeDescription as String?) != nil) else{
                    return "-"
            }
            
            return self.mChargeDescription!
        }
        set (chargeDescptn) {
            self.mChargeDescription = chargeDescptn
        }
    }
    
    public var purchaseChannel: String {
        get {
            guard
                ((self.mPurchaseChannel as String?) != nil) else{
                    return "-"
            }
            
            return self.mPurchaseChannel!
        }
        set (purchaseCh) {
            self.mPurchaseChannel = purchaseCh
        }
    }
    
    public var transactionSN: String {
        get {
            guard
                ((self.mTransactionSN as String?) != nil) else{
                    return "-"
            }
            
            return self.mTransactionSN!
        }
        set (transactnSN) {
            self.mTransactionSN = transactnSN
        }
    }
    
    public var otcType: String {
        get {
            guard
                ((self.mOtcType as String?) != nil) else{
                    return "-"
            }
            
            return self.mOtcType!
        }
        set (otcTyp) {
            self.mOtcType = otcTyp
        }
    }
    
}
