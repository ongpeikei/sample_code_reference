//
//  LatestPaymentDetailEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 18/07/2017.
//
//

import Foundation

class LatestPaymentDetailEntity: NSObject, NSCoding {
    
    //RESPONSE
    fileprivate var latestPaymentDetailResponse: [String:AnyObject]? = nil
    
    fileprivate var mPaymentAmount: String? = ""
    fileprivate var mPaymentDate: String? = ""
    fileprivate var mTransactionDate: String? = ""
    fileprivate var mPaymentMethod: String? = ""
    fileprivate var mPaymentChannel: String? = ""
    fileprivate var mTransactionSN: String? = ""
    fileprivate var mBankTransactionSN: String? = ""
    
    init(_ responseObject : [String : AnyObject]){
        super.init()
        
        self.mPaymentAmount = responseObject["paymentAmount"] as? String
        self.mPaymentDate = responseObject["paymentDate"] as? String
        self.mTransactionDate = responseObject["transactionDate"] as? String
        self.mPaymentMethod = responseObject["paymentMethod"] as? String
        self.mPaymentChannel = responseObject["paymentChannel"] as? String
        self.mTransactionSN = responseObject["transactionSN"] as? String
        self.mBankTransactionSN = responseObject["bankTransactionSN"] as? String
    }
    
    required init(coder decoder: NSCoder) {
        self.mPaymentAmount = decoder.decodeObject(forKey: "mPaymentAmount") as? String
        self.mPaymentDate = decoder.decodeObject(forKey: "mPaymentDate") as? String
        self.mTransactionDate = decoder.decodeObject(forKey: "mTransactionDate") as? String
        self.mPaymentMethod = decoder.decodeObject(forKey: "mPaymentMethod") as? String
        self.mPaymentChannel = decoder.decodeObject(forKey: "mPaymentChannel") as? String
        self.mTransactionSN = decoder.decodeObject(forKey: "mTransactionSN") as? String
        self.mBankTransactionSN = decoder.decodeObject(forKey: "mBankTransactionSN") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mPaymentAmount, forKey: "mPaymentAmount")
        coder.encode(mPaymentDate, forKey: "mPaymentDate")
        coder.encode(mTransactionDate, forKey: "mTransactionDate")
        coder.encode(mPaymentMethod, forKey: "mPaymentMethod")
        coder.encode(mPaymentChannel, forKey: "mPaymentChannel")
        coder.encode(mTransactionSN, forKey: "mTransactionSN")
        coder.encode(mBankTransactionSN, forKey: "mBankTransactionSN")
    }
    
    // entity attribute GETTER & SETTER
    public var paymentAmount: String {
        
        get {
            guard
                ((self.mPaymentAmount as String?) != nil) else{
                    return "0.00"
            }
            return self.mPaymentAmount != "" ? self.mPaymentAmount! : "0.00"
        }
        set(paymentAmt) {
            self.mPaymentAmount = paymentAmt
        }
        
    }
    
    public var paymentDate: String {
        
        get {
            guard
                ((self.mPaymentDate as String?) != nil) else{
                    return "-"
            }
            return self.mPaymentDate!
        }
        set(paymentDte) {
            self.mPaymentDate = paymentDte
        }
        
    }
    
    public var transactionDate: String {
        
        get {
            guard
                ((self.mTransactionDate as String?) != nil) else{
                    return "-"
            }
            return self.mTransactionDate!
        }
        set(transactionDte) {
            self.mTransactionDate = transactionDte
        }
        
    }
    
    public var paymentMethod: String {
        
        get {
            guard
                ((self.mPaymentMethod as String?) != nil) else{
                    return "-"
            }
            return self.mPaymentMethod!
        }
        set(paymentMethd) {
            self.mPaymentMethod = paymentMethd
        }
        
    }
    
    public var paymentChannel: String {
        
        get {
            guard
                ((self.mPaymentChannel as String?) != nil) else{
                    return "-"
            }
            return self.mPaymentChannel!
        }
        set(paymentChl) {
            self.mPaymentChannel = paymentChl
        }
        
    }
    
    public var transactionSN: String {
        
        get {
            guard
                ((self.mTransactionSN as String?) != nil) else{
                    return "-"
            }
            return self.mTransactionSN!
        }
        set(trnsactionSN) {
            self.mTransactionSN = trnsactionSN
        }
        
    }
    
    public var bankTransactionSN: String {
        
        get {
            guard
                ((self.mBankTransactionSN as String?) != nil) else{
                    return "-"
            }
            return self.mBankTransactionSN!
        }
        set(bankTransctnSN) {
            self.mBankTransactionSN = bankTransctnSN
        }
        
    }
    
}
