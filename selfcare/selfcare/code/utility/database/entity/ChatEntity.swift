//
//  ChatEntity.swift
//  selfcare
//
//  Created by ONG PEI KEI on 03/08/2017.
//
//

import Foundation
import CoreData

class ChatEntity : NSObject, NSCoding {
    fileprivate var mChatID: String? = ""
    fileprivate var mParticipantID: String? = ""
    fileprivate var mEvents: Array? = [ChatMessageDetailEntity]()
    
    struct MODEL {
        static let kCHAT_ID = "mChatID"
        static let kPARTICIPANT_ID = "mParticipantID"
        static let kEVENTS = "mEvents"
    }
    
    init(_ responseObject : [String : AnyObject]) {
        super.init()
        
        self.mChatID = responseObject[ChatModel.CONSTANTS.kCHAT_ID] as? String
        self.mParticipantID = responseObject[ChatModel.CONSTANTS.kPARTICIPANT_ID] as? String
        
        let eventList = responseObject[ChatModel.CONSTANTS.kEVENTS] as? Array<AnyObject>
        
        if (eventList?.count)! > 0 {
            for message in eventList! {
                let chatMessageDetail: ChatMessageDetailEntity = ChatMessageDetailEntity.init(message as! [String : AnyObject])
                mEvents?.append(chatMessageDetail)
            }
        }  else {
            mEvents = [ChatMessageDetailEntity]()
        }
    }
    
    required init(coder decoder: NSCoder) {
        self.mChatID = decoder.decodeObject(forKey: MODEL.kCHAT_ID) as? String
        self.mParticipantID = decoder.decodeObject(forKey: MODEL.kPARTICIPANT_ID) as? String
        self.mEvents = decoder.decodeObject(forKey: MODEL.kEVENTS) as? Array<ChatMessageDetailEntity>
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mChatID, forKey: MODEL.kCHAT_ID)
        coder.encode(mParticipantID, forKey: MODEL.kPARTICIPANT_ID)
        coder.encode(mEvents, forKey: MODEL.kEVENTS)
    }
    
    // entity attribute GETTER & SETTER
    public var chatID: String {
        get {
            guard
                ((self.mChatID as String?) != nil) else{
                    self.mChatID = ""
                    return self.mChatID!
            }
            return self.mChatID!
        }
        set(liveChatID) {
            self.mChatID = liveChatID
        }
    }
    
    public var participantID: String {
        get {
            guard
                ((self.mParticipantID as String?) != nil) else{
                    self.mParticipantID = ""
                    return self.mParticipantID!
            }
            return self.mParticipantID!
        }
        set(customerID) {
            self.mParticipantID = customerID
        }
    }
    
    public var eventList: [ChatMessageDetailEntity] {
        get {
            return self.mEvents!
        }
        set(events) {
            self.mEvents = events
        }
    }
    
}

@objc(LiveChatDB)
class LiveChatDB : NSManagedObject {
    
    @NSManaged public var chatID: String?
    @NSManaged public var participantID: String?
    @NSManaged public var events: Array<ChatMessageDetailEntity>?
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    convenience init(_ response : ChatEntity, entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        self.init(entity: entity, insertInto: context)
        self.chatID = response.chatID
        self.participantID = response.participantID
        self.events = response.eventList
    }
    
    public static func getChatDetail(_ context: NSManagedObjectContext) -> LiveChatDB? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "CustomerDetailsDB", in: context)
        
        fetchRequest.entity = entityDescription
        
        do {
            
            let results: Array<LiveChatDB> = try context.fetch(fetchRequest) as! Array<LiveChatDB>
            
            if results.count > 0 {
                return results[0]
            } else {
                return LiveChatDB()
            }
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return LiveChatDB()
    }
    
    public static func saveChatDetail(_ response : ChatEntity, context: NSManagedObjectContext?) -> Bool? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "LiveChatDB", in: context!)
        
        fetchRequest.entity = entityDescription
        
        var isSaveDidFail: Bool = true
        
        do {
            
            let results: Array<LiveChatDB> = try context!.fetch(fetchRequest) as! Array<LiveChatDB>
            
            //print(results.count)
            
            if results.count > 0 {
                for  LiveChatDB in results {
                    if (LiveChatDB.chatID == response.chatID) {
                        let updatedObj = self.updateChatDetail(LiveChatDB, response,  context: context)
                        
                        if (updatedObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    } else {
                        let newObj = self.createChatDetail(response, context: context)
                        
                        if (newObj != nil) {
                            do {
                                try context?.save()
                                isSaveDidFail = false
                                
                                break
                            } catch {
                                fatalError("Failure to save context: \(error)")
                            }
                        }
                    }
                }
                
            } else {
                //add customer details
                let newObj = self.createChatDetail(response, context: context)
                
                if (newObj != nil) {
                    do {
                        try context?.save()
                        return false
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            }
            
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return isSaveDidFail
        
    }
    
    private static func createChatDetail(_ response : ChatEntity, context: NSManagedObjectContext?) -> LiveChatDB? {
        if let chatDetail = NSEntityDescription.insertNewObject(forEntityName: "LiveChatDB", into: context!) as? LiveChatDB {
            
            chatDetail.chatID = response.chatID
            chatDetail.participantID = response.participantID
            chatDetail.events = response.eventList
            
            return chatDetail
        }
        
        return LiveChatDB()
    }
    
    private static func updateChatDetail(_ liveChatMO: LiveChatDB,_ chatEntity : ChatEntity, context: NSManagedObjectContext?) -> LiveChatDB? {
        let updatedChatEntity: LiveChatDB = liveChatMO
        
        updatedChatEntity.chatID = chatEntity.chatID
        updatedChatEntity.participantID = chatEntity.participantID
        updatedChatEntity.events = chatEntity.eventList
        
        return updatedChatEntity
        
    }
    
}
