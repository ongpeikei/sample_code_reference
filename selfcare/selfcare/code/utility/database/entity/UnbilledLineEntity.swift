//
//  UnbilledLineEntity.swift
//  selfcare
//
//
//

import Foundation

class UnbilledLineEntity: NSObject, NSCoding {
    
    fileprivate var mMsisdn:String? = ""
    fileprivate var mImsi:String? = ""
    fileprivate var mIccid:String? = ""
    fileprivate var mMnpFlag:String? = ""
    fileprivate var mRegistrationDate:String? = ""
    
    fileprivate var mSimAssignDate:String? = ""
    fileprivate var mActivationDate:String? = ""
    fileprivate var mProdCode:String? = ""
    fileprivate var mSubsStatus:String? = ""
    fileprivate var mSubsStatusDesc:String? = ""
    
    fileprivate var mProductType:String? = ""
    fileprivate var mPreRegistrationDate:String? = ""
    fileprivate var mUnbilledAmount:String? = ""
    
    override init() {
        super.init()
    }
    
    init(_ responseObject : [String: AnyObject]) {
        super.init()
        
        self.mMsisdn = ((responseObject["msisdn"]) != nil) ? responseObject["msisdn"] as? String : ""
        self.mImsi = ((responseObject["imsi"]) != nil) ? responseObject["imsi"] as? String : ""
        
        self.mIccid = ((responseObject["iccid"]) != nil) ? responseObject["iccid"] as? String : ""
        self.mMnpFlag = ((responseObject["mnpFlag"]) != nil) ? responseObject["mnpFlag"] as? String : ""
        
        self.mRegistrationDate = ((responseObject["registrationDate"]) != nil) ? responseObject["registrationDate"] as? String : ""
        self.mSimAssignDate = ((responseObject["simAssignDate"]) != nil) ? responseObject["simAssignDate"] as? String : ""
        
        self.mActivationDate = ((responseObject["activationDate"]) != nil) ? responseObject["activationDate"] as? String : ""
        self.mProdCode = ((responseObject["prodCode"]) != nil) ? responseObject["prodCode"] as? String : ""
        
        self.mSubsStatus = ((responseObject["subsStatus"]) != nil) ? responseObject["subsStatus"] as? String : ""
        
        self.mSubsStatusDesc = ((responseObject["subsStatusDesc"]) != nil) ? responseObject["subsStatusDesc"] as? String : ""
        self.mProductType = ((responseObject["productType"]) != nil) ? responseObject["productType"] as? String : ""
        
        self.mPreRegistrationDate = ((responseObject["preRegistrationDate"]) != nil) ? responseObject["preRegistrationDate"] as? String : ""
        self.mUnbilledAmount = ((responseObject["unbilledAmount"]) != nil) ? responseObject["unbilledAmount"] as? String : ""
        
    }
    
    required init(coder decoder: NSCoder) {
        self.mMsisdn = decoder.decodeObject(forKey: "mMsisdn") as? String
        self.mImsi = decoder.decodeObject(forKey: "mImsi") as? String
        self.mIccid = decoder.decodeObject(forKey: "mIccid") as? String
        self.mMnpFlag = decoder.decodeObject(forKey: "mMnpFlag") as? String
        self.mRegistrationDate = decoder.decodeObject(forKey: "mRegistrationDate") as? String
        self.mSimAssignDate = decoder.decodeObject(forKey: "mSimAssignDate") as? String
        self.mActivationDate = decoder.decodeObject(forKey: "mActivationDate") as? String
        self.mProdCode = decoder.decodeObject(forKey: "mProdCode") as? String
        self.mSubsStatus = decoder.decodeObject(forKey: "mSubsStatus") as? String
        self.mSubsStatusDesc = decoder.decodeObject(forKey: "mSubsStatusDesc") as? String
        self.mPreRegistrationDate = decoder.decodeObject(forKey: "mPreRegistrationDate") as? String
        self.mUnbilledAmount = decoder.decodeObject(forKey: "mUnbilledAmount") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(mMsisdn, forKey: "mMsisdn")
        coder.encode(mImsi, forKey: "mImsi")
        coder.encode(mIccid, forKey: "mIccid")
        coder.encode(mMnpFlag, forKey: "mMnpFlag")
        coder.encode(mRegistrationDate, forKey: "mRegistrationDate")
        coder.encode(mSimAssignDate, forKey: "mSimAssignDate")
        coder.encode(mActivationDate, forKey: "mActivationDate")
        coder.encode(mProdCode, forKey: "mProdCode")
        coder.encode(mSubsStatus, forKey: "mSubsStatus")
        coder.encode(mSubsStatusDesc, forKey: "mSubsStatusDesc")
        coder.encode(mProductType, forKey: "mProductType")
        coder.encode(mPreRegistrationDate, forKey: "mPreRegistrationDate")
        coder.encode(mUnbilledAmount, forKey: "mUnbilledAmount")
    }
    
    public var msisdn: String {
        get {
            guard
                ((self.mMsisdn as String?) != nil) else{
                    return "-"
            }
            
            return self.mMsisdn!
        }
        set(mssdn) {
            self.mMsisdn = mssdn
        }
    }
    
    public var imsi: String {
        get {
            guard
                ((self.mImsi as String?) != nil) else{
                    return "-"
            }
            
            return self.mImsi!
        }
        set(ims) {
            self.mImsi = ims
        }
    }
    
    public var iccid: String {
        get {
            guard
                ((self.mIccid as String?) != nil) else{
                    return "-"
            }
            
            return self.mIccid!
        }
        set(iccd) {
            self.mIccid = iccd
        }
    }
    
    public var mnpFlag: String {
        get {
            guard
                ((self.mMnpFlag as String?) != nil) else{
                    return "-"
            }
            
            return self.mMnpFlag!
        }
        set(flag) {
            self.mnpFlag = flag
        }
    }
    
    public var registrationDate: String {
        get {
            guard
                ((self.mRegistrationDate as String?) != nil) else{
                    return "-"
            }
            
            return self.mRegistrationDate!
        }
        set(regDate) {
            self.mRegistrationDate = regDate
        }
    }
    
    public var simAssignDate: String {
        get {
            guard
                ((self.mRegistrationDate as String?) != nil) else{
                    return "-"
            }
            
            return self.mSimAssignDate!
        }
        set(smAssDate) {
            self.simAssignDate = smAssDate
        }
    }
    
    public var activationDate: String {
        get {
            guard
                ((self.mActivationDate as String?) != nil) else{
                    return "-"
            }
            
            return self.mActivationDate!
        }
        set(actDate) {
            self.mActivationDate = actDate
        }
    }
    
    public var prodCode: String {
        get {
            guard
                ((self.mProdCode as String?) != nil) else{
                    return "-"
            }
            
            return self.mProdCode!
        }
        set(prodCde) {
            self.mProdCode = prodCde
        }
    }
    
    public var subsStatus: String {
        get {
            guard
                ((self.mSubsStatus as String?) != nil) else{
                    return "-"
            }
            
            return self.mSubsStatus!
        }
        set(sbStatus) {
            self.mSubsStatus = sbStatus
        }
    }
    
    public var subsStatusDesc: String {
        get {
            guard
                ((self.mSubsStatusDesc as String?) != nil) else{
                    return "-"
            }
            
            return self.mSubsStatusDesc!
        }
        set(sbsStatusDesc) {
            self.mSubsStatusDesc = sbsStatusDesc
        }
    }
    
    public var productType: String {
        get {
            guard
                ((self.mProductType as String?) != nil) else{
                    return "-"
            }
            
            return self.mProductType!
        }
        set(prodType) {
            self.mProductType = prodType
        }
    }
    
    public var preRegistrationDate: String {
        get {
            guard
                ((self.mPreRegistrationDate as String?) != nil) else{
                    return "-"
            }
            
            return self.mPreRegistrationDate!
        }
        set(preRegDate) {
            self.mPreRegistrationDate = preRegDate
        }
    }
    
    public var unbilledAmount: String {
        get {
            guard
                ((self.mUnbilledAmount as String?) != nil) else{
                    return "-"
            }
            
            return self.mUnbilledAmount != "" ? self.mUnbilledAmount! : "0.00"
        }
        set(unblledAmt) {
            self.mUnbilledAmount = unblledAmt
        }
    }
}
