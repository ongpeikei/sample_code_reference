//
//  DataFactory.swift
//  selfcare
//
//  Created by ONG PEI KEI on 28/06/2017.
//
//

import Foundation
import UIKit
import CoreData

class DataFactory {
    static let sharedInstance = DataFactory()
    
    struct DATABASE {
        static let ADDON_BALANCE = "AddOnBalanceDB"
        static let AUTH = "AuthDB"
        static let CREDIT_LIMIT = "CreditLimitListDB"
        static let CREDIT_UNBILLED = "CreditUnbilledDB"
        static let CUSTOMER_DETAILS = "CustomerDetailsDB"
        static let LIVE_CHAT = "LiveChatDB"
        static let OUTSTANDING_BALANCE = "OutstandingBalanceDB"
        static let HISTORY_BILL = "BillHistoryDB"
        static let HISTORY_ADDON = "DBHistoryAddOn"
        static let HISTORY_DCB = "DBHistoryDCB"
        static let SIM_DETAILS = "SimDetailsDB"
    }
    
    fileprivate var entity:NSManagedObject? = nil
    
    fileprivate var creditUnbilledBalance: CreditUnbilledBalanceEntity? = nil
    fileprivate var creditLimitList: [CreditLimitEntity]? = nil
    fileprivate var outstandingBalance:OutstandingBalanceEntity? = nil
    fileprivate var addOnVasServicesList:[AddOnVasServicesEntity]? = nil
    
    func managedObjectContext() -> NSManagedObjectContext {
        var context: NSManagedObjectContext? = nil
        if #available(iOS 10.0, *) {
            context = (UIApplication.shared.delegate! as! AppDelegate).persistentContainer.viewContext
        } else {
            // Fallback on earlier versions
            context = CoreDataStack.managedObjectContext
        }
        
        return context!
    }
    
    /************************* Delete Database ********************************/
    
    func deleteAllDatabase() {
        let databaseList: [String] = [DATABASE.ADDON_BALANCE, DATABASE.AUTH, DATABASE.CREDIT_LIMIT,
                                      DATABASE.CREDIT_UNBILLED, DATABASE.CUSTOMER_DETAILS, DATABASE.LIVE_CHAT, DATABASE.OUTSTANDING_BALANCE,
                                      DATABASE.HISTORY_ADDON, DATABASE.HISTORY_DCB, DATABASE.HISTORY_BILL, DATABASE.SIM_DETAILS]
        
        for entity in databaseList {
            deleteDatabase(entity)
        }
        
        //print(UserDefaults.standard.dictionaryRepresentation().keys.count)
    }
    
    func deleteDatabase(_ entityName: String) {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do {
            try managedObjectContext().execute(request)
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    /************************* Customer details ********************************/
    func getCustomerDetails(_ customerObjId: String) -> CustomerDetailsDB? {
        let details = CustomerDetailsDB.getCustomerDetail(self.managedObjectContext(), customerObjId)
        
        return details
    }
    
    func saveCustomerDetail(_ response:  AnyObject) {
        //parsing of response
        let entity: CustomerDetailEntity = CustomerDetailEntity.init(response);
    
        //saving - has logic to create or update
        let isSaveDidFail = CustomerDetailsDB.saveCustomerDetail(entity, context: self.managedObjectContext())!
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }
    
    /************************* Master API I - OutstandingBalance ********************************/
    
    public func getOutstandingBalance() -> OutstandingBalanceDB? {
        return OutstandingBalanceDB.getOutstandingBalance(self.managedObjectContext())
        
    }
    
    func saveOutstandingBalance(_ response: [String : AnyObject]) {
        //parsing of response
        let entity: OutstandingBalanceEntity = OutstandingBalanceEntity.init(response)
        
        //saving - has logic to create or update
        let isSaveDidFail = OutstandingBalanceDB.saveOutstandingBalance(entity, context: self.managedObjectContext())
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }

    /************************* Master API II - CreditUnbilledBalance ********************************/
    
    public func getCreditUnbilledBalance() -> CreditUnbilledDB? {
        return CreditUnbilledDB.getCreditUnbilledBalance(self.managedObjectContext())
    }
    
    func saveCreditUnbilledBalance(_ response : [String : AnyObject]) {
        //parsing of response
        let entity: CreditUnbilledBalanceEntity = CreditUnbilledBalanceEntity.init(response)
        
        //saving creditunbilled
        let isSaveDidFail = CreditUnbilledDB.saveCreditUnbilledBalance(entity, context: self.managedObjectContext())
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }
    

    /************************** Credit Limit List ******************************************/
    
    public func getCreditLimitList(_ accountNumber: String) -> CreditLimitListDB? {
        let creditLimitList = CreditLimitListDB.getCreditLimitList(self.managedObjectContext(), accountNumber)
        
        return creditLimitList
    }
    
    public func saveCreditLimitList(_ response : [String : AnyObject] ) {
        let entity: CreditLimitListEntity = CreditLimitListEntity.init(response)
        
        let isSaveDidFail = CreditLimitListDB.saveCreditLimitList(entity, context: self.managedObjectContext())
        
        if isSaveDidFail! {
            // do something when saving is failed
        }
    }

    /************************* Bill History ********************************/
    func getBillHistory(_ accountNumber: String) -> BillHistoryDB? {
        return BillHistoryDB.getBillHistory(self.managedObjectContext(), accountNumber)
    }
    
    func saveBillHistory(_ response: [String : AnyObject]) {
        //parsing of response
        let entity: BillHistoryEntity = BillHistoryEntity.init(response)
        
        //saving - has logic to create or update
        let isSaveDidFail = BillHistoryDB.saveBillHistoryList(entity, context: self.managedObjectContext())
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }

    /************************* Purchase Add On History ********************************/
    func getPurchaseAddOnHistory() -> DBHistoryAddOn? {
        return DBHistoryAddOn.getPurchaseAddOnHistory(self.managedObjectContext())
    }
    
    func savePurchaseAddOnHistory(_ response: [String : AnyObject]) {
        //parsing of response
        let entity: PurchaseAddOnHistoryEntity = PurchaseAddOnHistoryEntity.init(response)
        
        //saving - has logic to create or update
        let isSaveDidFail = DBHistoryAddOn.savePurchaseAddOnHistoryList(entity, context: self.managedObjectContext())
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }
    
    /************************* Purchase DCB History ********************************/
    func getPurchaseDCBHistory() -> DBHistoryDCB? {
        return DBHistoryDCB.getPurchaseDCBHistory(self.managedObjectContext())
    }
    
    func savePurchaseDCBHistory(_ response: [String : AnyObject]) {
        //parsing of response
        let entity: PurchaseDCBHistoryEntity = PurchaseDCBHistoryEntity.init(response)
        
        //saving - has logic to create or update
        let isSaveDidFail = DBHistoryDCB.savePurchaseDCBHistoryList(entity, context: self.managedObjectContext())
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }
    
    /************************* Line - Edit add on ********************************/
    func getAddOnVasServicesList() -> [AddOnVasServicesEntity]? {
        return self.addOnVasServicesList != nil ? self.addOnVasServicesList : [AddOnVasServicesEntity]()
    }
    
    func setAddOnVasServicesList(_ response: [AnyObject]) {
        var services: [AddOnVasServicesEntity] = [AddOnVasServicesEntity]()
        
        if response.count > 0 {
            for service in response {
                let serviceObj: AddOnVasServicesEntity = AddOnVasServicesEntity.init(service as! [String : AnyObject])
                services.append(serviceObj)
            }
        }
        
        self.addOnVasServicesList = services
        
    }
    
    /************************* Line - Add On Balance ********************************/
    func getAddOnBalance(_ msisdn: String) -> AddOnBalanceDB? {
        let addOnBalance = AddOnBalanceDB.getAddOnBalance(self.managedObjectContext(), msisdn)
        
        return /*(*/addOnBalance /*!= nil) ? addOnBalance! : AddOnBalanceDB()*/
    }
    
    func saveAddOnBalance(_ response: [String : AnyObject]) {
        //parsing of response
        let entity: AddOnBalanceEntity = AddOnBalanceEntity.init(response)
        
        //saving - has logic to create or update
        let isSaveDidFail = AddOnBalanceDB.saveAddOnBalance(entity, context: self.managedObjectContext())
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }
    
    /************************** Sim Details ******************************************/
    
    public func getSimDetails(_ msisdn: String) -> SimDetailsDB? {
        let simDetails = SimDetailsDB.getSimDetails(self.managedObjectContext(), msisdn)
        
        return simDetails
    }
    
    public func saveSimDetails(_ response : [String : AnyObject]) {
        let entity: SimDetailsEntity = SimDetailsEntity.init(response)
        
        let isSaveDidFail = SimDetailsDB.saveSimDetails(entity, context: self.managedObjectContext())!
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }
    
    
   /************************** Auth ******************************************/
    
    public func getAuth() -> AuthDB? {
        return AuthDB.getAuth(self.managedObjectContext())
    }
    
    public func saveAuth(_ response : [String: AnyObject]) {
        let entity: AuthEntity = AuthEntity.init(response)
        let isSaveDidFail = AuthDB.saveAuth(entity, context: self.managedObjectContext())!
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }
    
    /************************** LiveChat ******************************************/
    public func getChatDetail() -> LiveChatDB {
        return LiveChatDB.getChatDetail(self.managedObjectContext())!
    }
    
    public func saveChatDetail(_ response : [String: AnyObject]) {
        let entity: ChatEntity = ChatEntity.init(response)
        let isSaveDidFail = LiveChatDB.saveChatDetail(entity, context: self.managedObjectContext())!
        
        if isSaveDidFail {
            // do something when saving is failed
        }
    }
    
}
