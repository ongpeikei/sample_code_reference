//
//  FabricAnalytics.swift
//  selfcare
//
//  Created by ONG PEI KEI on 29/05/2017.
//
//

import Foundation
import Crashlytics
import Fabric

class FabricAnalytics {
    static let sharedInstance = FabricAnalytics()
    
    enum SCREEN_KEY {
        case kROOT
        case kLOGIN
        case kDRAWER
        case kMAIN
        case kDASHBOARD
        case kLINE
        case kCHARGESBREAKDOWN
        case kSWITCHPRODUCT
        case kUNBILLEDUSAGE
        case kWALKTHROUGHINTRO
        case kWALKTHROUGHPRODUCT
        case kWALKTHROUGHLANGUAGE
        case kADDON
        case kLIVECHAT
        case kPROFILE
        case kEDITPROFILE
        case kSUPPORT
        case kKNOWLEDGEBASE
        case kSETTINGS
        case kHISTORY
        case kPASSWORD
        case kSWITCHACCOUNT
        case kWHATSNEW
        case kLIVECHATINQUIRY
        
    }
    
    enum BUTTON_KEY {
        case kOVERVIEW_CLICKED
        case kLINE_CLICKED
        case kMAKEPAYMENT_CLICKED
        case kLOADMORE_CLICKED
        case kFAB_CLICKED
        case kFAB_LIVECHAT_CLICKED
        case kFAB_MENU_CLICKED
        case kSWITCH_MOBILE_CLICKED
        case kSWITCH_BROADBAND_CLICKED
        case kBACK_CLICKED
        case kEDIT_CREDIT_LIMIT_CLICKED
        case kUNBILLED_USAGE_CLICKED
        case kCHARGES_BREAKDOWN_CLICKED
        case kEDIT_ADD_ON_CLICKED
        case kREQUEST_APN_CLICKED
        case kSIM_PUK_CLICKED
        case kSIM_PIN_CLICKED
        case kSMS_CENTER_CLICKED
        case kSTART_CHAT_CLICKED
        case kSEND_CHAT_CLICKED
        case kHISTORY_BILL_CLICKED
        case kHISTORY_PURCHASE_CLICKED
        case kLOGOUT_CLICKED
        case kBUY_CLICKED
        case kTOGGLE_CLICKED
        case kFORGOT_EMAIL_CLICKED
        case kFORGOT_PASSWORD_CLICKED
        case kOKAY_CLICKED
        case kCANCEL_CLICKED
        case kPAYMENT_SUCCESS
        case kPAYMENT_FAILED
        case kPUK_SUCCESS
        case kPIN_SUCCESS
        case kVIEW_RECEIPT
        case kAPN_SUCCESS
        case kADD_ONS_SUCCESS
        case kADD_ONS_FAILED
        case kINCREASE_CREDIT_LIMIT_SUCCESS
        case kEND_CHAT
        case kMINIMIZE_CHAT
        case kDIALOG_LIVECHAT_CLICKED
        case kLOGIN_BUTTON_TAPPED
        
        
    }
    
    enum DIALOG_KEY {
        case kAPPLANGUAGE
        case kCHANGEALIAS
        case kCONFIRMATION
        case kDEFAULTPRODUCT
        case kDEPOSITNEEDED
        case kEDITCREDITLIMIT
        case kPIN
        case kPUK
        case kREQUESTAPNSETTINGS
        case kSMSCENTER
        case kENTER_PAYMENT
        case kVIEW_RECEIPT
        case kCREDITLIMIT_TOOLTIP
        case kUNBILLED_TOOLTIP
        case k401_DIALOG
        case k400_DIALOG
        case kMAINTENANCE_DIALOG
        case kVERSION_UPDATE_DIALOG
        case kLOGOUT_DIALOG
    }
    
    func crashlyticsScreen(_ key: SCREEN_KEY) {
        var msg: String
        
        switch key {
        case .kROOT:
            msg = "ROOT SCREEN"
            
            break
        case .kLOGIN:
            msg = "LOGIN EMAIL SCREEN"
            
            break
        case .kPASSWORD:
            msg = "LOGIN PASSWORD SCREEN"
            
            break
        case .kDRAWER:
            msg = "DRAWER SCREEN"
            
            break
        case .kMAIN:
            msg = "MAIN SCREEN"
            
            break
        case .kDASHBOARD:
            msg = "DASHBOARD SCREEN"
            
            break
        case .kLINE:
            msg = "LINE SCREEN"
            
            break
        case .kCHARGESBREAKDOWN:
            msg = "CHARGES BREAKDOWN SCREEN"
            
            break
        case .kSWITCHPRODUCT:
            msg = "SWITCH PRODUCT SCREEN"
            
            break
        case .kUNBILLEDUSAGE:
            msg = "UNBILLED USAGE SCREEN"
            
            break
        case .kWALKTHROUGHINTRO:
            msg = "WALKTHROUGH INTRO SCREEN"
            
            break
        case .kWALKTHROUGHPRODUCT:
            msg = "WALKTHROUGH PRODUCT SCREEN"
            break
            
        case .kWALKTHROUGHLANGUAGE:
            msg = "WALKTHROUGH LANGUAGE SCREEN"
            break
            
        case .kADDON:
            msg = "ADD ONS SCREEN"
            
            break
        case .kLIVECHAT:
            msg = "LIVE CHAT SCREEN"
            
            break
        case .kPROFILE:
            msg = "PROFILE SCREEN"
            
            break
        case .kEDITPROFILE:
            msg = "EDIT PROFILE SCREEN"
            
            break
        case .kSUPPORT:
            msg = "SUPPORT SCREEN"
            
            break
        case .kKNOWLEDGEBASE:
            msg = "KNOWLEDGE BASE SCREEN"
            
            break
        case .kHISTORY:
            msg = "HISTORY SCREEN"
            
            break
        case .kSWITCHACCOUNT:
            msg = "SWITCH ACCOUNT SCREEN"
            
            break
        case .kWHATSNEW:
            msg = "WHAT'S NEW SCREEN"
            
            break
        case .kLIVECHATINQUIRY:
            msg = "LIVE CHAT INQUIRY SCREEN"
            
            break
            
        default:
            msg = ""
            
            break
        }
        
        CLSLogv("%@", getVaList([msg]))
    }
    
    func crashlyticsButton(_ key: BUTTON_KEY) {
        var msg: String
        
        switch key {
        case .kOVERVIEW_CLICKED:
            msg = "DASHBOARD CLICKED"
            
            break
        case .kLINE_CLICKED:
            msg = "LINE CLICKED"
            
            break
        case .kMAKEPAYMENT_CLICKED:
            msg = "MAKE PAYMENT CLICKED"
            
            break
        case .kLOADMORE_CLICKED:
            msg = "LOAD MORE CLICKED"
            
            break
        case .kFAB_CLICKED:
            msg = "FAB CLICKED"
            
            break
        case .kFAB_LIVECHAT_CLICKED:
            msg = "FAB LIVECHAT CLICKED"
            
            break
        case .kFAB_MENU_CLICKED:
            msg = "FAB MENU CLICKED"
            
            break
        case .kSWITCH_MOBILE_CLICKED:
            msg = "SWITCH MOBILE CLICKED"
            
            break
        case .kSWITCH_BROADBAND_CLICKED:
            msg = "SWITCH BROADBAND CLICKED"
            
            break
        case .kBACK_CLICKED:
            msg = "BACK CLICKED"
            
            break
        case .kEDIT_CREDIT_LIMIT_CLICKED:
            msg = "EDIT CREDIT LIMIT CLICKED"
            
            break
        case .kUNBILLED_USAGE_CLICKED:
            msg = "UNBILLED USAGE CLICKED"
            
            break
        case .kCHARGES_BREAKDOWN_CLICKED:
            msg = "CHARGES BREAKDOWN CLICKED"
            
            break
        case .kEDIT_ADD_ON_CLICKED:
            msg = "EDIT ADD ON CLICKED"
            
            break
        case .kREQUEST_APN_CLICKED:
            msg = "REQUEST APN CLICKED"
            
            break
        case .kSIM_PUK_CLICKED:
            msg = "SIM PUK CLICKED"
            
            break
        case .kSIM_PIN_CLICKED:
            msg = "SIM PIN CLICKED"
            
            break
        case .kSMS_CENTER_CLICKED:
            msg = "SMS CENTER CLICKED"
            
            break
        case .kSTART_CHAT_CLICKED:
            msg = "START CHAT CLICKED"
            
            break
        case .kSEND_CHAT_CLICKED:
            msg = "SEND CHAT CLICKED"
            
            break
        case .kHISTORY_BILL_CLICKED:
            msg = "BILL CLICKED"
            
            break
        case .kHISTORY_PURCHASE_CLICKED:
            msg = "PURCHASE CLICKED"
            
            break
        case .kLOGOUT_CLICKED:
            msg = "LOGOUT CLICKED"
            
            break
        case .kBUY_CLICKED:
            msg = "BUY CLICKED"
            
            break
        case .kTOGGLE_CLICKED:
            msg = "TOGGLE CLICKED"
            
            break
        case .kFORGOT_EMAIL_CLICKED:
            msg = "FORGOT EMAIL CLICKED"
            
            break
        case .kFORGOT_PASSWORD_CLICKED:
            msg = "FORGOT PASSWORD CLICKED"
            
            break
        case .kOKAY_CLICKED:
            msg = "OKAY CLICKED"
            
            break
        case .kCANCEL_CLICKED:
            msg = "CANCEL CLICKED"
            
            break
        case .kPAYMENT_SUCCESS:
            msg = "PAYMENT SUCCESS"
            
            break
        case .kPAYMENT_FAILED:
            msg = "PAYMENT FAILED"
            
            break
        case .kPIN_SUCCESS:
            msg = "PIN SUCCESS"
            
            break
        case .kPUK_SUCCESS:
            msg = "PUK SUCCESS"
            
            break
        case .kAPN_SUCCESS:
            msg = "APN SUCCESS"
            
            break
        case .kVIEW_RECEIPT:
            msg = "VIEW_RECEIPT"
            
            break
        case .kADD_ONS_SUCCESS:
            msg = "ADD ONS SUCCESS"
            
            break
        case .kADD_ONS_FAILED:
            msg = "ADD ONS FAILED"
            
            break
        case .kINCREASE_CREDIT_LIMIT_SUCCESS:
            msg = "INCREASE CREDIT LIMIT SUCCESS"
            
            break
        case .kEND_CHAT:
            msg = "END CHAT CLICKED"
            
            break
        case .kMINIMIZE_CHAT:
            msg = "MINIMIZE CHAT CLICKED"
            
            break
        case .kDIALOG_LIVECHAT_CLICKED:
            msg = "LIVECHAT FROM DIALOG CLICKED"
            
            break
        case .kLOGIN_BUTTON_TAPPED:
            msg = "LOGIN BUTTON TAPPED"
            
            break
            
        }
        
        CLSNSLogv("%@", getVaList([msg]))
    }
    
    func crashlyticsDialog(_ key: DIALOG_KEY) {
        var msg: String
        
        switch key {
        case .kAPPLANGUAGE:
            msg = "APP LANGUAGE DIALOG"
            break
            
        case .kCHANGEALIAS:
            msg = "CHANGE ALIAS DIALOG"
            break
            
        case .kCONFIRMATION:
            msg = "CONFIRMATION DIALOG"
            break
            
        case .kDEFAULTPRODUCT:
            msg = "DEFAULT PRODUCT DIALOG"
            break
            
        case .kDEPOSITNEEDED:
            msg = "DEPOSIT NEEDED DIALOG"
            break
            
        case .kEDITCREDITLIMIT:
            msg = "EDIT CREDIT LIMIT DIALOG"
            break
            
        case .kPIN:
            msg = "PIN DIALOG"
            break
            
        case .kPUK:
            msg = "PUK DIALOG"
            break
            
        case .kREQUESTAPNSETTINGS:
            msg = "REQUEST APN SETTINGS DIALOG"
            break
            
        case .kSMSCENTER:
            msg = "SMS CENTER DIALOG"
            
            break
        case .kENTER_PAYMENT:
            msg = "ENTER PAYMENT DIALOG"
            
            break
        case .kVIEW_RECEIPT:
            msg = "VIEW RECEIPT DIALOG"
            
            break
        case .kCREDITLIMIT_TOOLTIP:
            msg = "VIEW UNBILLED USAGE TOOLTIP"
            
            break
        case .kUNBILLED_TOOLTIP:
            msg = "VIEW UNBILLED USAGE TOOLTIP"
            
            break
        case .k400_DIALOG:
            msg = "ERROR 400 DIALOG"
            
            break
        case .k401_DIALOG:
            msg = "ERROR 401 DIALOG"
            
            break
        case .kMAINTENANCE_DIALOG:
            msg = "MAINTENANCE DIALOG"
            
            break
        case .kVERSION_UPDATE_DIALOG:
            msg = "VERSION UPDATE DIALOG"
            
            break
        case .kLOGOUT_DIALOG:
            msg = "LOGOUT DIALOG"
            
            break
            
        }
        
//        CLSLogv("%@", getVaList([msg]))
        CLSNSLogv("%@", getVaList([msg]))
    }
    
    func answersScreen(_ key: SCREEN_KEY) {
        var msg: String
        
        switch key {
        case .kLOGIN:
            msg = "LOGIN EMAIL SCREEN"
            
            break
        case .kPASSWORD:
            msg = "LOGIN PASSWORD SCREEN"
            
            break
        case .kDRAWER:
            msg = "DRAWER SCREEN"
            
            break
        case .kDASHBOARD:
            msg = "DASHBOARD SCREEN"
            
            break
        case .kLINE:
            msg = "LINE SCREEN"
            
            break
        case .kCHARGESBREAKDOWN:
            msg = "CHARGES BREAKDOWN SCREEN"
            
            break
        case .kSWITCHPRODUCT:
            msg = "SWITCH PRODUCT SCREEN"
            
            break
        case .kUNBILLEDUSAGE:
            msg = "UNBILLED USAGE SCREEN"
            
            break
        case .kWALKTHROUGHINTRO:
            msg = "WALKTHROUGH INTRO SCREEN"
            
            break
        case .kWALKTHROUGHPRODUCT:
            msg = "WALKTHROUGH PRODUCT SCREEN"
            
            break
        case .kWALKTHROUGHLANGUAGE:
            msg = "WALKTHROUGH LANGUAGE SCREEN"
            
            break
        case .kADDON:
            msg = "ADD ONS SCREEN"
            
            break
        case .kLIVECHAT:
            msg = "LIVE CHAT SCREEN"
            
            break
        case .kPROFILE:
            msg = "PROFILE SCREEN"
            
            break
        case .kEDITPROFILE:
            msg = "EDIT PROFILE SCREEN"
            
            break
        case .kSUPPORT:
            msg = "SUPPORT SCREEN"
            
            break
        case .kKNOWLEDGEBASE:
            msg = "KNOWLEDGE BASE SCREEN"
            
            break
        case .kSETTINGS:
            msg = "SETTINGS SCREEN"
            
            break
        case .kHISTORY:
            msg = "HISTORY SCREEN"
            
            break
        case .kSWITCHACCOUNT:
            msg = "SWITCH ACCOUNT SCREEN"
            
            break
        case .kWHATSNEW:
            msg = "WHAT'S NEW SCREEN"
            
            break

        default:
            msg = ""
            
            break
        }
        
        Answers.logContentView(withName: msg, contentType: "", contentId: "", customAttributes: nil)
    }
    
    func answersButton(_ key: BUTTON_KEY) {
        var msg: String
        
        switch key {
        case .kOVERVIEW_CLICKED:
            msg = "DASHBOARD CLICKED"
            
            break
        case .kLINE_CLICKED:
            msg = "LINE CLICKED"
            
            break
        case .kMAKEPAYMENT_CLICKED:
            msg = "MAKE PAYMENT CLICKED"
            
            break
        case .kLOADMORE_CLICKED:
            msg = "LOAD MORE CLICKED"
            
            break
        case .kFAB_CLICKED:
            msg = "FAB CLICKED"
            
            break
        case .kFAB_LIVECHAT_CLICKED:
            msg = "FAB LIVECHAT CLICKED"
            
            break
        case .kFAB_MENU_CLICKED:
            msg = "FAB MENU CLICKED"
            
            break
        case .kSWITCH_MOBILE_CLICKED:
            msg = "SWITCH MOBILE CLICKED"
            
            break
        case .kSWITCH_BROADBAND_CLICKED:
            msg = "SWITCH BROADBAND CLICKED"
            
            break
        case .kBACK_CLICKED:
            msg = "BACK CLICKED"
            
            break
        case .kEDIT_CREDIT_LIMIT_CLICKED:
            msg = "EDIT CREDIT LIMIT CLICKED"
            
            break
        case .kUNBILLED_USAGE_CLICKED:
            msg = "UNBILLED USAGE CLICKED"
            
            break
        case .kCHARGES_BREAKDOWN_CLICKED:
            msg = "CHARGES BREAKDOWN CLICKED"
            
            break
        case .kEDIT_ADD_ON_CLICKED:
            msg = "EDIT ADD ON CLICKED"
            
            break
        case .kREQUEST_APN_CLICKED:
            msg = "REQUEST APN CLICKED"
            
            break
        case .kSIM_PUK_CLICKED:
            msg = "SIM PUK CLICKED"
            
            break
        case .kSIM_PIN_CLICKED:
            msg = "SIM PIN CLICKED"
            
            break
        case .kSMS_CENTER_CLICKED:
            msg = "SMS CENTER CLICKED"
            
            break
        case .kSTART_CHAT_CLICKED:
            msg = "START CHAT CLICKED"
            
            break
        case .kSEND_CHAT_CLICKED:
            msg = "SEND CHAT CLICKED"
            
            break
        case .kHISTORY_BILL_CLICKED:
            msg = "BILL CLICKED"
            
            break
        case .kHISTORY_PURCHASE_CLICKED:
            msg = "PURCHASE CLICKED"
            
            break
        case .kLOGOUT_CLICKED:
            msg = "LOGOUT CLICKED"
            
            break
        case .kBUY_CLICKED:
            msg = "BUY CLICKED"
            
            break
        case .kTOGGLE_CLICKED:
            msg = "TOGGLE CLICKED"
            
            break
        case .kFORGOT_EMAIL_CLICKED:
            msg = "FORGOT EMAIL CLICKED"
            
            break
        case .kFORGOT_PASSWORD_CLICKED:
            msg = "FORGOT PASSWORD CLICKED"
            
            break
        case .kOKAY_CLICKED:
            msg = "OKAY CLICKED"
            
            break
        case .kCANCEL_CLICKED:
            msg = "CANCEL CLICKED"
            
            break
        case .kPAYMENT_FAILED:
            msg = "PAYMENT FAILED"
            
            break
        case .kPIN_SUCCESS:
            msg = "PIN SUCCESS"
            
            break
        case .kPUK_SUCCESS:
            msg = "PUK SUCCESS"
            
            break
        case .kAPN_SUCCESS:
            msg = "APN SUCCESS"
            
            break
        case .kVIEW_RECEIPT:
            msg = "kVIEW_RECEIPT"
            
            break
        case .kINCREASE_CREDIT_LIMIT_SUCCESS:
            msg = "INCREASE CREDIT LIMIT SUCCESS"
            
            break
        default:
            msg = ""
            
            break
        }
        
        Answers.logCustomEvent(withName: "Events", customAttributes: ["description": msg])
    }
    
    func answersAnalyticsLogin(_ isSuccessful: Bool){
        var msg: String
        
        if isSuccessful {
            msg = "Login Success"
        }
        else {
            msg = "Login Failed"
            
        }
        
        Answers.logLogin(withMethod: msg, success: isSuccessful as NSNumber, customAttributes: nil)
    }
    
    func performForceCrash() {
         Crashlytics.sharedInstance().crash()
    }
}
