//
//  StaticStrings.swift
//  selfcare
//
//  Created by ONG PEI KEI on 23/08/2017.
//
//

import Foundation

class StaticStrings {
    static let sharedInstance = StaticStrings()
    
    /***  VALUE  ***/
    struct GENERAL_VALUE {
        static let APP_SOURCE = "iOS"
        //static let APP_BUNDLE_ID = "" // BUNDLE NAME
        static let APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String //****
    }
    
    //PROD: Change the client secret to prod
    struct LOGIN_MODEL {
        static let GRANT_TYPE = "refresh_token"
        static let CLIENT_ID = "<Sensitive_Data>"
        static let CLIENT_SECRET = "<Sensitive_Data>"
 
    }
    
    /***  KEYS  ***/
    struct GENERAL_KEY {
        static let kAPP_SOURCE = "appSource"
        static let kAPP_VERSION = "appVersion"
    }
}
