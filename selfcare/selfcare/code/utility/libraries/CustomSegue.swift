import UIKit

class CustomSegue: UIStoryboardSegue {
    override func perform() {
        let fromViewController = self.source
        let toViewController = self.destination
        
        let containerView = fromViewController.view.superview
        let originalCenter = fromViewController.view.center
        
        toViewController.view.transform = CGAffineTransform(scaleX: 0.05, y: 0.05)
        toViewController.view.center = originalCenter
        
        containerView?.addSubview(toViewController.view)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            toViewController.view.transform = CGAffineTransform.identity
        }, completion: { success in
            // to include navigation controller
            let navController = UINavigationController(rootViewController: toViewController)
            fromViewController.present(navController, animated: false, completion: nil)
        })
    }
    
}

class CustomUnwind: UIStoryboardSegue {
    override func perform() {
        let fromViewController = self.source
        let toViewController = self.destination
        
        fromViewController.view.superview?.insertSubview(toViewController.view, at: 0)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            fromViewController.view.transform = CGAffineTransform(scaleX: 0.05, y: 0.05)
        }, completion: { success in
            // to dismiss into the root page
            fromViewController.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
        })
    }
    
}
