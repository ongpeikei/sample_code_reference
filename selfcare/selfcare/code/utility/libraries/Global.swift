//
//  Global.swift
//  selfcare
//
//  Created by ONG PEI KEI on 05/05/2017.
//
//

import Foundation
import UIKit
import SVProgressHUD
import Locksmith

// TO DO : 
//PRODUTION = true, UAT = false
let isProductionServer: Bool = false

let REG_EX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
let REG_EX_FORMAT = "SELF MATCHES %@"

let RADIUS:CGFloat = 3

class Global {
    static let sharedInstance = Global()
    static var productType = Global.PROD_TYPE.kMOBILE
    static var language = Global.LANGUAGE.kENGLISH
    static let webServiceMngr = WebServiceManager.sharedInstance
    static let customText = CustomText.sharedInstance
    
    static var containerView:CustomCardCellViewController!
    
    static var pageType = VIEW_TYPE.kDEFAULT
    static var cellSequence:Array<String> = Array()
    
    static var dashboardHeight:CGFloat = 100
    static var lineHeight:CGFloat = 100
    static var unbilledUsageHeight:CGFloat = 100
    
    static let SELECTED_PRODUCT_TYPE:String = "selectedProductType"
    static let SELECTED_ACCOUNT_NO:String = "selectedAccountNo"
    
    fileprivate var isOffline: Bool = false
    
    fileprivate var isCustomerDetailsApiTokenRefreshed: Bool = false
    fileprivate var isDashboardApiTokenRefreshed: Bool = false
    fileprivate var isCreditUnbilledApiTokenRefreshed: Bool = false
    fileprivate var isAddOnBalanceApiTokenRefreshed: Bool = false
    fileprivate var isPukApiTokenRefreshed: Bool = false
    fileprivate var isBillHistoryApiTokenRefreshed: Bool = false
    fileprivate var isAddOnHistoryApiTokenRefreshed: Bool = false
    fileprivate var isDcbHistoryApiTokenRefreshed: Bool = false
    
    fileprivate var unbilledIsCalled: Bool = false
    fileprivate var paymentBillIsCalled: Bool = false
    fileprivate var paymentCreditLimitIsCalled: Bool = false
    fileprivate var paymentIRIsCalled: Bool = false
    fileprivate var paymentShouldShowError: Bool = false
    
    fileprivate var lineFlag: Bool = false
    fileprivate var isFromDrawer: Bool = false
    
    fileprivate var amountToDeposit: String = ""
    fileprivate var amountToPay: String = ""
    fileprivate var amountToDisplay: String = ""
    fileprivate var creditLimitToDisplay: String = ""
    fileprivate var paymentURL: String = ""
    fileprivate var paymentType: PAYMENT_TYPE = .kBILL // default value
    
    fileprivate var isLoadMoreShouldDisplay: Bool = true
    
    fileprivate var isLiveChatSession: Bool = false
    fileprivate var mainPageCurrentPosition: Int = 0;
    
    fileprivate var reloadBillHistoryFlag: Bool = false
    fileprivate var reloadPurchaseHistoryFlag: Bool = false

    fileprivate var shouldShowPushNotif: Bool = false
    fileprivate var shouldShowSeperateError: Bool = true
    fileprivate var isEndChatFromLogout: Bool = false
    
    // release details
    struct APP {
        static let UUID = NSUUID().uuidString.lowercased()
    }
    
    // locksmith Keys
    struct LOCKSMITH_KEY {
        static let CREDENTIALS = "credentials"
        static let EMAIL = "email"
        static let PASSWORD = "password"
    }
    
    struct USERDEFAULTS_KEY {
        static let LOAD_MORE_HIDE: String = "LOAD_MORE_HIDE"
        
        static let CURRENT_MSISDN: String = "CURRENT_MSISDN"
        static let PUK_KEY: String = "PUK"
        static let CUSTOMER_REFRESHED_TOKEN: String = "CUSTOMER_REFRESHED_TOKEN"
        static let OUTSTANDING_REFRESHED_TOKEN: String = "OUTSTANDING_REFRESHED_TOKEN"
        static let UNBILLED_REFRESHED_TOKEN: String = "UNBILLED_REFRESHED_TOKEN"
        static let ADDONBALANCE_REFRESHED_TOKEN: String = "ADDONBALANCE_REFRESHED_TOKEN"
        
        static let HISTORY_BILL_REFRESHED_TOKEN: String = "HISTORY_BILL_REFRESHED_TOKEN"
        static let HISTORY_ADDON_REFRESHED_TOKEN: String = "HISTORY_ADDON_REFRESHED_TOKEN"
        static let HISTORY_DCB_REFRESHED_TOKEN: String = "HISTORY_DCB_REFRESHED_TOKEN"
        
        static let UNBILLED_FLAG: String = "UNBILLED_FLAG"
        static let PAYMENT_CREDIT_LIMIT_FLAG = "PAYMENT_CREDIT_LIMIT_FLAG"
        
        static let FIRST_INSTALL = "QUICK_TIPS"
        
        static let CHAT_SESSION = "PARTICIPANT_ID"
        static let CHAT_MESSAGE_LIST = "CHAT_LIST"
    }
    
    // ViewController Key
    enum VIEW_TYPE {
        case kWALKTHROUGHINTRO
        case kWALKTHROUGHPRODUCT
        case kWALKTHROUGHLANGUAGE
        
        case kEMAIL
        case kPASSWORD
        
        case kDASHBOARD
        case kCHARGES_BREAKDOWN
        case kUNBILLED_USAGE
        
        case kLINE
        case kEDIT_ADDON
        
        case kSWITCH_PRODUCT
        case kSWITCH_ACCOUNT
        case kPROFILE
        case kBILL
        case kPURCHASE
        case kAPP_SETTINGS
        
        case kPAYMENT
        
        case kDEFAULT
    }
    
    enum PROD_TYPE : String {
        case kMOBILE = "SS"
        case kBROADBAND = "LS"
    }
    
    enum LANGUAGE {
        case kENGLISH
        case kMALAY
        case kCHINESE
    }
    
    enum PAYMENT_TYPE {
        case kBILL
        case kCREDIT_LIMIT
        case kIR
    }
    
    // storyboard IDs
    struct IDENTIFIER {
        static let DASHBOARD = "DashboardViewController"
        static let LINE = "LineViewController"
        
        static let WALKTHROUGHINTRO = "WalkthroughIntroController"
        static let WALKTHROUGHPRODUCT = "WalkthroughProductController"
        static let WALKTHROUGHLANGUAGE = "WalkthroughLanguageController"
        
        static let BILL = "BillViewController"
        static let PURCHASE = "PurchaseViewController"
        
        static let PAYMENT = "PaymentViewController"
    }
    
    struct STATUS {
        static let ACTIVE = "active"
        static let PROCESSING = "processing"
        static let INACTIVE = "inactive"
        static let SUSPENDED = "suspended"
        static let TERMINATED = "terminated"
    }
    
    struct RAW_DATE_FORMAT {
        static let YYYYMMDD = "yyyyMMdd"
        static let DDMMYYYY_HHMM_A = "dd/MM/yyyy hh:mm a"
    }
    
    struct FINAL_DATE_FORMAT {
        static let DD_MM_YYYY = "dd MMM yyyy"
        static let DD_MM_YYYY_HHMM_A = "dd MMM yyyy hh:mm a"
        static let RECEIPT_DATE_FORMAT = "dd MMM yyyy - hh:mm:ss"
    }
    
    // Account STATUS TYPE
    struct STATUS_TYPE {
        static let ACTIVE = "active"
        static let PROCESSING = "processing"
        static let INACTIVE = "inactive"
        static let SUSPENDED = "suspended"
        static let TERMINATED = "terminated"
    }
    
    // Payment Status Type
    struct PAYMENT_STATUS {
        static let SUCCESS = "0"
        static let FAILED = "1"
        static let PARTIAL = "2"
        static let PENDING = "3"
    }
    
    func initViewController(_ vcKey: VIEW_TYPE) -> UIViewController {
        let storyboard = UIStoryboard(name: "MainPage", bundle: nil)
        
        switch vcKey {
        case .kDASHBOARD:
            return storyboard.instantiateViewController(withIdentifier: IDENTIFIER.DASHBOARD) as! DashboardViewController
        case .kLINE:
            return storyboard.instantiateViewController(withIdentifier: IDENTIFIER.LINE) as! LineViewController
            
        /*case .kWALKTHROUGHINTRO:
            return storyboard.instantiateViewController(withIdentifier: ID.WALKTHROUGHINTRO) as! WalkthroughIntroViewController
        case .kWALKTHROUGHPRODUCT:
            return storyboard.instantiateViewController(withIdentifier: ID.WALKTHROUGHPRODUCT) as! WalkthroughProductViewController
        case .kWALKTHROUGHLANGUAGE:
            return storyboard.instantiateViewController(withIdentifier: ID.WALKTHROUGHLANGUAGE) as! WalkthroughLangaugeViewController*/
            
        case .kPAYMENT:
            return storyboard.instantiateViewController(withIdentifier: IDENTIFIER.PAYMENT) as! PaymentViewController
            
        case .kBILL:
            return storyboard.instantiateViewController(withIdentifier: IDENTIFIER.BILL) as! BillViewController
        default: // case .kPURCHASE: default value required
            return storyboard.instantiateViewController(withIdentifier: IDENTIFIER.PURCHASE) as! PurchaseViewController
        }
    }
    
    func getProductionServer() -> Bool {
        return isProductionServer
    }
    
    func getImagePath() -> String {
        if getProductionServer() {
            let imagePathPROD = "<Sensitive_Data>"
            
            return imagePathPROD
        } else {
            let imagePathUAT = "<Sensitive_Data>"
            return imagePathUAT
        }
    }
    
    func sharedUserDefault() -> UserDefaults {
        return UserDefaults.standard
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = REG_EX
        
        let emailTest = NSPredicate(format:REG_EX_FORMAT, emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func getShouldShowPushNotif() -> Bool {
        return shouldShowPushNotif
    }
    func setShouldShowPushNotif(_ shouldShow: Bool) {
        self.shouldShowPushNotif = shouldShow
    }
    
    func getShouldShowSeperateError() -> Bool {
        return shouldShowSeperateError
    }
    func setShouldShowSeperateError(_ shouldShow: Bool) {
        self.shouldShowSeperateError = shouldShow
    }
    
    func getShouldReloadBillHistory() -> Bool {
        return reloadBillHistoryFlag
    }
    func setShouldReloadBillHistory(_ flag: Bool) {
        self.reloadBillHistoryFlag = flag
    }
    
    func getShouldReloadPurchaseHistory() -> Bool {
        return reloadPurchaseHistoryFlag
    }
    func setShouldReloadPurchaseHistory(_ flag: Bool) {
        self.reloadPurchaseHistoryFlag = flag
    }
    
    func getIsCustomerDetailsApiTokenRefreshed() -> Bool {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.CUSTOMER_REFRESHED_TOKEN) != nil {
            isCustomerDetailsApiTokenRefreshed = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.CUSTOMER_REFRESHED_TOKEN)
        }
        
        return isCustomerDetailsApiTokenRefreshed
    }
    func setIsCustomerDetailsApiTokenRefreshed(_ flag: Bool) {
        isCustomerDetailsApiTokenRefreshed = flag
        UserDefaults.standard.set(flag, forKey: Global.USERDEFAULTS_KEY.CUSTOMER_REFRESHED_TOKEN)
    }
    
    func getIsDashboardApiTokenRefreshed() -> Bool {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.OUTSTANDING_REFRESHED_TOKEN) != nil {
            isDashboardApiTokenRefreshed = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.OUTSTANDING_REFRESHED_TOKEN)
        }
        
        return isDashboardApiTokenRefreshed
    }
    func setIsDashboardApiTokenRefreshed(_ flag: Bool) {
        isDashboardApiTokenRefreshed = flag
        UserDefaults.standard.set(flag, forKey: Global.USERDEFAULTS_KEY.OUTSTANDING_REFRESHED_TOKEN)
    }
    
    func getIsCreditUnbilledApiTokenRefreshed() -> Bool {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.UNBILLED_REFRESHED_TOKEN) != nil {
            isCreditUnbilledApiTokenRefreshed = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.UNBILLED_REFRESHED_TOKEN)
        }
        
        return isCreditUnbilledApiTokenRefreshed
    }
    func setIsCreditUnbilledApiTokenRefreshed(_ flag: Bool) {
        isCreditUnbilledApiTokenRefreshed = flag
        UserDefaults.standard.set(flag, forKey: Global.USERDEFAULTS_KEY.UNBILLED_REFRESHED_TOKEN)
    }
    
    func getIsAddOnBalanceApiTokenRefreshed(_ msisdn: String) -> Bool {
        if UserDefaults.standard.object(forKey: msisdn) != nil {
            isAddOnBalanceApiTokenRefreshed = UserDefaults.standard.bool(forKey: msisdn)
        }
        
        return isAddOnBalanceApiTokenRefreshed
    }
    
    func setIsAddOnBalanceApiTokenRefreshed(_ flag: Bool, _ isGlobalNotif: Bool) {
        let lines = CustomerDetailsModel.sharedInstance.getAccountLines()
        
        if lines.count > 0 {
            for line in lines {
                if isGlobalNotif {
                    UserDefaults.standard.set(flag, forKey: line.msisdn)
                    //print("isGlobalNotif -- \(line.msisdn) -- \(String(describing: UserDefaults.standard.object(forKey: line.msisdn)))")
                } else {
                   // print("notGlobalNotif -- \(line.msisdn)")
                    if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.CURRENT_MSISDN) != nil {
                        //print("current msisdn not nil -- \(line.msisdn) -- \(String(describing: UserDefaults.standard.object(forKey: line.msisdn)))")
                        let currentMsisdn = UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.CURRENT_MSISDN)
                        if  line.msisdn.isEqual(currentMsisdn)  {
                            UserDefaults.standard.set(flag, forKey: line.msisdn)
                          //  print("current msisdn is equal -- \(line.msisdn) -- \(String(describing: UserDefaults.standard.object(forKey: line.msisdn)))")
                            break
                        }
                    }
                }
            }
        }
    }
    
    func getIsPukApiTokenRefreshed(_ msisdn: String) -> Bool {
        if UserDefaults.standard.object(forKey: msisdn) != nil {
            isPukApiTokenRefreshed = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.PUK_KEY + msisdn)
        }
        
        return isPukApiTokenRefreshed
    }
    
    func setIsPukApiTokenRefreshed(_ flag: Bool, _ isGlobalNotif: Bool) {
        let lines = CustomerDetailsModel.sharedInstance.getAccountLines()
        
        if lines.count > 0 {
            for line in lines {
                if isGlobalNotif {
                    UserDefaults.standard.set(flag, forKey: Global.USERDEFAULTS_KEY.PUK_KEY + line.msisdn)
                    //print("isGlobalNotif -- \(line.msisdn) -- \(String(describing: UserDefaults.standard.object(forKey: line.msisdn)))")
                } else {
                    // print("notGlobalNotif -- \(line.msisdn)")
                    if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.CURRENT_MSISDN) != nil {
                        //print("current msisdn not nil -- \(line.msisdn) -- \(String(describing: UserDefaults.standard.object(forKey: line.msisdn)))")
                        let currentMsisdn = UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.CURRENT_MSISDN)
                        if  line.msisdn.isEqual(currentMsisdn)  {
                            UserDefaults.standard.set(flag, forKey: Global.USERDEFAULTS_KEY.PUK_KEY + line.msisdn)
                            //  print("current msisdn is equal -- \(line.msisdn) -- \(String(describing: UserDefaults.standard.object(forKey: line.msisdn)))")
                            break
                        }
                    }
                }
            }
        }
    }
    
    func getIsBillHistoryApiTokenRefreshed() -> Bool {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.HISTORY_BILL_REFRESHED_TOKEN) != nil {
            isBillHistoryApiTokenRefreshed = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.HISTORY_BILL_REFRESHED_TOKEN)
        }
        
        return isBillHistoryApiTokenRefreshed
    }
    func setIsBillHistoryApiTokenRefreshed(_ flag: Bool) {
        isBillHistoryApiTokenRefreshed = flag
        UserDefaults.standard.set(flag, forKey: Global.USERDEFAULTS_KEY.HISTORY_BILL_REFRESHED_TOKEN)
    }
    
    func getIsAddonHistoryApiTokenRefreshed() -> Bool {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.HISTORY_ADDON_REFRESHED_TOKEN) != nil {
            isAddOnHistoryApiTokenRefreshed = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.HISTORY_ADDON_REFRESHED_TOKEN)
        }
        
        return isAddOnHistoryApiTokenRefreshed
    }
    func setIsAddOnHistoryApiTokenRefreshed(_ flag: Bool) {
        isAddOnHistoryApiTokenRefreshed = flag
        UserDefaults.standard.set(flag, forKey: Global.USERDEFAULTS_KEY.HISTORY_ADDON_REFRESHED_TOKEN)
    }
    
    func getIsDcbHistoryApiTokenRefreshed() -> Bool {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.HISTORY_DCB_REFRESHED_TOKEN) != nil {
            isDcbHistoryApiTokenRefreshed = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.HISTORY_DCB_REFRESHED_TOKEN)
        }
        
        return isDcbHistoryApiTokenRefreshed
    }
    func setIsDcbHistoryApiTokenRefreshed(_ flag: Bool) {
        isDcbHistoryApiTokenRefreshed = flag
        UserDefaults.standard.set(flag, forKey: Global.USERDEFAULTS_KEY.HISTORY_DCB_REFRESHED_TOKEN)
    }
    
    func setIsApiTokenRefreshedToFalse(_ flag: Bool, _ isGlobalNotif: Bool) {
        setIsCustomerDetailsApiTokenRefreshed(flag)
        setIsDashboardApiTokenRefreshed(flag)
        setIsCreditUnbilledApiTokenRefreshed(flag)
        setIsAddOnBalanceApiTokenRefreshed(flag, isGlobalNotif)
        setIsPukApiTokenRefreshed(flag, isGlobalNotif)
        setIsBillHistoryApiTokenRefreshed(flag)
        setIsAddOnHistoryApiTokenRefreshed(flag)
        setIsDcbHistoryApiTokenRefreshed(flag)
    }
    
    func getSavedStringValue(_ key:String) -> String? {
        return sharedUserDefault().object(forKey: key) as? String
    }
    func saveStringValue(_ value:String, _ key:String) {
        sharedUserDefault().set(value, forKey: key)
    }
    
    func getSavedIntValue(_ key:String) -> Int? {
        return sharedUserDefault().object(forKey: key) as? Int
    }
    func saveIntValue(_value: Int, _ key:String) {
        sharedUserDefault().set(_value, forKey: key)
    }
    
    func getSavedBooleanValue(_ key:String) -> Bool? {
        return sharedUserDefault().object(forKey: key) as? Bool
    }
    func saveBooleanValue(_ value: Bool, _ key:String) {
        sharedUserDefault().set(value, forKey: key)
    }
    
    func removeKey(_ key: String){
        sharedUserDefault().removeObject(forKey: key)
    }
    
    func getProdType() -> PROD_TYPE {
        return Global.productType
    }
    func setProdType(_ type: PROD_TYPE) {
        Global.productType = type
        saveStringValue(type.rawValue, Global.SELECTED_PRODUCT_TYPE)
    }
    
    func getLanguage() -> LANGUAGE {
        return Global.language
    }
    func setLanguage(_ language: LANGUAGE) {
        Global.language = language
    }
    
    func getUnbilledFlag() -> Bool? {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.UNBILLED_FLAG) != nil {
            unbilledIsCalled = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.UNBILLED_FLAG)
        }
        return unbilledIsCalled
    }
    func setUnbilledFlag(_ flag: Bool) {
        unbilledIsCalled = flag
        UserDefaults.standard.set(unbilledIsCalled, forKey: Global.USERDEFAULTS_KEY.UNBILLED_FLAG)
    }
    
    func getPaymentBillFlag() -> Bool? {
        return paymentBillIsCalled
    }
    func setPaymentBillFlag(_ flag: Bool) {
        paymentBillIsCalled = flag
    }
    
    func getPaymentCreditLimitFlag() -> Bool? {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.PAYMENT_CREDIT_LIMIT_FLAG) != nil {
            paymentCreditLimitIsCalled = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.PAYMENT_CREDIT_LIMIT_FLAG)
        }
        return paymentCreditLimitIsCalled
    }
    func setPaymentCreditLimitFlag(_ flag: Bool) {
        paymentCreditLimitIsCalled = flag
        UserDefaults.standard.set(paymentCreditLimitIsCalled, forKey: Global.USERDEFAULTS_KEY.PAYMENT_CREDIT_LIMIT_FLAG)
    }
    
    func getPaymentIRFlag() -> Bool? {
        return paymentIRIsCalled
    }
    func setPaymentIRFlag(_ flag: Bool) {
        paymentIRIsCalled = flag
    }
    
    func getPaymentShouldShowError() -> Bool? {
        return paymentShouldShowError
    }
    func setPaymentShouldShowError(_ flag: Bool) {
        paymentShouldShowError = flag
    }
    
    func getAmountToDeposit() -> String? {
        return amountToDeposit
    }
    func setAmountToDeposit(_ amount:String) {
        amountToDeposit = amount
    }
    
    func getAmountToPay() -> String? {
        return amountToPay
    }
    func setAmountToPay(_ amount:String) {
        amountToPay = amount
    }
    
    func getAmountToDisplay() -> String {
        return amountToDisplay
    }
    func setAmountToDisplay(_ amount: String) {
        amountToDisplay = amount
    }
    
    func getCreditLimitValueDisplay() -> String? {
        return creditLimitToDisplay
    }
    func setCreditLimitValueDisplay(_ creditLimit: String) {
        creditLimitToDisplay = creditLimit
    }
    
    func getPaymentURL() -> String? {
        return paymentURL
    }
    func setPaymentURL(_ url:String) {
        paymentURL = url
    }
    
    func getPaymentType() -> PAYMENT_TYPE {
        return paymentType
    }
    func setPaymentType(_ paymentType: PAYMENT_TYPE) {
        self.paymentType = paymentType
    }
    
    func getLoadMoreShouldDisplay() -> Bool? {
        if UserDefaults.standard.object(forKey: Global.USERDEFAULTS_KEY.LOAD_MORE_HIDE) != nil {
           isLoadMoreShouldDisplay = UserDefaults.standard.bool(forKey: Global.USERDEFAULTS_KEY.LOAD_MORE_HIDE)
        }
        
        return isLoadMoreShouldDisplay
    }
    func setLoadMoreShouldDisplay(_ willDisplay:Bool) { // insert here the checking of data
        isLoadMoreShouldDisplay = willDisplay
        UserDefaults.standard.set(willDisplay, forKey: Global.USERDEFAULTS_KEY.LOAD_MORE_HIDE)
    }
    
    func getLineFlag() -> Bool? {
        return lineFlag
    }
    func setLineFlag(_ lineLoadFlag:Bool) {
        lineFlag = lineLoadFlag
    }
    
    func getLiveChatSession() -> Bool {
        if retrieveParticiapnId() != nil {
            isLiveChatSession = true
        } else {
            isLiveChatSession = false
        }
        
        return isLiveChatSession
    }
     
    func getMainPageCurrentPosition() -> Int {
        return mainPageCurrentPosition
    }
    func setMainPageCurrentPosition(_ position:Int) {
        mainPageCurrentPosition = position
    }
    
    func getSelectedMsisdn() -> Int {
        let position = getMainPageCurrentPosition()
        
        return position
    }
    
    func roundedImage(_ imageView:UIImageView) -> UIImageView {
        imageView.layer.borderWidth = 1
        imageView.layer.masksToBounds = false
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true
        
        return imageView
    }
    
    func setRoundedLabel(_ radius:CGFloat, _ accStatusView:UIView ) -> UIView {
        accStatusView.layer.masksToBounds = false
        accStatusView.layer.cornerRadius = radius
        accStatusView.clipsToBounds = true
        
        return accStatusView
    }
    
    func localized(_ stringKey:String) -> String {
        return NSLocalizedString(stringKey, comment: "")
    }
    
    func getFontAttribute(_ style: CustomText.FONT_STYLE, _ size: CustomText.FONT_SIZE) -> UIFont? {
        return UIFont(name: CustomText.sharedInstance.getStyle(style), size: CustomText.sharedInstance.getSize(size))
    }
    
    func getColorAttribute(_ color: CustomColor.COLOR, _ opacity: CustomColor.OPACITY) -> UIColor? {
        return CustomColor.sharedInstance.getColor(color, opacity)
    }
    
    func goBack(_ controller: UIViewController) {
        let transition: CATransition = CATransition()
        transition.duration = 0.25
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        controller.view.window!.layer.add(transition, forKey: nil)
        
        controller.dismiss(animated: false, completion: {});
    }
    
    func formatDate(_ date: String, _ rawFormat: String, _ finalFormat: String) -> String {
        guard
            !date.isEqual("") else {
            return "-"
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = rawFormat
        var formattedDate: String = ""
        
        if let updatedDate = dateFormatter.date(from: date) {
            dateFormatter.dateFormat = finalFormat
            formattedDate = dateFormatter.string(from: updatedDate)
        } else {
            // invalid format or convert other date format here
            return date
        }
        
        return formattedDate
    }
    
    func accountStatus(_ accStat: String) -> String {
        var status: String = ""
        
        if (accStat.isEqual("Active") || accStat.isEqual("ACC_STATUS_ACTIVE")) {
            status = STATUS.ACTIVE
        } else if (accStat.isEqual("Register")) {
            status = STATUS.PROCESSING
        } else if (accStat.isEqual("Inactive")) {
            status = STATUS.INACTIVE
        } else if (accStat.isEqual("One-way block") || accStat.isEqual("Two-way block") || accStat.isEqual("ACC_STATUS_SUSPEND_VOL")||accStat.isEqual("ACC_STATUS_SUSPEND_INVOL") || accStat.isEqual("Block") ) {
            status = STATUS.SUSPENDED
        } else if accStat.isEqual("Terminated") || accStat.isEqual("ACC_STATUS_TERMINATE_NON_OS") {
            status = STATUS.TERMINATED
        }
        
        return status
    }
    
    func getAccountStatusColor() -> UIColor {
        let status: String = CustomerDetailsModel.sharedInstance.getAccountStatus()
        var statusColor:UIColor? = getColorAttribute(.kGREY, .DEFAULT)
        
        if status.caseInsensitiveCompare(STATUS_TYPE.ACTIVE) == ComparisonResult.orderedSame {
            statusColor = getColorAttribute(.kLIGHT_BLUE, .DEFAULT)
            
        } else if status.caseInsensitiveCompare(STATUS_TYPE.PROCESSING) == ComparisonResult.orderedSame {
            statusColor = getColorAttribute(.kTEAL, .DEFAULT) // TODO: update when specs is given
            
        } else if status.caseInsensitiveCompare(STATUS_TYPE.INACTIVE) == ComparisonResult.orderedSame {
            statusColor = getColorAttribute(.kGREY, .DEFAULT) // TODO: update when specs is given
            
        } else if status.caseInsensitiveCompare(STATUS_TYPE.SUSPENDED) == ComparisonResult.orderedSame {
            statusColor = getColorAttribute(.kACCENT, .DEFAULT)
            
        } else { // terminated
            statusColor = getColorAttribute(.kDARK_GREY_3, .DEFAULT)
        }
        
        return statusColor!
    }
    
    func formatNegativeAmount(_ stringAmt: String, _ fromUnbilledDisplay: Bool) -> String {
        var updatedStringAmt: String = stringAmt.replacingOccurrences(of: " ", with: "")

        guard
            !(updatedStringAmt.isEqual("")),
            !(updatedStringAmt.isEqual("-"))else {
                
            return "0.00"
        }
        
        var doubleValue: Double = Double(updatedStringAmt)!
        if !fromUnbilledDisplay { //Default should be false
            doubleValue = doubleValue * (-1)
        }
     
        if (doubleValue < 0) {
            if fromUnbilledDisplay { //Default should be false
                doubleValue = doubleValue * (-1)
                updatedStringAmt = "(" + self.amountFormatter(String(doubleValue)) + ")" // use raw value for amount
            } else {
                updatedStringAmt = "(" + self.amountFormatter((updatedStringAmt)) + ")" // use raw value for amount
            }
        } else if (Double(updatedStringAmt) == 0) {
            updatedStringAmt = self.amountFormatter(updatedStringAmt) // use raw value for amount
        
        } else {
            updatedStringAmt = self.amountFormatter(String(doubleValue)) // use raw amount *1
        }
        
        return updatedStringAmt
    }
    
    func amountFormatter (_ amount: String) -> String {
        guard
            !(amount.isEqual("")),
            !(amount.isEqual("-"))else {
                return "0.00"
        }
        let amtFormat = Double(amount)
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 3
        formatter.groupingSize = 3
        formatter.groupingSeparator = ","
        formatter.minimumFractionDigits = 2
        formatter.decimalSeparator = "."
        formatter.alwaysShowsDecimalSeparator = true
        
        return formatter.string(from: amtFormat! as NSNumber)!
    }
    
    // 0 = dashboard
    // 1 = line
    // 2 = chargesBreakdown
    func isFeatureAccessible(_ pageTag: Int, _ delegate: CustomCardCellViewController) -> Bool {
        let statusValue: String = CustomerDetailsModel.sharedInstance.getAccountStatus()
        
        switch (pageTag) {
        case 0: // dashboard
            if (statusValue == STATUS.PROCESSING) {
                let dialog = CustomDialogView(type: .PROCESSING)
                dialog.delegateCard = delegate
                dialog.show(animated: true)
    
                return false;
            } else if (statusValue == STATUS.TERMINATED) {
                let dialog = CustomDialogView(type: .TERMINATED)
                dialog.delegateCard = delegate
                dialog.show(animated: true)
    
                return false;
            }
    
            break;
        case 1:
            if (statusValue == STATUS.TERMINATED) {
                let dialog = CustomDialogView(type: .TERMINATED)
                dialog.delegateCard = delegate
                dialog.show(animated: true)
    
                return false;
            } else if (statusValue == STATUS.PROCESSING) {
                let dialog = CustomDialogView(type: .PROCESSING)
                dialog.delegateCard = delegate
                dialog.show(animated: true)
                
                return false;
            }
    
            break;
        case 2:
            if (statusValue == STATUS.TERMINATED) {
                let dialog = CustomDialogView(type: .TERMINATED)
                dialog.delegateCard = delegate
                dialog.show(animated: true)
                
                return false;
            } else if (statusValue == STATUS.PROCESSING) {
                let dialog = CustomDialogView(type: .PROCESSING)
                dialog.delegateCard = delegate
                dialog.show(animated: true)
                
                return false;
            }
            
            break;
        default:
    
            break;
        }
    
        return true;
    }
    
    func showHUD() {
        if !SVProgressHUD.isVisible() { // to avoid showing two HUD
            var topController = UIApplication.shared.keyWindow?.rootViewController
            if topController == UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController?.presentedViewController {
                    topController = presentedViewController
                }
            }
            
            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.setBackgroundColor(UIColor.clear)
            SVProgressHUD.setForegroundColor(self.getColorAttribute(.kTEAL, .DEFAULT)!)
            SVProgressHUD.setRingThickness(5)
            SVProgressHUD.setContainerView(topController?.view)
            SVProgressHUD.show()
        }
    }
    
    func dismissHUD() {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
    }
    
    func formatMsisdn(_ msisdn :String) -> String {
        let msdnChar = msisdn.characters
        
        let range1 = msdnChar.index(msdnChar.startIndex, offsetBy: 4)
        let word1 = msisdn[..<range1]
        
        let start = msdnChar.index(msdnChar.startIndex, offsetBy: 4)
        let end = msdnChar.index(msdnChar.endIndex, offsetBy: 0)
        let range2 = start..<end
        let word2 = msisdn[range2]
        
        return "+" + word1 + "-" + word2
    }
    
    func getEstimatedLabelHeight(byText: String, font:UIFont, labelWidth:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = byText
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func getIsOffline() -> Bool {
        return isOffline
    }
    func setIsOffline(_ isOffline: Bool) {
        self.isOffline = isOffline
    }
    
    func getIsFromDrawer() -> Bool {
        return isFromDrawer
    }
    func setIsFromDrawer(_ flag: Bool) {
        isFromDrawer = flag
    }
    
    func checkUserCredentials() -> Bool {
        let email = UserDefaults.standard.object(forKey: LOCKSMITH_KEY.EMAIL)
        let password = UserDefaults.standard.object(forKey: LOCKSMITH_KEY.PASSWORD)
        
        guard
            (email != nil),
            (password != nil) else {
                
                return false // no credentials saved
        }
        
        return true
    }
    
    func getIsEndChatFromLogout() -> Bool {
        return isEndChatFromLogout
    }
    func setIsEndChatFromLogout(_ isFromLogout: Bool) {
        isEndChatFromLogout = isFromLogout
    }
    
    func saveParticipantId(_ participantId: String) {
        UserDefaults.standard.set(participantId, forKey: USERDEFAULTS_KEY.CHAT_SESSION)
    }
    
    func retrieveParticiapnId() -> String? {
        return UserDefaults.standard.string(forKey: USERDEFAULTS_KEY.CHAT_SESSION)
    }
    
    func clearParticipanID() {
        UserDefaults.standard.removeObject(forKey: USERDEFAULTS_KEY.CHAT_SESSION)
    }
    
    func saveChatMsg(_ msgList: [AnyObject]) {
        UserDefaults.standard.set(msgList, forKey: USERDEFAULTS_KEY.CHAT_MESSAGE_LIST)
    }
    
    func retrieveChatMsg() -> [AnyObject]? {
        return UserDefaults.standard.object(forKey: USERDEFAULTS_KEY.CHAT_MESSAGE_LIST) as? [AnyObject]
    }
    
    func showGeneralError() {
        let dialog = CustomDialogView(type: .GENERAL_ERROR)
        dialog.show(animated: true)
        
        return
    }

}
