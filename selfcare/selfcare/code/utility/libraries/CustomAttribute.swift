//
//  CustomAttribute.swift
//  selfcare
//
//  Created by ONG PEI KEI on 28/07/2017.
//
//

import Foundation
import UIKit

class CustomText {
    static let sharedInstance = CustomText()
    
    enum FONT_STYLE {
        case kLIGHT
        case kREGULAR
        case kSEMI_BOLD
        case kBOLD
        case kEXTRA_BOLD
        case kBLACK
    }
    
    enum FONT_SIZE {
        case kSMALL_XX
        case kSMALL_X
        case kSMALL
        case kMEDIUM
        case kLARGE
        case kLARGE_X
        case kLARGE_XX
        case kLARGE_XXX
    }

    func getStyle(_ style: FONT_STYLE) -> String {
        var fontStyle = ""
        
        switch style {
        case .kLIGHT:
            fontStyle = "Panton-Light"
            
            break
        case .kREGULAR:
            fontStyle = "Panton-Regular"
            
            break
        case .kSEMI_BOLD:
            fontStyle = "Panton-SemiBold"
            
            break
        case .kBOLD:
            fontStyle = "Panton-Bold"
            
            break
        case .kEXTRA_BOLD:
            fontStyle = "Panton-ExtraBold"
            
            break
        case .kBLACK:
            fontStyle = "Panton-Black"
            
            break
        }
        
        return fontStyle
    }
    
    func getSize(_ size: FONT_SIZE) -> CGFloat {
        var fontSize: CGFloat = 0
        
        switch size {
        case .kSMALL_XX:
            fontSize = 10
            
            break
        case .kSMALL_X:
            fontSize = 11
            
            break
        case .kSMALL:
            fontSize = 12
            
            break
        case .kMEDIUM:
            fontSize = 14
            
            break
        case .kLARGE:
            fontSize = 16
            
            break
        case .kLARGE_X:
            fontSize = 20
            
            break
        case .kLARGE_XX:
            fontSize = 28
            
            break
        case .kLARGE_XXX:
            fontSize = 30
            
            break
        }
        
        return fontSize
    }
    
    func getLblLineSpacing(_ space: CGFloat, _ strValue: String) -> NSAttributedString {
        let attrString = NSMutableAttributedString(string: strValue)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = space
        attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: NSRange(location: 0, length: strValue.characters.count))
        //customLbl.attributedText = attrString
        
        return attrString
    }
    
}

class CustomColor {
    static let sharedInstance = CustomColor()
    
    enum COLOR {
        case kPRIMARY
        case kPRIMARY_DARK
        case kACCENT
        
        case kWHITE
        case kBLACK
        case kBLUE
        case kLIGHT_BLUE
        case kLIGHT_GREY
        case kGREEN
        case kRED
        case kYELLOW
        case kGREY
        case kDARK_GREY
        case kDARK_GREY_2
        case kDARK_GREY_3
        case kTEAL
        case kTRANSPARENT
    }
    
    enum OPACITY {
        case OPACITY_24
        case OPACITY_34
        case OPACITY_38
        case OPACITY_54
        case OPACITY_58
        case OPACITY_70
        case DEFAULT
    }
    
    func getColor(_ color: COLOR, _ opacity: OPACITY) -> UIColor {
        var customColor: UIColor = UIColor()
        var redColor: CGFloat = 255/255
        var greenColor: CGFloat = 255/255
        var blueColor: CGFloat = 255/255
        var opacityLevel: CGFloat = 1.0
        
        switch color {
        case .kPRIMARY:
            redColor = 44/255
            greenColor = 37/255
            blueColor = 86/255
            
            break
        case .kPRIMARY_DARK:
            redColor = 32/255
            greenColor = 28/255
            blueColor = 65/255
            
            break
        case .kACCENT:
            redColor = 218/255
            greenColor = 0/255
            blueColor = 107/255
            
            break
        case .kWHITE:
            redColor = 255/255
            greenColor = 255/255
            blueColor = 255/255
            
            break
        case .kBLACK:
            redColor = 0/255
            greenColor = 0/255
            blueColor = 0/255
            
            break
        case .kBLUE:
            redColor = 33/255
            greenColor = 150/255
            blueColor = 243/255
            
            break
        case .kLIGHT_BLUE: 
            redColor = 0/255
            greenColor = 174/255
            blueColor = 239/255
            
            break
        case .kLIGHT_GREY:
            redColor = 170/255
            greenColor = 170/255
            blueColor = 170/255
            
            break
        case .kGREEN:
            redColor = 33/255
            greenColor = 148/255
            blueColor = 50/255
            
            break
        case .kRED:
            redColor = 255/255
            greenColor = 0/255
            blueColor = 0/255
            
            break
        case .kYELLOW:
            redColor = 251/255
            greenColor = 166/255
            blueColor = 0/255
            
            break
        case .kGREY:
            redColor = 211/255
            greenColor = 211/255
            blueColor = 211/255
            
            break
        case .kTEAL:
            redColor = 1/255
            greenColor = 164/255
            blueColor = 164/255
            
            break
        case .kDARK_GREY:
            redColor = 44/255
            greenColor = 37/255
            blueColor = 86/255
            
            break
        case .kDARK_GREY_2:
            redColor = 102/255
            greenColor = 102/255
            blueColor = 102/255
            
            break
        case .kDARK_GREY_3:
            redColor = 153/255
            greenColor = 153/255
            blueColor = 153/255
            
            break
        case .kTRANSPARENT:
            customColor = UIColor.clear
            
            break
        }
        
        switch opacity {
        case .OPACITY_24:
            opacityLevel = 0.24
            
            break
        case .OPACITY_34:
            opacityLevel = 0.34
            
            break
        case .OPACITY_38:
            opacityLevel = 0.38
            
            break
        case .OPACITY_54:
            opacityLevel = 0.54
            
            break
        case .OPACITY_58:
            opacityLevel = 0.58
            
            break
        case .OPACITY_70:
            opacityLevel = 0.70
            
            break
        default:
            opacityLevel = 1.0
            
            break
        }
        
        customColor = UIColor(red: redColor, green: greenColor, blue: blueColor, alpha: opacityLevel)
        
        return customColor
    }
    
}
