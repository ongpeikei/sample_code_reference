//
//  PushNotification.swift
//  selfcare
//
//  Created by ONG PEI KEI on 03/08/2017.
//
//

import Foundation
import UIKit
import CoreTelephony
import Locksmith

class PushNotification {
    static let sharedInstance = PushNotification()
    let globalInstance = Global.sharedInstance
    
    fileprivate var isPushNotifEnabled: Bool = true
    fileprivate var isUserFromLogin: Bool = false
    fileprivate let deviceSettingsPath = "App-Prefs:root"
    fileprivate var isPNDialogDisplayed: Bool = false
    fileprivate var isLogout: Bool = false
    
    var dialog = CustomDialogView(type: .PUSH_NOTIF, pushTitle: "<Sensitive_Data>", pushBody: "<Sensitive_Data>") // default value
    
    typealias CompletionBlock = (_ responseDidFail : Bool, _ isServerMaintenance : Bool) -> Void
    
    struct NOTIFICATION {
        static let kAPS = "aps"
        static let kALERT = "alert"
        static let kTITLE = "title"
        static let kBODY = "body"
        static let kCATEGORY = "category"
    }
    
    struct USER_DEFAULTS {
        static let kFCM_TOKEN = "fcmToken"
        static let kIS_PUSH_NOTIF_ENABLED = "isPushNotifEnabled"
    }
    
    struct NETWORK_TYPE {
        static let LTE = "4G_LTE"
        static let OTHERS = "Others"
    }
    
    struct CLICK_ACTION {
        static let ZERO_DR = "zerodr"
    }
    
    enum PUSH_NOTIFICATION_PAGE {
        case kLOGIN
        case kFCM_TOKEN_RECEIVED
        case kLOGOUT
        //case kSETTINGS // just add the page when push notification settings is available
    }
    
    func handlePushTokenAPICall(_ fromPage: PUSH_NOTIFICATION_PAGE, responseHandler: CompletionBlock?) {
        switch fromPage {
        case .kLOGIN:
            isLogout = false
            
            if getIsUserFromLogin() && (fetchFcmToken() != nil) {
                callRequestPushToken(responseHandler: {(isResponseDidFail, isServerMaintenance) in
                    return responseHandler!(isResponseDidFail, isServerMaintenance)
                })
            } else {
                return responseHandler!(false, false)
            }
            
            break
        case .kFCM_TOKEN_RECEIVED:
            /*if getIsUserFromLogin() {
                callRequestPushToken()
            }*/
            
            break
        case .kLOGOUT:
            isLogout = true
            
            if (fetchFcmToken() != nil) {
                callRequestPushToken(responseHandler: {(isResponseDidFail, isServerMaintenance) in
                    return responseHandler!(isResponseDidFail, isServerMaintenance)
                })
            } else {
                return responseHandler!(false, false)
            }
            
            break
        }
    }
    
    // didWakeByNotif = TRUE means the app is dead and waken via push notif
    func handleNotification(_ title: String, _ body: String, _ categoryIdentifier: String) {
        
        // to validate if the dialog should be the "ZERO DR"
        let clickAction = categoryIdentifier.replacingOccurrences(of: " ", with: "")
        
        // handle multiple notification fire and check if user is logged in
        if !getIsPushNotifDialogDisplayed() && globalInstance.checkUserCredentials() {
            if (clickAction.caseInsensitiveCompare(CLICK_ACTION.ZERO_DR) == ComparisonResult.orderedSame) && (getNetworkType() == NETWORK_TYPE.OTHERS){
                dialog = CustomDialogView(type: .PUSH_NOTIF_ZERODR, pushTitle: title, pushBody: body)
            } else {
                dialog = CustomDialogView(type: .PUSH_NOTIF, pushTitle: title, pushBody: body)
            }
            
            let rootFrontView = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController // get the view at the front of ROOT
            if rootFrontView != nil  {
                showDialog()
                
            } else { // execute this so dialog will show on the dashboard NOT in the ROOT/MAIN PAGE
                globalInstance.setShouldShowPushNotif(true)
            }
            
            setIsPushNotifDialogDisplayed(true)
        }
    }
    
    func postNotif() {
        NotificationCenter.default.post(name: Notification.Name(rawValue:"PushNotification"),
                                      object: nil,
                                    userInfo: nil)
    }
    
    func showDialog() {
        dialog.show(animated: true)
    }
    
    func openDeviceSettings() {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: deviceSettingsPath)!, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(URL(string: deviceSettingsPath)!)
        }
    }
    
    fileprivate func getNetworkType() -> String {
        let telefonyInfo = CTTelephonyNetworkInfo()
        if let radioAccessTechnology = telefonyInfo.currentRadioAccessTechnology{
            switch radioAccessTechnology{
            case CTRadioAccessTechnologyLTE:
                
                return NETWORK_TYPE.LTE
            default:
                return NETWORK_TYPE.OTHERS
            }
        }
        
        return NETWORK_TYPE.OTHERS
    }
    
    // GETTER and SETTER
    func getPushNotifEnable() -> Bool {
        isPushNotifEnabled = UserDefaults.standard.bool(forKey: USER_DEFAULTS.kIS_PUSH_NOTIF_ENABLED)
        setPushNotifEnable(isPushNotifEnabled)
        
        return isPushNotifEnabled
    }
    func setPushNotifEnable(_ isEnabled: Bool) {
        UserDefaults.standard.set(isEnabled, forKey: USER_DEFAULTS.kIS_PUSH_NOTIF_ENABLED)
        isPushNotifEnabled = isEnabled
    }
    
    func getIsPushNotifDialogDisplayed() -> Bool {
        return isPNDialogDisplayed
    }
    func setIsPushNotifDialogDisplayed(_ isDisplayed: Bool) {
        isPNDialogDisplayed = isDisplayed
    }
    
    func getIsUserFromLogin() -> Bool {
        return isUserFromLogin
    }
    func setIsUserFromLogin(_ isFromLogin: Bool)  {
        isUserFromLogin = isFromLogin
    }
    
    func fetchFcmToken() -> String? {
        return UserDefaults.standard.value(forKey: USER_DEFAULTS.kFCM_TOKEN) as? String
    }
    func saveFcmToken(_ token: String) {
        UserDefaults.standard.set(token, forKey: USER_DEFAULTS.kFCM_TOKEN)
    }
    
    // API call here
    func callRequestPushToken(responseHandler: CompletionBlock?) {
        var clickAction: String = PushNotificationModel.ACTION_TYPE.kDISABLE
        if getPushNotifEnable() {
            clickAction = PushNotificationModel.ACTION_TYPE.kENABLE
        }
        
        PushNotificationModel.sharedInstance.preparePushTokenParams(SwitchModel.getSelectedAccountNo()!, getNetworkType(), clickAction)
        Global.webServiceMngr.makeRequest(PushNotificationModel.sharedInstance.getPushTokenParams()) { (isResponseDidFail, isServerMaintenance) in
            return responseHandler!(isResponseDidFail, isServerMaintenance)
        }
    }
    
    func clearCache() {
        // clear data base
        DataFactory.sharedInstance.deleteAllDatabase()
        
        //reset livechat model
        if globalInstance.getLiveChatSession() {
            globalInstance.clearParticipanID()
            globalInstance.setIsEndChatFromLogout(true)
            
            LiveChatViewController.sharedInstance.callEndSessionFromDialog()
        }
        
        // reset refresh token
        globalInstance.setIsCustomerDetailsApiTokenRefreshed(false)
        
        // reset is from drawer flag (because logout is from drawer)
        globalInstance.setIsFromDrawer(false)
        
        // clear  all userDefaults except push notification fcmToken and participant ID
        let keys = UserDefaults.standard.dictionaryRepresentation().keys
        for (key) in keys {
//            print("Removed Object: " + key)
            
            if key == Global.USERDEFAULTS_KEY.FIRST_INSTALL {
                continue
            }
            
            if (key == CHECKER_FCM_TOKEN) {
                continue
            }
            
            UserDefaults.standard.removeObject(forKey: key)
        }
    }
    
}
