//
//  CustomDialogView.swift
//  selfcare
//
//  Created by ONG PEI KEI on 06/07/2017.
//
//

import UIKit
import Alamofire

class CustomDialogView: UIView, UITableViewDelegate, UITableViewDataSource, CustomDialogAnimation, UIDocumentInteractionControllerDelegate {
    var delegateRoot: RootDelegator!
    var delegateList: ListTableViewCellDelegator!
    var delegateDashboard: DashboardDelegator!
    var delegateCard: CustomCardCellDelegator!
    var delegateDrawer: DrawerDelegator!
    var delegateChargesBreakdown: ChargesBreakdownDelegator!
    var delegateChat: ChatDelegator!
    var delegateLogin: LoginDelegator!
    var delegatePassword: PasswordDelegator!
    
    let loginModel = LoginModel.sharedInstance
    
    let ROW_SIZE: CGFloat = 38
    let ROW_MAX_HEIGHT: CGFloat = 5
    let DELAY: Double = 0// 0.33
    
    var backgroundView = UIView()
    var dialogView = UIView()
    var selectedDepositAmt: Double = 0.00
    
    var docUrl: URL?
    
    var creditLimitList: [CreditLimitEntity]? = nil
    
    //sample only. remove and get the billListItems
    var billCount: Int = 5 + (2) // to include header and footer when populating
    var amountDeposit: String = ""
    var selectedCreditLimit: String = ""
    var upgradeCreditLimit: String = ""
    
    var btnHeight: CGFloat = 68
    
    let EMPTY_AMOUNT = "please input a valid amount"
    let INVALID_AMOUNT = "invalid format"
    
    @IBOutlet weak var svDialog: UIStackView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblBodyTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var vTextFieldHolder: UIView!
    @IBOutlet weak var tfDetail: UITextField!
    
    @IBOutlet weak var vHeaderHolder: UIView!
    @IBOutlet weak var lblHeaderTableTitle: UILabel!
    @IBOutlet weak var lcHeaderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var vTableDialogHolder: UIView!
    @IBOutlet weak var lcTableHeight: NSLayoutConstraint!
    @IBOutlet weak var tvTableDialog: UITableView!
    @IBOutlet weak var lcTableLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var lcTableRightMargin: NSLayoutConstraint!
    
    @IBOutlet weak var vFooterDescription: UIView!
    @IBOutlet weak var lblFooterDescription: UILabel!
  
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOK: UIButton!
    
    @IBOutlet weak var lcBottomViewMargin: NSLayoutConstraint!
    
    var type: DIALOG? = nil
    var position: Int = 0
    var switchIsOn: Bool = true
    var executeOnce: Bool = false
    
    var pushNotifTitle: String = ""
    var pushNotifBody: String = ""
    var receiptSource: RECEIPT_SOURCE = .kHISTORY // default value
    
    var selectedFormat: Int = 0
    
    let globalInstance = Global.sharedInstance

    struct TYPE {
        static let HEADER = "header"
        static let BODY = "body"
        static let DETAIL = "detail"
        static let DESCRIPTION_TITLE = "desccription_title"
        static let DESCRIPTION = "description"
        
        static let TEXT_FIELD = "text_field"
        
        static let TABLE = "table"
        static let TABLE_HEADER = "table_header"
    }
    
    struct IDENTIFIER {
        static let RADIO = "RADIO"
        static let SIMPLE = "SIMPLE"
        static let CUSTOM_DIALOG_VIEW = "CustomDialogView"
    }
    
    enum DIALOG {
        // server
        case SERVER_MAINTENANCE
        case NEW_VERSION
        
        // account types
        case TERMINATED
        case SUSPENDED
        case PROCESSING
        
        // firebase push notification
        case PUSH_NOTIF
        case PUSH_NOTIF_ZERODR
        
        // forgot email
        case SELECTION_FORGOT_EMAIL
        case INPUT_FORGOT_EMAIL
        case SUCCESS_FORGOT_EMAIL
        case ERROR_FORGOT_EMAIL
        
        // forgot password
        case INPUT_FORGOT_PASSWORD
        case SUCCESS_FORGOT_PASSWORD
        case ERROR_FORGOT_PASSWORD
        
        // payments (dashboard & charges breakdown)
        /* combine & change this to MAKE_PAYMENT*/
        case DASHBOARD_MAKE_PAYMENT
        case CHARGES_BREAKDOWN_MAKE_PAYMENT
        
        // APN settings
        case REQUEST_APN
        case INPUT_TAC
        
        // sim details
        case SIM_PUK
        case SIM_PIN
        case SMS_CENTER
        
        // tool tip
        case CREDIT_LIMIT
        case UNBILLED_USAGE
        
        // credit limit
        case EDIT_CREDIT_LIMIT
        case CONFIRMATION_GPM // CONFIRMATION_CREDIT_LIMIT
        case CREDIT_LIMIT_DEPOSIT_NEEDED // DEPOSIT_NEEDED_IR
        case IR_DEPOSIT_NEEDED
        
        // payment webview (credit limit)
        case SUCCESS_CREDIT_LIMIT
        case FAILED_CREDIT_LIMIT
        
        // bill receipt
        case VIEW_RECEIPT
        case NO_RECEIPT
        
        // payment webview
        case PAYMENT_SUCCESS
        case PAYMENT_FAILED
        case PAYMENT_CREDIT_LIMIT_SUCCESS
        case PAYMENT_CREDIT_LIMIT_FAILED
        
        // live chat
        case CHAT_DISCONNECTED
        case CHAT_END
        
        // edit add on
        case CONFIRMATION_VAS
        case SUCCESS_EDIT_ADD_ON
        case FAILED_EDIT_ADD_ON
        
        // remove this and update CONFIRMATION_VAS accordingly
        case UNSUBSCRIBE_VAS
        case CONFIRMATION_ADDON
        
        case REFRESH_TOKEN_FAILED
        
        case CONNECTION_ERROR
        case GENERAL_ERROR
        
        case LOGOUT
        
        case DEFAULT
    }
    
    enum RECEIPT_SOURCE {
        case kPAYMENT
        case kHISTORY
    }
    
    convenience init(type: DIALOG, position: Int = 0, switchIsOn: Bool = false, amount: String = "", pushTitle: String = "", pushBody: String = "", receiptSource: RECEIPT_SOURCE = .kHISTORY) {
        self.init(frame: UIScreen.main.bounds)
        
        self.type = type
        self.position = position
        self.switchIsOn = switchIsOn
        self.selectedCreditLimit = amount
        self.executeOnce = true
        self.receiptSource = receiptSource
        
        // for push notification
        self.pushNotifTitle = pushTitle
        self.pushNotifBody = pushBody
    }

    override init(frame: CGRect) {
        super.init(frame: UIScreen.main.bounds)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: Swift.type(of: self))
        let nib = UINib(nibName: IDENTIFIER.CUSTOM_DIALOG_VIEW, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        setupLayout()
        
        return view
    }
    
    override func didMoveToSuperview() {
        if executeOnce {
            setupLayout()
            
            // setting the default value for the views
            defaultLayout()
            
            // setting the specific view
            prepareLayout()
            
            // setting the height accordingly
            updateLayout()
            
            // to avoid executing the above code when closing the view
            executeOnce = false
        }
    }
    
    func xibSetup() {
        dialogView.clipsToBounds = true
        
        backgroundView.frame = UIScreen.main.bounds
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
        backgroundView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(backgroundView)
        
        dialogView = loadViewFromNib()
        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogView.frame.height)
        dialogView.layer.cornerRadius = 10
        dialogView.layer.masksToBounds = true
        addSubview(dialogView)
        
        tfDetail.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        self.tvTableDialog?.register(UINib(nibName: "RadioButtonTableViewCell", bundle: nil), forCellReuseIdentifier: IDENTIFIER.RADIO)
        self.tvTableDialog?.register(UINib(nibName: "SimpleTableViewCell", bundle: nil), forCellReuseIdentifier: IDENTIFIER.SIMPLE)
    }
    
    func setupLayout() {
        lblHeaderTableTitle.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE)
        lblHeaderTableTitle.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblHeaderTitle.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE_X)
        lblHeaderTitle.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblBodyTitle.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE)
        lblBodyTitle.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_58)
        
        lblDetail.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE)
        lblDetail.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblDescriptionTitle.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE)
        lblDescriptionTitle.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblDescription.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblDescription.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        btnCancel.titleLabel?.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE)
        btnCancel.titleLabel?.textColor = globalInstance.getColorAttribute(.kPRIMARY_DARK, .OPACITY_38)
        
        btnOK.titleLabel?.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE)
        btnOK.titleLabel?.textColor = globalInstance.getColorAttribute(.kPRIMARY_DARK, .DEFAULT)
    }
    
    fileprivate func defaultLayout(){
        lblHeaderTitle.isHidden = false
        lblBodyTitle.isHidden = false
        lblDetail.isHidden = false
        lblDescriptionTitle.isHidden = false
        lblDescription.isHidden = false
        
        vTextFieldHolder.isHidden = true
        
        vHeaderHolder.isHidden = true
        vTableDialogHolder.isHidden = true
        vFooterDescription.isHidden = true
        
        btnCancel.isHidden = false
        btnOK.isHidden = false
        
        dialogView.frame.size.height = 200
        
        switch type! {
        case .SELECTION_FORGOT_EMAIL:
            setDefaultValueOnRadio()
            
            break
        case .EDIT_CREDIT_LIMIT:
            setDefaultValueOnRadio()
            
            break
        default: // will not do anything
            
            break
        }
    }
    
    fileprivate func prepareLayout() {
        switch type! {
        case .SERVER_MAINTENANCE, // server
             .NEW_VERSION:
            
            populateServer()
            break
        // NEGATIVE SCENARIOS
        case .TERMINATED, // account types
             .SUSPENDED,
             .PROCESSING:
            
            populateAccountTypes()
            break
        case .PUSH_NOTIF, // firebase push notification
             .PUSH_NOTIF_ZERODR:
            
            populatePushNotification()
            break
        case .SELECTION_FORGOT_EMAIL,// forgot email
             .INPUT_FORGOT_EMAIL,
             .SUCCESS_FORGOT_EMAIL,
             .ERROR_FORGOT_EMAIL:
            
            populateForgotEmail()
            break
        case .INPUT_FORGOT_PASSWORD, // forgot password
             .SUCCESS_FORGOT_PASSWORD,
             .ERROR_FORGOT_PASSWORD:
            
            populateFogotPassword()
            break
        case .DASHBOARD_MAKE_PAYMENT, // payments (dashboard & charges breakdown)
             .CHARGES_BREAKDOWN_MAKE_PAYMENT:
            
            populateMakePayment()
            break
        case .REQUEST_APN, // APN settings
             .INPUT_TAC:
            
            populateAPNSettings()
            break
        case .SIM_PUK, // sim details
             .SIM_PIN,
             .SMS_CENTER:
            
            populateSimDetails()
            break
        case .CREDIT_LIMIT, // tool tip
             .UNBILLED_USAGE:
            
            populateToolTip()
            break
        case .EDIT_CREDIT_LIMIT,// credit limit
             .CONFIRMATION_GPM,
             .CREDIT_LIMIT_DEPOSIT_NEEDED,
             .IR_DEPOSIT_NEEDED:
            
            populateCreditLimit()
            break
        case .SUCCESS_CREDIT_LIMIT, // payment webview (credit limit)
             .FAILED_CREDIT_LIMIT:
            
            populatePaymentCreditLimit()
            break
        case .VIEW_RECEIPT, // bill receipt
             .NO_RECEIPT:
            
            populateBillReceipt()
            break
        case .PAYMENT_SUCCESS, // payment webview
             .PAYMENT_FAILED,
             .PAYMENT_CREDIT_LIMIT_SUCCESS,
             .PAYMENT_CREDIT_LIMIT_FAILED:
            
            populatePaymentWebview()
            break
        case .CHAT_DISCONNECTED, // live chat
             .CHAT_END:
            
            populateLiveChat()
            break
        case .CONFIRMATION_VAS, // edit add on
             .SUCCESS_EDIT_ADD_ON,
             .FAILED_EDIT_ADD_ON:
            
            populateEditAddon()
            break
        case .UNSUBSCRIBE_VAS, // TODO: remove this and update CONFIRMATION_VAS accordingly
             .CONFIRMATION_ADDON:
            
            populateConfirmationVas()
            break
        case .REFRESH_TOKEN_FAILED: // refresh token failed
            populateRefreshTokenFailed()
            
            break
        case .CONNECTION_ERROR: // no internet connection
            populateConnectionError()
            
            break
        case .GENERAL_ERROR:
            populateGeneralError()
            
            break
        case .LOGOUT:
            populateLogout()
            
            break
        default: // DEFAULT will be the default value to avoid unexpected crashing
            
            break
        }
    }
    
    fileprivate func updateLayout() {
        let maxDialogHeight = frame.height - 60
        
        var dialogHeight:CGFloat = 0
        let components = componentListHeight()
        
        for (_, value) in components {
            dialogHeight += CGFloat(value + 16)
        }
        
        switch type! {
        case .INPUT_FORGOT_PASSWORD,
             .INPUT_FORGOT_EMAIL,
             .DASHBOARD_MAKE_PAYMENT,
             .CHARGES_BREAKDOWN_MAKE_PAYMENT:
            
            btnHeight += 30
            
            break
        case .CONFIRMATION_VAS,
             .UNSUBSCRIBE_VAS,
             .CONFIRMATION_ADDON,
             //.SUCCESS_EDIT_ADD_ON,
        .CHAT_END:
            
            btnHeight += 20
            
            break
        case .CREDIT_LIMIT,
             .TERMINATED,
             .PROCESSING,
             .SUSPENDED,
             .SERVER_MAINTENANCE,
             .NEW_VERSION,
             .REFRESH_TOKEN_FAILED,
             .CONNECTION_ERROR:
            
            btnHeight += 16
            
            break
        case .PUSH_NOTIF,
             .PUSH_NOTIF_ZERODR:
            
            btnHeight += 5
            
            break
        case .VIEW_RECEIPT:
            btnHeight -= 16
            svDialog.spacing = 0
            
            break
        default: // will not do anything
            
            break
        }
        
        dialogHeight += btnHeight
        if dialogHeight > maxDialogHeight {
            dialogHeight = maxDialogHeight
        }
        
        dialogView.frame.size = CGSize(width: frame.width - 64 , height: dialogHeight)
    }

    //MARK: - TEXTFIELD DELEGATE
    @objc func textFieldDidChange(textField: UITextField){
        if selectedFormat == 0 && type == .INPUT_FORGOT_EMAIL { // this formatter logic should apply ONLY in NRIC
            guard (textField.text?.isEmpty)! else { // prevent doing the logic is input is empty
                let format = "######-##-####"
                let inputString: String = textField.text!
                
                if (inputString.endIndex) <= format.endIndex {
                    var updatedString: String = ""

                    let inputChars = inputString.characters
                    let formatChars = format.characters
                    
                    var charIndex = inputChars.startIndex
                    var formatIndex = formatChars.startIndex
                    
                    let charCount:String.CharacterView.Index = (inputChars.index(before: (inputChars.endIndex)))
                    
                    while charIndex <= charCount {
                        if formatChars[formatIndex] == "-" {
                            if inputChars[formatIndex] != "-" { // to add the dash sign “-” ONCE ONLY
                                updatedString.append(formatChars[formatIndex]) // to append the dash sign “-”
                                formatIndex = formatChars.index(after: formatIndex)// to increment the index of format
                            }
                        }
                        
                        updatedString.append((inputChars[charIndex])) // to append the character
                        charIndex = inputChars.index(after: charIndex) // to increment the index of the input char
                        
                        formatIndex = formatChars.index(after: formatIndex) // to increment the index of format
                    }
                    
                    textField.text = updatedString
                } else { // to handle the input if it exceeds the maximum (based on format)
                    let indexRange = inputString.index(format.characters.startIndex, offsetBy: format.characters.count)
                    textField.text = String(inputString[..<indexRange])
                }
                
                return
            }
        }
    }
    
    //MARK: - TABLEVIEW RADIO BUTTON DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //set the credit limit list once click
        switch type! {
        case .SELECTION_FORGOT_EMAIL:
            return 2
        case .EDIT_CREDIT_LIMIT:
            creditLimitList = CreditLimitModel.sharedInstance.getCreditLimitList(SwitchModel.getSelectedAccountNo()!)
            return (creditLimitList?.count)!
        case .VIEW_RECEIPT:
            return billCount
        default: // it'll not display anything
            
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch type! {
        case .SELECTION_FORGOT_EMAIL:
            let cell = tableView.dequeueReusableCell(withIdentifier: CustomDialogView.IDENTIFIER.RADIO, for: indexPath) as! RadioButtonTableViewCell
            
            var option:String
            let index = indexPath.row
            if index == 0 {
                option = "NRIC"
            } else {
                option = "Passport"
            }
            
            cell.lblRadioBtn.text = option
            
            return cell
        case .EDIT_CREDIT_LIMIT:
            let cell = tableView.dequeueReusableCell(withIdentifier: CustomDialogView.IDENTIFIER.RADIO, for: indexPath) as! RadioButtonTableViewCell
            
            let creditLimit: CreditLimitEntity = creditLimitList![indexPath.row]
    
            let creditLimitAmt = (creditLimit.creditLimitUpgradeAmount as NSString).doubleValue
            let totalCreditLimit = DashboardModel.sharedInstance.getCreditUnbilledBalanceResponse()?.totalCreditLimit
            let creditLimitValue = Double(creditLimitAmt) + (totalCreditLimit! as NSString).doubleValue
            cell.lblRadioBtn.text = "RM " + globalInstance.amountFormatter(String(creditLimitValue))
            
            return cell
        case .VIEW_RECEIPT:
            tableView.allowsSelection = false //To prevent selection
            let cell = populateReceipt(indexPath)
            
            return cell
        default: // To prevent crashing
            let cell = tableView.dequeueReusableCell(withIdentifier: CustomDialogView.IDENTIFIER.RADIO, for: indexPath) as! SimpleTableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch type! {
        case .VIEW_RECEIPT:
            if indexPath.row == 6 { // receipt table's last row
                return 40
            } else {
                return 38
            }
        default: // this will be the default height of each row
            
            return 38
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tvTableDialog.cellForRow(at: indexPath) as! RadioButtonTableViewCell
        cell.btnRadio.image = UIImage(named: "btn_radio_selected")
        
        switch type! {
        case .SELECTION_FORGOT_EMAIL:
            selectedFormat = indexPath.row
            
            break
        case .EDIT_CREDIT_LIMIT:
            let creditLimit: CreditLimitEntity = creditLimitList![indexPath.row]
            let creditLimitAmt = creditLimit.creditLimitUpgradeAmount
            let totalCreditLimit = DashboardModel.sharedInstance.getCreditUnbilledBalanceResponse()?.totalCreditLimit
            let creditLimitValue = Double(creditLimitAmt)! + (totalCreditLimit! as NSString).doubleValue
            
            selectedCreditLimit = String(creditLimitValue)
            globalInstance.setCreditLimitValueDisplay(selectedCreditLimit)
            
            amountDeposit = creditLimitAmt
            globalInstance.setAmountToDeposit(amountDeposit)
            
            break
        default: // will not do anything
            
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tvTableDialog.cellForRow(at: indexPath) as! RadioButtonTableViewCell
        
        cell.btnRadio.image = UIImage(named: "btn_radio_unselected")
    }
    
// MARK: ACTION
    @IBAction func tappedOKBtn(_ sender: Any) {
        if shouldDismiss() {
            dismiss(animated: true)
        }
        
        switch type! {
        case .SERVER_MAINTENANCE:
            dismissDialog()
            
            break
        case .NEW_VERSION:
            okButtonNewVersion()
            
            break
        case .SUSPENDED:
            okButtonSuspended()
            
            break
        case .PROCESSING:
            okButtonProcessing()
            
            break
        case .PUSH_NOTIF:
            PushNotification.sharedInstance.setIsPushNotifDialogDisplayed(false)
            
            break
        case .PUSH_NOTIF_ZERODR:
            okButtonPushNotifZeroDr()
            
            break
        case .SELECTION_FORGOT_EMAIL:
            okButtonSelectionForgotEmail()
            
            break
        case .INPUT_FORGOT_EMAIL:
            okButtonInputForgotEmail()
            
            break
        case .SUCCESS_FORGOT_EMAIL:
            dismissLoginDialog()
            
            break
        case .INPUT_FORGOT_PASSWORD:
            okButtonForgotPassword()
            
            break
        case .SUCCESS_FORGOT_PASSWORD:
            dismissPasswordDialog()
            
            break
        case .DASHBOARD_MAKE_PAYMENT:
            okButtonDashboardMakePayment()
            
            break
        case .CHARGES_BREAKDOWN_MAKE_PAYMENT:
            okButtonChargesBreakdownMakePayment()
            
            break
        case .REQUEST_APN:
            okButtonRequestApn()
            
            break
        case .INPUT_TAC:
            okButtonInputTac()
            
            break
        case .EDIT_CREDIT_LIMIT:
            okButtonEditCreditLimit()
            
            break
        case .CONFIRMATION_GPM:
            okButtonConfirmationGpm()
            
            break
        case .CREDIT_LIMIT_DEPOSIT_NEEDED:
            okButtonCreditLimitDepositNeeded()
            
            break
        case .IR_DEPOSIT_NEEDED:
            okButtonIrDepositNeeded()
            
            break
        case .VIEW_RECEIPT:
            okButtonViewReceipt()
            
            break
        case .PAYMENT_SUCCESS:
            okButtonPaymentSuccess()
            
            break
        case .CHAT_DISCONNECTED:
            okButtonChatDisconnected()
            
            break
        case .CHAT_END:
            okButtonChatEnd()
            
            break
        case .CONFIRMATION_VAS:
            okButtonConfirmationVas()
            
            break
        case .SUCCESS_EDIT_ADD_ON:
            let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
            let serviceType = services[position].serviceType
            
            if (serviceType?.caseInsensitiveCompare(ServicesModel.SERVICE_TYPE.kVAS) == ComparisonResult.orderedSame) {
                return
            } else {
                okButtonSuccessEditAddOn()
            }
            
            break
        case .UNSUBSCRIBE_VAS:
            okButtonUnsubscribeVas()
            
            break
        case .CONFIRMATION_ADDON:
            okButtonConfirmationAddon()
            
            break
        case .REFRESH_TOKEN_FAILED:
            dismissDialog()
            
            break
        case .CONNECTION_ERROR:
            okButtonConnectionError()
            
            break
        case .GENERAL_ERROR:
            okButtonGeneralError()
            
            break
        case .LOGOUT:
            okButtonLogout()
            
            break
        default: // will not do anything
            
            break
        }
    }
    
    // this are the buttons that is not really a CANCEL button or aside from dismiss it'll do something more
    @IBAction func tappedCancelBtn(_ sender: Any) {
        dismiss(animated: true)
        
        switch type! {
        case .NEW_VERSION:
            cancelButtonNewVersion()
            
            break
        case .PROCESSING,
             .SUSPENDED,
             .TERMINATED:
            goToLiveChatFromCard()
            
            break
        case .PUSH_NOTIF,
             .PUSH_NOTIF_ZERODR:
            PushNotification.sharedInstance.setIsPushNotifDialogDisplayed(false)
            
            break
        case .SELECTION_FORGOT_EMAIL:
            loginUpdateUI()
            
            break
        case .INPUT_FORGOT_EMAIL:
            loginUpdateUI()
            
            break
        case .INPUT_FORGOT_PASSWORD:
            passwordUpdateUI()
            
            break
        case .DASHBOARD_MAKE_PAYMENT,
             .CHARGES_BREAKDOWN_MAKE_PAYMENT:
            
            clearFooterDescription()
            
            break
        case .SMS_CENTER: // go to live chat
            goToLiveChatFromList()
            
            break
        case .IR_DEPOSIT_NEEDED:
            cancelButtonIrDepositNeeded()
            
            break
        case .VIEW_RECEIPT:
            cancelButtonViewReceipt()
            
            break
        case .CHAT_DISCONNECTED:
            cancelButtonChatDisconnected()
            
            break
        case .CHAT_END:
            cancelButtonEndChat()
            
            break
        case .CONFIRMATION_VAS:
            reloadTableFromCard()
            
            break
        case .UNSUBSCRIBE_VAS:
            reloadTableFromCard()
            
            break
        default: // will not do anything
            
            break
        }
    }
    
    func dismissDialog() {
        // turn off push notification
        PushNotification.sharedInstance.setPushNotifEnable(false)
        PushNotification.sharedInstance.clearCache()
        Global.webServiceMngr.shouldPerformRefreshToken = false
        
        switch Global.pageType {
        case .kEMAIL:
            self.dismiss(animated: true)
            
            if (self.delegateLogin != nil) { //Just to be safe.
                self.delegateLogin.callDismissToRootFromDialog()
            }
            
            break
        case .kPASSWORD:
            if (self.delegatePassword != nil) { //Just to be safe.
                self.delegatePassword.callDismissToRootFromDialog()
            }
            
            break
        default:
            if (self.delegateDrawer != nil) { //Just to be safe.
                let nc = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.childViewControllers[1] as! UINavigationController
                let vc:DrawerMenuViewController = nc.topViewController as! DrawerMenuViewController
                
                if vc.presentedViewController != nil {
                    vc.presentedViewController?.dismiss(animated: true, completion: {
                        self.delegateDrawer.callSegueFromDialog(UNWIND_TO_ROOT)
                    })
                } else {
                    self.delegateDrawer.callSegueFromDialog(UNWIND_TO_ROOT)
                }
            } else {
                self.delegateRoot.callLoginFromDialog()
            }
            
            break
        }
    }
    
    @objc func didTappedOnBackgroundView(){
        switch type! {
        case .SELECTION_FORGOT_EMAIL,
             .INPUT_FORGOT_EMAIL,
             .SUCCESS_FORGOT_EMAIL:
            dismissLoginDialog()
            
            break
        case .INPUT_FORGOT_PASSWORD,
             .SUCCESS_FORGOT_PASSWORD:
            dismissPasswordDialog()
            
            break
        case .DASHBOARD_MAKE_PAYMENT,
             .CHARGES_BREAKDOWN_MAKE_PAYMENT,
             .EDIT_CREDIT_LIMIT: // to prevent from dismissing
            
            break
        case .VIEW_RECEIPT:
            switch receiptSource {
            case .kPAYMENT:
                
                break
            default:
                dismiss(animated: true)
                
                break
            }
            break
        case .CREDIT_LIMIT_DEPOSIT_NEEDED,
             .IR_DEPOSIT_NEEDED,
             .CONFIRMATION_VAS,
             .CONFIRMATION_ADDON,
             .INPUT_TAC: // to prevent from dismissing
            
            break
        case .PUSH_NOTIF,
             .PUSH_NOTIF_ZERODR:
            
            PushNotification.sharedInstance.setIsPushNotifDialogDisplayed(false)
            dismiss(animated: true)
            
            break
        case .SERVER_MAINTENANCE,
             .NEW_VERSION,
             .REFRESH_TOKEN_FAILED,
             .CONNECTION_ERROR,
             .CHAT_DISCONNECTED: // to prevent from dismissing
            
            break
        default: // do the default behavior
            dismiss(animated: true)
            
            break
        }
    }
    
// MARK: OK BUTTON FUNCTION
    fileprivate func okButtonNewVersion() {
        let url = URL(string: "https://itunes.apple.com/my/app/webe-self-care/id1123862527?ls=1&mt=8")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        dismissDialog()
    }
    
    fileprivate func okButtonSuspended() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
            if (self.delegateCard != nil) { //Just to be safe.
                self.delegateCard.callSegueFromCardCell(SHOW_PAYMENT)
            }
        }
    }
    
    fileprivate func okButtonProcessing() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
            self.dismiss(animated: true)
        }
    }
    
    fileprivate func okButtonPushNotifZeroDr() {
        PushNotification.sharedInstance.setIsPushNotifDialogDisplayed(false)
        PushNotification.sharedInstance.openDeviceSettings()
    }
    
    fileprivate func okButtonSelectionForgotEmail() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
            let alert = CustomDialogView(type: .INPUT_FORGOT_EMAIL)
            alert.delegateLogin = self.delegateLogin
            alert.selectedFormat = self.selectedFormat
            alert.show(animated: true)
        }
    }
    
    fileprivate func okButtonInputForgotEmail() {
        let identification = tfDetail.text
        
        guard identification?.isEmpty == false else {
            lblFooterDescription.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
            
            if selectedFormat == 0 {
                lblFooterDescription.text = EMPTY_NRIC
            } else {
                lblFooterDescription.text = EMPTY_PASSPORT
            }
            
            return
        }
        if selectedFormat == 1 {
            let charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            let characterset = CharacterSet(charactersIn: charSet)
            
            if (identification?.rangeOfCharacter(from: characterset.inverted) != nil) {
                lblFooterDescription.text = INVALID_INPUT
                lblFooterDescription.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
                
                return
            }
        }
        
        // call API here & transfer this code on the response of the API
        callForgotUsernameApi(identification!)
    }
    
    fileprivate func okButtonForgotPassword() {
        let identification = tfDetail.text
        
        guard identification?.isEmpty == false else {
            lblFooterDescription.text = EMPTY_PASSWORD
            lblFooterDescription.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
            
            return
        }
        
        guard globalInstance.isValidEmail(testStr: identification!) else {
            lblFooterDescription.text = INVALID_EMAIL
            lblFooterDescription.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
            
            return
        }
        // call API here & transfer this code on the response of the API
        callForgotPasswordApi(identification!)
    }
    
    fileprivate func okButtonDashboardMakePayment() {
        var amount:String = ""
        let amountVal = tfDetail.text
        
        if !isValidAmount() {
            // show error message and do not continue on API call
            return
        }
        
        if (globalInstance.getAmountToPay() != nil) {
            amount = amountVal!
        } else {
            amount = globalInstance.getAmountToPay()!
        }
        globalInstance.setPaymentType(Global.PAYMENT_TYPE.kBILL)
        self.delegateCard.callPaymentIpay(amount)
    }
    
    fileprivate func okButtonChargesBreakdownMakePayment() {
        var amount: String = ""
        let amountVal = tfDetail.text
        
        if !isValidAmount() {
            // show error message and do not continue on API call
            return
        }
        
        if (globalInstance.getAmountToPay() != nil) {
            amount = amountVal!
        } else {
            amount = globalInstance.getAmountToPay()!
        }
        globalInstance.setPaymentType(Global.PAYMENT_TYPE.kBILL)
        self.delegateChargesBreakdown.callPaymentFromDialog(amount)
    }
    
    fileprivate func okButtonRequestApn() {
        let model = SimDetailsModel.sharedInstance
        let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
        
        model.prepareRequestAPNParams(SwitchModel.getSelectedAccountNo()!, msisdnByPosition)
        Global.webServiceMngr.makeRequest(model.getRequestAPNParams()) { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                // by default this is the action
                self.globalInstance.showGeneralError()
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.DELAY) {
                    let alert = CustomDialogView(type: .INPUT_TAC)
                    alert.show(animated: true)
                }
            }
        }
    }
    
    fileprivate func okButtonInputTac() {
        let TAC = tfDetail.text
        
        guard TAC?.isEmpty == false else {
            //lblEmailErrorMsg.text = EMPTY_EMAIL <-- put here, something like this
            
            return
        }
        
        // call API here
        let model = SimDetailsModel.sharedInstance
        let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
        globalInstance.showHUD()
        
        model.prepareTriggerAPNSettingsParams(SwitchModel.getSelectedAccountNo()!, msisdnByPosition, TAC!)
        Global.webServiceMngr.makeRequest(model.getTriggerAPNSettingsParams()) { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                // by default this is the action
                self.globalInstance.showGeneralError()
            } else {
                self.dismiss(animated: true)
            }
        }
    }
    
    fileprivate func okButtonEditCreditLimit() {
        if (self.delegateCard != nil) {
            self.delegateCard.callRetrieveGoodPayMaster(selectedCreditLimit)
        }
    }
    
    fileprivate func okButtonConfirmationGpm() {
        if (self.delegateCard != nil) { //Just to be safe.
            self.delegateCard.callUpdateCreditLimit()
        }
    }
    
    fileprivate func okButtonCreditLimitDepositNeeded() {
        if (self.delegateCard != nil) { //Just to be safe.
            globalInstance.setPaymentType(Global.PAYMENT_TYPE.kCREDIT_LIMIT)
            self.delegateCard.callPaymentFromDialog(Global.sharedInstance.getAmountToDeposit()!)
        }
    }
    
    fileprivate func okButtonIrDepositNeeded() {
        let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
        let depositValue = services[position].depositValue!
        
        Global.sharedInstance.setAmountToDeposit(depositValue)
        self.globalInstance.setPaymentIRFlag(true)
        
        if (self.delegateCard != nil) { //Just to be safe.
            globalInstance.setPaymentType(Global.PAYMENT_TYPE.kIR)
            self.delegateCard.callPaymentFromDialog(Global.sharedInstance.getAmountToDeposit()!)
            dismiss(animated: true)
        }
    }
    
    fileprivate func okButtonViewReceipt() {
        
        if self.delegateCard != nil { //Just to be safe.
            pdfDataWithTableView(tvTableDialog)
            self.delegateCard.callPreviewPDF(docUrl!)
        } else if self.delegateDashboard != nil { //Just to be safe.
            pdfDataWithTableView(tvTableDialog)
            
            self.delegateDashboard.callPreviewPDF(docUrl!)
        }
    }
    
    fileprivate func okButtonPaymentSuccess() {
        if (self.delegateCard != nil) { //Just to be safe.
            self.delegateCard.callReloadTable()
        }
    }
    
    fileprivate func okButtonChatDisconnected() {
        if (self.delegateChat != nil) { //Just to be safe.
            self.delegateChat.callRestartChatFromDialog()
        }
    }
    
    fileprivate func okButtonChatEnd() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kMINIMIZE_CHAT)
        
        if (self.delegateChat != nil) { //Just to be safe.
            self.delegateChat.callRetainSessionFromDialog()
        }
    }

    fileprivate func okButtonConfirmationVas() {
        if (self.delegateCard != nil) { //Just to be safe.
            self.delegateCard.callSubscribeVas(position, switchIsOn)
        }
    }
    
    fileprivate func okButtonSuccessEditAddOn() {
        if (self.delegateCard != nil) { //Just to be safe.
            self.delegateCard.callReloadTable()
        }
    }
    
    fileprivate func okButtonUnsubscribeVas() {
        if (self.delegateCard != nil) {
            self.delegateCard.callSubscribeVas(position, switchIsOn)
        }
    }
    
    fileprivate func okButtonConfirmationAddon() {
        if (self.delegateCard != nil) { //Just to be safe.
            self.delegateCard.callSubscribeAddOn(position)
        }
    }
    
    fileprivate func okButtonConnectionError() {
        if (self.delegateRoot != nil) { //Just to be safe.
            self.delegateRoot.callLoginFromDialog()
        }
    }
    
    fileprivate func okButtonGeneralError() {
        if (self.delegateRoot != nil) { //Just to be safe.
            self.delegateRoot.callRootMethodFromDialog()
        }
    }
    
    fileprivate func okButtonLogout() {
        if (self.delegateDrawer != nil) { //Just to be safe.
            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kLOGOUT_CLICKED)
            globalInstance.setLoadMoreShouldDisplay(true)
            globalInstance.setIsApiTokenRefreshedToFalse(false, true)
            self.delegateDrawer.callSegueFromDialog(UNWIND_TO_ROOT)
        }
    }
    
// MARK: CANCEL BUTTON FUNCTION
// this are the buttons that is not really a CANCEL button or aside from dismiss it'll do something more
    fileprivate func cancelButtonNewVersion() {
        dismissDialog()
    }
    
    fileprivate func goToLiveChatFromCard() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kDIALOG_LIVECHAT_CLICKED)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
            if (self.delegateCard != nil) { //Just to be safe.
                self.globalInstance.setIsFromDrawer(true)
                self.delegateCard.callSegueFromCardCell(SHOW_LIVE_CHAT)
            }
        }
    }
    
    fileprivate func loginUpdateUI() {
        if delegateLogin != nil { //Just to be safe.
            delegateLogin.updateUI()
        }
    }
    
    fileprivate func passwordUpdateUI() {
        if delegatePassword != nil {
            delegatePassword.updateUI()
        }
    }
    
    fileprivate func clearFooterDescription() {
        //            let amount: Double = globalInstance.getAmountToDisplay()
        //            let amountStr = String(format:"%f", globalInstance.getAmountToDisplay())
        //
        //            if (amount == 0.00) {
        //                tfDetail.text = ""
        //            } else {
        //                tfDetail.text = globalInstance.formatNegativeAmount(amountStr)
        //            }
        
        lblFooterDescription.text = ""
    }
    
    fileprivate func goToLiveChatFromList() {
        if (self.delegateList != nil) { //Just to be safe.
            self.globalInstance.setIsFromDrawer(true)
            self.delegateList.callSegueFromCustomDialog(SHOW_LIVE_CHAT)
        }
    }
    
    fileprivate func cancelButtonIrDepositNeeded() {
        reloadTableFromCard()
        globalInstance.dismissHUD()
    }
    
    fileprivate func cancelButtonViewReceipt() {
        if receiptSource == .kPAYMENT {
            // show general error if payment status is successful
            // but daashboard, customer and credit unbilled failed.
            if self.globalInstance.getShouldShowSeperateError() {
                self.globalInstance.showGeneralError()
            }
        }
    }
    
    fileprivate func cancelButtonChatDisconnected() {
        if (self.delegateChat != nil) { //Just to be safe.
            self.delegateChat.callGoBackFromDialog()
        }
    }
    
    fileprivate func cancelButtonEndChat() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kEND_CHAT)
        
        if (self.delegateChat != nil) { //Just to be safe.
            self.delegateChat.callEndSessionFromDialog()
        }
    }
    
    fileprivate func reloadTableFromCard() {
        if (self.delegateCard != nil) { //Just to be safe.
            self.delegateCard.callReloadTable()
        }
    }

    

// MARK: FUNCTIONS
    
    fileprivate func dismissLoginDialog() {
        if delegateLogin != nil { //Just to be safe.
            dismiss(animated: true)
            delegateLogin.updateUI()
        }
    }
    
    fileprivate func dismissPasswordDialog() {
        if delegatePassword != nil { //Just to be safe.
            dismiss(animated: true)
            delegatePassword.updateUI()
        }
    }
    
    fileprivate func shouldDismiss() -> Bool {
        switch type! {
        case .INPUT_FORGOT_EMAIL,
             .SUCCESS_FORGOT_EMAIL,
             .INPUT_FORGOT_PASSWORD,
             .SUCCESS_FORGOT_PASSWORD,
             .INPUT_TAC,
             .DASHBOARD_MAKE_PAYMENT,
             .CHARGES_BREAKDOWN_MAKE_PAYMENT,
             .IR_DEPOSIT_NEEDED,
             .CONNECTION_ERROR:
            return false
        default:
            return true
        }
    }
    
    func showForgotUsernameSuccessDialog() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
            // dismiss first the existing one before showing the new alert
            self.dismiss(animated: true)
            
            let alert = CustomDialogView(type: .SUCCESS_FORGOT_EMAIL)
            alert.delegateLogin = self.delegateLogin
            alert.show(animated: true)
        }
    }
    
    func showForgotUsernameErrorDialog() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
            // dismiss first the existing one before showing the new alert
            self.dismiss(animated: true)
            
            let alert = CustomDialogView(type: .ERROR_FORGOT_EMAIL)
            alert.show(animated: true)
        }
    }
    
    func showForgotPasswordSuccessDialog() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
            // dismiss first the existing one before showing the new alert
            self.dismiss(animated: true)
            
            let alert = CustomDialogView(type: .SUCCESS_FORGOT_PASSWORD)
            alert.delegatePassword = self.delegatePassword
            alert.show(animated: true)
        }
    }
    
    func showForgotPasswordErrorDialog() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DELAY) {
            // dismiss first the existing one before showing the new alert
            self.dismiss(animated: true)
            
            let alert = CustomDialogView(type: .ERROR_FORGOT_PASSWORD)
            alert.show(animated: true)
        }
    }
    
    func pdfDataWithTableView(_ tableView: UITableView) {
        let priorBounds = tableView.bounds
        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height))
        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:tableView.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        var pageOriginY: CGFloat = 0
        while pageOriginY < fittedSize.height {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()!.restoreGState()
            pageOriginY += pdfPageBounds.size.height
        }
        UIGraphicsEndPDFContext()
        tableView.bounds = priorBounds
        
        //saving pdf
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Global.FINAL_DATE_FORMAT.RECEIPT_DATE_FORMAT
        let date = dateformatter.string(from: Date())
    
        docUrl = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docUrl = docUrl?.appendingPathComponent("\(date).pdf")
        pdfData.write(to: docUrl!, atomically: true)
        
        //check if file is saved
        /*let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        print("here here >>>>>>>>>>  " + documentsDirectory) -- uncomment when debugging */
    }
    
    func callForgotUsernameApi(_ identification: String) {
        tfDetail.resignFirstResponder()
        self.globalInstance.showHUD()
        
        loginModel.prepareForgotUsernameParams(identification)
        Global.webServiceMngr.makeRequest(loginModel.getRequestForgotUsername()) { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail {  // if fail do something here
                // if network fails, this should show
                if Global.webServiceMngr.isOffline() {
                    self.lblFooterDescription.text = NO_CONNECTION
                    self.lblFooterDescription.textColor = self.globalInstance.getColorAttribute(.kRED, .DEFAULT)
                    
                    return
                }
                
                // if specific error, this should show
                if self.globalInstance.getShouldShowSeperateError() {
                    if self.selectedFormat == 0 {
                        self.lblFooterDescription.text = INVALID_NRIC
                        self.lblFooterDescription.textColor = self.globalInstance.getColorAttribute(.kRED, .DEFAULT)
                    } else {
                        self.lblFooterDescription.text = INVALID_PASSPORT
                        self.lblFooterDescription.textColor = self.globalInstance.getColorAttribute(.kRED, .DEFAULT)
                    }
                    
                    return
                }
                
                // by default this is the action
                self.dismiss(animated: true) // to dismiss the dialog first before showing the error
                self.globalInstance.showGeneralError()
            } else {
                self.showForgotUsernameSuccessDialog()
            }
        }
    }
    
    func callForgotPasswordApi(_ username : String) {
        tfDetail.resignFirstResponder()
        globalInstance.showHUD()
        
        loginModel.prepareForgotPasswordParams(username)
        Global.webServiceMngr.makeRequest(loginModel.getRequestForgotPassword()) { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail {  // if fail do something here
                // if network fails, this should show
                if Global.webServiceMngr.isOffline() {
                    self.lblFooterDescription.text = NO_CONNECTION
                    self.lblFooterDescription.textColor = self.globalInstance.getColorAttribute(.kRED, .DEFAULT)
                    
                    return
                }
                
                // if specific error, this should show
                if self.globalInstance.getShouldShowSeperateError() {
                    self.lblFooterDescription.text = INVALID_EMAIL
                    self.lblFooterDescription.textColor = self.globalInstance.getColorAttribute(.kRED, .DEFAULT)
                    
                    return
                }
                
                // by default this is the action
                self.dismiss(animated: true) // to dismiss the dialog first before showing the error
                self.globalInstance.showGeneralError()
            } else {
                self.showForgotPasswordSuccessDialog()
            }
        }
    }
    
    fileprivate func componentListHeight() -> [String:CGFloat] {
        var components:[String:CGFloat] = [String:CGFloat]()
        let width = frame.width - 104
        
        if !lblHeaderTitle.isHidden {
            components[TYPE.HEADER] = globalInstance.getEstimatedLabelHeight(byText: lblHeaderTitle.text!,
                                                                             font: lblHeaderTitle.font,
                                                                             labelWidth: width)
        }
        
        if !lblBodyTitle.isHidden {
            components[TYPE.BODY] = globalInstance.getEstimatedLabelHeight(byText: lblBodyTitle.text!,
                                                                           font: lblBodyTitle.font,
                                                                           labelWidth: width)
        }
        
        if !lblDetail.isHidden {
            components[TYPE.DETAIL] = globalInstance.getEstimatedLabelHeight(byText: lblDetail.text!,
                                                                             font: lblDetail.font,
                                                                             labelWidth: width)
        }
        
        if !lblDescriptionTitle.isHidden {
            components[TYPE.DESCRIPTION_TITLE] = globalInstance.getEstimatedLabelHeight(byText: lblDescriptionTitle.text!,
                                                                                        font: lblDescriptionTitle.font,
                                                                                        labelWidth: width)
        }
        
        if !lblDescription.isHidden {
            components[TYPE.DESCRIPTION] = globalInstance.getEstimatedLabelHeight(byText: lblDescription.text!,
                                                                                  font: lblDescription.font,
                                                                                  labelWidth: width)
        }
        
        if !vTextFieldHolder.isHidden {
            components[TYPE.TEXT_FIELD] = vTextFieldHolder.frame.size.height
        }
        
        if !vTableDialogHolder.isHidden {
            let maxTableHeight = ROW_SIZE * CGFloat(ROW_MAX_HEIGHT)
            
            var rowCount = CGFloat(1)
            
            switch type! {
            case .SELECTION_FORGOT_EMAIL:
                rowCount = 2.0
                
                lcTableLeftMargin.constant = 0
                lcTableRightMargin.constant = 0
                
                let tblHeight = ROW_SIZE * rowCount
                lcTableHeight.constant = tblHeight
                tvTableDialog.isScrollEnabled = false
                
                break
            case .EDIT_CREDIT_LIMIT:
                rowCount = CGFloat((creditLimitList?.count)!)
                
                lcTableLeftMargin.constant = 0
                lcTableRightMargin.constant = 0
                
                let tblHeight = ROW_SIZE * rowCount
                if tblHeight > maxTableHeight {
                    lcTableHeight.constant = maxTableHeight
                    tvTableDialog.isScrollEnabled = true // will be scrollable if maxheight exceeded
                } else {
                    lcTableHeight.constant = tblHeight
                    tvTableDialog.isScrollEnabled = false
                }
                
                break
            case .VIEW_RECEIPT:
                rowCount = CGFloat(billCount)
                let tblHeight = ROW_SIZE * rowCount
                lcTableHeight.constant = tblHeight
                tvTableDialog.isScrollEnabled = false
                lcHeaderHeight.constant = 13
                
                break
            default: // will not do anything
                
                break
            }
            
            components[TYPE.TABLE] = lcTableHeight.constant
        }
        
        return components
    }
    
    func getErrorMsg() -> String {
        var errorMsg = WebServiceManager.sharedInstance.errorMessage as? String
        if errorMsg == nil {
            errorMsg = " "
        } else {
            return errorMsg!
        }
        
        return errorMsg!
    }
    
    fileprivate func setDefaultValueOnRadio() {
        let initialIndexPath = NSIndexPath(row: 0, section: 0)
        tvTableDialog.selectRow(at: initialIndexPath as IndexPath, animated: false, scrollPosition: .none)
        tvTableDialog.delegate?.tableView!(tvTableDialog, didSelectRowAt: initialIndexPath as IndexPath)
    }
    
    fileprivate func getCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let stringDate = dateFormatter.string(from: NSDate() as Date)
        
        return stringDate
    }
    
    fileprivate func isValidAmount() -> Bool {
        let amountVal = tfDetail.text
        
        if amountVal?.isEmpty == true {
            lblFooterDescription.text = EMPTY_AMOUNT
            lblFooterDescription.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
            
            return false
        } else if Double(amountVal!) == nil {
            lblFooterDescription.text = INVALID_AMOUNT
            lblFooterDescription.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
            
            return false
            
        } else if Double(amountVal!)! <= 0.00  {
            lblFooterDescription.text = EMPTY_AMOUNT
            lblFooterDescription.textColor = globalInstance.getColorAttribute(.kRED, .DEFAULT)
            
            return false
        }
        
        return true
    }

/****************************         DIAlOG POPULATION         ****************************/
// MARK: POPULATION
    fileprivate func populateServer() {
        switch type! {
        case .SERVER_MAINTENANCE:
            lblHeaderTitle.text = "maintenance"
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.text = getErrorMsg() //WebServiceManager.sharedInstance.errorMessage as? String
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .NEW_VERSION:
            lblHeaderTitle.text = "new version available"
            lblBodyTitle.text = getErrorMsg() //WebServiceManager.sharedInstance.errorMessage as? String
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("dismiss", for: .normal)
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateAccountTypes() {
        switch type! {
        case .TERMINATED:
            lblHeaderTitle.text = "account terminated"
            lblBodyTitle.text = "your account has been terminated. please contact us for any assistance."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("go to live chat", for: .normal)
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .SUSPENDED:
            lblHeaderTitle.text = "account suspended"
            lblBodyTitle.text = "we're sorry but your not allowed to purchase international roaming. please make payment to continue."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("go to live chat", for: .normal)
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .PROCESSING:
            lblHeaderTitle.text = "account processing"
            lblBodyTitle.text = "your account is currently under the processing. please contact us for any assistance."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("go to live chat", for: .normal)
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populatePushNotification() {
        switch type! {
        case .PUSH_NOTIF:
            lblHeaderTitle.text = pushNotifTitle
            lblBodyTitle.text = pushNotifBody
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .PUSH_NOTIF_ZERODR:
            lblHeaderTitle.text = pushNotifTitle
            lblBodyTitle.text = pushNotifBody
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("okay", for: .normal)
            btnOK.setTitle("go to settings", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateForgotEmail() {
        switch type! {
        case .SELECTION_FORGOT_EMAIL:
            lblHeaderTitle.text = "forgot email"
            lblBodyTitle.text = "Please choose your registered identification format to continue."
            vTableDialogHolder.isHidden = false
            
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .INPUT_FORGOT_EMAIL:
            lblHeaderTitle.text = "forgot email"
            
            var msg: String
            if selectedFormat == 0 {
                msg = "Please enter your registered NRIC number."
                tfDetail.placeholder = "(i.e format: ######-##-####)"
                tfDetail.keyboardType = UIKeyboardType.decimalPad
            } else {
                msg = "Please enter your registered passport number."
            }
            
            lblBodyTitle.text = msg
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            vFooterDescription.isHidden = false
            lblFooterDescription.text = " "
            
            vTextFieldHolder.isHidden = false
            tfDetail.autocorrectionType = .no
            
            if tfDetail.canBecomeFirstResponder {
                tfDetail.becomeFirstResponder()
            }
            
            btnCancel.setTitle("cancel",for: .normal)
            btnOK.setTitle("confirm", for: .normal)
            
            break
        case .SUCCESS_FORGOT_EMAIL:
            lblHeaderTitle.text = "forgot email"
            lblBodyTitle.text = "Your email is"
            lblDetail.text = LoginModel.sharedInstance.getEmail()
            
            lblDescriptionTitle.isHidden = true
            lblDescription.text = "We've sent you an email with further instructions."
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .ERROR_FORGOT_EMAIL:
            lblHeaderTitle.text = "forgot email"
            lblBodyTitle.text = "error"
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateFogotPassword() {
        switch type! {
        case .INPUT_FORGOT_PASSWORD:
            lblHeaderTitle.text = "forgot password"
            lblBodyTitle.text = "Please enter your registered email address."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            vFooterDescription.isHidden = false
            lblFooterDescription.text = " "
            
            vTextFieldHolder.isHidden = false
            tfDetail.autocorrectionType = .no
            
            if tfDetail.canBecomeFirstResponder {
                tfDetail.becomeFirstResponder()
            }
            
            btnCancel.setTitle("cancel",for: .normal)
            btnOK.setTitle("confirm", for: .normal)
            
            break
        case .SUCCESS_FORGOT_PASSWORD:
            lblHeaderTitle.text = "forgot password"
            lblBodyTitle.text = "We've reset your password. Kindly check your email for further instructions."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .ERROR_FORGOT_PASSWORD:
            lblHeaderTitle.text = "forgot email"
            lblBodyTitle.text = "error"
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateMakePayment() {
        switch type! {
        case .DASHBOARD_MAKE_PAYMENT:
            let leftBalance = globalInstance.getAmountToDisplay()
            var amount = ""
            if !(leftBalance.isEqual(amount)){
                amount = String(leftBalance)
                tfDetail.text = globalInstance.formatNegativeAmount(amount, false)
            } else {
                tfDetail.text = amount
            }
            
            lblHeaderTitle.text = "enter amount"
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            vFooterDescription.isHidden = false
            lblFooterDescription.text = " "
            
            vTextFieldHolder.isHidden = false
            tfDetail.keyboardType = UIKeyboardType.decimalPad
            tfDetail.becomeFirstResponder()
            
            btnCancel.setTitle("cancel",for: .normal)
            btnOK.setTitle("next", for: .normal)
            
            break
        case .CHARGES_BREAKDOWN_MAKE_PAYMENT:
            let leftBalance = globalInstance.getAmountToDisplay()
            var amount = ""
            if !(leftBalance.isEqual(amount)){
                amount = String(leftBalance)
                tfDetail.text = globalInstance.formatNegativeAmount(amount, false)
            } else {
                tfDetail.text = amount
            }
            
            lblHeaderTitle.text = "enter amount"
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true

            vFooterDescription.isHidden = false
            lblFooterDescription.text = " "
            
            vTextFieldHolder.isHidden = false
            tfDetail.keyboardType = UIKeyboardType.decimalPad
            tfDetail.becomeFirstResponder()
            
            btnCancel.setTitle("cancel",for: .normal)
            btnOK.setTitle("next", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateAPNSettings() {
        switch type! {
        case .REQUEST_APN:
            let msisdn = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
            
            lblHeaderTitle.text = "request for APN settings"
            lblBodyTitle.text = "You're requesting for APN settings for the number " + Global.sharedInstance.formatMsisdn(msisdn) + " \nAre you sure?"
            lblDetail.isHidden = true
            lblDescriptionTitle.text = "Good to know"
            lblDescription.text = "Access Point Name (APN) is the name for setting your phone reads to set up a connection to the gateway between the webe network and the internet."
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("request", for: .normal)
            
            break
        case .INPUT_TAC:
            lblHeaderTitle.text = "enter TAC"
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            vTextFieldHolder.isHidden = false
            tfDetail.autocorrectionType = .no
            
            if tfDetail.canBecomeFirstResponder {
                tfDetail.becomeFirstResponder()
            }
            
            btnCancel.setTitle("cancel",for: .normal)
            btnOK.setTitle("send", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateSimDetails() {
        switch type! {
        case .SIM_PUK:
            lblHeaderTitle.text = "SIM pin unlock code (PUK)"
            lblBodyTitle.text = "Your PUK code:"
            
            lblDetail.text = SimDetailsModel.sharedInstance.getPuk1(CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()))
            lblDetail.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE_XX)
            lblDetail.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
            
            lblDescriptionTitle.text = "Good to know"
            lblDescription.text = "The Personal Unlocking Key or PUK is a security feature that protects the information on your SIM card. You will need to enter your PUK code to unlock your SIM card should you fail to enter your SIM card pin code on your device correctly."
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)

            break
        case .SIM_PIN:
            lblHeaderTitle.text = "SIM pin code (PIN)"
            lblBodyTitle.text = "Your PIN code:"
            
            lblDetail.text = SimDetailsModel.sharedInstance.getPin1(CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()))
            lblDetail.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE_XX)
            lblDetail.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
            
            lblDescriptionTitle.text = "Good to know"
            lblDescription.text = "The Peronal Identification Number or PIN is a security feature that protects the information on your SIM card. You will need to enter your PIN code to unlock your SIM. The PIN code is the default PIN for unlocking your SIM. Shall you change the PIN through your phone settings, it will not reflect in there."
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .SMS_CENTER:
            let msisdn = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
            
            lblHeaderTitle.text = "SMS center number"
            lblBodyTitle.text = "SMS center number:"
            lblDetail.text = Global.sharedInstance.formatMsisdn(msisdn)
            lblDescriptionTitle.isHidden = true
            lblDescription.text = "Can't receive or send SMS? Type this number into your SMSC and click update. Didn't fix the issue? Chat with us."
            
            btnCancel.setTitle("go to live chat", for: .normal)
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateToolTip() {
        switch type! {
        case .CREDIT_LIMIT:
            lblHeaderTitle.text = "credit limit"
            lblBodyTitle.text = "All lines will be suspended if your credit limit has been reached. An advance deposit not required if you have good track record of paying your monthly bills on time. Your credit limit is twice the amount of the deposit."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay, got it", for: .normal)
            
            break
        case .UNBILLED_USAGE:
            lblHeaderTitle.text = "unbilled usage"
            lblBodyTitle.text = "Your unbilled usage is the amount of credit you have used that has not been included in your current bill. It will be included in your next bill."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay, got it",for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateCreditLimit() {
        switch type! {
        case .EDIT_CREDIT_LIMIT:
            lblHeaderTitle.text = "edit credit limit"
            vTableDialogHolder.isHidden = false
            
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .CONFIRMATION_GPM:
            let totalCreditLimit = DashboardModel.sharedInstance.getCreditUnbilledBalanceResponse()?.totalCreditLimit!
            let selectedLimit: Int = Int(selectedCreditLimit)!
            
            lblHeaderTitle.text = "confirmation"
            lblBodyTitle.text = "Are you sure to increase your credit limit from RM\(totalCreditLimit ?? "") to RM\(selectedLimit)? "
            lblDetail.text = "No deposit is required."
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("confirm", for: .normal)
            
            break
        case .CREDIT_LIMIT_DEPOSIT_NEEDED:
            lblHeaderTitle.text = "deposit needed"
            lblBodyTitle.text = "Please make an advance deposit to change your credit limit."
            lblDetail.text = "deposit needed"
            lblDescriptionTitle.isHidden = true
            
            let depositAmount = Global.sharedInstance.getAmountToDeposit()!
            let depositNeededAmt = globalInstance.amountFormatter(String(depositAmount))
            lblDescription.text = depositNeededAmt
            // the meaning of the code after the "amount" variable is for the default value of it, incase it doesn't have a value or nil
            lblDescription.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
            lblDescription.textColor = globalInstance.getColorAttribute(.kYELLOW, .DEFAULT)
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("make payment", for: .normal)
            
            break
        case .IR_DEPOSIT_NEEDED:
            lblHeaderTitle.text = "deposit needed"
            lblBodyTitle.text = "Please make an advance deposit to activate IR."
            lblDetail.text = "deposit needed"
            lblDescriptionTitle.isHidden = true
            
            let amount = Global.sharedInstance.getAmountToDeposit()!
            let depositNeededAmt = globalInstance.amountFormatter(String(amount))
            lblDescription.text = depositNeededAmt
            // the meaning of the code after the "amount" variable is for the default value of it, incase it doesn't have a value or nil
            lblDescription.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
            lblDescription.textColor = globalInstance.getColorAttribute(.kYELLOW, .DEFAULT)
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("make payment", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populatePaymentCreditLimit() {
        switch type! {
        case .SUCCESS_CREDIT_LIMIT:
            //let amount = Global.sharedInstance.getAmountToDeposit()!
            let amountValue = globalInstance.getCreditLimitValueDisplay()
            let amountValueFormatted: String = globalInstance.amountFormatter(amountValue!)
            
            lblHeaderTitle.text = "success"
            lblBodyTitle.text = "Your current credit limit has been changed to \(amountValueFormatted)."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .FAILED_CREDIT_LIMIT:
            
            lblHeaderTitle.text = "update credit limit failed"
            lblBodyTitle.text = "There was a problem upgrading your credit limit, please try again."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateBillReceipt() {
        switch type! {
        case .VIEW_RECEIPT:
            lblHeaderTitle.isHidden = true
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            vTableDialogHolder.isHidden = false
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("save receipt", for: .normal)
            
            break
        case .NO_RECEIPT:
            lblHeaderTitle.text = "No receipt"
            lblBodyTitle.text = "No receipt available"
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populatePaymentWebview() {
        switch type! {
        case .PAYMENT_SUCCESS:
            lblHeaderTitle.text = "success"
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.text = "Payment is successful."
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .PAYMENT_FAILED:
            lblHeaderTitle.text = "failed"
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.text = "Payment is unsuccessful."
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .PAYMENT_CREDIT_LIMIT_SUCCESS:
            lblHeaderTitle.text = "update credit limit success"
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            
            
            //let amount = Global.sharedInstance.getAmountToDeposit()!
            let amountValue: String = globalInstance.getCreditLimitValueDisplay()!
            let amountValueFormatted: String = globalInstance.amountFormatter(amountValue)
            
            lblDescription.text = "your current credit limit has been changed to \(amountValueFormatted)"
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .PAYMENT_CREDIT_LIMIT_FAILED:
            lblHeaderTitle.text = "update credit limit failed"
            lblBodyTitle.isHidden = true
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            
            lblDescription.text = "There was a problem regarding your payment, please try again."
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateLiveChat() {
        switch type! {
        case .CHAT_DISCONNECTED:
            lblHeaderTitle.text = "live chat"
            lblBodyTitle.text = "Live chat session has ended. Do you want to have a new session?"
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("no", for: .normal)
            btnOK.setTitle("yes", for: .normal)
            
            break
        case .CHAT_END:
            lblHeaderTitle.text = "confirmation"
            lblBodyTitle.text = "Do you want to minimize or end chat?"
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("end chat", for: .normal)
            btnOK.setTitle("minimize", for: .normal)
            btnOK.setTitleColor(globalInstance.getColorAttribute(.kTEAL, .DEFAULT), for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateEditAddon() {
        switch type! {
        case .CONFIRMATION_VAS:
            let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
            
            let addOnTitle = services[position].name!
            let line = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
            
            lblHeaderTitle.text = "confirmation"
            lblBodyTitle.text = "You're about to subscribe \(addOnTitle) for \(Global.sharedInstance.formatMsisdn(line)). Confirm?"
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("confirm", for: .normal)
            
            break
        case .SUCCESS_EDIT_ADD_ON:
            let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
            
            let addOnTitle = services[position].name!
            let serviceType = services[position].serviceType
            
            lblHeaderTitle.text = "success"
            lblBodyTitle.text = "You've successfully subscribed to \(addOnTitle)."
            lblDetail.isHidden = true
            
            if (serviceType?.caseInsensitiveCompare(ServicesModel.SERVICE_TYPE.kVAS) == ComparisonResult.orderedSame) {
                lblDescriptionTitle.text = "reminder"
                lblDescription.text = "Please be reminded that some changes might not be reflected immediately. Thank you for your patience."
            } else {
                lblDescriptionTitle.isHidden = true
                lblDescription.isHidden = true
            }
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        case .FAILED_EDIT_ADD_ON:
            lblHeaderTitle.text = "edit add on failed"
            lblBodyTitle.text = "request failed."
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.isHidden = true
            btnOK.setTitle("okay", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateConfirmationVas() {
        switch type! {
        case .UNSUBSCRIBE_VAS:
            let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
            
            let addOnTitle = services[position].name!
            let line = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
            
            lblHeaderTitle.text = "confirmation"
            lblBodyTitle.text = "You're about to unsubscribe \(addOnTitle) for \(Global.sharedInstance.formatMsisdn(line)). Confirm?"
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("confirm", for: .normal)
            
            break
        case .CONFIRMATION_ADDON:
            let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
            
            let addOnTitle = services[position].name!
            let line = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
            
            lblHeaderTitle.text = "confirmation"
            lblBodyTitle.text = "You're about to purchase \(addOnTitle) for \(Global.sharedInstance.formatMsisdn(line)). Confirm?"
            lblDetail.isHidden = true
            lblDescriptionTitle.isHidden = true
            lblDescription.isHidden = true
            
            btnCancel.setTitle("cancel", for: .normal)
            btnOK.setTitle("confirm", for: .normal)
            
            break
        default:
            break
        }
    }
    
    fileprivate func populateRefreshTokenFailed() {
        lblHeaderTitle.isHidden = true
        lblBodyTitle.isHidden = true
        lblDetail.isHidden = true
        lblDescriptionTitle.isHidden = true
        lblDescription.text = "Oops! Something went wrong."
        
        btnCancel.isHidden = true
        btnOK.setTitle("okay", for: .normal)
    }
    
    fileprivate func populateConnectionError() {
        lblHeaderTitle.text = "connection error"
        lblBodyTitle.isHidden = true
        lblDetail.isHidden = true
        lblDescriptionTitle.isHidden = true
        lblDescription.text = "Please check your internet connection and let's get linked again."
        
        btnCancel.isHidden = true
        btnOK.setTitle("okay", for: .normal)
    }
    
    fileprivate func populateGeneralError() {
        lblHeaderTitle.text = "error"
        lblBodyTitle.text = "Something went wrong. Please try again."
        lblDetail.isHidden = true
        lblDescriptionTitle.isHidden = true
        lblDescription.isHidden = true
        
        btnCancel.isHidden = true
        btnOK.setTitle("okay", for: .normal)
    }
    
    fileprivate func populateLogout() {
        lblHeaderTitle.text = "log out"
        lblBodyTitle.isHidden = true
        lblDetail.isHidden = true
        lblDescriptionTitle.isHidden = true
        lblDescription.text = "Do you really want to logout?"
        
        btnCancel.setTitle("no", for: .normal)
        btnCancel.setTitleColor(globalInstance.getColorAttribute(.kGREY, .DEFAULT), for: .normal)
        btnOK.setTitle("yes", for: .normal)
        btnOK.setTitleColor(globalInstance.getColorAttribute(.kTEAL, .DEFAULT), for: .normal)
    }
    
    func populateReceipt(_ indexPath:IndexPath) -> SimpleTableViewCell {
        let cell = tvTableDialog.dequeueReusableCell(withIdentifier: CustomDialogView.IDENTIFIER.SIMPLE, for: indexPath) as! SimpleTableViewCell
        
        var bankTransaction: String = ""
        var acctNo: String = ""
        var paymentDate: String = ""
        var paymentMethod: String = ""
        var paymentAmt: String = ""
        
        switch receiptSource {
        case .kPAYMENT:
            let model = PaymentModel.sharedInstance
            
            bankTransaction = model.getTranId()
            acctNo = model.getAcctCode()
            paymentDate = getCurrentDate()
            paymentMethod = model.getPaymentMethod()
            paymentAmt = model.getPaymentAmt()
            
            break
        default: // receipt in History
            let latestPayment = HistoryModel.sharedInstance.getLatestPaymentDetailValues(position)
            
            bankTransaction = (latestPayment?.bankTransactionSN)!
            acctNo = SwitchModel.getSelectedAccountNo()!
            paymentDate = globalInstance.formatDate((latestPayment?.paymentDate)!, Global.RAW_DATE_FORMAT.YYYYMMDD, Global.FINAL_DATE_FORMAT.DD_MM_YYYY)
            paymentMethod = (latestPayment?.paymentMethod)!
            paymentAmt = (latestPayment?.paymentAmount)!
            
            break
        }
        
        switch indexPath.row {
        case 0:
            cell.lcLeft.constant = 0
            cell.lcIdention.constant = 16
            cell.lblLeft.text = "receipt: " + bankTransaction
            cell.lblLeft.font = globalInstance.getFontAttribute(.kBOLD, .kMEDIUM)
            cell.lblLeft.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
            
            cell.lcRight.constant = 0
            cell.lblRight.isHidden = true
            
            break
        case 1:
            cell.lblLeft.text = "account no."
            cell.lblLeft.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            
            cell.lblRight.text = acctNo
            cell.lblRight.font = globalInstance.getFontAttribute(.kBOLD, .kSMALL)
            
            break
        case 2:
            cell.lblLeft.text = "payment date"
            cell.lblLeft.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            
            cell.lblRight.text = paymentDate
            cell.lblRight.font = globalInstance.getFontAttribute(.kBOLD, .kSMALL)
            
            break
        case 3:
            cell.lblLeft.text = "paid via"
            cell.lblLeft.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            
            cell.lblRight.text = paymentMethod
            cell.lblRight.font = globalInstance.getFontAttribute(.kBOLD, .kSMALL)
            
            break
        case 4:
            cell.lblLeft.text = "payment status"
            cell.lblLeft.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            
            cell.lblRight.text = "successful"
            cell.lblRight.font = globalInstance.getFontAttribute(.kBOLD, .kSMALL)
            
            break
        case 5:
            cell.lblLeft.text = "payment amount"
            cell.lblLeft.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            
            cell.lblRight.text = "RM " + paymentAmt
            cell.lblRight.font = globalInstance.getFontAttribute(.kBOLD, .kSMALL)
            
            break
        case 6:
            cell.lblLeft.text = "This is a computer generated receipt and requires no signature"
            cell.lblRight.isHidden = true
            cell.vSeparator.isHidden = true
            
            break
        default: // will not do anything
            
            break
        }
        
        return cell
    }
    
    fileprivate func showGeneralError() {
        let dialog = CustomDialogView(type: .GENERAL_ERROR)
        dialog.show(animated: true)
        
        return
    }
    
}
