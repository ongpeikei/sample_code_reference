//
//  CustomDialogAnimation.swift
//  selfcare
//
//  Created by ONG PEI KEI on 06/07/2017.
//
//

import Foundation
import UIKit

protocol CustomDialogAnimation {
    func show(animated:Bool)
    func dismiss(animated:Bool)
    var backgroundView:UIView {get}
    var dialogView:UIView {get set}
}

extension CustomDialogAnimation where Self:UIView{
    func show(animated:Bool){
        self.backgroundView.alpha = 0
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            topController.view.addSubview(self)
        }
        
        if animated {
            self.dialogView.center  = self.center
            self.dialogView.alpha = 0.66
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: .beginFromCurrentState, animations: {
                self.backgroundView.alpha = 0.66
                self.dialogView.alpha = 1
            })
        } else {
            self.backgroundView.alpha = 0.66
            self.dialogView.center  = self.center
        }
    }
    
    func dismiss(animated:Bool){
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0
                self.dialogView.alpha = 0
            }, completion: { (completed) in
                self.removeFromSuperview()
            })
        }else{
            self.removeFromSuperview()
        }
        
    }
}
