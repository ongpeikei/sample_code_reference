//
//  CustomCardCellViewController.swift
//  selfcare
//
//  Created by ONG PEI KEI on 07/06/2017.
//
//

import UIKit
import SVProgressHUD

var ROW_SIZE:CGFloat = 100
var INNER_TBL_BTM_SPACE:CGFloat = 16

protocol CustomCardCellDelegator {
    func callSegueFromCardCell(_ segue: String)
    func callPaymentFromDialog(_ amount: String)
    func callPaymentIpay(_ amount: String)
    func callRetrieveGoodPayMaster(_ amount: String)
    func callPreviewPDF(_ url : URL)
    func callSubscribeVas(_ position: Int, _ switchIsOn: Bool)
    func callSubscribeAddOn(_ position: Int)
    func callUpdateCreditLimit()
    func callReloadTable()
}

class CustomCardCellViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CustomCardCellDelegator , UIDocumentInteractionControllerDelegate {
    let globalInstance = Global.sharedInstance
    let servicesModel = ServicesModel.sharedInstance
    
    @IBOutlet var tvCustomCardCell: UITableView?
    @IBOutlet weak var lcTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblCustomCardCell: UILabel!
    
    var cellData:Array = [AnyObject]()
    
    var tableSize: CGSize? = nil
    
    var delegateDashboard:DashboardDelegator!
    var delegateLine:LineDelegator!
    var delegateUnbilledUsage:UnbilledUsageDelegator!
    let makePaymentDialog = CustomDialogView(type: .DASHBOARD_MAKE_PAYMENT)
    
    var selectedProduct: Int = 0
    
    struct CARD_CELL {
        static let kLIST = "LIST"
        static let kDETAILED = "DETAILED"
        
        static let kPAGER = "PAGER"
        static let kCARD = "CARD"
        static let kTWO_ROWS = "TWO_ROWS"
        
        static let kBANNER = "BANNER"
        static let kBUTTON = "BUTTON"
    }
    
    //Purchase History Position Checker
    enum PURCHASE_HISTORY_POSITION: Int {
        case kDCB_LIST
        case kADDON_LIST
    }
    
    //ToolTip Position Checker
    enum TOOLTIP_TYPE {
        case kCREDIT
        case kUNBILLED
    }
    
    fileprivate var leftUpper = "default"
    fileprivate var leftCenter = "default"
    fileprivate var leftLower = "default"
    fileprivate var rightUpper = "default"
    fileprivate var rightCenter = "default"
    fileprivate var rightLower = "default"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(Global.pageType == (Global.VIEW_TYPE.kSWITCH_PRODUCT)) {
            let destinationNavigationController = segue.destination as! UINavigationController
            let switchAccountViewController = destinationNavigationController.topViewController as! SwitchAccountViewController
            switchAccountViewController.selectedProductType = selectedProduct
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tvCustomCardCell?.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: CARD_CELL.kLIST)
        self.tvCustomCardCell?.register(UINib(nibName: "DetailedTableViewCell", bundle: nil), forCellReuseIdentifier: CARD_CELL.kDETAILED)
        
        self.tvCustomCardCell?.register(UINib(nibName: "PagerTableViewCell", bundle: nil), forCellReuseIdentifier: CARD_CELL.kPAGER)
        self.tvCustomCardCell?.register(UINib(nibName: "CardTableViewCell", bundle: nil), forCellReuseIdentifier: CARD_CELL.kCARD)
        self.tvCustomCardCell?.register(UINib(nibName: "TwoRowsTableViewCell", bundle:nil), forCellReuseIdentifier: CARD_CELL.kTWO_ROWS)

        self.tvCustomCardCell?.register(UINib(nibName: "BannerTableViewCell", bundle: nil), forCellReuseIdentifier: CARD_CELL.kBANNER)
        self.tvCustomCardCell?.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: CARD_CELL.kBUTTON)
    }
    
    fileprivate func willDisplayTableMessage() { // determin if table is EMPTY or NOT
        if Global.cellSequence.count == 0 {
            lblCustomCardCell.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE)
            
            switch Global.pageType {
            case .kBILL:
                lblCustomCardCell.isHidden = false
                lblCustomCardCell.text = "No bill history"
                
                break
            case .kPURCHASE:
                lblCustomCardCell.isHidden = false
                lblCustomCardCell.text = "No purchase history"
                
                break
            default: // doesn't do anything
                
                break
            }
        }
    }
    
    fileprivate func isLastRowVisible() -> Bool {
        let tableHeight:Double = Double((tvCustomCardCell?.frame.height)!)
        if Double((tvCustomCardCell?.contentSize.height)!) < tableHeight {
            return true
        }
        
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Global.cellSequence.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cardHeight:CGFloat = UITableViewAutomaticDimension
        
        let position = indexPath.row
        let card = Global.cellSequence[position]
        switch card {
        case CARD_CELL.kBUTTON:
            if position == 3 {
                if !Global.sharedInstance.getLoadMoreShouldDisplay()! {
                    cardHeight = 0
                }
            }
            
            break
        case CARD_CELL.kPAGER:
            switch Global.pageType {
            case .kLINE:
                if position == 0 {
                    let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
                    if (AddOnBalanceModel.sharedInstance.getAddOnBalanceList(msisdnByPosition)?.count)! == 0 {
                        cardHeight = 0
                    }
                }
                
                break
            default:
                break
            }
        case CARD_CELL.kCARD:
            switch Global.pageType {
            case .kDASHBOARD:
                if position == 4 || position == 5 {
                    if Global.sharedInstance.getLoadMoreShouldDisplay()! {
                        cardHeight = 0
                    }
                }
                
                break
            default:
                break
            }
        default:
            break
        }
        
        return cardHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let position = indexPath.row
        let card = Global.cellSequence[position]
        switch card {
        // LIST
        case CARD_CELL.kLIST:
            let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kLIST, for: indexPath) as! ListTableViewCell
            cell.selectionStyle = .none
            
            switch Global.pageType {
            case Global.VIEW_TYPE.kCHARGES_BREAKDOWN:
                INNER_TBL_BTM_SPACE = 16
                
                if (position == 0) {
                    ROW_SIZE = 37
                    cell.lblHeader.text = "overdue charges"
                    cell.listType = ListTableViewCell.LIST_TYPE.kPREVIOUS_CHARGES
                    cell.cellData = getCellDataPreviousCharges() // <------ populate data based on Model
                    cell.tvList?.allowsSelection = false
                    
                } else if (position == 1) {
                    ROW_SIZE = 38
                    cell.lblHeader.text = "current charges"
                    cell.listType = ListTableViewCell.LIST_TYPE.kCURRENT_CHARGES
                    cell.cellData = getCellDataCurrentCharges() // <------ populate data based on Model
                    cell.tvList?.allowsSelection = false
                } else {
                    ROW_SIZE = 27
                    cell.lblHeader.text = "total amount"
                    cell.listType = ListTableViewCell.LIST_TYPE.kTOTAL_AMOUNT
                    cell.cellData = getCellDataTotalAmount() // <------ populate data based on Model
                    cell.tvList?.allowsSelection = false
                }
                
                cell.lcCellHeight.constant = (ROW_SIZE * CGFloat(cell.cellData.count)) + INNER_TBL_BTM_SPACE
                cell.vFooter.isHidden = true
                
                break
            case Global.VIEW_TYPE.kLINE:
                cell.delegate = self // <--- to be capable of calling segue inside
                
                if (position == 1) {
                    cell.lblHeader.text = "current add on"
                    cell.listType = ListTableViewCell.LIST_TYPE.kCURRENT_ADDON
                    cell.cellData = AddOnBalanceModel.sharedInstance.getSubsAddOnsList(CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()))!
                    cell.btnFooter.setTitle("edit add on", for: .normal)
                    cell.btnFooter.titleLabel?.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE)
                    cell.btnFooter.setTitleColor(globalInstance.getColorAttribute(.kPRIMARY_DARK, .DEFAULT), for: .normal)
                    
                    // compute for estimated row size
                    ROW_SIZE = 0
                    INNER_TBL_BTM_SPACE = 5
                    
                    let fontLarge: UIFont = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE_X)!
                    let fontMedium: UIFont = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)!
                    let heightMargin: CGFloat = 36.5
                    let widthMargin: CGFloat = 154
                    let estimatedWidth = UIScreen.main.bounds.width - widthMargin
                    
                    for (index, _) in cell.cellData.enumerated() { // NOTE: check POPULATION in LIST TABLE VIEW
                        let addOns:SubscribedAddOnsEntity = cell.cellData[index] as! SubscribedAddOnsEntity
                        
                        let hLargeXLabel = globalInstance.getEstimatedLabelHeight(byText: addOns.name, font: fontLarge, labelWidth: estimatedWidth)
                        var hMediumLabel = globalInstance.getEstimatedLabelHeight(byText: addOns.desc, font: fontMedium, labelWidth: estimatedWidth)
                        let lineNumber: CGFloat = hMediumLabel / CGFloat(14)
                        hMediumLabel += (lineNumber * 3)
                        
                        ROW_SIZE += (heightMargin + hLargeXLabel + hMediumLabel) //+ INNER_TBL_BTM_SPACE
                    }
                    
                    cell.lcCellHeight.constant = ROW_SIZE
                    cell.callReloadTable()
                    
                } else {
                    ROW_SIZE = 45
                    INNER_TBL_BTM_SPACE = 16
                    
                    cell.lblHeader.text = "phone settings"
                    cell.listType = ListTableViewCell.LIST_TYPE.kPHONE_SETTINGS
                    cell.cellData = getCellDataPhoneSetting() // this is a static items
                    cell.vFooter.isHidden = true
                    
                    cell.lcCellHeight.constant = (ROW_SIZE * CGFloat(cell.cellData.count)) + INNER_TBL_BTM_SPACE
                }
                
                break
            case Global.VIEW_TYPE.kPROFILE:
                ROW_SIZE = 54
                
                cell.lblHeader.text = "profile information"
                cell.listType = ListTableViewCell.LIST_TYPE.kPROFILE
                cell.cellData = getCellDataProfile() // <---- this should be update according to model
                cell.vFooter.isHidden = true
                cell.tvList?.allowsSelection = false
                
                cell.lcCellHeight.constant = ROW_SIZE * CGFloat(cell.cellData.count)
                
                break
            case Global.VIEW_TYPE.kPURCHASE:
                INNER_TBL_BTM_SPACE = 7
                cell.tvList?.allowsSelection = false
                
                if (SwitchModel.getSelectedProductType()?.isEqual(Global.PROD_TYPE.kBROADBAND.rawValue))! { // HISTORY PURCHASE ADD ON
                    cell.lblHeader.text = "add on"
                    cell.listType = ListTableViewCell.LIST_TYPE.kPURCHASE_ADDON
                    cell.cellData = HistoryModel.sharedInstance.getPurchaseAddOnList()!
                    
                    // compute for estimated row size
                    ROW_SIZE = 0
                    let fontMedium: UIFont = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)!
                    let fontSmall: UIFont = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)!
                    let heightMargin: CGFloat = 58
                    let widthMargin: CGFloat = 124
                    let estimatedWidth = UIScreen.main.bounds.width - widthMargin

                    for _ in cell.cellData {
                        let hMediumLabel = globalInstance.getEstimatedLabelHeight(byText: "text", font: fontMedium, labelWidth: estimatedWidth)
                        let hSmallLabel = globalInstance.getEstimatedLabelHeight(byText: "text", font: fontSmall, labelWidth: estimatedWidth)
                        
                        ROW_SIZE += (heightMargin + hMediumLabel + hSmallLabel + hSmallLabel)
                    }
                    
                    cell.lcCellHeight.constant = ROW_SIZE + INNER_TBL_BTM_SPACE
                    cell.callReloadTable()
                    
                } else { // HISTORY PURCHASE DCB
                    cell.lblHeader.text = "DCB purchase history"
                    cell.listType = ListTableViewCell.LIST_TYPE.kPURCHASE_DCB
                    cell.cellData = HistoryModel.sharedInstance.getPurchaseDCBList()!
                    
                    // compute for estimated row size
                    ROW_SIZE = 0
                    let fontMedium: UIFont = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)!
                    let fontSmall: UIFont = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)!
                    let heightMargin: CGFloat = 65
                    let widthMargin: CGFloat = 124
                    let estimatedWidth = UIScreen.main.bounds.width - widthMargin
                    
                    for _ in cell.cellData {
                        let hMediumLabel = globalInstance.getEstimatedLabelHeight(byText: "text", font: fontMedium, labelWidth: estimatedWidth)
                        let hSmallLabel = globalInstance.getEstimatedLabelHeight(byText: "text", font: fontSmall, labelWidth: estimatedWidth)
                        
                        ROW_SIZE += (heightMargin + hMediumLabel + hSmallLabel + hSmallLabel + hSmallLabel)
                    }
                    
                    cell.lcCellHeight.constant = ROW_SIZE + INNER_TBL_BTM_SPACE
                    cell.callReloadTable()
                }
                
                lblCustomCardCell.isHidden = true
                
                //cell.lcCellHeight.constant = (ROW_SIZE * CGFloat(cell.cellData.count)) + INNER_TBL_BTM_SPACE
                cell.vFooter.isHidden = true
                
                break
            default:
                break
            }
            
            return cell
        case CARD_CELL.kDETAILED:
            let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kDETAILED, for: indexPath) as! DetailedTableViewCell
            cell.selectionStyle = .none
            
            switch Global.pageType {
            case .kAPP_SETTINGS:
                cell.vHolderTopConstraint.constant = -10
                cell.vHolderBtmConstraint.constant = 20
                
                return getCellAppSettings(cell, position)
            default: // .kEDIT_ADDON
                cell.vHolderTopConstraint.constant = -10
                cell.vHolderBtmConstraint.constant = 10
                
                cell.switchRightCenter.addTarget(self, action: #selector(callVasDialog(addOnSwitch:)), for: .valueChanged)
                cell.switchRightCenter.tag = position
                cell.switchRightCenter.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
                
                cell.btnRightCenter.addTarget(self, action: #selector(callPurchaseAddOnDialog(addOnButton:)), for: .touchUpInside)
                cell.btnRightCenter.tag = position
                return getCellEditAddon(cell, position)
            }
            
        // PAGER
        case CARD_CELL.kPAGER:
            let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
            if (AddOnBalanceModel.sharedInstance.getAddOnBalanceList(msisdnByPosition)?.count)! > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kPAGER, for: indexPath) as! PagerTableViewCell
                cell.selectionStyle = .none
                
                cell.callReloadCollectionView()
                
                return cell
            } else { // just to avoid returning NIL (this will be hidden so don't worry)
                let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kCARD, for: indexPath) as! CardTableViewCell
                cell.selectionStyle = .none
                
                return cell
            }
        case CARD_CELL.kCARD:
            let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kCARD, for: indexPath) as! CardTableViewCell
            cell.selectionStyle = .none
            
            switch Global.pageType {
            case .kDASHBOARD:
                return getDashboardCard(cell, position)
            case .kBILL:
                return getBillCard(cell, position)
            case .kSWITCH_PRODUCT:
                return getSwitchCard(cell, position)
            case .kSWITCH_ACCOUNT:
                return getSwitchCard(cell, position)
            default:
                return getDashboardCard(cell, position)
            }
        case CARD_CELL.kTWO_ROWS:
            let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kTWO_ROWS, for: indexPath) as! TwoRowsTableViewCell
            cell.selectionStyle = .none
            
            switch Global.pageType {
            case .kUNBILLED_USAGE:
                let unbilledLines = DashboardModel.sharedInstance.getUnbilledLines()
                
                let unbilledLine = unbilledLines?[indexPath.row]
                cell.txtHeader.text = CustomerDetailsModel.sharedInstance.getLineByMsisdn((unbilledLine?.msisdn)!)?.lineAlias
                cell.txtHeaderValue.text = globalInstance.formatMsisdn((unbilledLine?.msisdn)!)
                cell.txtBodyValue.text = "RM " + (globalInstance.formatNegativeAmount((unbilledLine?.unbilledAmount)!, true))
                cell.txtBodyValue.textColor = globalInstance.getColorAttribute(.kPRIMARY_DARK, .DEFAULT)
                
                break
            default:
                break
            }
            
            return cell
            
        // BUTTONS
        case CARD_CELL.kBANNER:
            let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kBANNER, for: indexPath) as! BannerTableViewCell
            cell.selectionStyle = .none
        
            return cell
        case CARD_CELL.kBUTTON:
            let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kBUTTON, for: indexPath) as! ButtonTableViewCell
            cell.selectionStyle = .none
            
            switch position {
            case 1:
                cell.btnCenter.setTitle("make payment", for: .normal)
                cell.btnCenter.setTitleColor(UIColor.white, for: .normal)
                cell.btnCenter.backgroundColor? = globalInstance.getColorAttribute(.kBLUE, .DEFAULT)!
                cell.btnCenter.addTarget(self, action: #selector(callMakePayment), for: UIControlEvents.touchUpInside)
                
                break
            case 3:
                if !Global.sharedInstance.getLoadMoreShouldDisplay()! {
                    cell.btnCenter.isHidden = true
                } else {
                    cell.btnCenter.isHidden = false
                    
                    cell.btnCenter.setTitle("load more", for: .normal)
                    cell.btnCenter.setTitleColor(globalInstance.getColorAttribute(.kDARK_GREY_2, .DEFAULT), for: .normal)
                    cell.btnCenter.backgroundColor? = globalInstance.getColorAttribute(.kGREY, .DEFAULT)!
                    cell.btnCenter.addTarget(self, action: #selector(callLoadMoreAPI), for: UIControlEvents.touchUpInside)
                }
                
                break
            default:
                break
            }
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: CARD_CELL.kCARD, for: indexPath) as! CardTableViewCell
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowIndex = (tableView.numberOfRows(inSection: 0) - 1)
        if indexPath.row == lastRowIndex {
            switch Global.pageType {
            case .kDASHBOARD:
                if self.delegateDashboard != nil {
                    if Global.sharedInstance.getLoadMoreShouldDisplay()! { // this means the dashboard should not be scrollable
                        tableSize = CGSize(width: 0, height: 0)
                    } else {
                        adjustTable()
                    }
                    
                    self.delegateDashboard.updateDashboardContainer(tableSize!)
                }
                
                break
            case .kLINE:
                if self.delegateLine != nil {
                    adjustTable()
                    self.delegateLine.updateLineContainer(tableSize!)
                }
                
                break
            case .kUNBILLED_USAGE:
                if self.delegateUnbilledUsage != nil {
                    adjustTable()
                    self.delegateUnbilledUsage.updateUnbilledUsageContainer(tableSize!)
                }
                
                break
            default:
                if !isLastRowVisible() { // if last row is NOT VISIBLE
                    enableTableScroll()
                }
                
                break
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*print("You selected cell #\(indexPath.row)!") -- uncomment when debugging */
        
        switch Global.pageType {
        case .kDASHBOARD:
            let position = indexPath.row
            let card = Global.cellSequence[position]
            switch card {
            case CARD_CELL.kBANNER:
                UIApplication.shared.openURL(URL(string: "https://www.webe.com.my/shop/pay-with-webe")!)
                break
            default:
                break
            }
        case .kSWITCH_PRODUCT:
           /* print("previous: " + SwitchModel.getSelectedProductType()!)
            print("new:" + SwitchModel.getSelectedProductType()!) -- uncomment when debugging */
            
            selectedProduct = indexPath.row
            self.performSegue(withIdentifier: DRAWER_SWITCH_ACCOUNT, sender: self)
            
            break
        case .kSWITCH_ACCOUNT:
            self.performSegue(withIdentifier: UNWIND_TO_ROOT, sender: self)
            
            break
        default:
            break
        }
    }
    
    //MARK: - CustomCardCellDelegator Methods
    func callSegueFromCardCell(_ segue: String) {
        
        self.performSegue(withIdentifier: segue, sender:self)
    }
    
    func callPaymentFromDialog(_ amount: String) {
        globalInstance.setAmountToPay(amount)
        callPayment()
    }
    
    func callSubscribeVas(_ position: Int, _ switchIsOn: Bool) {
        subscribedVas(position, switchIsOn)
    }
    
    func callSubscribeAddOn(_ position:Int) {
        purchaseAddOn(position)
    }
    
    func callUpdateCreditLimit() {
        updateCreditLimit()
    }
    
    func callRetrieveGoodPayMaster(_ amount: String) {
        retrievePayMaster(amount)
    }
    
    //MARK: - TOOLTIP Methods:
    func imgTooltipTapped(_ toolTip: UIImageView, _ type: TOOLTIP_TYPE) {
        switch type {
        case .kCREDIT:
            let imgTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(callCreditLimitDialog))
            toolTip.isUserInteractionEnabled = true
            toolTip.addGestureRecognizer(imgTapGestureRecognizer)
            
            break
        default: // .kUNBILLED default is required to avoid crash
            let imgTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(callUnbilledDialog))
            toolTip.isUserInteractionEnabled = true
            toolTip.addGestureRecognizer(imgTapGestureRecognizer)
            
            break
        }
    }
    
    fileprivate func adjustTable() {
        self.view.layoutIfNeeded()
        
        tableSize = (tvCustomCardCell?.contentSize)!
        lcTableHeight.constant = (tableSize?.height)!
    }
    
    @objc func callMakePayment() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kMAKEPAYMENT_CLICKED)
        FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kMAKEPAYMENT_CLICKED)
        
        if Global.sharedInstance.isFeatureAccessible(0, self) {
            makePaymentDialog.delegateCard = self
            makePaymentDialog.show(animated: true)
        }
    }
    
    @objc func callLoadMoreAPI() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kLOADMORE_CLICKED)
        FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kLOADMORE_CLICKED)
        
        let dashboardModel = DashboardModel.sharedInstance
        
        if globalInstance.getIsOffline() {
            let didSaveFailed: Bool = dashboardModel.setCreditUnbilledResponse(DummyResponse.getUnbilledLines())
            
            if !didSaveFailed {
                self.callLoadMore()
            }
        } else {
            if Global.webServiceMngr.isOffline() {
                // don't do anything when offline
            } else {
                if Global.sharedInstance.isFeatureAccessible(0, self) {
                    self.globalInstance.showHUD()
                    
                    dashboardModel.prepareDashboardApi(SwitchModel.getSelectedAccountNo()!)
                    Global.webServiceMngr.makeRequest(dashboardModel.getCreditUnbilledRequestParams()) { (isResponseDidFail, isServerMaintenance) in
                        if isServerMaintenance { // to stop the process and avoid unexpected crashing
                            self.globalInstance.dismissHUD()
                            
                            return
                        }
                        
                        if isResponseDidFail { // if fail do something here
                            self.globalInstance.dismissHUD()
                            
                            // by default this is the action
                            self.globalInstance.showGeneralError()
                        } else {
                            self.callLoadMore()
                        }
                    }
                }
            }
        }
    }

    func callLoadMore() {
        Global.sharedInstance.setLoadMoreShouldDisplay(false)
        Global.sharedInstance.setIsCreditUnbilledApiTokenRefreshed(true)
            
        DispatchQueue.main.async{
            Global.pageType = Global.VIEW_TYPE.kDASHBOARD
            Global.cellSequence = [CustomCardCellViewController.CARD_CELL.kCARD,
                                       CustomCardCellViewController.CARD_CELL.kBUTTON,
                                       CustomCardCellViewController.CARD_CELL.kBANNER,
                                       CustomCardCellViewController.CARD_CELL.kBUTTON,
                                       CustomCardCellViewController.CARD_CELL.kCARD,
                                       CustomCardCellViewController.CARD_CELL.kCARD]
                
            // for animation purposes
            let indexPath: IndexPath = NSIndexPath(row: 3, section: 0) as IndexPath
            self.tvCustomCardCell?.reloadRows(at: [indexPath], with: .automatic)
                
            self.tvCustomCardCell?.reloadData()
            self.globalInstance.dismissHUD()
        }
    }
    
    func callPayment() {
        self.performSegue(withIdentifier: SHOW_PAYMENT, sender:self)
    }
    
    @objc func callChargesBreakdown() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kCHARGES_BREAKDOWN_CLICKED)
        FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kCHARGES_BREAKDOWN_CLICKED)
        
        callSegueFromCardCell(SHOW_CHARGES_BREAKDOWN)
    }
    
    @objc func callUnbilledUsage() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kUNBILLED_USAGE_CLICKED)
        FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kUNBILLED_USAGE_CLICKED)
        
        callSegueFromCardCell(SHOW_UNBILLED_USAGE)
    }
    
    @objc func callViewReceipt(sender:UIButton) {
        let shouldShowReceipt = HistoryModel.sharedInstance.ArrLatestPymtEBool?[sender.tag]
        
        if shouldShowReceipt! {
            FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kVIEW_RECEIPT)
            let receiptDialog = CustomDialogView(type: .VIEW_RECEIPT, position: sender.tag)
            receiptDialog.delegateCard = self
            receiptDialog.show(animated: true)
        } else {
            let noReceiptDialog = CustomDialogView(type: .NO_RECEIPT)
            noReceiptDialog.show(animated: true)
        }
    }
    
    @objc func callEditCreditLimit() {
        FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kEDIT_CREDIT_LIMIT_CLICKED)
        FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kEDIT_CREDIT_LIMIT_CLICKED)
        
        if Global.sharedInstance.isFeatureAccessible(0, self) {
            let model = CreditLimitModel.sharedInstance
            
            if globalInstance.getIsOffline() {
                let didSaveFail: Bool = model.setCreditLimitResponse(DummyResponse.creditLimitList())
                
                if !didSaveFail {
                    FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kEDITCREDITLIMIT)
                    let dialog = CustomDialogView(type: .EDIT_CREDIT_LIMIT)
                    dialog.delegateCard = self
                    dialog.show(animated: true)
                }
            } else {
                globalInstance.showHUD()
                model.prepareCreditLimitParams(SwitchModel.getSelectedAccountNo()!)
                Global.webServiceMngr.makeRequest(model.getCreditLimitList()) { (isResponseDidFail, isServerMaintenance) in
                    self.globalInstance.dismissHUD()
                    
                    if isServerMaintenance { // to stop the process and avoid unexpected crashing
                        return
                    }
                    
                    if isResponseDidFail { // if fail do something here
                        // by default this is the action
                        self.globalInstance.showGeneralError()
                    } else {
                        self.globalInstance.setUnbilledFlag(true)
                        
                        let dialog = CustomDialogView(type: .EDIT_CREDIT_LIMIT)
                        dialog.delegateCard = self
                        dialog.show(animated: true)
                    }
                }
            }
        }
    }
    
    @objc func callCreditLimitDialog() {
        FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kCREDITLIMIT_TOOLTIP)
        
        let dialog = CustomDialogView(type: .CREDIT_LIMIT)
        dialog.show(animated: true)
    }
    
    @objc func callUnbilledDialog() {
        FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kUNBILLED_TOOLTIP)
        
        let dialog = CustomDialogView(type: .UNBILLED_USAGE)
        dialog.show(animated: true)
    }
    
    fileprivate func enableTableScroll() {
        if (lcTableHeight != nil) {
            lcTableHeight.isActive = false
            tvCustomCardCell?.isScrollEnabled = true
        }
    }
    
    //preview pdf
    func callPreviewPDF(_ url: URL) {
        let docController = UIDocumentInteractionController(url: url)
        docController.delegate = self
        docController.presentPreview(animated: true)
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
// **************************************************************************************************************************************** \\
    
    // Update accordingly for the model values ============================================================================================================================================
    private func getDashboardCard(_ cell: CardTableViewCell, _ position: Int) -> CardTableViewCell {
        cell.btnLowerRight.isHidden = false
        
        cellData = getCellDataBill()
        
        var header = "null"
        var bodyUpper = "null"
        var bodyCenter = "null"
        var bodyLower = "null"
        var lowerRight = "null"
        
        let outstandingBal = DashboardModel.sharedInstance.getBalanceList()
        let creditUnbilledBalance = DashboardModel.sharedInstance.getCreditUnbilledBalanceResponse()
        
        switch position {
        case 0: // kCHARGES_BREAKDOWN
            let billBal: BillDetailsEntity = DashboardModel.sharedInstance.getBillList()!
            var leftBalance: Double = 0.0
            
            if outstandingBal != nil {
                leftBalance = (outstandingBal?.leftBalance.isEqual("-"))! ? 0.00 : Double((outstandingBal?.leftBalance)!)!
        
                var amount = ""
                if leftBalance <= 0.00 {
                    amount = String(leftBalance)
                    globalInstance.setAmountToDisplay(amount)
                } else {
                    globalInstance.setAmountToDisplay(amount)
                }
            }

            header = "total outstanding"
            bodyUpper = "RM " + globalInstance.formatNegativeAmount(String(describing: leftBalance), false)
            bodyCenter = "bill date: " + globalInstance.formatDate((billBal.billDate), Global.RAW_DATE_FORMAT.YYYYMMDD, Global.FINAL_DATE_FORMAT.DD_MM_YYYY)
            bodyLower = "pay by: " + globalInstance.formatDate((billBal.dueDate), Global.RAW_DATE_FORMAT.YYYYMMDD, Global.FINAL_DATE_FORMAT.DD_MM_YYYY)
            
            lowerRight = "view charges breakdown"
            
            cell.btnLowerLeft.isHidden = true
            cell.btnLowerRight.addTarget(self, action: #selector(callChargesBreakdown), for: UIControlEvents.touchUpInside)
            
            cell.lblBodyUpper.text = bodyUpper
            cell.lblBodyCenter.text = bodyCenter
            break
        case 4: // kCREDIT_LIMIT
            if Global.sharedInstance.getLoadMoreShouldDisplay()! {
                cell.contentView.isHidden = true
            } else {
                cell.contentView.isHidden = false
                
                let totalCreditLimit = (creditUnbilledBalance != nil) ? (creditUnbilledBalance?.totalCreditLimit!)! : "0.00"
                let creditLimitLeft = (creditUnbilledBalance != nil) ? (creditUnbilledBalance?.creditLimitLeft!)! : "0.00"
                let depositValue = (creditUnbilledBalance != nil) ? (creditUnbilledBalance?.depositValue!)! : "0.00"
                
                header = "credit limit";
                bodyUpper = "RM " + globalInstance.amountFormatter(totalCreditLimit)
                bodyCenter = "balance left: RM " + globalInstance.amountFormatter(creditLimitLeft)
                bodyLower = "current deposit: RM " + globalInstance.amountFormatter(depositValue)
                lowerRight = "edit credit limit"
                
                cell.btnLowerLeft.isHidden = true
                cell.btnLowerRight.addTarget(self, action: #selector(callEditCreditLimit), for: UIControlEvents.touchUpInside)
                
                cell.imgTooltip.isHidden = false
                imgTooltipTapped(cell.imgTooltip, .kCREDIT)
                
                cell.lblBodyUpper.text = bodyUpper
                cell.lblBodyCenter.text = bodyCenter
            }
            
            break
        case 5:
            if Global.sharedInstance.getLoadMoreShouldDisplay()! {
                cell.contentView.isHidden = true
            } else {
                cell.contentView.isHidden = false
                cell.lblBodyCenter.isHidden = true
                cell.lblBodyLower.isHidden = true
                
                let rawTotalUnbilled = (creditUnbilledBalance != nil) ? creditUnbilledBalance!.totalUnbilledAmount! : "0.00"
                let totalUnbilledAmt: String = globalInstance.formatNegativeAmount((rawTotalUnbilled), true)
                let cnt: Int = (DashboardModel.sharedInstance.getUnbilledLines()?.count)!
                var lineStr: String = ""
                
                if (cnt == 1) {
                    lineStr = "line"
                } else {
                    lineStr = "lines"
                }
            
                header = "unbilled usage"
                lowerRight = "view unbilled usage"
                
                cell.btnLowerLeft.isHidden = true
                cell.btnLowerRight.addTarget(self, action: #selector(callUnbilledUsage), for: UIControlEvents.touchUpInside)
                
                cell.imgTooltip.isHidden = false
                imgTooltipTapped(cell.imgTooltip, .kUNBILLED)
                
                // set 2 different font size in a single label
                let size1Attribute:[NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_XXX)!]
                let size2Attribute:[NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue):globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)!]
                
                let amountStr:String = "RM " + totalUnbilledAmt
                let lineNumberStr:String = " / \(cnt) " + lineStr
                
                let amtSize1 = NSMutableAttributedString(string: amountStr, attributes: size1Attribute)
                let lineSize2 = NSMutableAttributedString(string: lineNumberStr, attributes: size2Attribute)
                
                let totalUnbilledValue = NSMutableAttributedString()
                totalUnbilledValue.append(amtSize1)
                totalUnbilledValue.append(lineSize2)
                
                cell.lblBodyUpper.attributedText = totalUnbilledValue
            }
            
            break
        default:
            break
        }
        
        cell.lblHeader.text = header
        cell.lblBodyLower.text = bodyLower
        cell.btnLowerRight.setTitle(lowerRight, for: .normal)
        cell.btnLowerRight.setTitleColor(globalInstance.getColorAttribute(.kPRIMARY_DARK, .DEFAULT), for: .normal)
        
        return cell;
    }
    
    private func getCellEditAddon(_ cell: DetailedTableViewCell, _ position: Int) -> DetailedTableViewCell {
        cell.lblLeftUpper.isHidden = false
        cell.lblLeftCenter.isHidden = false
        cell.lblLeftLower.isHidden = false
        cell.separatorLine.isHidden = false
        
        let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
        let service: AddOnVasServicesEntity = services[position]
        
        leftUpper = service.name!
        leftCenter = service.descrption!
        leftLower = ServicesModel.addonPrice(service)
        
        let display: String? = service.buttonDisplay
        let isActivated: Bool = service.isActivated == "1"
        let shouldKeepDisplay: Bool = service.keepDisplay == "1"

        if service.serviceType == "vas" {
            cell.vObjectNonStatic.isHidden = false
            cell.vObjectStatic.isHidden = true
            cell.btnRightCenter.isHidden = true
            cell.lblRightCenter.isHidden = true
            cell.switchRightCenter.isHidden = false
            cell.switchRightCenter.setOn(isActivated, animated: true)
        } else {
            if isActivated && !shouldKeepDisplay {
                cell.vObjectStatic.isHidden = false
                cell.vObjectNonStatic.isHidden = true
                cell.lblRightCenter.isHidden = false
                cell.switchRightCenter.isHidden = true
                cell.btnRightCenter.isHidden = true
                cell.lblRightCenter.text = "added"
                cell.lblRightCenter.textColor = globalInstance.getColorAttribute(.kTEAL, .DEFAULT)
            } else {
                cell.vObjectNonStatic.isHidden = false
                cell.vObjectStatic.isHidden = true
                cell.switchRightCenter.isHidden = true
                cell.btnRightCenter.isHidden = false
                cell.btnRightCenter.setTitle(display,for: UIControlState.normal)
                cell.btnRightCenter.setTitleColor(globalInstance.getColorAttribute(.kTEAL, .DEFAULT), for: .normal)
            }
        }
        
        cell.lblLeftUpper.text = leftUpper
        cell.lblLeftUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE)
        
        cell.lblLeftCenter.text = leftCenter
        cell.lblLeftCenter.attributedText = Global.customText.getLblLineSpacing(3, leftCenter)
        
        cell.lblLeftLower.text = leftLower
        cell.lblLeftLower.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE)
        cell.lblLeftLower.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        return cell;
    }
    
    // Update accordingly for the model values ============================================================================================================================================
    private func getSwitchCard(_ cell: CardTableViewCell, _ position: Int) -> CardTableViewCell {
        tvCustomCardCell?.allowsSelection = true
        
        cell.lblBodyLower.isHidden = true
        cell.btnLowerLeft.isHidden = true
        cell.btnLowerRight.isHidden = true
        
        cellData = getCellDataBill()
        
        var header = "null"
        var bodyUpper = "null"
        var bodyCenter = "null"
        
        switch Global.pageType {
        case .kSWITCH_PRODUCT:
            cell.lblBodyUpper.isHidden = true
            cell.lblBodyCenter.isHidden = true
            
            let prodTypes: [String] =  SwitchModel.getProductTypes()
            //let accountNumbers = CustomerDetailsModel.sharedInstance.getAccountNoList()
            let prodType: String = prodTypes[position]
            
            if(prodType.isEqual(SwitchModel.getSelectedProductType())) {
                cell.switchSelector.isHidden = false
            } else {
                cell.switchSelector.isHidden = true
            }
            
            let blackAttribute:[NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): globalInstance.getFontAttribute(.kBLACK, .kLARGE_X)!]
            let semiBoldAttribute:[NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue):globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE_X)!]
            
            let productStr:String = "webe"
            var productTypeStr:String = ""
            
            if prodType == Global.PROD_TYPE.kMOBILE.rawValue {
                productTypeStr = "mobile®"
                
            } else if prodType == Global.PROD_TYPE.kBROADBAND.rawValue {
                productTypeStr = "broadband®"
               
            }
        
            header = productTypeStr
            bodyUpper = "line";
            bodyCenter = "xxxxxx";
            
            let productBlack = NSMutableAttributedString(string: productStr, attributes: blackAttribute)
            let productTypeSemiBold = NSMutableAttributedString(string: productTypeStr, attributes: semiBoldAttribute)
            
            let productTypeValue = NSMutableAttributedString()
            productTypeValue.append(productBlack)
            productTypeValue.append(productTypeSemiBold)
            
            cell.lblHeader.attributedText = productTypeValue
            cell.lblHeader.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
            
            
            break
        default: // .kSWITCH_ACCOUNT:
            header = "account number"
            bodyUpper = "20003218";
            bodyCenter = "status: active";
            
            cell.lblHeader.text = header
            
            break
        }
        
        cell.lblBodyUpper.text = bodyUpper
        cell.lblBodyCenter.text = bodyCenter
        
        return cell;
    }
    
    // Update accordingly for the model values ============================================================================================================================================
    private func getBillCard(_ cell: CardTableViewCell, _ position: Int) -> CardTableViewCell {
        cell.btnLowerLeft.isHidden = true // save bill will not include on upcoming release
        cell.btnLowerRight.isHidden = false
        cell.lblBodyLower.isHidden = true
        
        let billHistory:Array = HistoryModel.sharedInstance.getBillHistoryList(SwitchModel.getSelectedAccountNo()!)!
        let bill = billHistory[position]
        let latestPayment = HistoryModel.sharedInstance.getLatestPaymentDetailValues(position)
        
        let header = bill.billDate
        let bodyUpper = "RM " + globalInstance.amountFormatter(bill.currentChargeAmt)
        var bodyCenter = ""
        let bodyLower = ((latestPayment?.paymentDate) != nil) ? latestPayment?.paymentDate : ""
        let lowerLeft = "save bill"
        let lowerRight = "view receipt"
        
        if bill.billStatus.isEqual("1") {
            bodyCenter = "fully paid on:"
            
            cell.lblBodyLower.isHidden = false
        } else if bill.billStatus.isEqual("2") {
            bodyCenter = "partially paid"
        } else {
            bodyCenter = "not paid"
        }
        
        cell.lblHeader.text = globalInstance.formatDate(header, Global.RAW_DATE_FORMAT.YYYYMMDD, Global.FINAL_DATE_FORMAT.DD_MM_YYYY)
        cell.lblBodyUpper.text = bodyUpper
        cell.lblBodyCenter.text = bodyCenter
        cell.lblBodyLower.text = globalInstance.formatDate(bodyLower!, Global.RAW_DATE_FORMAT.YYYYMMDD, Global.FINAL_DATE_FORMAT.DD_MM_YYYY)
        
        cell.btnLowerLeft.setTitle(lowerLeft, for: .normal)
        cell.btnLowerRight.setTitle(lowerRight, for: .normal)
        cell.btnLowerRight.setTitleColor(globalInstance.getColorAttribute(.kPRIMARY_DARK, .DEFAULT), for: .normal)
        
        if latestPayment != nil {
            HistoryModel.sharedInstance.ArrLatestPymtEBool?.append(true)
        } else {
            HistoryModel.sharedInstance.ArrLatestPymtEBool?.append(false)
        }
        cell.btnLowerRight.tag = position
        
        cell.btnLowerRight.addTarget(self, action: #selector(callViewReceipt), for: UIControlEvents.touchUpInside)
        
        return cell;
    }
    
    private func getCellAppSettings(_ cell: DetailedTableViewCell, _ position: Int) -> DetailedTableViewCell {
        cell.lblLeftUpper.isHidden = false
        cell.lblLeftCenter.isHidden = false
        cell.vObjectNonStatic.isHidden = false
        cell.separatorLine.isHidden = false
        cell.switchRightConstraint.constant = 0
        
        switch position {
        case 0:
            leftUpper = "app version"
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                leftCenter = version
            }
            
            /* Hidden for Newsletter Pref
            leftCenter = "newsletter preference"
            leftLower = "billing / customer service"
            
            cell.imgRightCenter.isHidden = false
            */
            
            break
        /*case 1: // for push notification
            cell.lblLeftCenter.isHidden = true
            cell.switchRightCenter.isHidden = false
            
            leftUpper = "push notification"
            cell.switchRightCenter.isOn = //PushNotification.sharedInstance.isPushNotifEnabled use getter method
            
            cell.switchCallback = { (switchIsOn) in
                PushNotification.sharedInstance.setPushNotifEnable(switchIsOn)
            }
            
            break*/
        default:
            leftUpper = "app version"
            leftCenter = "ver 1.0"
            
            break
        }
        
        cell.lblLeftUpper.text = leftUpper
        cell.lblLeftUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        
        cell.lblLeftCenter.text = leftCenter
        cell.lblLeftCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
        return cell;
    }

    func getCellDataProfile() -> [AnyObject] {
        let data = [["title": "full name", "detail": CustomerDetailsModel.sharedInstance.getFullName()],
                    ["title": "email address", "detail":  CustomerDetailsModel.sharedInstance.getEmail()],
                    ["title": "password", "detail":  CustomerDetailsModel.sharedInstance.getPassword()],
                    ["title": "contact number", "detail": globalInstance.formatMsisdn(CustomerDetailsModel.sharedInstance.getContactNumber())]]
        
        /*print(CustomerDetailsModel.sharedInstance.getAccountNoList()!) -- uncomment when debugging */
        
        return data as [AnyObject]
    }
    
    func getCellDataPreviousCharges() -> [AnyObject] {
        let prevBill = DashboardModel.sharedInstance.getBillList()
        
        // computation for total previous amount
        let prevBal: Double = Double((prevBill?.previousBalAmt)!)!
        let payment: Double = Double((prevBill?.paymentAmt)!)!
        let totalPrevAmt = prevBal + payment
        
        let data = [["title": "previous balance", "detail": "RM " + globalInstance.amountFormatter((prevBill?.previousBalAmt)!)],
                    ["title": "amount paid", "detail": "RM " + globalInstance.amountFormatter((prevBill?.paymentAmt)!)],
                    ["title": "adjustment", "detail": "RM " + globalInstance.amountFormatter((prevBill?.adjustmentAmt)!)],
                    ["title": "overdue since: " + globalInstance.formatDate((prevBill?.billDate)!, Global.RAW_DATE_FORMAT.YYYYMMDD, Global.FINAL_DATE_FORMAT.DD_MM_YYYY),
                     "detail": "RM " + globalInstance.amountFormatter(String(totalPrevAmt))]]
        
        //print(DashboardModel.sharedInstance.getBillList() ?? "nil")
        
        return data as [AnyObject]
    }
    
    func getCellDataCurrentCharges() -> [AnyObject] {
        let currentBill = DashboardModel.sharedInstance.getBillList()
        
        let currentChargeAmt = Float((currentBill?.currentChargeAmt)!)
        let gstAmt = Float((currentBill?.gstAmt)!)
        let rebateAmt = Float((currentBill?.rebateAmt)!)
        
        let total: Float = (currentChargeAmt! - gstAmt! - rebateAmt!)
        
        let data = [["title": "total amount excluding GST", "detail": "RM " + globalInstance.amountFormatter(String(total))],
                    ["title": "GST (6% of RM)", "detail": "RM " + globalInstance.amountFormatter((currentBill?.gstAmt)!)],
                    ["title": "rebate", "detail": "RM " + globalInstance.amountFormatter((currentBill?.rebateAmt)!)],
                    ["title": "rounding adjustment", "detail": "RM " + globalInstance.amountFormatter((currentBill?.roundingAmount)!)],
                    ["title": "due: " + globalInstance.formatDate((currentBill?.dueDate)!, Global.RAW_DATE_FORMAT.YYYYMMDD, Global.FINAL_DATE_FORMAT.DD_MM_YYYY),
                     "detail": "RM " + globalInstance.amountFormatter((currentBill?.currentChargeAmt)!)]]
        
        //print(DashboardModel.sharedInstance.getBillList() ?? "nil")
        
        return data as [AnyObject]
        
    }
    
    func getCellDataTotalAmount() -> [AnyObject] {
        let rawTotalAmt = (DashboardModel.sharedInstance.getBillList()?.totalAmt)!
        var totalAmt: String = ""
        totalAmt = globalInstance.formatNegativeAmount(rawTotalAmt, true)
        
        let data = [["title": "total amount",
                     "detail": "RM " + totalAmt]]
        
        return data as [AnyObject]
    }
    
    func getCellDataPurchase() -> [AnyObject] {
        let data = [["title": "Pokemon GO Premium", "detail": "Google Play Store", "date" : "03 Apr 2017", "time": "3:30PM", "price": "RM 29.99", "status":"refunded"],
                    ["title": "FIFA", "detail": "Google Play Store", "date" : "13 Mar 2017", "time": "4:30AM", "price": "RM 20.99", "status":" "],
                    ["title": "FIFA", "detail": "Google Play Store", "date" : "13 Mar 2017", "time": "4:30AM", "price": "RM 20.99", "status":" "]]
        
        return data as [AnyObject]
    }
    
    func getCellDataBill() -> [AnyObject] {
        let data = [["date": "1st December 2017", "price": "RM86.29", "label" : "full paid on:", "datePaid": "03 Apr 2017"], ["date": "1st January 2017", "price": "RM85.23", "label" : "full paid on:", "datePaid": "10 January 2017"], ["date": "1st January 2017", "price": "RM85.23", "label" : "full paid on:", "datePaid": "10 January 2017"]]
        
        return data as [AnyObject]
    }
    
    func getCellDummy() -> [AnyObject] {
        let data = [["title": "full name", "detail": "Nico Solis"],//CustomerDetailsModel.sharedInstance.getFullName()],
            ["title": "email address", "detail":  "explosivex0000000000@yahoo.com"],//CustomerDetailsModel.sharedInstance.getEmail()],
            ["title": "password", "detail":  "thishouldbehidden"],//CustomerDetailsModel.sharedInstance.getPassword()],
            ["title": "contact number", "detail": "09157755186"]]//CustomerDetailsModel.sharedInstance.getContactNumber()]]
        
        return data as [AnyObject]
    }
    
    func getCellDataPhoneSetting() -> [AnyObject] {
        let data = [["title": "request for APN settings"],
                    ["title": "SIM pin unlock code (PUK)"],
                    ["title": "SIM  pin code (PIN)"],
                    ["title": "SMS center number"]]
        
        return data as [AnyObject]
    }
    
    @objc func callVasDialog(addOnSwitch: UISwitch){
        let position = addOnSwitch.tag
        if addOnSwitch.isOn{
            let dialog = CustomDialogView(type: .CONFIRMATION_VAS , position: position, switchIsOn: addOnSwitch.isOn)
            dialog.delegateCard = self
            dialog.show(animated: true)
        } else {
            let dialog = CustomDialogView(type: .UNSUBSCRIBE_VAS , position: position, switchIsOn: addOnSwitch.isOn)
            dialog.delegateCard = self
            dialog.show(animated: true)
        }
    }
    
    @objc func callPurchaseAddOnDialog(addOnButton: UIButton){
        let position = addOnButton.tag
        
        let dialog = CustomDialogView(type: .CONFIRMATION_ADDON , position: position)
        dialog.delegateCard = self
        dialog.show(animated: true)
    }
    
    func callSuccessDialog(_ position:Int){
        globalInstance.dismissHUD()
        
        let dialog = CustomDialogView(type: .SUCCESS_EDIT_ADD_ON, position: position)
        dialog.delegateCard = self
        dialog.show(animated: true)
    }
    
    func callFailedEditAddOnDialog(_ position:Int){
        globalInstance.dismissHUD()
        
        let dialog = CustomDialogView(type: .FAILED_EDIT_ADD_ON, position: position)
        dialog.delegateCard = self
        dialog.show(animated: true)
    }
    
    func callSuccessUpdateCreditLimit(){
        globalInstance.dismissHUD()
        
        let dialog = CustomDialogView(type: .SUCCESS_CREDIT_LIMIT)
        dialog.delegateCard = self
        dialog.show(animated: true)
    }
    
    func callFailedEditCreditLimit(){
        globalInstance.dismissHUD()
        
        let dialog = CustomDialogView(type: .FAILED_CREDIT_LIMIT)
        dialog.delegateCard = self
        dialog.show(animated: true)
    }
    
    func callDepositCreditLimit(){
        FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kDEPOSITNEEDED)
        
        globalInstance.dismissHUD()
        
        let dialog = CustomDialogView(type: .CREDIT_LIMIT_DEPOSIT_NEEDED)
        dialog.delegateCard = self
        dialog.show(animated: true)
    }
    
    func callPaymentIpay(_ amount: String) {
        let paymentModel = PaymentModel.sharedInstance
        let amountValue:String = amount
        globalInstance.setAmountToPay(amount)
        paymentModel.preparePaymentIPayParams(SwitchModel.getSelectedAccountNo()!, amountValue)
        /*let param: [String: AnyObject] = paymentModel.getRequestPaymentParams() as [String : AnyObject]
        print(param) -- uncomment when debugging */
        
        globalInstance.showHUD()
        
        Global.webServiceMngr.makeRequest(paymentModel.getRequestPaymentParams()) { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                // if specific error, this should show
                if self.globalInstance.getShouldShowSeperateError() {
                    self.makePaymentDialog.lblFooterDescription.text = EXCEEDED_AMOUNT
                    self.makePaymentDialog.lblFooterDescription.textColor = self.globalInstance.getColorAttribute(.kRED, .DEFAULT)
                    
                    return
                }
                
                // by default this is the action
                self.globalInstance.showGeneralError()
            } else {
                self.makePaymentDialog.dismiss(animated: true)
                
                self.globalInstance.setPaymentBillFlag(true)
                self.callPayment()
            }
        }
    }
    
    func updateCreditLimit() {
        let creditLimitModel = CreditLimitModel.sharedInstance
        
        let amount = Global.sharedInstance.getAmountToDeposit()!
        
        globalInstance.showHUD()
        
        creditLimitModel.prepareUpdateCreditLimitParams(SwitchModel.getSelectedAccountNo()!, amount, CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()))
        /*let param: [String: AnyObject] = creditLimitModel.getUpdateCreditLimitParams() as [String : AnyObject]
        print(param) -- uncomment when debugging */
        
        Global.webServiceMngr.makeRequest(creditLimitModel.getUpdateCreditLimitParams()) { (isResponseDidFail, isServerMaintenance) in
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                self.globalInstance.dismissHUD()
                
                return
            }
            
            if isResponseDidFail {  // if fail do something here
                self.callFailedEditCreditLimit()
            } else {
                self.shouldDepositCreditLimit()
            }
        }
    }
    
    func retrievePayMaster(_ amount: String) {
        let creditLimitModel = CreditLimitModel.sharedInstance
        
        let amount = Global.sharedInstance.getAmountToDeposit()!
        let amountValue:String =  amount
        
        globalInstance.showHUD()
        
        creditLimitModel.prepareRetrieveGoodPayMaster(SwitchModel.getSelectedAccountNo()!, amountValue)
        /*let param: [String: AnyObject] = creditLimitModel.getRetrieveGoodPayMasterParams() as [String : AnyObject]
        print(param) -- uncomment when debugging */
        
        Global.webServiceMngr.makeRequest(creditLimitModel.getRetrieveGoodPayMasterParams()) { (isResponseDidFail, isServerMaintenance) in
            self.globalInstance.dismissHUD()
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                return
            }
            
            if isResponseDidFail { // if fail do something here
                // by default this is the action
                self.globalInstance.showGeneralError()
            } else {
                let payDeposit = UserDefaults.standard.bool(forKey: CreditLimitModel.CONSTANTS.kPAY_DEPOSIT)
                if payDeposit {
                    self.updateCreditLimit()
                } else {
                    self.confirmationUpdateCreditLimit(amount)
                }
            }
        }
    }
    
    func confirmationUpdateCreditLimit(_ selectedAmount: String) {
        let dialog = CustomDialogView(type: .CONFIRMATION_GPM, amount: selectedAmount)
        dialog.delegateCard = self
        dialog.show(animated: true)
    }
    
    func shouldDepositCreditLimit() {
        let shouldPayDeposit = UserDefaults.standard.bool(forKey: CreditLimitModel.CONSTANTS.kSHOULD_DEPOSIT)
        if shouldPayDeposit {
            callDepositCreditLimit()
        } else {
            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kINCREASE_CREDIT_LIMIT_SUCCESS)
            FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kINCREASE_CREDIT_LIMIT_SUCCESS)
            callSuccessUpdateCreditLimit()
        }
    }
    
    func subscribedVas(_ position: Int, _ switchIsOn: Bool) {
        let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
        let service: AddOnVasServicesEntity = services[position]
        globalInstance.showHUD()
        
        /*
        * Negate isChecked to pass 1 as action value for deactivating vas
         * 0 - activate vas
         * 1 - deactivate
         * */
        
        let code: String = service.code!
        let name: String = service.name!
        let deposit: String = service.depositValue!
        let action: String = (switchIsOn ? "0" : "1")
       
        if service.code != "VASIR" {
            ServicesModel.sharedInstance.prepareSubscribeVasParams(SwitchModel.getSelectedAccountNo()!, CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()), code, name, action)
            /*let param: [String: AnyObject] = ServicesModel.sharedInstance.getSubscribedVasParams()
            print(param) -- uncomment when debugging */
            
            Global.webServiceMngr.makeRequest(servicesModel.getSubscribedVasParams()) { (isResponseDidFail, isServerMaintenance) in
                if isServerMaintenance { // to stop the process and avoid unexpected crashing
                    self.globalInstance.dismissHUD()
                    
                    return
                }
                
                if isResponseDidFail { // if fail do something here
                    FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_FAILED)
                    FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_FAILED)
                    
                    self.globalInstance.dismissHUD()
                    self.callReloadTable()
                    self.callFailedEditAddOnDialog(position)
                } else {
                    FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_SUCCESS)
                    FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_SUCCESS)
                    
                    self.globalInstance.setLineFlag(true)
                    self.callSuccessDialog(position)
                }
            }

            
        } else {
            let msisdn: String = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
            let status: String = (CustomerDetailsModel.sharedInstance.getLineByMsisdn(msisdn)?.subStatus)!
            
            if status.caseInsensitiveCompare(Global.STATUS_TYPE.SUSPENDED) == ComparisonResult.orderedSame {
                let dialog = CustomDialogView(type: .SUSPENDED)
                dialog.delegateCard = self
                dialog.show(animated: true)
            } else {
                //turn off vas ir
                if action == "1" {
                    ServicesModel.sharedInstance.prepareSubscribeVasParams(SwitchModel.getSelectedAccountNo()!, CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()), code, name, action)
                    /*let param: [String: AnyObject] = ServicesModel.sharedInstance.getSubscribedVasParams()
                    print(param) -- uncomment when debugging*/
                    
                    Global.webServiceMngr.makeRequest(servicesModel.getSubscribedVasParams()) { (isResponseDidFail, isServerMaintenance) in
                        if isServerMaintenance { // to stop the process and avoid unexpected crashing
                            self.globalInstance.dismissHUD()
                            
                            return
                        }
                        
                        if isResponseDidFail { // if fail do something here
                            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_FAILED)
                            FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_FAILED)
                            
                            self.globalInstance.dismissHUD()
                            self.callReloadTable()
                            self.callFailedEditAddOnDialog(position)
                        } else {
                            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_SUCCESS)
                            FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_SUCCESS)
                            
                            self.globalInstance.setLineFlag(true)
                            self.callSuccessDialog(position)
                        }
                    }
                    
                } else {
                    //turn on vas ir
                    globalInstance.setAmountToDeposit(deposit)
                    ServicesModel.sharedInstance.prepareSubscribeVasIrParams(SwitchModel.getSelectedAccountNo()!, CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()),deposit, code, name, action)
                    /*let param: [String: AnyObject] = ServicesModel.sharedInstance.getSubscribedVasParams()
                    print(param) -- uncomment when debugging */
                    
                    Global.webServiceMngr.makeRequest(servicesModel.getSubscribedVasIrParams()) { (isResponseDidFail, isServerMaintenance) in
                        if isServerMaintenance { // to stop the process and avoid unexpected crashing
                            self.globalInstance.dismissHUD()
                            
                            return
                        }
                        
                        if isResponseDidFail { // if fail do something here
                            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_FAILED)
                            FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_FAILED)
                            
                            self.globalInstance.dismissHUD()
                            self.callReloadTable()
                            self.callFailedEditAddOnDialog(position)
                        } else {
                            self.globalInstance.setLineFlag(true)
                            self.shouldDepositIR(position)
                        }
                    }
                }

            }
        }
    }
    
    func shouldDepositIR(_ position: Int) {
        let shouldPayDeposit = UserDefaults.standard.bool(forKey: ServicesModel.CONSTANTS.kSHOULD_DEPOSIT_IR)
        if shouldPayDeposit {
            globalInstance.setPaymentIRFlag(true)
            callDepositIR()
        } else {
            FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_SUCCESS)
            FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_SUCCESS)
            self.callSuccessDialog(position)
        }
    }
    
    func callDepositIR(){
        let dialog = CustomDialogView(type: .IR_DEPOSIT_NEEDED)
        dialog.delegateCard = self
        dialog.show(animated: true)
    }
    
    func purchaseAddOn(_ position:Int) {
        let services: [AddOnVasServicesEntity] = ServicesModel.getServices()!
        let service: AddOnVasServicesEntity = services[position]
        globalInstance.showHUD()
        
        //purchase add on
        ServicesModel.sharedInstance.preparePurchaseAddOnParams(SwitchModel.getSelectedAccountNo()!,CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()), service.code!)
        let param: [String: AnyObject] = ServicesModel.sharedInstance.getPurchaseAddOnParams()
        
        Global.webServiceMngr.makeRequest(param) { (isResponseDidFail, isServerMaintenance) in
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                self.globalInstance.dismissHUD()
                
                return
            }
            
            if isResponseDidFail { // if fail do something here
                FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_FAILED)
                FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_FAILED)
                
                self.globalInstance.dismissHUD()
                self.callReloadTable()
                self.callFailedEditAddOnDialog(position)
            } else {
                FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_SUCCESS)
                FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kADD_ONS_SUCCESS)
                
                self.globalInstance.setLineFlag(true)
                self.globalInstance.setUnbilledFlag(true)
                
                if service.serviceType == ServicesModel.SERVICE_TYPE.kTOPUP {
                    self.globalInstance.setShouldReloadPurchaseHistory(true)
                }
                
                self.callSubscribedVasApi(position)
            }
        }
    }
    
    func callSubscribedVasApi(_ position: Int) {
        servicesModel.prepareRetrieveServicesParams(SwitchModel.getSelectedAccountNo()!, CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()), ListTableViewCell.POSTPAID )
        
        globalInstance.showHUD()
        
        Global.webServiceMngr.makeRequest(servicesModel.getRetrieveServicesParams()) { (isResponseDidFail, isServerMaintenance) in
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                self.globalInstance.dismissHUD()
                
                return
            }
            
            if isResponseDidFail { // if fail do something here
                self.callFailedEditAddOnDialog(position)
            } else {
                self.callSuccessDialog(position)
            }
        }
        
    }
    
    func callReloadTable() {
        tvCustomCardCell?.reloadData()
        
        willDisplayTableMessage()
    }
    
}
