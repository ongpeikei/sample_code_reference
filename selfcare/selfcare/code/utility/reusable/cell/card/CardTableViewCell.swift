//
//  CardTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 15/06/2017.
//
//

import UIKit

class CardTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lblHeader: UILabel!

    @IBOutlet weak var lblBodyUpper: UILabel!
    @IBOutlet weak var lblBodyCenter: UILabel!
    @IBOutlet weak var lblBodyLower: UILabel!
    
    @IBOutlet weak var btnLowerLeft: UIButton!
    @IBOutlet weak var btnLowerRight: UIButton!
    @IBOutlet weak var imgTooltip: UIImageView!
    @IBOutlet weak var switchSelector: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func prepareLayout() {
        lblHeader.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblHeader.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_58)
        
        lblBodyUpper.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_XXX)
        lblBodyUpper.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblBodyCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblBodyCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_58)
        
        lblBodyLower.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblBodyLower.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_58)
        
        btnLowerLeft.titleLabel?.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE)
        btnLowerLeft.titleLabel?.textColor = globalInstance.getColorAttribute(.kPRIMARY_DARK, .DEFAULT)
        
        btnLowerRight.titleLabel?.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE)
        btnLowerRight.titleLabel?.textColor = globalInstance.getColorAttribute(.kPRIMARY_DARK, .DEFAULT)
    }
    
}
