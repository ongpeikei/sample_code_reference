//
//  TwoRowsTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 12/07/2017.
//
//

import UIKit

class TwoRowsTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var txtHeader: UILabel!
    @IBOutlet weak var txtHeaderValue: UILabel!
    @IBOutlet weak var txtBodyTitle: UILabel!
    @IBOutlet weak var txtBodyValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func prepareLayout() {
        txtHeader.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        txtHeader.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        txtHeaderValue.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        txtHeaderValue.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        txtBodyTitle.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        txtBodyTitle.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        txtBodyValue.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_X)
        txtBodyValue.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
    }
    
}
