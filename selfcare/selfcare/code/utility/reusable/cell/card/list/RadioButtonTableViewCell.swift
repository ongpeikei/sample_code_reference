//
//  RadioButtonTableViewCell.swift
//  
//
//  Created by ONG PEI KEI on 19/07/2017.
//
//

import UIKit

class RadioButtonTableViewCell: UITableViewCell {
    @IBOutlet weak var btnRadio: UIImageView!
    @IBOutlet weak var lblRadioBtn: UILabel!
    
    let globalInstance = Global.sharedInstance
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareLayout()
    }
    
    func prepareLayout() {
        lblRadioBtn.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE_X)
        lblRadioBtn.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_58)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
