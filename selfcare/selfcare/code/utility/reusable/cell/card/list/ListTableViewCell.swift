//
//  ListTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 07/06/2017.
//
//

import UIKit

protocol ListTableViewCellDelegator {
    func callSegueFromCustomDialog(_ segue: String)
}

class ListTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource, ListTableViewCellDelegator {
    let globalInstance = Global.sharedInstance
    let customText = Global.customText
    let servicesModel = ServicesModel.sharedInstance
    
    var delegate:CustomCardCellDelegator!
    
    @IBOutlet weak var lcCellHeight: NSLayoutConstraint!
    
    @IBOutlet weak var vHeader: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet var tvList: UITableView?
    
    @IBOutlet weak var vFooter: UIView!
    @IBOutlet weak var btnFooter: UIButton!
    
    var listType = LIST_TYPE.kPROFILE
    var cellData:Array = [AnyObject]()
    
    static var POSTPAID = "postpaid"
    
    var leftUpper = "default"
    var leftCenter = "default"
    var leftLower = "default"
    var rightUpper = "default"
    var rightCenter = "default"
    var rightLower = "default"
    var footer = "default"
    
    enum LIST_TYPE {
        case kPREVIOUS_CHARGES
        case kCURRENT_CHARGES
        case kTOTAL_AMOUNT
        
        case kCURRENT_ADDON
        case kPHONE_SETTINGS
        case kEDIT_ADDON
        
        case kKNOWLEDGE_BASE
        case kOTHERS
        
        case kPROFILE
        
        case kPURCHASE_DCB
        case kPURCHASE_ADDON
    }
    
    override func layoutSubviews() {
        tvList?.register(UINib(nibName: "DetailedTableViewCell", bundle: nil), forCellReuseIdentifier: CustomCardCellViewController.CARD_CELL.kDETAILED)
        
        prepareLayout()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomCardCellViewController.CARD_CELL.kDETAILED, for: indexPath) as! DetailedTableViewCell
        cell.selectionStyle = .none
        
        cell.textLabel?.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
        cell.textLabel?.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_34)
        
        let position = indexPath.row
        
        let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
        if position == lastRowIndex {
            switch listType {
            case .kPREVIOUS_CHARGES,
                 .kCURRENT_CHARGES,
                 .kTOTAL_AMOUNT,
                 .kPHONE_SETTINGS,
                 .kPROFILE,
                 .kPURCHASE_DCB,
                 .kPURCHASE_ADDON:
                cell.separatorLine.backgroundColor = UIColor.clear
                
                break
            default:
                break
            }
        }
        
        switch listType {
        case .kPROFILE:
            return getCellProfile(cell, position)
            
            // SAME PAGE/VIEW
        case .kPURCHASE_DCB:
            return getCellPurchase(cell, position)
        case .kPURCHASE_ADDON:
            return getCellPurchase(cell, position)
            
            // SAME PAGE/VIEW
        case .kCURRENT_ADDON:
            return getCellLine(cell, position)
        case .kPHONE_SETTINGS:
            return getCellLine(cell, position)
        
            // SAME PAGE/VIEW
        case .kPREVIOUS_CHARGES:
            return getCellBreakdownCharges(cell, position)
        case .kCURRENT_CHARGES:
            return getCellBreakdownCharges(cell, position)
        case .kTOTAL_AMOUNT:
            return getCellBreakdownCharges(cell, position)
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: CustomCardCellViewController.CARD_CELL.kDETAILED, for: indexPath) as! DetailedTableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Global.sharedInstance.isFeatureAccessible(1, delegate as! CustomCardCellViewController) {
            var type:CustomDialogView.DIALOG = .DEFAULT
            
            switch listType {
            case .kPHONE_SETTINGS:
                switch indexPath.row {
                case 0:
                    FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kREQUEST_APN_CLICKED)
                    FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kREQUEST_APN_CLICKED)
                    FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kREQUESTAPNSETTINGS)
                    
                    type = .REQUEST_APN
                    getAPNSettings(type)
                    
                    break
                case 1:
                    FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kSIM_PUK_CLICKED)
                    FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kSIM_PUK_CLICKED)
                    FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kPUK)
                    
                    type = .SIM_PUK
                    getSimDetails(type)
                    
                    break
                case 2:
                    FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kSIM_PIN_CLICKED)
                    FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kSIM_PIN_CLICKED)
                    FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kPIN)
                    
                    type = .SIM_PIN
                    getSimDetails(type)
                    
                    break
                default: // 3
                    FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kSMS_CENTER_CLICKED)
                    FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kSMS_CENTER_CLICKED)
                    FabricAnalytics.sharedInstance.crashlyticsDialog(FabricAnalytics.DIALOG_KEY.kSMSCENTER)
                    
                    type = .SMS_CENTER
                    getSimDetails(type)
                    
                    break
                }
                
                break
            default:
                break
            }
        }
    }
    
    //MARK: - ListTableViewCellDelegator Methods
    func callSegueFromCustomDialog(_ segue: String) {
        if (self.delegate != nil) { //Just to be safe.
            self.delegate.callSegueFromCardCell(segue)
        }
    }
    
    @IBAction func footerButtonTapped(_ sender: Any) { // show edit addon
        if Global.sharedInstance.isFeatureAccessible(1, delegate as! CustomCardCellViewController) {
            switch listType {
            case .kCURRENT_ADDON:
                FabricAnalytics.sharedInstance.crashlyticsButton(FabricAnalytics.BUTTON_KEY.kEDIT_ADD_ON_CLICKED)
                FabricAnalytics.sharedInstance.answersButton(FabricAnalytics.BUTTON_KEY.kEDIT_ADD_ON_CLICKED)
                
                callSubscribedVasApi()
                
                break
            default: // doesn't do anything
                
                break
            }
        }
    }
    
    func callSubscribedVasApi() {
        servicesModel.prepareRetrieveServicesParams(SwitchModel.getSelectedAccountNo()!, CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()), ListTableViewCell.POSTPAID )
        
        globalInstance.showHUD()
        
        Global.webServiceMngr.makeRequest(servicesModel.getRetrieveServicesParams()) { (isResponseDidFail, isServerMaintenance) in
            if isServerMaintenance { // to stop the process and avoid unexpected crashing
                self.globalInstance.dismissHUD()
                
                return
            }
            
            if isResponseDidFail { // if fail do something here
                // if fail do something here
                self.globalInstance.dismissHUD()
                
                // by default this is the action
                self.globalInstance.showGeneralError()
            } else {
                self.showEditAddOn()
            }
        }

    }
    
    func showEditAddOn(){
        globalInstance.dismissHUD()
        
        if (self.delegate != nil) { //Just to be safe.
            self.delegate.callSegueFromCardCell(SHOW_EDIT_ADDON)
        }
    }
// **************************************************************************************************************************************** \\
    
    // Update accordingly for the model values ============================================================================================================================================
    private func getCellLine(_ cell: DetailedTableViewCell, _ position: Int) -> DetailedTableViewCell {
        cell.lblLeftCenter.isHidden = false
        
        switch listType {
        case .kCURRENT_ADDON:
            let currentAddOnList:Array<SubscribedAddOnsEntity> = cellData as! Array<SubscribedAddOnsEntity>
            let addOns:SubscribedAddOnsEntity = currentAddOnList[position]
            
            cell.lblLeftUpper.isHidden = false
            cell.lblRightCenter.isHidden = false
            
            cell.vHolderTopConstraint.constant = -10
            cell.vHolderBtmConstraint.constant = 10
            
            leftUpper = addOns.name
            leftCenter = addOns.desc
            
            if (addOns.isActivated == "1") {
                rightCenter = "enabled"
            }
            
            break
        default: // .kPHONE_SETTINGS:
            cell.vObjectStatic.isHidden = true
            cell.vObjectNonStatic.isHidden = false
            cell.imgRightCenter.isHidden = false
            cell.vObjNonStaticWidthConstraint.constant = 36
            cell.lblLeftCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
            
            leftCenter = (cellData[position]["title"] as? String)!
            
            break
        }
        
        cell.lblLeftUpper.text = leftUpper
        cell.lblLeftUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE)
        
        cell.lblLeftCenter.text = leftCenter
        cell.lblLeftCenter.attributedText = customText.getLblLineSpacing(3, leftCenter)
        
        cell.lblRightCenter.text = rightCenter
        cell.lblRightCenter.textColor = globalInstance.getColorAttribute(.kTEAL, .DEFAULT)
        
        return cell;
    }
    
    // Update accordingly for the model values ============================================================================================================================================
    private func getCellBreakdownCharges(_ cell: DetailedTableViewCell, _ position: Int) -> DetailedTableViewCell {
        cell.lblLeftCenter.isHidden = false
        cell.lblRightCenter.isHidden = false
        
        cell.vHolderTopConstraint.constant = -5
        cell.vHolderBtmConstraint.constant = 5
 
        switch listType {
        case .kPREVIOUS_CHARGES:
            leftCenter = (cellData[position]["title"] as? String)!
            rightCenter = (cellData[position]["detail"] as? String)! //bill.previousBalAmt
            
            break
        case .kCURRENT_CHARGES:
            leftCenter = (cellData[position]["title"] as? String)!
            rightCenter = (cellData[position]["detail"] as? String)! //bill.gstAmt
            
            break
        default: // .kTOTAL_AMOUNT:
            leftCenter = (cellData[position]["title"] as? String)!
            rightCenter = (cellData[position]["detail"] as? String)! //bill.currentchargeamt
            
            break
        }
        
        cell.lblLeftCenter.text = leftCenter
        
        cell.lblRightCenter.text = rightCenter
        cell.lblRightCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        return cell;
    }
    
    // Update accordingly for the model values ============================================================================================================================================
    private func getCellProfile(_ cell: DetailedTableViewCell, _ position: Int) -> DetailedTableViewCell {
        cell.lblLeftUpper.isHidden = false
        cell.lblLeftCenter.isHidden = false
        
        leftUpper = (cellData[position]["title"] as? String)!
        leftCenter = (cellData[position]["detail"] as? String)!
        
        if (leftUpper == "password") {
            leftCenter = String(leftCenter.characters.map { _ in return "•" })
        }
        
        cell.lblLeftUpper.text = leftUpper
        cell.lblLeftUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
        cell.lblLeftUpper.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_34)
        
        cell.lblLeftCenter.text = leftCenter
        cell.lblLeftCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        cell.lblLeftCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        return cell;
    }
    
    // Update accordingly for the model values ============================================================================================================================================
    private func getCellPurchase(_ cell: DetailedTableViewCell, _ position: Int) -> DetailedTableViewCell {
        cell.lblLeftUpper.isHidden = false
        cell.lblLeftCenter.isHidden = false
        cell.lblLeftLower.isHidden = false
        cell.lblFooter.isHidden = true
        cell.lblRightUpper.isHidden = false
        cell.lblRightCenter.isHidden = false
        cell.lblRightLower.isHidden = false
        
        cell.vHolderTopConstraint.constant = -15
        cell.vHolderBtmConstraint.constant = 15
        
        switch listType {
        case .kPURCHASE_DCB:
            let historyDCBList:Array<PurchaseDCBDetailEntity> = cellData as! Array<PurchaseDCBDetailEntity>
            let historyDCB:PurchaseDCBDetailEntity = historyDCBList[position]
            let chargeAmount: Double = Double(historyDCB.chargeAmount)! * (-1)
            
            // set labels attributes
            cell.lblLeftUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
            cell.lblLeftUpper.numberOfLines = 1
            cell.lblLeftUpper.lineBreakMode = .byTruncatingTail
            
            cell.lblLeftCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            cell.lblLeftCenter.numberOfLines = 1
            
            cell.lblLeftLower.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            cell.lblLeftLower.numberOfLines = 1
            
            cell.lblFooter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            cell.lblFooter.numberOfLines = 1
            
            cell.lblRightCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            cell.lblRightCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
            cell.lblRightCenter.numberOfLines = 1
            
            cell.lblRightLower.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            cell.lblRightLower.numberOfLines = 1
            
            // populate
            leftUpper = historyDCB.chargeDescription.isEqual("") ? "-": historyDCB.chargeDescription
            leftCenter = historyDCB.purchaseChannel.isEqual("") ? "-": historyDCB.purchaseChannel
            leftLower = historyDCB.msisdn.isEqual("") ? "-": "+\(historyDCB.msisdn)"
            footer = historyDCB.chargeDate.isEqual("") ? "-": historyDCB.chargeDate
            rightUpper = ""
            rightCenter = historyDCB.chargeAmount.isEqual("") ? "-": "RM \(historyDCB.chargeAmount)"
            rightLower = chargeAmount < 0 ? "": "refunded"
            
            cell.lblFooter.isHidden = false
            cell.lblFooter.text = footer
            
            break
        default: // .kPURCHASE_ADDON
            cell.lblFooter.isHidden = true
            
            let historyAddonList:Array<PurchaseAddOnDetailEntity> = cellData as! Array<PurchaseAddOnDetailEntity>
            let historyAddon:PurchaseAddOnDetailEntity = historyAddonList[position]
            
            // set labels attributes
            cell.lblLeftUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
            cell.lblLeftUpper.numberOfLines = 1
            
            cell.lblLeftCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            cell.lblLeftCenter.numberOfLines = 1
            
            cell.lblLeftLower.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            cell.lblLeftLower.numberOfLines = 1
            
            cell.lblRightCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
            cell.lblRightCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
            cell.lblRightCenter.numberOfLines = 1
            
            leftUpper = historyAddon.ippName
            leftCenter = "+" + historyAddon.msisdn
            leftLower = globalInstance.formatDate(historyAddon.purchaseDatetime, Global.RAW_DATE_FORMAT.DDMMYYYY_HHMM_A, Global.FINAL_DATE_FORMAT.DD_MM_YYYY_HHMM_A)
            rightUpper = " "
            rightCenter = "RM " + historyAddon.price
            rightLower = " "
            
            break
        }
        
        cell.lblLeftUpper.text = leftUpper
        cell.lblLeftCenter.text = leftCenter
        cell.lblLeftLower.text = leftLower
        cell.lblRightUpper.text = rightUpper
        cell.lblRightCenter.text = rightCenter
        cell.lblRightLower.text = rightLower
        
        return cell;
    }
    
    private func getSimDetails(_ type: CustomDialogView.DIALOG) {
        let model = SimDetailsModel.sharedInstance
        let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
        let lineDetail = CustomerDetailsModel.sharedInstance.getLineByMsisdn(msisdnByPosition)
        
        let simDetails = SimDetailsModel.sharedInstance.getSimDetails(CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn()))
        if LoginModel.isTokenExpired() || simDetails == nil || !globalInstance.getIsPukApiTokenRefreshed(msisdnByPosition) {
            globalInstance.showHUD()
            
            model.prepareSimDetailsParams(SwitchModel.getSelectedAccountNo()!, (lineDetail?.imsi)! , (lineDetail?.iccid)!)
            Global.webServiceMngr.makeRequest(model.getSimDetailsParams()) { (isResponseDidFail, isServerMaintenance) in
                self.globalInstance.dismissHUD()
                
                if isServerMaintenance { // to stop the process and avoid unexpected crashing
                    return
                }
                
                if isResponseDidFail { // if fail do something here
                    // by default this is the action
                    self.globalInstance.showGeneralError()
                } else {
                    self.globalInstance.setIsPukApiTokenRefreshed(true, false)
                    
                    let dialog = CustomDialogView(type: type)
                    dialog.delegateList = self
                    dialog.show(animated: true)
                }
            }
        } else {
            let dialog = CustomDialogView(type: type)
            dialog.delegateList = self
            dialog.show(animated: true)
        }
    }
    
    private func getAPNSettings(_ type: CustomDialogView.DIALOG) {
        let dialog = CustomDialogView(type: type)
        dialog.delegateList = self
        dialog.show(animated: true)
    }
    
    private func prepareLayout() {
        lblHeader.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE)
        lblHeader.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        btnFooter.titleLabel?.font = globalInstance.getFontAttribute(.kBOLD, .kLARGE)
        btnFooter.titleLabel?.textColor = globalInstance.getColorAttribute(.kBLUE, .DEFAULT)
    }
    
    func callReloadTable() {
        tvList?.reloadData()
    }
    
}
