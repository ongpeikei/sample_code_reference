//
//  SimpleTableViewCell.swift
//  
//
//  Created by ONG PEI KEI on 24/07/2017.
//
//

import UIKit

class SimpleTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lcLeft: NSLayoutConstraint!
    @IBOutlet weak var lcRight: NSLayoutConstraint!
    
    @IBOutlet weak var lcIdention: NSLayoutConstraint!
    @IBOutlet weak var lblLeft: UILabel!
    @IBOutlet weak var lblRight: UILabel!
    
    @IBOutlet weak var vSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func prepareLayout() {
        lblLeft.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
        lblLeft.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_34)
        
        lblRight.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL)
        lblRight.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
    }
    
}
