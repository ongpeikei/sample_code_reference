//
//  DetailedTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 07/06/2017.
//
//

import UIKit

class DetailedTableViewCell: UITableViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lblLeftUpper: UILabel!
    @IBOutlet weak var lblLeftCenter: UILabel!
    @IBOutlet weak var lblLeftLower: UILabel!
    @IBOutlet weak var lblFooter: UILabel!
    
    @IBOutlet weak var vObjectStatic: UIView!
    @IBOutlet weak var lblRightUpper: UILabel!
    @IBOutlet weak var lblRightCenter: UILabel!
    @IBOutlet weak var lblRightLower: UILabel!
    
    @IBOutlet weak var vObjectNonStatic: UIView!
    @IBOutlet weak var vObjNonStaticWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgRightCenter: UIImageView!
    @IBOutlet weak var btnRightCenter: UIButton!
    @IBOutlet weak var switchRightCenter: UISwitch!
    @IBOutlet weak var switchRightConstraint: NSLayoutConstraint!

    @IBOutlet weak var separatorLine: UIView!
    @IBOutlet weak var vHolderTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var vHolderBtmConstraint: NSLayoutConstraint!
    
    typealias SwitchCallback = (Bool) -> Void
    var switchCallback: SwitchCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        prepareLayout()
    }
    
    private func prepareLayout() {
        lblLeftUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE_X)
        lblLeftUpper.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblLeftCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblLeftCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_34)
        
        lblLeftLower.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblLeftLower.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_34)
        
        lblFooter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblFooter.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_34)
        
        lblRightUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE_X)
        lblRightUpper.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblRightCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblRightCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_34)
        
        lblRightLower.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblRightLower.textColor = globalInstance.getColorAttribute(.kBLACK, .OPACITY_34)
        
        btnRightCenter.titleLabel?.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        btnRightCenter.setTitleColor(globalInstance.getColorAttribute(.kBLUE, .DEFAULT), for: UIControlState.normal)
    }
    
    @IBAction func switchRightCenterChangedState(_ sender: UISwitch) {
        switchCallback?(sender.isOn)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
