//
//  ButtonTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 14/07/2017.
//
//

import UIKit

class ButtonTableViewCell: UITableViewCell {
    @IBOutlet weak var btnCenter: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnCenter.layer.cornerRadius = 3
        btnCenter.titleLabel?.font = Global.sharedInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE)
        btnCenter.translatesAutoresizingMaskIntoConstraints = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
