//
//  LoginCollectionViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 31/07/2017.
//
//

import UIKit

class LoginCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
