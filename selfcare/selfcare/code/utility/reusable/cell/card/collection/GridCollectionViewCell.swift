//
//  GridCollectionViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 14/07/2017.
//
//

import UIKit

class GridCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTop: UILabel!
    @IBOutlet weak var lblCenter: UILabel!
    @IBOutlet weak var lblBottom: UILabel!
    @IBOutlet weak var switchIndicator: UIView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
