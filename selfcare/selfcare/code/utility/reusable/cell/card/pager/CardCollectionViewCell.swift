//
//  CardCollectionViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 15/06/2017.
//
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var lblTopLeftUpper: UILabel!
    @IBOutlet weak var lblTopLeftCenter: UILabel!
    @IBOutlet weak var lblTopLeftLower: UILabel!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var separatorBar: UIView!
    
    @IBOutlet weak var lblBottomLeftUpper: UILabel!
    @IBOutlet weak var lblBottomLeftCenter: UILabel!
    @IBOutlet weak var lblBottomLeftLower: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareLayout()
        separatorBar.translatesAutoresizingMaskIntoConstraints = false
        lblBottomLeftLower.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func prepareLayout() {
        lblTopLeftUpper.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kLARGE)
        lblTopLeftUpper.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblTopLeftCenter.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL_X)
        lblTopLeftCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblTopLeftLower.font = globalInstance.getFontAttribute(.kEXTRA_BOLD, .kLARGE_XXX)
        lblTopLeftLower.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblBottomLeftUpper.font = globalInstance.getFontAttribute(.kREGULAR, .kSMALL_X)
        lblBottomLeftUpper.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblBottomLeftCenter.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kSMALL_X)
        lblBottomLeftCenter.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
        
        lblBottomLeftLower.font = globalInstance.getFontAttribute(.kSEMI_BOLD, .kMEDIUM)
        lblBottomLeftLower.textColor = globalInstance.getColorAttribute(.kBLACK, .DEFAULT)
    }

}
