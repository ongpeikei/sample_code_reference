//
//  PagerTableViewCell.swift
//  selfcare
//
//  Created by ONG PEI KEI on 15/06/2017.
//
//

import UIKit

class PagerTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    let globalInstance = Global.sharedInstance
    
    @IBOutlet weak var cvPager: UICollectionView!
    @IBOutlet weak var pcIndicator: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        cvPager.register(UINib(nibName: "CardCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CustomCardCellViewController.CARD_CELL.kCARD)
        let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
        
        pcIndicator.numberOfPages = (AddOnBalanceModel.sharedInstance.getAddOnBalanceList(msisdnByPosition)?.count)!
        pcIndicator.currentPage = 0
        pcIndicator.pageIndicatorTintColor = globalInstance.getColorAttribute(.kLIGHT_GREY, .DEFAULT)
        pcIndicator.currentPageIndicatorTintColor = globalInstance.getColorAttribute(.kPRIMARY_DARK, .DEFAULT)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
        return AddOnBalanceModel.sharedInstance.getAddOnBalanceList(msisdnByPosition)!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCardCellViewController.CARD_CELL.kCARD, for: indexPath) as! CardCollectionViewCell
        let msisdnByPosition = CustomerDetailsModel.sharedInstance.getMsisdnByPosition(Global.sharedInstance.getSelectedMsisdn())
        let pagerCell:Array = AddOnBalanceModel.sharedInstance.getAddOnBalanceList(msisdnByPosition)!
        let cellContents = pagerCell[indexPath.row]
        
        let updatedProgValue = Float(cellContents.leftPercent)!/100
        
        cell.lblTopLeftUpper.text = cellContents.name
        cell.lblTopLeftLower.text = cellContents.leftBal
        cell.lblBottomLeftCenter.text = cellContents.effDate + " - " + cellContents.expDate
        cell.lblBottomLeftLower.text = cellContents.desc
        cell.lblBottomLeftLower.attributedText = Global.customText.getLblLineSpacing(3, cellContents.desc)
        cell.progressView.setProgress(Float(updatedProgValue), animated: false)
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = cvPager.frame.size.width;
        let currentPage = cvPager.contentOffset.x / pageWidth;
        
        if currentPage <= 0 {
            pcIndicator.currentPage = 0;
        } else {
            pcIndicator.currentPage = Int(currentPage);
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func callReloadCollectionView() {
        cvPager?.reloadData()
        cvPager.setContentOffset(CGPoint.zero, animated: false)
    }
}
